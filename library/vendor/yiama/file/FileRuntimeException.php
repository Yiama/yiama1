<?php
namespace Yiama\File;

use Yiama\Throwable\RuntimeException;

/**
 * Yiama File Runtime Exception
 * 
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class FileRuntimeException extends RuntimeException {}