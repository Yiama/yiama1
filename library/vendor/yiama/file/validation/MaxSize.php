<?php
namespace Yiama\File\Validation;

use Yiama\File\File;
use Yiama\Validator\ValidationInterface;

/**
 * Validation File Media max size
 *
 * Validates an array of strings as file's media top-level type
 * (top level type [image, text, ...]).
 *
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class MaxSize implements ValidationInterface {
    
    /**
     * @var integer 
     */
    private $max_size;
    
    /**
     * @param string $max_size
     */
    public function __construct(int $max_size) {
        $this->max_size = $max_size;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::validate()
     */
    public function validate($value): bool {
        if (!$value instanceof File) {
            $this->message = 'Argument $value must be of type Yiama\File\File';
            
            return false;
        }
        
        $file = $value;
        if ($file->getSize() <= $this->max_size) {
            return true;
        }
        
        $this->message = "Media max size is larger than ".$this->max_size
            .' bytes';
        
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::getMessage()
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::setMessage()
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }
}

