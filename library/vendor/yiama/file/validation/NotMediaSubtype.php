<?php
namespace Yiama\File\Validation;

use Yiama\File\File;
use Yiama\Validator\ValidationInterface;

/**
 * Validation File Media subtype
 *
 * Validates an array of strings as not a file's media subtype
 * (top level type [png, pdf, ...]).
 *
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class NotMediaSubtype implements ValidationInterface {
    
    /**
     * @var string
     */
    private $subtype;
    
    /**
     * @param string $subtype
     */
    public function __construct(string $subtype) {
        $this->subtype = $subtype;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::validate()
     */
    public function validate($value): bool {
        if (!$value instanceof File) {
            $this->message = 'Argument $value must be of type Yiama\File\File';
            
            return false;
        }
        
        $file = $value;
        $path = $file->getPath();
        if (($mimetype = $file->getMimeType()) !== false) {
            $subtype = \substr($mimetype, \strpos($mimetype, '/')+1);
            if ($subtype != $this->subtype) {
                return true;
            } else {
                if (!isset($this->messages[0])) {
                    $this->message = "File " . $path . " is a media "
                        . "subtype: '" . $this->subtype . "'";
                }
                
                return false;
            }
        }
        $this->message = "Media subtype of ".$path." could not be specified";
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::getMessage()
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::setMessage()
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }
}