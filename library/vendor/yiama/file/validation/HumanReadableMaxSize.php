<?php
namespace Yiama\File\Validation;

use Yiama\File\File;
use Yiama\Validator\ValidationInterface;

/**
 * Validation File HumanReadableMaxSize
 *
 * Validates if a file is not larger than $max_size (human readable size).
 *
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class HumanReadableMaxSize implements ValidationInterface {
    
    /**
     * @var array
     */
    private $max_human_size;
    
    /**
     * @param string $max_human_size e.g. '2MB'
     */
    public function __construct(string $max_human_size) {
        $this->max_human_size = $max_human_size;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::validate()
     */
    public function validate($value): bool {
        if (!$value instanceof File) {
            $this->message = 'Argument $value must be of type Yiama\File\File';
            
            return false;
        }
        
        $file = $value;
        $path = $file->getPath();
        $ordered_units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        if (($human_size = $file->getHumanReadableSize()) !== false) {
            $unit_from = preg_replace('/[^BKMGPEZY]/', '', $human_size);
            $unit_to = preg_replace('/[^BKMGPEZY]/', '', $this->max_human_size);
            $size = preg_replace('/[BKMGPEZY]/', '', $human_size);
            $max_size = preg_replace('/[BKMGPEZY]/', '', $this->max_human_size);
            $unit_from_index = \array_search($unit_from, $ordered_units);
            $unit_to_index = \array_search($unit_to, $ordered_units);
            if ((float)$size <= (float)$max_size &&
                $unit_from_index <= $unit_to_index) {
                    
                return true;
            } else {
                if (!isset($this->message)) {
                    $this->message = "File " . $path . " is larger than "
                        . $this->max_human_size . ' bytes';
                }
                
                return false;
            }
        }
        $this->message = "Size of ".$path." could not be specified";
        
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::getMessage()
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::setMessage()
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }
}

