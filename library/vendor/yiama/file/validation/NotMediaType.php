<?php
namespace Yiama\File\Validation;

use Yiama\File\File;
use Yiama\Validator\ValidationInterface;

/**
 * Validation File Not Media top-level type
 *
 * Validates an array of strings as not a file's media top-level type
 * (top level type [image, text, ...]).
 *
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class NotMediaType implements ValidationInterface {
    
    /**
     * @var string 
     */
    private $top_level_type;
    
    /**
     * @param string $top_level_type
     */
    public function __construct(string $top_level_type) {
        $this->top_level_type = $top_level_type;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::validate()
     */
    public function validate($value): bool {
        if (!$value instanceof File) {
            $this->message = 'Argument $value must be of type Yiama\File\File';
            
            return false;
        }
        
        $file = $value;
        $path = $file->getPath();
        if (($mimetype = $file->getMimeType()) !== false) {
            $top_level_type = \substr($mimetype, 0, \strpos($mimetype, '/'));
            if ($top_level_type != $this->top_level_type) {
                return true;
            } else {
                if (!isset($this->messages[0])) {
                    $this->message = "File " . $path . " is of media "
                        . "top-level type: '" . $this->top_level_type . "'";
                }
                
                return false;
            }
        }
        $this->message = "Media top-level type: '".$path."' could not be specified";
        
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::getMessage()
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Validator\ValidationInterface::setMessage()
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }
}

