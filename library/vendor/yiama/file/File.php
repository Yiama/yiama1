<?php
namespace Yiama\File;

use Yiama\Throwable\InvalidArgumentException;

/**
 * File
 *
 * @package Yiama/File
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class File {
    
    /**
     * Absolute path
     * 
     * @var string
     */
    private $path;
    
    /**
     * @var string
     */
    private $dirname;
    
    /**
     * @var string
     */
    private $fullname;
    
    /**
     * @var string
     */
    private $extension;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * Notice: pathinfo() is locale aware, so for it to parse a path containing 
     * multibyte characters correctly, the matching locale must be set using the 
     * setlocale() function.
     * 
     * @param string $path
     */
    public function __construct(string $path) {
        $this->path = $path;
        $info = \pathinfo($path);
        $this->dirname = !empty($info['dirname']) ? $info['dirname'] : '';
        $this->fullname = !empty($info['basename']) ? $info['basename'] : '';
        $this->extension = !empty($info['extension']) ? $info['extension'] : '';
        $this->name = !empty($info['filename']) ? $info['filename'] : '';
    }
    
    /**
     * Linux mode.<br />
     * Mode is not automatically assumed to be an octal value, in order to 
     * ensure the expected operation, you need to prefix mode with a zero (0).
     * 
     * @param integer $mode
     * @throws FileRuntimeException
     * @return boolean
     */
    public function setPermissions(int $mode): string {
        if (strlen((string)$mode) != 3) {
            throw new InvalidArgumentException('$mode must be an integer'.
                ' with 3 digits');
        }
        if (($mode = @\chmod($this->path, $mode)) !== false) {
            return $mode;
        } 
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the files' or directory's full path.
     * 
     * @return string
     */
    public function getPath(): string {
        return $this->path;
    }
    
    /**
     * Returns the parent directory path.
     * 
     * @return string
     */
    public function getDirname(): string {
        return $this->dirname;
    }
    
    /**
     * Returns a directory name or a filename including it's extension.
     * 
     * @return string
     */
    public function getFullname(): string {
        return $this->fullname;
    }
    
    /**
     * Returns the file's extension or an empty string.
     * 
     * @return string
     */
    public function getExtension(): string {
        return $this->extension;
    }
    
    /**
     * Returns a directory name or a filename excluding it's extension.
     * 
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }
    
    /**
     * Returns the time the file or directory was last accessed, as a Unix 
     * timestamp.
     * 
     * @throws FileRuntimeException
     * @return int
     */
    public function getLastAccess(): int {
        if (($time = @\fileatime($this->path)) !== false) {
            return $time;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the file's or directory's owner's user id.
     * 
     * @throws FileRuntimeException
     * @return array
     */
    public function getOwner(): array {
        if (($o = (int)@\fileowner($this->path)) !== false &&
            ($owner = @\posix_getpwuid($o)) !== false) {
            
            return $owner;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns simple numeric permissions of the file or directory.
     * 
     * @throws FileRuntimeException
     * @return string
     */
    public function getPermissions(): string {
        if (($perms = @\fileperms($this->path)) !== false) {
            return \substr(\sprintf('%o', $perms), -4);
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns Linux permissions in octal value of a the file  directory.
     * 
     * @return integer|null
     */
    public function getOctalPermissions(): int {
        if (($perms = @\fileperms($this->path)) !== false) {
            // Owner
            $digit2  = (($perms & 0x0100) ? 4 : 0);
            $digit2 += (($perms & 0x0080) ? 2 : 0);
            $digit2 += (($perms & 0x0040) ?
                        (($perms & 0x0800) ? 0 : 1 ) :
                        (($perms & 0x0800) ? 0 : 0));
            // Group
            $digit3  = (($perms & 0x0020) ? 4 : 0);
            $digit3 += (($perms & 0x0010) ? 2 : 0);
            $digit3 += (($perms & 0x0008) ?
                        (($perms & 0x0400) ? 0 : 1 ) :
                        (($perms & 0x0400) ? 0 : 0));
            // Others
            $digit4  = (($perms & 0x0004) ? 4 : 0);
            $digit4 += (($perms & 0x0002) ? 2 : 0);
            $digit4 += (($perms & 0x0001) ?
                        (($perms & 0x0200) ? 0 : 1 ) :
                        (($perms & 0x0200) ? 0 : 0));
            
            return sprintf('%04d', $digit2 . $digit3 . $digit4);
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns Linux permissions in symbolic value of the file or directory.
     * 
     * @throws FileRuntimeException
     * @return string
     */
    public function getSymbolicPermissions(): string {
        if (($perms = @\fileperms($this->path)) !== false) {
            switch ($perms & 0xF000) {
                case 0xC000: // socket
                    $info = 's';
                    break;
                case 0xA000: // symbolic link
                    $info = 'l';
                    break;
                case 0x8000: // regular
                    $info = 'r';
                    break;
                case 0x6000: // block special
                    $info = 'b';
                    break;
                case 0x4000: // directory
                    $info = 'd';
                    break;
                case 0x2000: // character special
                    $info = 'c';
                    break;
                case 0x1000: // FIFO pipe
                    $info = 'p';
                    break;
                default: // unknown
                    $info = 'u';
            }
            // Owner
            $info .= (($perms & 0x0100) ? 'r' : '-');
            $info .= (($perms & 0x0080) ? 'w' : '-');
            $info .= (($perms & 0x0040) ?
                        (($perms & 0x0800) ? 's' : 'x' ) :
                        (($perms & 0x0800) ? 'S' : '-'));
            // Group
            $info .= (($perms & 0x0020) ? 'r' : '-');
            $info .= (($perms & 0x0010) ? 'w' : '-');
            $info .= (($perms & 0x0008) ?
                        (($perms & 0x0400) ? 's' : 'x' ) :
                        (($perms & 0x0400) ? 'S' : '-'));
            // Others
            $info .= (($perms & 0x0004) ? 'r' : '-');
            $info .= (($perms & 0x0002) ? 'w' : '-');
            $info .= (($perms & 0x0001) ?
                        (($perms & 0x0200) ? 't' : 'x' ) :
                        (($perms & 0x0200) ? 'T' : '-'));
            
            return $info;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the file's size or 0 for the directory.<br />
     * Because PHP's integer type is signed and many platforms use 32bit 
     * integers, some filesystem functions may return unexpected results for 
     * files which are larger than 2GB.
     * 
     * @throws FileRuntimeException
     * @return integer
     */
    public function getSize(): int {
        if (\is_dir($this->path)) {
            return 0;
        }
        
        if (($bytes = @\filesize($this->path)) !== false) {
            return $bytes;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the file's size in human readable value or empty string for the
     * directory.
     * 
     * @param int $decimals
     * @throws FileRuntimeException
     * @return string
     */
    public function getHumanReadableSize(int $decimals = 2): string {
        if (\is_dir($this->path)) {
            return '';
        }
        
        if (($bytes = @\filesize($this->path)) !== false) {
            $sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            $index = \floor((\strlen($bytes)-1)/3);
            $size = $sizes[$index];
            $divisor = $index*1024;
            
            return (string)\sprintf(
                "%.{$decimals}f", 
                $divisor > 0 ? $bytes/$divisor : $bytes
            ) . $size;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the mime type of the file or an empty string for a directory.
     * 
     * @throws FileRuntimeException
     * @return string
     */
    public function getMimeType(): string {
        if (\is_dir($this->path)) {
            return '';
        }
        
        if (($type = @\mime_content_type($this->path)) !== false) {
            return $type; 
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns the time the file or directory was last modified, as a Unix 
     * timestamp.
     * 
     * @throws FileRuntimeException
     * @return int
     */
    public function getLastModified(): int {
        if (($time = @\filemtime($this->path)) !== false) {
            return $time;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Returns an array of File objects for subdirectories and files that are 
     * contained inside a directory.
     * 
     * @throws FileRuntimeException
     * @return array
     */
    public function getChildren(): array {
        if (!\is_dir($this->path)) {
            return [];
        }
        
        $ym_files = [];
        if (($files = @\scandir($this->path)) !== false) {
            foreach ($files as $file) {
                if ($file != '.' && 
                    $file != '..') {
                    $ym_files[] = new self($this->path.DIRECTORY_SEPARATOR.$file);
                }
            }
        } else {
            throw new FileRuntimeException(\error_get_last()['message']);
        }
        
        return $ym_files;
    }
    
    /**
     * Returns a File object for the parent directory.
     * 
     * @return File
     */
    public function getParent(): File {
        return new self($this->dirname);
    }
    
    /**
     * Returns true if the file or directory is readable by the running user.
     * 
     * @return boolean
     */
    public function isReadable(): bool {
        return (boolean) @\is_readable($this->path);
    }
    
    /**
     * Returns true if the file or directory is executable by the running user.
     * 
     * @return boolean
     */
    public function isExecutable(): bool {
        return (boolean) @\is_executable($this->path);
    }
    
    /**
     * Returns true if the file or directory is writable by the running user.
     * 
     * @return boolean
     */
    public function isWritable(): bool {
        return (boolean) @\is_writable($this->path);
    }
    
    /**
     * Returns true if this File object wraps a file.
     * 
     * @return boolean
     */
    public function isFile(): bool {
        return (boolean) @\is_file($this->path);
    }
    
    /**
     * Returns true if this File object wraps a directory.
     * 
     * @return boolean
     */
    public function isDirectory(): bool {
        return (boolean) @\is_dir($this->path);
    }
    
    /**
     * Returns true if file or directory exist.
     * 
     * @return boolean
     */
    public function exists(): bool {
        return (boolean) @\file_exists($this->path);
    }
    
    /**
     * Creates a directory recursively.
     * 
     * @param int $mode Linux mode
     * @throws InvalidArgumentException
     * @throws FileRuntimeException
     * @return bool
     */
    public function createDir(int $mode = 0777): bool {
        if (strlen((string)$mode) != 3) {
            throw new InvalidArgumentException('$mode must be an integer'.
                ' with 3 digits');
        }
        if (($result = @\mkdir($this->path, $mode, true)) !== false) {
            return $result;    
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Creates a file.
     * 
     * @param int $mode Linux mode
     * @throws InvalidArgumentException
     * @throws FileRuntimeException
     * @return bool
     */
    public function createFile(int $mode = 0777): bool {
        if (strlen((string)$mode) != 3) {
            throw new InvalidArgumentException('$mode must be an integer'.
                ' with 3 digits');
        }
        if (@\file_put_contents($this->path, "") !== false) {
            if (@\chmod($this->path, $mode) === false) {
                throw new FileRuntimeException(\error_get_last()['message']);
            }
            
            return true;
        }
        
        throw new FileRuntimeException(\error_get_last()['message']);
    }
    
    /**
     * Deletes the file or directory.
     * 
     * @throws FileRuntimeException
     * @return bool
     */
    public function delete(): bool {
        if (\is_dir($this->path)) {
            if (@\rmdir($this->path) !== false) {
                return true;
            }
            
            throw new FileRuntimeException(\error_get_last()['message']);
        } elseif (\is_file($this->path)) {
            if (@\unlink($this->path) !== false) {
                return true;
            }
            
            throw new FileRuntimeException(\error_get_last()['message']);
        }
        
        return true;
    }
}