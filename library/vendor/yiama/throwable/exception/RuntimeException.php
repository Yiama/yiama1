<?php
namespace Yiama\Throwable;

/**
 * Ym Runtime Exception
 * 
 * @package Yiama/Throwable
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class RuntimeException extends \RuntimeException {}