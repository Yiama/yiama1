<?php
namespace Yiama\Throwable;

/**
 * Yiama UnexpectedValue Exception
 * 
 * @package Yiama/Throwable
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class UnexpectedValueException extends \UnexpectedValueException {}