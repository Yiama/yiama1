<?php
namespace Yiama\Throwable;

/**
 * Yiama Error Exception
 *
 * Provides extra methods to ErrorException.
 * 
 * @package Yiama/Throwable
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class ErrorException extends \ErrorException{
    
    /**
     * @var array
     */
    private $severity_strings = [
        'E_ERROR' => E_ERROR,
        'E_WARNING' => E_WARNING,
        'E_PARSE' => E_PARSE,
        'E_NOTICE' => E_NOTICE,
        'E_CORE_ERROR' => E_CORE_ERROR,
        'E_CORE_WARNING' => E_CORE_WARNING,
        'E_COMPILE_ERROR' => E_COMPILE_ERROR,
        'E_COMPILE_WARNING' => E_COMPILE_WARNING,
        'E_USER_ERROR' => E_USER_ERROR,
        'E_USER_WARNING' => E_USER_WARNING,
        'E_USER_NOTICE' => E_USER_NOTICE,
        'E_STRICT' => E_STRICT,
        'E_RECOVERABLE_ERROR' => E_RECOVERABLE_ERROR,
        'E_DEPRECATED' => E_DEPRECATED,
        'E_USER_DEPRECATED' => E_USER_DEPRECATED
    ];
    
    /**
     * @return string
     */
    public function getSeverityString(): string {
        $code = $this->getCode();
        foreach ($this->severities as $key => $value) {
            if ($value & $code) {
                return $key;
            }
        }
    }   
}

