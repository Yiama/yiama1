<?php
namespace Yiama\Throwable;

/**
 * Yiama Invalid Argument Exception
 * 
 * @package Yiama/Throwable
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class InvalidArgumentException extends \InvalidArgumentException {}