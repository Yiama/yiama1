<?php
use Yiama\Autoload\LoaderCollection;
use Yiama\Autoload\Loader\Psr4Loader;
use Yiama\Autoload\Registrator;

$path = dirname(__FILE__).DIRECTORY_SEPARATOR;
include($path.'std'.DIRECTORY_SEPARATOR.'CollectionInterface.php');
include($path.'autoload'.DIRECTORY_SEPARATOR.'Registrator.php');
include($path.'autoload'.DIRECTORY_SEPARATOR.'LoaderCollection.php');
include($path.'autoload'.DIRECTORY_SEPARATOR.'LoaderInterface.php');
include($path.'autoload'.DIRECTORY_SEPARATOR.'loader'.DIRECTORY_SEPARATOR.'Psr4Loader.php');
$psr4 = new Psr4Loader();
$psr4->addMappingArray(array(
    'Yiama\Cache' => array(
        $path.'cache'
    ),
    'Yiama\Cache\Repository' => array(
        $path.'cache'.DIRECTORY_SEPARATOR.'repository'
    ),
    'Yiama\File' => array(
        $path.'file'
    ),
    'Yiama\File\Validation' => array(
        $path.'file'.DIRECTORY_SEPARATOR.'validation'
    ),
    'Yiama\I18n' => array(
        $path.'i18n'
    ),
    'Yiama\I18n\Loader' => array(
        $path.'i18n'.DIRECTORY_SEPARATOR.'loader'
    ),
    'Yiama\Throwable' => array(
        $path.'throwable'.DIRECTORY_SEPARATOR.'exception'
    ),
    'Yiama\Std' => array(
        $path.'std'
    ),
    'Yiama\Mail' => array(
        $path.'mail'
    ),
    'Yiama\Mail\Client' => array(
        $path.'mail'.DIRECTORY_SEPARATOR.'client'
    ),
    'Yiama\Mail\Smtp' => array(
        $path.'mail'.DIRECTORY_SEPARATOR.'smtp'
    )
));

$loaders = new LoaderCollection();
$loaders['yiamaPsr4'] = $psr4;
$registrator = new Registrator();
$registrator->register($loaders);