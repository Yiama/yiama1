<?php
namespace Yiama\Std;

/**
 * Std Collection Interface
 * 
 * @package Yiama/Std
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
interface CollectionInterface 
extends \Countable, \ArrayAccess, \IteratorAggregate {
    
    /**
     * Deletes all items.
     */
    public function clear();
    
    /**
     * Returns true if an item with the given $value exists.
     * 
     * @return bool
     */
    public function has($value): bool;

    /**
     * Adds new items or replaces existing ones.<br />
     * Any existing numeric or string keys will have their values replaced.
     * 
     * @param CollectionInterface $collection
     */
    public function merge(CollectionInterface $collection);
    
    /**
     * Deletes any items which have same keys and values with those from the 
     * given collection.
     * 
     * @param CollectionInterface $collection
     */
    public function difference(CollectionInterface $collection);
}