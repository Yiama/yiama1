<?php
namespace Yiama\Mail;

/**
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */  
class Multipart implements MessageContentInterface {
    
    /**
     * Use of types:
     * -If is Html message with alternative Plain text use -alternative-.
     * -If has Attachments use -related-.
     * -If none of the above then use -mixed- which applys in all of the above cases.
     *
     * @var string
     */
    public static $TYPE_ALTERNATIVE = 'alternative';
    public static $TYPE_RELATED = 'related';
    public static $TYPE_MIXED = 'mixed';
    
    /**
     * @var string
     */
    private static $nl = "\r\n";
    
    /**
     * @var string
     */
    private $boundary;
    
    /**
     * @var Part[]
     */
    private $parts = [];
    
    /**
     * @var HeaderInterface[]
     */
    private $headers = [];
    
    /**
     * @var string
     */
    private $body;
    
    /**
     * https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
     * 
     * @param string $type
     */
    public function __construct(string $type) {
        $this->boundary = md5(time());
        $this->headers = [
            Message::Header('MIME-Version', '1.0'),
            Message::Header(Message::$HEADER_CONTENT_TYPE, 'multipart/'.$type.'; boundary='.$this->boundary)
        ];
    }
    
    /**
     * @param string $type
     */
    public function setMultipartType(string $type) {
        $this->type = $type;
    }
    
    /**
     * @param Mail_Mime_Part[]
     */
    public function addParts(array $parts) {
        $this->parts = array_merge($this->parts, $parts);
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Mail\MessageContentInterface::getHeaders()
     */
    public function getHeaders(): array {
        return $this->headers;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Mail\MessageContentInterface::getBody()
     */
    public function getBody(): string {
        $result = '';
        foreach($this->parts as $part) {
            $result .= self::$nl.self::$nl.'--'.$this->boundary.self::$nl.$part;
        }
        return $result.self::$nl.self::$nl.'--'.$this->boundary.'--';
    }
}