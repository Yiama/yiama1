<?php
namespace Yiama\Mail;

/**
 * Address Interface
 *
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
interface AddressInterface {
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @return string
     */
    public function getEmail(): string;
    
    /**
     * @return string
     */
    public function __toString(): string;
}