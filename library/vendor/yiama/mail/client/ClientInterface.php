<?php
namespace Yiama\Mail\Client;

use Yiama\Mail\Message;

/**
 * Mail Client Interface
 *
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
interface ClientInterface {
    
    /**
     * @param Message $message
     */
    public function send(Message $message);    
}