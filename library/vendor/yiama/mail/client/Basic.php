<?php
namespace Yiama\Mail\Client;

use Yiama\Mail\Message;
use Yiama\Mail\MailException;

/**
 * For Linux the 'sendmail' must be installed in the machine in /usr/bin/sendmail
 * 
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */  
class Basic implements ClientInterface {
    
    
    /**
     * {@inheritDoc}
     * @see ClientInterface::send()
     */
    public function send(Message $message) {
        if (mail($message->addressesToString($message->getRecipients()), $message->getSubject(), $message->getContent()->getBody(), $message->headersString()) === false) {
            throw new MailException("Send failed");
        }
    }
}