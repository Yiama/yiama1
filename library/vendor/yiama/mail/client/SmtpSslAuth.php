<?php
namespace Yiama\Mail\Client;

use Yiama\Mail\Smtp\ServerReply;
use Yiama\Mail\MailException;
use Yiama\Mail\Message;
use Yiama\Mail\Smtp\ClientCommand;
use Yiama\Mail\Smtp\Status;
use Yiama\Mail\Timeout;

/**
 * Supports only Authentication AUTH LOGIN using SSL connection
 * 
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class SmtpSslAuth implements ClientInterface {
    
    /**
     * @var string
     */
    private string $host;
    
    /**
     * @var int
     */
    private int $port;
    
    /**
     * @var string
     */
    private string $username;
    
    /**
     * @var string
     */
    private string $password;
    
    /**
     * @var Timeout
     */
    private ?Timeout $timeout = null;
    
    /**
     * @param string $host
     * @param int $port
     * @param string $username
     * @param string $password
     */
    public function __construct(string $host, int $port, string $username, string $password) {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
    }
    
    /**
     * @param Resource $socket
     * @return ServerReply[]
     */
    private function replies($socket): array {
        $replies = [];
        while(@strpos(($r = fgets($socket, 256)), '-', 3)) {
            $replies[] = ServerReply::fromString($r);
        }
        $replies[] = ServerReply::fromString($r);
        
        return $replies;
    }
    
    /**
     * Compares only the severity level not the exact code value
     * 
     * @param ServerReply[] $replies
     * @param Status[] $validStatuses
     */
    private function validateReplies(array $replies, array $validStatuses) {
        foreach ($replies as $reply) {
            $isValid = false;
            foreach ($validStatuses as $validStatus) {
                if ($validStatus->equalsSeverity($reply->getStatus())) {
                    $isValid = true;
                    break;
                }
            }
            if (!$isValid) {
                throw new MailException('Server reply: '.$reply.' returned unexpected code ['.$reply->getStatus()->getCode().'].');
            }
        }
    }
    
    /**
     * @param ServerReply[] $serverReplies
     * @throws MailException
     */
    private function supportsAuthLogin(array $serverReplies) {
        $supports = false;
        foreach ($serverReplies as $reply) {
            if (strpos($reply->getMessage(), 'AUTH') !== false && strpos($reply->getMessage(), 'LOGIN') !== false) {
                $supports = true;
                break;
            }
        }
        if (!$supports) {
            throw new MailException('Authentication LOGIN not supported.');
        }
    }
    
    /**
     * @param string $message
     * @throws MailException
     */
    private function checkTimeout(string $message) {
        if ($this->timeout != null && $this->timeout->expired()) {
            throw new MailException($message.' timed out.');
        }
    }
    
    /**
     * @param Timeout $timeout
     */
    public function setTimeout(Timeout $timeout) {
        $this->timeout = $timeout;
    }
    
    /**
     * {@inheritDoc}
     * @see ClientInterface::send()
     */
    public function send(Message $message) {
        $errno = $errstr = '';
        if (($socket = stream_socket_client('ssl://'.$this->host.':'.$this->port, $errno, $errstr, 30, STREAM_CLIENT_CONNECT)) === false) {
            throw new MailException('Errno:'.$errno.', '.$errstr);
        }
        
        $nl = "\r\n";
        
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(220)]);
        $this->checkTimeout('Connection');
        
        $command = ClientCommand::Hello();
        fwrite($socket, $command->build([$message->getSender()->getEmail()]));
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(250)]);
        $this->checkTimeout($command->getName());
        
        $this->supportsAuthLogin($replies);
        $command = ClientCommand::authLogin();
        fwrite($socket, $command->build());
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(334)]);
        $this->checkTimeout($command->getName());
        
        fwrite($socket, base64_encode($this->username).$nl);
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(334)]);
        $this->checkTimeout($command->getName().' username');
        
        fwrite($socket, base64_encode($this->password).$nl);
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(235)]);
        $this->checkTimeout($command->getName().' password');
        
        $command = ClientCommand::MailFrom();
        fwrite($socket, $command->build([$message->getSender()->getEmail()]));
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(250)]);
        $this->checkTimeout($command->getName());
        
        $command = ClientCommand::Recipient();
        foreach ($message->getRecipients() as $recipient) {
            fwrite($socket, $command->build([$recipient->getEmail()]));
            $replies = $this->replies($socket);
            $this->validateReplies($replies, [new Status(250)]);
            $this->checkTimeout($command->getName());
        }
        
        $command = ClientCommand::Data();
        fwrite($socket, $command->build());
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(354)]);
        $this->checkTimeout($command->getName());
        
        $command = ClientCommand::EndData();
        fwrite($socket, $message.$command->build());
        $replies = $this->replies($socket);
        $this->validateReplies($replies, [new Status(250)]);
        $this->checkTimeout($command->getName());
        
        $command = ClientCommand::Quit();
        fwrite($socket, $command->build());
        $this->validateReplies($replies, [new Status(221)]);
        $this->checkTimeout($command->getName());
        
        fclose($socket);
    }
}