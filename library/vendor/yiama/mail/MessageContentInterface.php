<?php
namespace Yiama\Mail;

/**
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */  
interface MessageContentInterface {
    
    /**
     * @return HeaderInterface[]
     */
    public function getHeaders(): array;
    
    /**
     * @return string
     */
    public function getBody(): string;
        
}