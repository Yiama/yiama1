<?php
namespace Yiama\Mail;

/**
 * Header Interface
 *
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
interface HeaderInterface {
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @return string
     */
    public function getValue(): string;
    
    /**
     * @return string
     */
    public function __toString(): string;
}