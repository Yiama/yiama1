<?php
namespace Yiama\Mail;

/**
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
*/  
class Message {
    
    /**
     * Frequently used headers
     * 
     * @var string
     */
    public static $HEADER_CONTENT_TYPE = 'Content-Type';
    public static $HEADER_CONTENT_TRANSFER_ENCODING = 'Content-Transfer-Encoding';
    public static $HEADER_CONTENT_DISPOSITION = 'Content-Disposition';
    
    /**
     * @var string
     */
    public static $nl = "\r\n";
    
    /**
     * @param AddressInterface
     */
    private $sender = null;
    
    /**
     * @var AddressInterface[]
     */
    private $recipients = [];
    
    /**
     * @var AddressInterface[]
     */
    private $recipientsCc = [];
    
    /**
     * @var AddressInterface[]
     */
    private $recipientsBcc = [];
    
    /**
     * @param AddressInterface
     */
    private $replyTo = null;
    
    /**
     * @var string
     */
    private $subject = '';
    
    /**
     * @var MessageContentInterface
     */
    private $content;
    
    /**
     * @param string $string
     */
    private static function encodedString(string $string): string {
        return '=?utf-8?B?'.base64_encode($string).'?=';
    }
    
    /**
     * @param AddressInterface $sender
     */
    public function setSender(AddressInterface $sender) {
        $this->sender = $sender; 
    }
    
    /**
     * @return AddressInterface
     */
    public function getSender(): AddressInterface {
        return $this->sender;
    }
    
    /**
     * @param AddressInterface[] $recipients
     */
    public function addRecipients(array $recipients) {
        $this->recipients = array_merge($this->recipients, $recipients);
    }
    
    /**
     * @return AddressInterface[]
     */
    public function getRecipients(): array {
        return $this->recipients;
    }
    
    /**
     * @param AddressInterface[]  $recipientsCc
     */
    public function addRecipientsCc(array $recipientsCc) {
        $this->recipientsCc = array_merge($this->recipientsCc, $recipientsCc);
    }
    
    /**
     * @return AddressInterface[]
     */
    public function getRecipientsCc() {
        return $this->recipientsCc;
    }
    
    /**
     * @param AddressInterface[]  $recipientsBcc
     */
    public function addRecipientsBcc(array $recipientsBcc) {
        $this->recipientsBcc = array_merge($this->recipientsBcc, $recipientsBcc);
    }
    
    /**
     * @return AddressInterface[]
     */
    public function getRecipientsBcc() {
        return $this->recipientsBcc;
    }
    
    /**
     * @param AddressInterface $replyTo
     */
    public function setReplyTo($replyTo) {
        $this->replyTo = $replyTo;
    }
    
    /**
     * @return AddressInterface
     */
    public function getReplyTo(): AddressInterface {
        return $this->replyTo;
    }
    
    /**
     * @param string $subject
     */
    public function setSubject(string $subject) {
        $this->subject = self::encodedString($subject);
    }
    
    /**
     * @return string
     */
    public function getSubject():string {
        return $this->subject;
    }
    
    /**
     * @param MessageContentInterface $content
     */
    public function setContent(MessageContentInterface $content) {
        $this->content = $content;
    }
    
    /**
     * @return MessageContentInterface
     */
    public function getContent(): MessageContentInterface {
        return $this->content;          
    }
    
    /**
     * @param bool $encoded
     * @return string
     */
    public function addressesToString(array $addresses): string {
        $result = [];
        foreach($addresses as $address) {
            $result[] = $address.'';
        }
        return implode(',', $result);
    }
    
    /**
     * @return string
     */
    public function headersString(): string {    
        $contentHeaders = [];
        if ($this->content != null) {
            foreach ($this->content->getHeaders() as $header) {
                $contentHeaders[] = $header.'';
            }
        }
        
        return
            'From: '.$this->sender.self::$nl.
            'To: '.$this->addressesToString($this->recipients).self::$nl.
            (!empty($this->recipientsCc) ? 'Cc: '.$this->addressesToString($this->recipientsCc).self::$nl : '').
            (!empty($this->recipientsBcc) ? 'Bcc: '.$this->addressesToString($this->recipientsBcc).self::$nl : '').
            (!empty($this->replyTo) ? 'Reply-To: '.$this->replyTo.self::$nl : '').
            (!empty($this->subject) ? 'Subject: '.$this->subject.self::$nl : '').
            (!empty($contentHeaders) ? implode(self::$nl, $contentHeaders) : '');
    }
    
    /**
     * @return string
     */
    public function __toString(): string {
        return $this->headersString().self::$nl.self::$nl.$this->content->getBody();
    }
    
    /**
     * @param string $email
     * @param string $name
     * @return AddressInterface
     */
    public static function Address(string $email, string $name = ''): AddressInterface {
        return new class($email, $name) implements AddressInterface {
            
            /**
             * @var string
             */
            private string $email;
            
            /**
             * @var string
             */
            private string $name;
            
            /**
             * @param string $email
             * @param string $name
             */
            public function __construct(string $email, string $name = '') {
                $this->email = $email;
                $this->name = $name;
            }
            
            /**
             * {@inheritDoc}
             * @see \Yiama\Mail\AddressInterface::getEmail()
             */
            public function getEmail(): string {
                return $this->email;
            }
            
            /**
             * {@inheritDoc}
             * @see \Yiama\Mail\AddressInterface::getName()
             */
            public function getName(): string {
                return $this->name;
            }
            
            /**
             * @return string
             */
            public function __toString(): string {
                return ($this->name == null ? '' : '=?utf-8?B?'.base64_encode($this->name).'?= ').'<'.$this->email.'>';
            }
        };
    }
    
    /**
     * @param string $email
     * @param string $name
     * @return HeaderInterface
     */
    public static function Header(string $name, string $value): HeaderInterface {
        return new class($name, $value) implements HeaderInterface {
            
            /**
             * @var string
             */
            private $name;
            
            /**
             * @var string
             */
            private $value;
            
            /**
             * @param string $name
             * @param string $value
             */
            public function __construct(string $name, string $value) {
                $this->name = $name;
                $this->value = $value;
            }
        
            /**
             * {@inheritDoc}
             * @see \Yiama\Mail\AddressInterface::getName()
             */
            public function getName(): string {
                return $this->name;
                
            }
            
            /**
             * {@inheritDoc}
             * @see \Yiama\Mail\AddressInterface::getValue()
             */
            public function getValue(): string {
                return $this->value;
            }
            
            /**
             * {@inheritDoc}
             * @see \Yiama\Mail\AddressInterface::__toString()
             */
            public function __toString(): string {
                return $this->name.': '.$this->value;
            }
        };
    }
}
