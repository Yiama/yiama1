<?php
namespace Yiama\Mail;

/**
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */  
class Part implements MessageContentInterface {
    
    /**
     * @var string
     */
    private static $nl = "\r\n";
    
    /**
     * @var HeaderInterface[]
     */
    private $headers = [];
    
    /**
     * @var string
     */
    private $body;
    
    /**
     * @param HeaderInterface[]
     */
    public function addHeaders(array $headers) {
        $this->headers = array_merge($this->headers, $headers);
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Mail\MessageContentInterface::getHeaders()
     */
    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     * @param string $body
     */
    public function setBodyString(string $body) {
        $this->body = $body;
    }
    
    /**
     * @param string $html
     */
    public function setBodyHtml(string $html) {
        $this->addHeaders([Message::Header(Message::$HEADER_CONTENT_TYPE, 'text/html; charset=utf-8')]);
        // Add <html> and <body> if not present
        if( strpos( $html, '<html>') === false && strpos( $html, '<body>') === false) {
            $html = '<html><body>' . $html . '</body></html>';
        }
        $this->body = $html;
    }
    
    /**
     * @param string $text
     */
    public function setBodyPlain(string $text) {
        $this->addHeaders([Message::Header(Message::$HEADER_CONTENT_TYPE, 'text/plain; charset=utf-8')]);
        $this->body = $text;
    }
    
    /**
     * @param string $path
     * @param string $type
     * @param string $filename
     */
    public function setBodyAttachment(string $path, string $type, string $filename) {
        $this->addHeaders([
            Message::Header(Message::$HEADER_CONTENT_TYPE, $type."; name='".$filename."'"),
            Message::Header(Message::$HEADER_CONTENT_TRANSFER_ENCODING, 'base64'),
            Message::Header(Message::$HEADER_CONTENT_DISPOSITION, 'attachment; filename="'.$filename.'"')
        ]);
        $attachment = chunk_split(base64_encode(file_get_contents($path)));
        $this->body = $attachment;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Mail\MessageContentInterface::getBody()
     */
    public function getBody(): string {
        return $this->body;
    }
    
    /**
     * @return string
     */
    public function __toString(): string {
        $headers = [];
        foreach($this->headers as $header) {
            $headers[] = $header.'';
        }
        return implode(self::$nl, $headers).self::$nl.self::$nl.$this->getBody();
        
    }
}