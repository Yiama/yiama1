<?php
namespace Yiama\Mail\Smtp;

/**
 * https://tools.ietf.org/html/rfc5321#section-2.3.7
 * https://tools.ietf.org/html/rfc5321#section-2.4 *
 * Lines consist of zero or more data characters terminated by <CRLF>.
 * All server replies begin with a three digit numeric code.
 * The maximum total length of a reply line including the reply code and the <CRLF> is 512 octets.
 * 
 * @package Yiama/Mail/Smtp
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class ServerReply {
    
    /**
     * @var Status
     */
    private $status;
    
    /**
     * @var string
     */
    private $message;
    
    /**
     * @var bool
     */
    private $isLast;
    
    /**
     * @return int
     */
    public function getStatus(): Status {
        return $this->status;
    }
    
    /**
     * @return string
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * @param Status $status
     */
    public function setStatus(Status $status) {
        $this->status = $status;
    }
    
    /**
     * The maximum total length of a message content (including any message header section as well as the message body)
     * MUST BE at least 64K octets.
     *
     * @param string $message
     */
    public function setMessage(string $message) {
        $this->message = $message;
    }
    
    /**
     * @return bool
     */
    public function getIsLast(): bool {
        return $this->isLast;
    }
    
    /**
     * @param bool $isLast
     */
    public function setIsLast(bool $isLast) {
        $this->isLast = $isLast;
    }
    
    /**
     * @return ServerReply
     */
    public static function fromString(string $replyString) {
        if (strlen($replyString) < 3) {
            return null;
        }
        
        $reply = new ServerReply();
        $status = new Status(substr($replyString, 0, 3));
        $reply->setStatus($status);
        
        if (strlen($replyString) > 3) {
            $c = substr($replyString, 3, 1);
            $reply->setIsLast($c === ' ');
        }
        
        if (strlen($replyString) > 4) {
            $reply->setMessage(substr($replyString, 4));
        }
        
        return $reply;
    }
    
    /**
     * @return string
     */
    public function __toString(): string {
        return $this->status->getCode().($this->isLast ? ' ' : '-').$this->message;
    }
}