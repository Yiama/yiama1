<?php
namespace Yiama\Mail\Smtp;

/**
 * ClientCommand Interface
 * 
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
interface ClientCommandInterface {
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @param array $args
     * @return string
     */
    public function build(?array $args = null): string;
}