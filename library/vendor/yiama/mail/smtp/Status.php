<?php
namespace Yiama\Mail\Smtp;

/**
 * https://tools.ietf.org/html/rfc5321#section-4.2.1
 * 
 * @package Yiama/Mail/Smtp
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class Status {
    
    /**
     * @var int
     */
    private $code;
    
    /**
     * @param int $code
     */
    public function __construct(int $code) {
        $this->code = $code;   
    }
    
    /**
     * @return int
     */
    public function getCode(): int {
        return $this->code;
    }
    
    /**
     * The requested action has been successfully completed.
     *
     * @return bool
     */
    public function isSuccessful(): bool {
        return ((string) $this->code)[0] == 2;        
    }
    
    /**
     * The command has been accepted, but the requested action is being held in abeyance, pending receipt of further
     * information.
     *
     * @return bool
     */
    public function isIntermediate(): bool {
        return ((string) $this->code)[0] == 3; 
    }
    
    /**
     * The command was not accepted, and the requested action did not occur.  However, the error condition is temporary,
     * and the action may be requested again.
     *       
     * @return bool
     */
    public function isRetryFailure(): bool {
        return ((string) $this->code)[0] == 4; 
    }
    
    /**
     *  The command was not accepted and the requested action did not occur.  The SMTP client SHOULD NOT repeat the 
     *  exact request (in the same sequence).
     * 
     * @return bool
     */
    public function isFailure(): bool {
        return ((string) $this->code)[0] == 5; 
    }
    
    /**
     * @param Status $status
     * @return bool
     */
    public function equalsSeverity(Status $status) {
        return 
            ($this->isSuccessful() && $status->isSuccessful()) ||
            ($this->isIntermediate() && $status->isIntermediate()) ||
            ($this->isRetryFailure() && $status->isRetryFailure()) ||
            ($this->isFailure() && $status->isFailure());
    }
}