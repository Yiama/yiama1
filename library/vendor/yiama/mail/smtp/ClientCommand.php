<?php
namespace Yiama\Mail\Smtp;

/**
 * https://tools.ietf.org/html/rfc5321#section-2.3.7
 * https://tools.ietf.org/html/rfc5321#section-2.4
 * Not supported commands yet: RESET,VERIFY,EXPAND,HELP,NOOP
 * Lines consist of zero or more data characters terminated by <CRLF>.
 * All commands begin with a command verb.
 * A few SMTP servers, in violation of this specification (and RFC 821) require that command verbs be encoded
 * by clients in UPPER CASE.
 * The maximum total length of a command line including the command word and the <CRLF> is 512 octets.
 *
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class ClientCommand {
    
    /**
     * @var string line separator
     */
    private static $crlf = "\r\n";
    
    /**
     * https://tools.ietf.org/html/rfc5321#section-4.1.1.1
     *
     * @param string $domain fully-qualified domain name
     * @return string
     */    
    public static function Hello(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'HELLO';
            }
            
            public function build(?array $args = null): string {
                return 'EHLO '.$args[0]."\r\n";
            }
        };
    }
    
    /**
     * https://tools.ietf.org/html/rfc5321#section-4.1.1.2
     *
     * @param string $email sender address
     * @return string
     */
    public static function MailFrom(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'MAIL FROM';
            }
            
            public function build(?array $args = null): string {
                return 'MAIL FROM: <'.$args[0].'>'."\r\n";
            }
        };
    }
    
    /**
     * https://tools.ietf.org/html/rfc5321#section-4.1.1.3
     *
     * @param string $email recipient address
     * @return string
     */
    public static function Recipient(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'RCPT TO';
            }
            
            public function build(?array $args = null): string {
                return 'RCPT TO: <'.$args[0].'>'."\r\n";
            }
        };
    }
    
    /**
     * https://tools.ietf.org/html/rfc5321#section-4.1.1.4
     * 
     * @return string
     */
    public static function Data(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'DATA';
            }
            
            public function build(?array $args = null): string {
                return 'DATA'."\r\n";
            }
        };
    }
    
    /**
     * Sent dot before the QUIT command
     *
     * @return string
     */
    public static function EndData(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'end data';
            }
            
            public function build(?array $args = null): string {
                return "\r\n".'.'."\r\n";
            }
        };
    }
    
    /**
     * https://tools.ietf.org/html/rfc5321#section-4.1.1.10
     *
     * @return string
     */
    public static function Quit(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'QUIT';
            }
            
            public function build(?array $args = null): string {
                return 'QUIT'."\r\n";
            }
        };
    }
    
    /**
     * https://www.fehcom.de/qmail/smtpauth.html
     *
     * @return string
     */
    public static function AuthLogin(): ClientCommandInterface {
        return new class implements ClientCommandInterface {
            public function getName(): string {
                return 'AUTH LOGIN';
            }
            
            public function build(?array $args = null): string {
                return 'AUTH LOGIN'."\r\n";
            }
        };
    }
}