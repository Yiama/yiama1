<?php
namespace Yiama\Mail;

/**
 * Yiama Mail Exception
 *
 * @package Yiama/Mail
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class MailException extends \Exception {}