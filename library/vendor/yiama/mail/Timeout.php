<?php
namespace Yiama\Mail;

/**
 * @package Yiama/Mail/Smtp
 * @author Yiannis Magkanaris
 * @version 1.0
 * @since 1.0
 */
class Timeout {
    
    /**
     * @var int
     */    
    private $start;
    
    /**
     * @var int
     */
    private $end;
    
    public function __construct() {
        $this->start = time();
    }
    
    /**
     * @param int $timestamp
     */
    public function setEnd(int $timestamp) {
        $this->end = $timestamp;
    }
    
    /**
     * @return int timestamp
     */
    public function diffFromNow(): int {
        return time()-$this->start;
    }
    
    /**
     * @return int $timestamp
     */
    public function diffFrom(int $timestamp): int {
        return $timestamp-$this->start;
    }
    
    /**
     * @throws MailException
     * @return bool
     */
    public function expired(): bool {
        if (empty($this->end)) {
            throw new MailException('End timestamp is not set.');
        }
        return time() >= $this->end;
    }
}