<?php 
namespace Yiama\Autoload;

use Yiama\Throwable\InvalidArgumentException;

/**
 * Autoload Registrator<br />
 * <br />
 * Adds functionality over the basic spl_autoload_<> functions.
 *
 * @package Yiama/Autoload
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class Registrator {
    
    /**
     * @var LoaderCollection
     */
    private $collection;
    
    /**
     * Instantiates an autoloader collection.
     */
    public function __construct() {
        $this->collection = new LoaderCollection();
    }
    
    /**
     * Returns all registered autoloaders.
     * 
     * @return LoaderCollection
     */
    public function getRegistered(): LoaderCollection {
        return $this->collection;
    }
    
    /**
     * Registers a collection of autoloaders.<br />
     * If the $key already exists or the same instance is already registered
     * then the registration will be skipped.
     * 
     * @param LoaderCollection $collection
     * @return bool
     */
    public function register(LoaderCollection $collection): bool {
        foreach ($collection as $key => $loader) {
            if (isset($this->collection[$key]) ||
                $this->collection->has($loader)) {
                    return true;
            }
            
            if (\spl_autoload_register([$loader, 'load']) === true) {
                $this->collection[$key] = $loader;
                
                return true;
            }
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Unregisters an autoloader.
     * 
     * @param string $key
     * @throws InvalidArgumentException
     * @return bool
     */
    public function unregister(string $key): bool {
        $registered_functions = \spl_autoload_functions();
        if (empty($registered_functions)) {
            return true;
        }
        
        foreach ($registered_functions as $f) {
            if ($this->collection[$key] === $f[0]) {
                if (\spl_autoload_unregister([$f[0], 'load']) === true) {
                    unset($this->collection[$key]);
                    
                    return true;
                }
                
                return false;
            }
        }
        
        return true;
    }
}