<?php
namespace Yiama\Autoload;

use Yiama\Std\CollectionInterface;
use Yiama\Throwable\InvalidArgumentException;

/**
 * Autoload Loader Collection
 *
 * @package Yiama/Autoload
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class LoaderCollection implements CollectionInterface {
    
    private $collection = [];
    
    /**
     * {@inheritDoc}
     * @see \Countable::count()
     */
    public function count(): int {
        return \count($this->collection);
    }
    
    /**
     * {@inheritDoc}
     * @see \IteratorAggregate::getIterator()
     */
    public function getIterator(): \ArrayIterator {
        return new \ArrayIterator($this->collection);
    }
    
    /**
     * {@inheritDoc}
     * @see \ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value) {
        if (!is_string($offset)) {
           throw new InvalidArgumentException('Argument $offset must be a'.
                ' string');
        }
        
        if (!$value instanceof LoaderInterface) {
            throw new InvalidArgumentException('Argument $value must be of type'.
                ' Yiama\Autoload\LoaderInterface');
        }
        
        $this->collection[$offset] = $value;
    }
    
    /**
     * {@inheritDoc}
     * @see \ArrayAccess::offsetExists()
     */
    public function offsetExists($offset): bool {
        return isset($this->collection[$offset]);
    }
    
    /**
     * {@inheritDoc}
     * @see \ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset) {
        if (isset($this[$offset])) {
            unset($this->collection[$offset]);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \ArrayAccess::offsetGet()
     */
    public function offsetGet($offset): ?LoaderInterface {
        return isset($this[$offset])
            ? $this->collection[$offset]
            : null;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Std\CollectionInterface::clear()
     */
    public function clear() {
        $this->collection = [];
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Std\CollectionInterface::has()
     */
    public function has($value): bool {
        foreach ($this->collection as $v) {
            if ($v === $value) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Std\CollectionInterface::merge()
     */
    public function merge(CollectionInterface $collection) {
        if (!$collection instanceof LoaderCollection) {
            throw new InvalidArgumentException('Argument $collection must be'.
                ' of type Yiama\Autoload\LoaderCollection');
        }
        
        foreach ($collection as $key => $value) {
            $this->collection[$key] = $value;
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\Std\CollectionInterface::difference()
     */
    public function difference(CollectionInterface $collection) {
        if (!$collection instanceof LoaderCollection) {
            throw new InvalidArgumentException('Argument $collection must be'.
                ' of type Yiama\Autoload\LoaderCollection');
        }
        
        foreach ($this->collection as $key1 => $value1) {
            foreach ($collection as $key2 => $value2) {
                if ($key1 === $key2 && $value1 === $value2) {
                    unset($this->collection[$key1]);
                    break;
                }
            }
        }
    }
}