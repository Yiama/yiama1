<?php
namespace Yiama\Autoload;

/**
 * Autoload Loader Interface
 *
 * @package Yiama/Autoload
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
interface LoaderInterface {
    
    /**
     * @param string $class_name
     */
    public function load(string $class_name);
}