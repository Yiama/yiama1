<?php 
namespace Yiama\Autoload\Loader;

use Yiama\Autoload\LoaderInterface;
use Yiama\Throwable\InvalidArgumentException;

/**
 * Autoload Loader Psr4<br />
 *<br />
 * Complies with PSR-4 Autoload <a href='standard'>http://www.php-fig.org/psr/psr-4/</a>
 *
 * @package Yiama/Autoload
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class Psr4Loader implements LoaderInterface{
    
    /**
     * @var array 
     */
    private $mapping = [];
    
    /**
     * Adds a namespace-to-paths mapping.
     * 
     * @param string $namespace
     * @param array $paths
     * @throws InvalidArgumentException
     * @return \Yiama\Autoload\Loader\Psr4Loader
     */
    public function addMapping(string $namespace, array $paths): Psr4Loader {
        if (!isset($this->mapping[$namespace])) {
            $this->mapping[$namespace] = [];
        }
        foreach ($paths as $path) {
            if (!\is_string($path)) {
                throw new InvalidArgumentException("Path must be of type String");
            }
            $this->mapping[$namespace][] = $path;
        }
        
        return $this;
    }
    
    /**
     * Adds an array with mappings.
     * 
     * @param array $mappings
     */
    public function addMappingArray(array $mappings) {
        foreach ($mappings as $namespace => $paths) {
            $this->addMapping($namespace, $paths);
        }
    }
    
    /**
     * Tries to load the class with the specific $classname.<br />
     * When PHP comes upon a class that is not included then calls 
     * all registered autoloaders one by one passing the class name as argument, 
     * till it finds the one that will load the class.
     * 
     * @param string $class_name
     */
    public function load(string $class_name) {
        // Separate namespace from classname
        $namespace = \substr($class_name, 0, \strrpos($class_name, '\\'));
        $c = \substr($class_name, \strrpos($class_name, '\\')+1);
        
        // Check if namespace is mapped with a path
        if (!empty($this->mapping)) {
            foreach ($this->mapping as $key => $paths) {
                if ($key == $namespace) {
                    foreach ($paths as $path) {
                        $file = $path.DIRECTORY_SEPARATOR.$c.'.php';
                        
                        // Include file
                        if (\file_exists($file)) {
                            // Require_once is used in case the file is required again
                            // manually somewhere in the code.
                            require_once($file);
                            
                            return;
                        }
                    }
                }
            }
        }
    }
}