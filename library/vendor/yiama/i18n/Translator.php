<?php
namespace Yiama\I18n;

use Yiama\File\File;

/**
 * I18n Translator
 *
 * @package Yiama/I18n
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class Translator {
    
    /**
     * @var LoaderInterface 
     */
    private $loader;
    
    /**
     * @var array 
     */
    private $translations = array();
    
    /**
     * @var string
     */
    private $locale;
    
    /**
     * @var string
     */
    private $default_locale;
    
    /**
     * @param LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader) {
        $this->loader = $loader;
    }
    
    /** 
     * Sets the locale that will be used when the method _() is called. 
     * 
     * @param string $locale
     */
    public function setLocale(string $locale) {
        $this->locale = $locale;
    }
    
    /**
     * Sets default locale.
     * 
     * @param string $locale
     */
    public function setDefaultLocale(string $locale) {
        $this->default_locale = $locale;
    }
    
    /**
     * Adds translation for a specific locale or merges if the locale already 
     * exists.
     * 
     * @param array $translation
     * @param string $locale
     */
    public function addTranslation(array $translation, string $locale) {
        if (isset($this->translations[$locale])) {
            $this->translations[$locale] = array_merge(
                $this->translations[$locale], 
                $translation
            );
        } else {
            $this->translations[$locale] = $translation;
        }
    }
    
    /**
     * Adds a translation for a specific locale from it's resource.
     * 
     * @param File $file
     * @param string $locale
     */
    public function addResource(File $file, string $locale) {
        $translation = (array)$this->loader->load($file);
        $this->addTranslation($translation, $locale);
    }
    
    /**
     * Returns the translation for a key, also can replace wildcard with
     * $replacements. $replacements must have the same order with the one they
     * are found inside translation. Replacable elements inside translation
     * will be identified with '%%' characters. If no matches are found, 
     * the translation will be returned unchanged.
     * 
     * @param string $key
     * @param array $replacements
     * @return string
     */
    public function _(string $key, array $replacements = []): string {
        $translation = '';
        
        // Check locale
        if (!empty($this->locale) && 
            !empty($this->translations[$this->locale][$key])) {
                
            $translation = $this->translations[$this->locale][$key];
        } // Check default locale
        elseif (!empty($this->default_locale) && 
                !empty($this->translations[$this->default_locale][$key])) {
            
            $translation = $this->translations[$this->default_locale][$key];
        }
        
        // Replace
        if (!empty($translation) &&
            !empty($replacements)) {
                
            foreach ($replacements as $rep) {
                $translation = @\preg_replace('/%%/', $rep, $translation, 1);
            }
        }
        
        // If key was not found in translations return the given key
        return empty($translation) ? $key : $translation;
    }
    
    /**
     * If $key is not found for the given locale in translations then returns
     * the initial key.
     * 
     * @param string $key
     * @param string $locale
     * @param array $replacements
     * @return string
     */
    public function _l(string $key, string $locale, array $replacements = []): string {
        $translation = '';
        
        if (!empty($this->translations[$locale][$key])) {
            $translation = $this->translations[$locale][$key];
        }
        
        // Replace
        if (!empty($translation) &&
            !empty($replacements)) {
                
            foreach ($replacements as $rep) {
                $translation = @\preg_replace('/%%/', $rep, $translation, 1);
            }
        }
        
        return !empty($translation) ? $translation : $key;
    }
}