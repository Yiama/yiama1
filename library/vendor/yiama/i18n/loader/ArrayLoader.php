<?php
namespace Yiama\I18n\Loader;

use Yiama\I18n\LoaderInterface;
use Yiama\File\File;

/**
 * I18n Array Loader
 *
 * Loads a .php file for use in Translator.
 *
 * @package Yiama/I18n
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class ArrayLoader implements LoaderInterface {
    
    /**
     * {@inheritDoc}
     * @see \Yiama\i18n\LoaderInterface::load()
     */
    public function load(File $file): array {
        $content = @include($file->getPath());
        
        return is_array($content)? $content: [];
    }
}