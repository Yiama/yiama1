<?php
namespace Yiama\I18n\Loader;

use Yiama\I18n\LoaderInterface;
use Yiama\File\File;

/**
 * Translation Ini Loader
 *
 * Loads a .ini file for use in Translator.
 *
 * @package Yiama/I18n
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class IniLoader implements LoaderInterface {
    
    /**
     * @var integer 
     */
    private $mode = INI_SCANNER_NORMAL;
    
    /**
     * @param bool $has_sections
     */
    public function setHasSections(bool $has_sections) {
        $this->has_sections = $has_sections;
    }
    
    /**
     * @param int $mode
     */
    public function setMode(int $mode) {
        if ($mode === INI_SCANNER_NORMAL ||
            $mode === INI_SCANNER_RAW ||
            $mode === INI_SCANNER_TYPED) {
                
            $this->mode = $mode;
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\i18n\LoaderInterface::load()
     */
    public function load(File $file): array {
        $content = @\parse_ini_file($file->getPath(), false, $this->mode);
        
        return is_array($content)? $content: [];
    }
}