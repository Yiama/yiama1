<?php
namespace Yiama\I18n\Loader;

use Yiama\I18n\LoaderInterface;
use Yiama\File\File;

/**
 * I18n Csv Loader
 *
 * Loads a .csv file for use in Translator.
 *
 * @package Yiama/I18n
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
class CsvLoader implements LoaderInterface {
    
    /**
     * @var string 
     */
    private $delimiter = ',';
    
    /**
     * @var string 
     */
    private $enclosure = '"';
    
    /**
     * @var string 
     */
    private $escape = "\\";
    
    /**
     * @var integer 
     */
    private $length = 1000;
    
    /**
     * Sets the field delimiter (one character only).
     * 
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter) {
        if (\strlen($delimiter) === 1) {
            $this->delimiter = $delimiter;
        }
    }
    
    /**
     * Sets the field enclosure character (one character only)
     * 
     * @param string $enclosure
     */
    public function setEnclosure(string $enclosure) {
        if (\strlen($enclosure) === 1) {
            $this->enclosure = $enclosure;
        }
    }
    
    /**
     * Sets the escape character (one character only).
     * 
     * @param string $escape
     */
    public function setEscape(string $escape) {
        if (\strlen($escape) === 1) {
            $this->escape = $escape;
        }
    }
    
    /**
     * Sets maximum line length.
     * 
     * @param int $length
     */
    public function setLength(int $length) {
        if (!empty($length)) {
            $this->length = $length;
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \Yiama\i18n\LoaderInterface::load()
     */
    public function load(File $file): array {
        if (($handle = @\fopen($file->getPath(), "r")) !== false) {
            $content = [];
            while (($data = @\fgetcsv($handle, $this->length, $this->delimiter, $this->enclosure, $this->escape)) !== false) {
                $content[$data[0]] = isset($data[1]) ? $data[1] : '';
            }
            @\fclose($handle);
            
            return $content;
        }
        
        return [];
    }
}