<?php
namespace Yiama\I18n;

use Yiama\File\File;

/**
 * I18n Translator Loader Interface
 *
 * @package Yiama/I18n
 * @author Yiannis Magkanaris
 * @version 1.1
 * @since 1.1
 */
interface LoaderInterface {
    
    /**
     * Parses to array the content of a file.
     * 
     * @param File $file
     * @return array
     */
    public function load(File $file): array;
}
