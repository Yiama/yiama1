<?php

/*
	Array @helper.
*/
class Core_Array
{
	private static $sortField;
	private static $sortDirection;
	
	/*
		Επιστρέφει το περιεχόμενο του field ενός array που δίνεται με dotseperated fields.
			$var = 'field.subfield.subsubfield'
			return $array[field][subfield][subsubfield]
		!!! Αν το ίδιο index υπάρχει σε διαφορετικά subfields θα επιστρέψει το πρώτο που θα βρει
		Με την χρήση brackets
	
		@param  ( string ) $var    { dot seperated [χρήση brackets '[]' για πεδία που η ονομασία τους περιέχει dot '.']  }
		@param  ( array )  $array  { associative }
		@param  ( array )  $indx   { index tou array $parts }
		@return ( mixed )  array field
	*/
	public static function shorthand( $var, $array, $indx = 0 ) 
	{
		$exp_dots = explode( '.', $var );
		$parts = array();
		foreach( $exp_dots as $key => $val ) {
			if( strpos( $val, '[' ) === 0 ) {
				$parts[] = ltrim( $val, '[' ) . '.' . rtrim( $exp_dots[ $key+1 ], ']' );
			} elseif ( strrpos( $val, ']' ) === false ) {
				$parts[] = $val;
			}
		}
		/* Δεν έχει άλλο subfield να ελέγξει */
		if( $indx == count( $parts ) )
			return $array;
			
		foreach( ( array ) $array as $key => $val )
			if( $key == $parts[ $indx ])
				return self::shorthand( $var, $val, ++$indx );

		return false;
	}
	
	/*
		MultiD array σε 1D array.
		
		@param  ( multiD array ) $array
		@param field ( string ) { Κελί ως προς το οποίο θα γίνεται το flatten, αυτό δηλαδή θα περιέχει τα sublevels. }
		@return 1D array 
	*/
	public static function flatten( array $array, $field ) 
	{
		$return = array();
		foreach( $array as $ar )
		{
			/* Δεν δίνουμε το πεδίο που περιέχει τα sublevels */
			$return[] = array_diff_key( $ar, array_flip( array( $field ) ) );
				
			if( isset( $ar[ $field ] ) )
			{ 
				$return = array_merge( $return, self::flatten( $ar[ $field ], $field ) );
			}
		}
		
		return $return;
	}
	
	/*
		
		Δημιουργεί ένα multiD array διασυνδέοντας τα rows { fields τύπου array } με δενδρική μορφή
		βάσει ενός field που χρησιμοποιείται για την ιεράρχιση των rows. Ο array προερχεται από έναν sql table.
		
		@param ( array )  $array
		@param ( mixed )  $initial_value
		@param ( string ) $value_field { field που χρησιμοποιείται για τον έλεγχο στο δεύτερο πέρασμα }
		@param ( string ) $hierarchy_field { field που χρησιμοποιείται για την ιεράρχιση στο πρώτο πέρασμα }
		@return ( array )
	*/
	public static function tree( $array, $initial_value, $value_field, $hierarchy_field, $treeLevel = 1 )
	{
		$branch = array();
		
		foreach( $array as $ar ) 
		{
			if( $ar[ $hierarchy_field ] == $initial_value ) 
			{
				$ar['treeLevel'] = $treeLevel;
				$ar['countChildren'] = 0;
				$children = self::tree( $array, $ar[ $value_field ], $value_field, $hierarchy_field, ($treeLevel+1) );
				if( $children ) 
				{
					$ar['children'] = $children;
					$ar['countChildren'] = count( $children );
				}
				$branch[] = $ar;
			}
		}
		return $branch;
	}
	
	/*
	 * Επιστρέφει έναν ή περισσότερους κλάδους - branch του tree ελέγχοντας ως προς την ισότητα '=' των τιμών.
	 * 
	 * @param $array { Array φτιαγμένο με την tree() }
	 * @param $search { array( "key" => "value", ... ) }
	 */
	public static function treeSearch( $array, $search )
	{
		$result = array();
		$searchTotal = count( array_keys( $search ) );

		foreach( $array as $ar )
		{
			$foundTotal = 0;	
			foreach( $search as $sKey => $sVal ){
				if( isset( $ar[ $sKey ] ) && $sVal == $ar[ $sKey ] ){
					$foundTotal++;
				}
			}
		
			if( $foundTotal == $searchTotal ){
				$result[] = $ar;
			}
			
			if( isset( $ar['children'] ) )
			{
				$subResult = self::treeSearch( $ar['children'], $search );
				if( ! empty( $subResult ) ){
					$result = array_merge( $result, $subResult );
				}
			}
		}
		
		return $result;
	}
	
	/*
	 * Επιστρέφει ελέγχοντας ως προς την ισότητα '=' των τιμών.
	 * 
	 * @param $array { 2D }
	 * @param $search { array( "key" => "value", ... ) }
	 * @param $operator ( string ) { '>', '<', '!==', '>=', '<=' }
	 * @return mixed { array, false}
	 */
	public static function search( $array, $search, $operator = '==', $type = null )
	{
		$result = array();
		$searchTotal = count( array_keys( $search ) );
		
		foreach( $array as $ar )
		{
			$foundTotal = 0;	
			foreach( $search as $sKey => $sVal )
			{
				if( isset( $ar[ $sKey ] ) )
				{
					$arKey = $ar[ $sKey ];
					if( $type )
					{
						if( $type == 'int' )
						{
							$sVal = intval( $sVal );
							$arKey = intval( $arKey );
						}
						elseif ( $type == 'float' )
						{
							$sVal = floatval( $sVal );
							$arKey = floatval( $arKey );
						}
					}
					
					switch( $operator )
					{
						case "==":
							if( $arKey == $sVal )
							{
								$foundTotal++;
							}
							break;
					 	case "!=":
							if( $arKey !== $sVal )
							{
								$foundTotal++;
							}
							break;
					 	case ">":
							if( $arKey > $sVal )
							{
								$foundTotal++;
							}
							break;
					 	case "<":
							if( $arKey < $sVal )
							{
								$foundTotal++;
							}
							break;
					 	case ">=":
							if( $arKey >= $sVal )
							{
								$foundTotal++;
							}
							break;
					 	case "<=":
							if( $arKey <= $sVal )
							{
								$foundTotal++;
							}
							break;
					}
				}
			}
			if( $foundTotal == $searchTotal ){
				$result[] = $ar;
			}
		}
		
		return $result;
	}
	
	/*
		Επιστρέφει τις τιμές από ένα μόνο column ενός 2D array,
		σε έναν numeric array. 
		
		@param $array ( array )
		@param $column ( string )
		@return ( array )
	*/
	public static function array_column( $array, $column )
	{
		$return = array();
		foreach( ( array ) $array as $val )
		{
			$return[] = $val[ $column ];
		}
		
		return $return;
	}
	
	/*
	 * Usort compare function
	 */
	public static function sortCompare( $row1, $row2 )
	{
		$field = self::$sortField;
		$row1Field = $row1[ $field ];
		$row2Field = $row2[ $field ];
		
		if( is_numeric( $row1Field ) )
		{
			$row1Field = floatval( $row1Field );
			$row2Field = floatval( $row2Field );
		}
		
		if( $row1Field == $row2Field )
		{
			return 0;
		}
		
		if( self::$sortDirection === 1 )
		{
	    	return ( $row1Field < $row2Field ) ? -1 : 1;
		}
		else
		{
	    	return ( $row1Field > $row2Field ) ? 1 : -1;
		}
	}
	
	/*
	 * @param $array
	 * @param $field { fieldname για το sorting }
	 * @param $direction { sorting direction [ 1: asc, 0: desc ] }
	 */
	public static function sort( $array, $field, $direction = 1 )
	{
		self::$sortField = $field;
		self::$sortDirection = $direction;
		
		usort( $array, array( 'Core_Array', 'sortCompare' ) );
		
		return $array;
	}
	
	/*
	 * Αντικατάσταση των values του πρώτου array με αυτές του δεύτερου
	 * 
	 * @param $array1
	 * @param $array2
	 * @param $fields ( array ) { Fields των οποίων οι τιμές θα αντικατασταθούν }
	 */
	public static function replace( $array1, $array2, $fields )
	{
		foreach( $array1 as $key => &$ar )
		{
			foreach( ( array ) $fields as $f )
			{
				$ar[ $f ] = $array2[ $key ][ $f ];
			}
		}
		
		return $array1;
	}
}
?>