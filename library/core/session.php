<?php

/*
	Εκκινεί το session αν δεν είναι εκκινημένο.
	Αποθηκεύει και ανακτά από την $_SESSION της PHP.
	Το instance αναφέρεται στο συγκεκριμένο session που έχει δημιουργηθεί με την session_start(),
	οπότε δεν μπορούμε να έχουμε περισσότερα από ένα session.
*/
class Core_Session
{
	/*
		Singleton instance.
	*/
	private static $instance = null;
	
	public function __construct(){}
	
	public function __destruct(){}
	
	/*
		Get instance.
		Χρησιμοποιείται για να γίνει εκκίνηση του session ( session_start() ).
	*/
	public static function getInstance() 
	{
        if ( self::$instance == null ) 
		{
            self::$instance = new Core_Session();
			session_start();
		}
		
		return self::$instance;
	}
	
	/*
		Set session values.
		
		@param $key ( mixed )
		@param $value ( mixed )
	*/
	public function set( $key, $value ) 
	{
		$_SESSION[ $key ] = $value;
	}
	
	/*
		Get session values.
		
		@param $key ( string )
		@param $default ( string ) { Default return value. }
		@return ( mixed ) { Αν δεν βρέθηκε και δεν έχει δοθεί $default επιστρέφει null. }
	*/
	public function get( $key, $default = null ) 
	{
		if( isset( $_SESSION[ $key ] ) )
		{
			return $_SESSION[ $key ];
		}
		
		return $default;
	}
	
	/*
		Remove session key.
		
		@param $key ( string )
		@return ( boolean ) { False => αν δεν υπάρχει το $key }
	*/
	public function remove( $key ) 
	{
		if( isset( $_SESSION[ $key ] ) )
		{
			unset( $_SESSION[ $key ] );
			return true;
		}
		
		return false;
	}
}