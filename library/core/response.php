<?php

/*
	Response.
	Singleton class.
*/
class Core_Response
{
	/*
		Singleton instance.
	*/
	private static $instance = null;
	
	public function __construct(){}
	
	public function __destruct(){}
	
	/* 
		Response headers. 
		
		@var ( array )
	*/
	private $headers;
	
	/* 
		Response body. 
		
		@var ( string )
	*/
	private $body = '';
	
	/*
		Get instance.
	*/
	public static function getInstance() 
	{
        if ( self::$instance == null ) 
		{
            self::$instance = new Core_Response();
			// Cache plugin set no cache headers
	        if( Core_App::getConfig( 'library.plugins.cache_nocache' ) ) {
				$plugin = new Plugin_Cache_Nocache();
				$plugin->setHeaders();
			}
		}
		
		return self::$instance;
	}
	
	/*
		@param $headers ( array )
	*/
	public function setHeaders( $headers )
	{	
		$this->headers = array_merge( ( array ) $this->headers, ( array ) $headers );
	}
	
	/*
		@param $body ( string )
	*/
	public function setBody( $body )
	{	
		$this->body = $body;
	}
	
	public function setJson($object)
	{
	    $this->setHeaders( 'content-type: application/json; charset="utf-8"' );
	    $this->body = json_encode($object);
	}

	/*
		������ �� headers ��� ����� echo �� body.
	*/
	public function get()
	{	
		foreach( ( array ) $this->headers as $h )
		{
			header( $h );
		}

		return $this->body;
	}
	
	/*
		Redirect page.
		
		@param url ( string )
	*/
	public function redirect( $url )
	{	
		header( "Location: {$url}" );
		
		exit();
	}
	
	/*
	 * Αποστολή request σε κάποιον server
	 * 
	 * $context_options = array(
			'http' => array(
			    'method' => 'POST',
			    'header' => array(
			    	'Content-type: application/x-www-form-urlencoded', 
					'Content-Length: ' . strlen( $data ),
				),
				'content' => $data
			)
		);
	 * !! Πιθανόν το $context_options['http']['header'] να είναι array και όχι linebreak (\r\n) seperated string
	 * 
	 * @param $url ( string )
	 * @param $context_options ( array ) { http://php.net/manual/en/context.php }
	 * @return 
	 */
	public function request( $url, $context_options = array() )
	{		
		$context = stream_context_create( $context_options );
		
		$fp = @fopen( $url, 'r', false, $context );
		if( $fp !== false )
		{
			return stream_get_contents( $fp );
			fclose( $fp );
		}
		return false;
	}
}

?>