<?php

/*
	Error - fatal error - exception  handler
*/
class Core_Error
{	
	/*
	 * @return ( string )
	 */
	private static function exceptionMode()
	{
		$error_mode = Core_App::getConfig( 'library.core.exception.mode' );
		return ( $error_mode['production']['on'] ) ? 'production' : 'development';
	}
	
	private static function redirect()
	{
		/*
		 * Πρόληψη redirect loop όταν βρίσκεται ήδη στην error page του site
		 */
		if( Core_Request::getInstance()->getController() == 'error' ) 
		{
			/* Εξαφανίζουμε τυχόν output που είναι να εμφανιστεί */
			@ob_end_clean();
			
			include PATH_APPS . Core_App::getAppName() . DS . 'views' . DS . 'error' . DS . 'serious' . DS . 'view.php';
			
			exit( 0 );
		}	
		
		$langcode = ( $l = Core_Request::getInstance()->getParams( 'langcode' ) ) ? $l : 'el';
		$url_meta = json_decode( Core_App::getConfig( 'library.core.exception.page' ), true );
		$redirectUrl = Core_Request::getInstance()->url(
			$url_meta['routename'],
			array_merge( $url_meta['routevalues'], array( 'langcode' => $langcode ) )
		);
		
		Core_Response::getInstance()->redirect( $redirectUrl ); 
	}
	
	/*	
		Αντιστοιχεί το όνομα του error type με ένα string. 
	*/
	public static function getErrorStr( $error )
	{
		switch( $error )
		{
			case E_ERROR             : return 'E_ERROR'; 
			case E_WARNING           : return 'E_WARNING';
			case E_PARSE             : return 'E_PARSE';
			case E_NOTICE            : return 'E_NOTICE';
			case E_CORE_ERROR        : return 'E_CORE_ERROR';
			case E_CORE_WARNING      : return 'E_CORE_WARNING';
			case E_COMPILE_ERROR     : return 'E_COMPILE_ERROR';
			case E_COMPILE_WARNING   : return 'E_COMPILE_WARNING';
			case E_USER_ERROR        : return 'E_USER_ERROR';
			case E_USER_WARNING      : return 'E_USER_WARNING';
			case E_USER_NOTICE       : return 'E_USER_NOTICE';
			case E_STRICT            : return 'E_STRICT';
			case E_RECOVERABLE_ERROR : return 'E_RECOVERABLE_ERROR';
			case E_DEPRECATED        : return 'E_DEPRECATED';
			case E_USER_DEPRECATED   : return 'E_USER_DEPRECATED';
			case E_ALL               : return 'E_ALL';
			default                  : return false;
			/* extend to XML errors and HTTP errors */
		}
	}
	
	/*	
		Fatal error handler. 
		Επειδή καλείται πάντα στο τέλος της PHP ως shutdown function πρέπει να ελέγξουμε 
		αν κάποιο error ήταν ο λόγος που τερματίστηκε η PHP και αν αυτό το error είναι fatal.
		Η error_reporting() πρέπει να καλείται πάντα σε ένα custom error handler, για παράδειγμα για την περίπτωση 
		που θέλουμε να χρησιμοποιήσουμε το σύμβολο '@' που κάνει error message suppress και επιστρέφει 0 η error_reporting().
	*/
	public static function fatal_error_handler()
	{
		$lastError = error_get_last();
		
		/* 	Αν δεν υπήρξε fatal error πριν το κλείσιμο της PHP. */
		if( ! is_array( $lastError ) && error_reporting() !== 0 )
		{
			return;
		}
		
		/* Production mode. */
		if( self::exceptionMode() == 'production' && Core_App::getAppName() != 'admin'  )
		{
			self::redirect();
			return;
		}
		
		/* Development mode. */
		echo '<html><body>
		<div style=\'width: 96%; padding: 5px 2%; line-height: 30px; font-size: 12pt; color: #fff; background-color: #888\'>
		FATAL ERROR - ' . $lastError['message'] . ', file: ' . $lastError['file'] . ', line: ' . $lastError['line'] . '
		</div></body></html>';
	}
	
	/*	
		Error handler. 
		Ελέγχει αν το error είναι μέσα σε αυτά που ορίζονται στο config.php για το συγκεκριμένο mode.
		Αν είμαστε στο production error_mode τότε κάνει redirection στην error page.
		Η error_reporting() πρέπει να καλείται πάντα σε ένα custom error handler, για παράδειγμα για την περίπτωση 
		που θέλουμε να χρησιμοποιήσουμε το σύμβολο '@' που κάνει error message suppress και επιστρέφει 0 η error_reporting().
		
		$param ( int )    $errno
		$param ( string ) $errstr { Σε αυτό δίνουμε το action του error controller. }
		$param ( string ) $errfile
		$param ( int )    $errline
	*/
	public static function error_handler( $errno, $errstr, $errfile, $errline )
	{
		/* Έλεγχος αν το error code είναι μέσα σε αυτά που ορίσαμε στην error_reporting() στην Core_Application. */
		if ( ( error_reporting() && $errno ) )
		{		
			/* Production mode. */
			//if( $error_mode['production']['on'] && ( strpos( self::getErrorStr( $errno ), 'E_USER_' ) === false ) )
			if( self::exceptionMode() == 'production' && Core_App::getAppName() != 'admin' )
			{
				self::redirect();
				return;
			}
			
			/* Development mode. */
			echo 
			'<html><body><div style=\'width: 96%; border-top: 2px solid #888; padding: 10px 2%; line-height: 30px; font-size: 12pt; color: #6688aa; background-color: #eee\'>'
			. 'ERROR-HANDLER:' . '<br/>'
			. '<div style="overflow: hidden; padding: 5px 0; letter-spacing: 2px; font-size: 15pt; font-weight: bold; color: #bb5555">' . self::getErrorStr( $errno ) . '</div>'
			. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 5px 2%; background-color: #ddd; font-size: 12pt\'><b>File:</b> &nbsp; ' . $errfile . '</div>'
			. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 5px 2%; background-color: #ddd; font-size: 12pt\'><b>Trace:</b> &nbsp; ' . $errstr . '</div>'
			. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 5px 2%; background-color: #ddd; font-size: 12pt\'><b>Line:</b> &nbsp; ' . $errline . '</div>'
			. '</div></body></html>';
		}
	}

	/*	
		Exception handler. 
		Μία exception είναι fatal.
		Ελέγχει πρώτα αν το string που δίνεται στην Exception υπάρχει ως κελί στον $errorMsgs,
		αλλιώς επιστρέφει το string ακέραιο.
		Σύνταξη:
			throw new Exception( "<error.ini field>[:$variable]" )
			=> throw new Exception( "library_core.autoload.no_class_found:$className" );
			ή 
			=>throw new Exception( 'library_core.autoload.no_class_found' );
	
		@param ( Exception object ) $e
	*/
	public static function exception_handler( $e )
	{
		/* Production mode. */
	    if( self::exceptionMode() == 'production' && Core_App::getAppName() != 'admin'  )
		{
			self::redirect();
			return;
		}
		
		/* Development mode. */
		/*	Exception variables */
		$message = $e->getMessage();
		$previous = $e->getPrevious();   
		$code = $e->getCode();          
		$file = $e->getFile();
		$line = $e->getLine();          
		$trace = $e->getTrace();                 
		$traceAsString = $e->getTraceAsString();         
		$__toString = $e->__toString();

		/* Αν το message δεν είναι cell του error.ini array το εμφανίζουμε ακέραιο. */
		$msg = explode( ':', $message );
		$ini = array_shift( $msg );
		$traceString = '';
        array_walk_recursive( $trace[0], function( &$val, $key ) use( &$traceString ){ $traceString .= "<br>$key : <b>" . (is_object($val) ? (method_exists($val, "__toString") ? $val->__toString() : "" ) : $val) . "</b>"; });
		echo 
		'<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF8"></head><body>'
		.'<div style=\'width: 96%; border-top: 2px solid #333; padding: 10px 2%; line-height: 30px; font-size: 12pt; color: #113355; background-color: #aaa\'>'
		. 'EXCEPTION-HANDLER:'
		. '<div style="overflow: hidden; padding: 10px 0; letter-spacing: 2px; font-size: 15pt; font-weight: bold; color: #bb5555">' . Core_App::getErrorMsg( $ini ) . '<br/>' . implode( ' : ', $msg ) . '</div>'
		//. '<b>code:</b> ' . $code . '<br/>'
		. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 10px 2%; background-color: #bbb; font-size: 12pt\'><b>Message:</b> &nbsp; ' . $message . '</div>'
		. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 10px 2%; background-color: #bbb; font-size: 12pt\'><b>File:</b> &nbsp; ' . $file . '</div>'
		. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 10px 2%; background-color: #bbb; font-size: 12pt\'><b>Trace last:</b> &nbsp; ' . $traceString . '</div>'
		. '<div style=\'margin-top: 5px; width: 96%; overflow: hidden; padding: 10px 2%; border:1px solid #bbb; font-size: 12pt\'><b>Full error string:</b> ' . str_replace( '#', '<br/>#', $e->getTraceAsString() )
		.'</div></body></html>';
	}
}

?>