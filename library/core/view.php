<?php

class Core_View
{	
	private $vars;
	private $filename;
	private $content;
	
	/* @param  $filename ( string ) */
	public function __construct( $filename = null )
	{
		if( $filename ) {
			$this->filename = $filename;
		}
	}
	
	public function __destruct(){}
	
	/* Add / override view variables.
	 * @param $vars ( array )
	 */
	public function addVars( $vars )
	{
		foreach( ( array ) $vars as $key => $val ) {
			$this->$key = $val;
		}
		return $this;
	}
	
	/* @param  $content ( string ) */
	public function setContent( $content )
	{
		$this->content = $content;
		return $this;
	}
	
	public function render()
	{
		ob_start();
		if( is_null( $this->content ) && $this->filename ) {
			include $this->filename; 
			$return = ob_get_contents();
		} elseif ( $this->content ) {
			 $return = $this->content;
		} else {
			throw new Exception( 'library_core.view.no_filename_or_content_set' );
		}
		ob_end_clean();
		
		return $return;
	}
	
	/* 
		Magic method set.
		Ορίζει μία property.
		
		@param $name ( property name )
		@param $value ( property value )
		@return  ( class property )
	*/
	public function __set( $name, $value )
	{
		return $this->$name = $value;
	}
	
	/* 
		Magic method get.
		Επιστρέφει το property.
		
		@param  ( property name )
		@return  ( class property )
	*/
	public function __get( $name )
	{
		return $this->$name;
	}
}

?>