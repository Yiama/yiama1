<?php
abstract class Core_ModuleAdminAbstract {
    
    public function __construct() {
    }
    
    abstract public function render($args = null);
    
    abstract public function save();

    public function getConfig() {
        $model = new Model_Yiama_Module($this->name);
        $data = $model->data();
        if ($data != null) {
            return Core_ModuleConfig::fromString($data);
        }
        return null;
    }

    protected function saveConfig(Core_ModuleConfig $config) {
        $model = new Model_Yiama_Module($this->name);
        $data = json_encode($config->toObject());
        $model->insertUpdateData($data);
    }

    protected function checkCsrf(array $params = array()) {
        $csrf = new Security_Csrf(Core_Session::getInstance());
        if (    empty($params[$csrf->getRequestTokenName()])
            ||  !$csrf->isValid($params[$csrf->getRequestTokenName()])) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array(
                        'controller' => 'error',
                        'action' => 'notfound'
                    )
                )
            );
        }
    }
}