<?php

/*
 �������� �� headers ��� HTML.
 */
class Core_HTML
{
	/*
		HTML header.

		@var ( array )
		*/
	public static $headers = array(
	'full'             => array(), /* Απαραίτητο */
	'content-type'     => '', /* �������� ��� ��� Core_Request */
	'charset'          => 'utf-8',
	'x-ua-compatible'  => 'X-UA-Compatible',
	'robots'           => 'index,follow',
	'description'      => '',
	'keywords'         => '',
	'HandheldFriendly' => 'True',
	'viewport'         => 'width=device-width, initial-scale=1',
	'generator'        => 'YiaMa - A simple micro-framework',
	'title'            => ''
	);
	public static $count = 0;
	public static $scripts = '';
	public static $styles = '';
	
	/*
	 HTML header files.

	 @var ( array )
	 */
	public static $files = [];
	
	/*
	 HTML header files before others.

	 @var ( array )
	 */
	public static $filesTop = [];

	/*
	Header title.

	 @param ( string ) $text
	 */
	public static function addToTitle( $text )
	{
		self::$headers['title'] .= $text;
	}

	/*
	Header metatag description.

	 @param ( string ) $text
	 */
	public static function addToDescription( $text )
	{
		self::$headers['description'] .= $text;
	}

	/*
	Header metatag keywords.

	 @param ( string ) $text
	 */
	public static function addToKeywords( $text )
	{
		self::$headers['keywords'] .= $text;
	}

	/*
	Header js.

	 @param ( string ) $text
	 */
	public static function addJs( $text )
	{
		self::$scripts .= "<script type='text/javascript'>{$text}</script>" . PHP_EOL;
	}
	
	/*
	 Header <style>.
	 
	 @param ( string ) $text
	 */
	public static function addStyle( $text )
	{
	    self::$styles .= $text . PHP_EOL;
	}

	/*
	 Basei tou extension psaxnei ston antistoixo subfolder tou assets.
	 	
	 @param $path ( mixed ) { File path meta ton antistoixo subfolder p.x. assets/acss/,assets/js/ }
	*/
	public static function addFiles( $path )
	{
		foreach( ( array ) $path as $p ) {
			$info = pathinfo( $p );
			self::$files = array_merge_recursive( ( array ) self::$files, array( $info['extension'] => array( ( $info['dirname'] !== '.' ? $info['dirname']  : '' ) . DS . $info['filename'] ) ) );
		}
	}

	/*
	 Gives the ability to put inside header files before others.
	 	
	 @param $path ( mixed ) { File path meta ton antistoixo subfolder p.x. assets/acss/,assets/js/ }
	*/
	public static function addFilesTop( $path )
	{
		foreach( ( array ) $path as $p ) {
			$info = pathinfo( $p );
			self::$filesTop = array_merge_recursive( ( array ) self::$filesTop, array( $info['extension'] => array( ( $info['dirname'] !== '.' ? $info['dirname']  : '' ) . DS . $info['filename'] ) ) );
		}
	}

	/*
	 ��������� ��� header.
	 Override header.

	 @param ( array ) $headers { array( 'headerName' => 'value' ) }
	 */
	public static function addHeaders( $headers )
	{
		self::$headers = array_merge( ( array ) self::$headers, ( array ) $headers );
	}
	
	/*
	 Add to full body headers.

	 @param ( mixed ) $full_headers
	 */
	public static function addFullHeaders( $full_headers )
	{
		self::$headers['full'] = array_merge( ( array ) self::$headers['full'], ( array ) $full_headers );
	}

	/*
	 ���������� �� HTML js script.

	 @return ( string )
	 */
	public static function getScripts()
	{
		return self::$scripts;
	}
	
	/**
	 * 
	 * @return string
	 */
	public static function getStyles()
	{
	    return "<style>".self::$styles."</style>";
	}
	
	/*
	 ���������� �� HTML header files.

	 @return ( string )
	 */
	public static function getFiles()
	{
		$basePath = DS . PATH_BASEDIR . DS . 'assets' . DS;
		$files = '';
		// Assets plugin , σύμπτυξη css, js αρχείων
		if( Core_App::getConfig( 'library.plugins.assets_bundle' ) ) {
			$plugin = new Plugin_Assets_Bundle();
			$files = $plugin->getFiles(array_merge(self::$filesTop, self::$files));
		} else {
            $files .= self::_getFiles(self::$filesTop);
            $files .= self::_getFiles(self::$files);
		}
		return $files;
	}
    
    private static function _getFiles(array $files): string {
        $result = '';
        foreach( $files as $key => $val ) {
            foreach( array_unique( $val ) as $v ) {
                if( php_uname( 's' ) == 'Linux' ) {
                    $v = str_replace( '\\', '/', $v );
                }
                if( $key == 'css' ) {
                    $result .= "<link rel='stylesheet' href='" . str_replace( array( '\\', '//' ), '/', PATH_REL_ASSETS . $key . DS . $v ) . ".css' type='text/css' />" . PHP_EOL;
                } elseif ( $key == 'js' ) {
                    $result .= "<script type='text/javascript' src='" . str_replace( array( '\\', '//' ), '/', PATH_REL_ASSETS . $key . DS . $v ) . ".js'></script>" . PHP_EOL;
                }
            }
        }
        return $result;
    }

	/*
	 ���������� �� HTML header.
	 */
	public static function getHeaders()
	{
	    $title_postfix = Core_App::getConfig('library.core.html.title_postfix');
		return
		( ( isset( self::$headers['content-type'] ) )          ? '<meta http-equiv=\'Content-Type\'           content=\'' . self::$headers['content-type'] . '\' />' . PHP_EOL . PHP_EOL : '' )
		. ( ( isset( self::$headers['charset'] ) )          ? '<meta http-equiv=\'content-type\'         content=\'text/html; charset=' . self::$headers['charset'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['x-ua-compatible'] ) )  ? '<meta http-equiv=\'X-UA-Compatible\'      content=\'' . self::$headers['x-ua-compatible'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['robots'] ) )           ? '<meta name=\'robots\'                     content=\'' . self::$headers['robots'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['description'] ) )      ? '<meta name=\'description\'                content=\'' . self::$headers['description'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['keywords'] ) )         ? '<meta name=\'keywords\'                   content=\'' . self::$headers['keywords'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['HandheldFriendly'] ) ) ? '<meta name=\'HandheldFriendly\'           content=\'' . self::$headers['HandheldFriendly'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['viewport'] ) )         ? '<meta name=\'viewport\'                   content=\'' . self::$headers['viewport'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['google'] ) )           ? '<meta name=\'google-site-verification\'   content=\'' . self::$headers['google'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['alexa'] ) )            ? '<meta name=\'alexaVerifyID\'              content=\'' . self::$headers['alexa'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['microsoft'] ) )        ? '<meta name=\'majestic-site-verification\' content=\'' . self::$headers['microsoft'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['generator'] ) )        ? '<meta name=\'generator\'                  content=\'' . self::$headers['generator'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['title'] ) )            ? '<title>' . self::$headers['title'] . (!empty($title_postfix) ? ' | ' . $title_postfix : '') . '</title>'. PHP_EOL : '' )
		. ( ( isset( self::$headers['canonical'] ) )        ? '<link rel=\'canonical\'                   href=\'' . self::$headers['canonical'] . '\' />' . PHP_EOL : '' )
		. ( ( isset( self::$headers['full'] ) )             ? implode( PHP_EOL, self::$headers['full'] ) . PHP_EOL : '' )
		;
	}
}

?>