<?php
class Core_ModuleInstance{
    
    /**
     * @var string
     */
    private $id;
    
    /**     
     * @var stdClass
     */
    private $options;
    
    /**      
     * @var array
     */
    private $params;
    
    public function __construct($id) {
        $this->id = $id;
    }
    
    /**
     * @param array $options
     */
    public function setOptions(stdClass $options) {
        $this->options = $options;
    }
    
    /**
     * @param array $params
     */
    public function setParams(array $params) {
        $this->params = $params;
    }
    
    /**
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }
    
    /**
     * @return array
     */
    public function getParams() {
        return $this->params;
    }
    
    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @return stdClass
     */
    public function toObject() {
        $obj = new stdClass();
        $obj->id = $this->id;
        $obj->options = $this->options;
        $obj->params = $this->params;
        return $obj;
    }
}