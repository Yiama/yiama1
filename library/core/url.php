<?php

/*
	URL.
	Language letters reference: http://mylanguages.org/
	Accents reference: http://www.languageguide.org/<language_name>/grammar/pronunciation/accents.html, http://www.bbc.co.uk/languages/<language_name>/guide/alphabet.shtml
	
	@Helper
*/
class Core_URL
{	
	/*
	 * Επειδή η Urlencode και η rawurlencode αντικαθιστούν το '/' με το '%2F'
	 * δημιουργούνται προβλήματα με κάποιους server ή browser.
	 * Αυτή η μέθοδος αντικαθιστά το '%2F' με το '/'
	 *
	 * @param $url ( string )
	 */
	public static function encode( $url )
	{
		return str_replace( "%2F", "/", rawurlencode( $url ) );
	}
	
	/*
		@param $str ( string )
		@param $lang ( string ) { language code }
	*/
	public static function strToLatin( $str, $lang )
	{
		//an uparxei o xarakthras '&' ton antikathistoume me to 'kai' ths antistoixhs glwssas
		switch( $lang )
		{
			case "el":
				$str = str_replace( "&", "kai", $str );
				$fromArray = array( "Α", "Β", "Γ", "Δ", "Ε", "Ζ", "Η", "Θ",  "Ι", "Κ", "Λ", "Μ", "Ν", "Ξ", "Ο", "Π", "Ρ", "Σ", "Τ", "Υ", "Φ", "Χ", "Ψ", "Ω", "Ά", "Έ", "Ή", "Ί", "Ό", "Ύ", "Ώ", "α", "β", "γ", "δ", "ε", "ζ", "η", "θ",  "ι", "κ", "λ", "μ", "ν", "ξ", "ο", "π", "ρ", "σ", "τ", "υ", "φ", "χ", "ψ", "ω", "ς", "ά", "έ", "ή", "ί", "ό", "ύ", "ώ", " ", ";" );
				$toArray =   array( "a", "b", "g", "d", "e", "z", "h", "th", "i", "k", "l", "m", "n", "x", "o", "p", "r", "s", "t", "u", "f", "x", "p", "w", "a", "e", "h", "i", "o", "u", "w", "a", "b", "g", "d", "e", "z", "h", "th", "i", "k", "l", "m", "n", "x", "o", "p", "r", "s", "t", "u", "f", "x", "p", "w", "s", "a", "e", "h", "i", "o", "u", "w", "-", "" );
				break;
			case "en":
				$str = str_replace( "&", "and", $str );
				$fromArray = array( "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", " ", "?" ); 
				$toArray =   array( "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "-", "" );
				break;
		}	
					
		return str_replace( $fromArray, $toArray, $str );
	}
	
	/*
		Μετατρέπει όλους τους χαρακτήρες των υποστηριζόμενων γλωσσών στους αντίστοιχους λατινικούς.
		Υποστηρίζονται:
			Αγγλικά 
			Ελληνικά
			Διακριτικά λατινικών γλωσσών ( δεν συμπεριλαμβάνονται όλα )
			
		@param $str ( string )
	*/
	public static function transToLatin( $str )
	{
		$array = array(/*'en', 'greek', 'latinAccents' */
			'A' => array( 'A', 'Α|Ά'  , 'Á|Ấ|Ắ|À|Â|Ã|Ä|Ǻ|Ǻ'     ),
			'a' => array( 'a', 'α|ά'  , 'á|ấ|ắ|à|â|ã|ä|å|ǻ'     ),
			'B' => array( 'B', 'Β'    , 'Þ'                     ),
			'b' => array( 'b', 'β'    , 'þ'                     ),
			'C' => array( 'C', ''     , 'Ć|Ḉ|Ç'                 ),
			'c' => array( 'c', ''     , 'ć|ḉ|ç'                 ),
			'D' => array( 'D', 'Δ'    , ''                      ),
			'd' => array( 'd', 'δ'    , ''                      ),
			'E' => array( 'E', 'Ε|Έ'  , 'É|Ǽ|Æ|Ǿ|Ø|Ḗ|Ế|È|Ê|Ë'   ),
			'e' => array( 'e', 'ε|έ'  , 'é|ǽ|æ|ǿ|ø|ḗ|ế|è|ê|ë'   ),
			'F' => array( 'F', 'Φ'    , ''                      ),
			'f' => array( 'f', 'φ'    , ''                      ),
			'G' => array( 'G', 'Γ'    , 'Ǵ'                     ),
			'g' => array( 'g', 'γ'    , 'ǵ'                     ),
			'H' => array( 'H', 'Η|Ή'  , ''                      ),
			'h' => array( 'h', 'η|ή'  , ''                      ),
			'I' => array( 'I', 'Ι|Ί'  , 'Í|Ḯ|Ì|Î|Ï'             ),
			'i' => array( 'i', 'ι|ί|ΐ', 'í|ḯ|ì|î|ï'             ),
			'J' => array( 'J', ''     , ''                      ),
			'j' => array( 'j', ''     , ''                      ),
			'K' => array( 'K', 'Κ'    , 'Ḱ'                     ),
			'k' => array( 'k', 'κ'    , 'ḱ'                     ),
			'L' => array( 'L', 'Λ'    , 'Ĺ'                     ),
			'l' => array( 'l', 'λ'    , 'ĺ'                     ),
			'M' => array( 'M', 'Μ'    , 'Ḿ'                     ),
			'm' => array( 'm', 'μ'    , 'ḿ'                     ),
			'N' => array( 'N', 'Ν'    , 'Ń|Ñ'                   ),
			'n' => array( 'n', 'ν'    , 'ń|ñ'                   ),
			'O' => array( 'O', 'Ο|Ό'  , 'Ó|Ő|Ṓ|Ṍ|Ố|Ớ|Ò|Ô|Õ|Ö'   ),
			'o' => array( 'o', 'ο|ό'  , 'ó|ő|ṓ|ṍ|ố|ớ|ò|ô|õ|ö|ð' ),
			'P' => array( 'P', 'Π|Ψ'  , 'Ṕ'                     ),
			'p' => array( 'p', 'π|ψ'  , 'ṕ'                     ),
			'Q' => array( 'Q', ''     , ''                      ),
			'q' => array( 'q', ''     , ''                      ),
			'R' => array( 'R', 'Ρ'    , 'Ŕ'                     ),
			'r' => array( 'r', 'ρ'    , 'ŕ'                     ),
			'S' => array( 'S', 'Σ'    , 'Ś|Ṥ|Š|ß'               ),
			's' => array( 's', 'σ|ς'  , 'ś|ṥ|š'                 ),
			'T' => array( 'T', 'Τ|Θ'  , ''                      ),
			't' => array( 't', 'τ|θ'  , ''                      ),
			'U' => array( 'U', ''     , 'Ú|Ű|Ǘ|Ṹ|Ứ|Ù|Û|Ü'       ),
			'u' => array( 'u', ''     , 'ú|ű|ǘ|ṹ|ứ|ù|û|ü'       ),
			'V' => array( 'V', ''     , ''                      ),
			'v' => array( 'v', ''     , ''                      ),
			'W' => array( 'W', 'Ω'    , 'Ẃ'                     ),
			'w' => array( 'w', 'ω|ώ'  , 'ẃ'                     ),
			'X' => array( 'X', 'Χ|Ξ'  , ''                      ),
			'x' => array( 'x', 'χ|ξ'  , ''                      ),
			'Y' => array( 'Y', 'Υ|Ύ'  , 'Ý|Ÿ'                   ),
			'y' => array( 'y', 'υ|ύ|ΰ', 'ý|ÿ|ӳ|Ӳ'               ),
			'Z' => array( 'Z', 'Ζ'    , 'Ź|Ž'                   ),
			'z' => array( 'z', 'ζ'    , 'ź|ž'                   )
		);
							
		$str = ( array ) $str;
		foreach( $str as $charKey => &$charVal )
		{
			foreach( $array as $arKey => $arVal )
			{
				foreach( $arVal as $lettersTo )
				{
					$expLettersTo = explode( '|', $lettersTo );
					foreach( $expLettersTo as $l )
					{
						$str[ $charKey ] = str_replace( $l, $arKey, $charVal );
					}
				}
			}
		}

		return implode( '', $str );
	}
	
	/*
		@param $string ( string )
		@param $params ( array )
		@return sanitized string
	*/
	public static function sanitize( $string, $params )
	{
		switch( $params['filter'] )
		{
			case 'url_alias':
				$string = preg_replace('~[^\\pL0-9_]+~u', '-', $string);
				$string = trim($string, "-");
				$string = self::transToLatin( $string );
				$string = strtolower($string);
				break;
			case 'url_path': /* Επιτρέπει μόνο αλφαριθμητικά και underscore */
				$string = preg_replace('~[^\\pL0-9_]+~u', '-', $string);
				break;
		}
		
		return $string;
	 }
	 
	 /*
		Προσθέτει και αφαιρεί τιμές των στοιχείων του URL
		$array => 	
		array( 
			'query' => array( 
				'remove' => array(
					'order'
				),
				'add' => array(
					'page' => 2
				)
			)
		)
			
		@param $url ( string )
		@param $array ( array )
		@return array
	*/
	public static function manipulate( $url, $array )
	{
		$URLComponents = parse_url( $url );
		
		foreach( $array as $component => $c )
		{
			/* scheme */
			if( $component == 'scheme' ){
				$URLComponents['scheme'] = $c;
			}
			
			/* host */
			if( $component == 'host' ){
				$URLComponents['host'] = $c;
			}
			
			/* query */
			if( $component == 'query' )
			{
				$query = array();
				if( isset( $URLComponents['query'] ) ){
					parse_str( $URLComponents['query'], $query );
				}
				if( isset( $c['remove'] ) )
				{
					$query = array_diff_key( $query, array_flip( $c['remove'] ) );
					$URLComponents['query'] = http_build_query( $query );
				}
				if( isset( $c['add'] ) )
				{
					$query = array_merge( $query, $c['add'] );
					$URLComponents['query'] = http_build_query( $query );
				}
			}
		}
		
		return self::http_build_url( $URLComponents );
	}
	 
	 /*
		Δημιουργεί ένα URL από ένα πίνακα με στοιχεία που περιλαμβάνει η parse_url
		
		@param $URLComponents ( array )
		@return string
	*/
	public static function http_build_url( $URLComponents )
	{
		return 
			( ( isset( $URLComponents['scheme'] ) ) ? "{$URLComponents['scheme']}://" : "" )
			. ( ( isset( $URLComponents['user'] ) ) ? "{$URLComponents['user']}:" : "" )
			. ( ( isset( $URLComponents['pass'] ) ) ? "{$URLComponents['pass']}@" : "" )
			. ( ( isset( $URLComponents['host'] ) ) ? $URLComponents['host'] : "" )
			. ( ( isset( $URLComponents['path'] ) ) ? substr( $URLComponents['path'], strpos( $URLComponents['path'], '/' ) ) : "" )
			. ( ( isset( $URLComponents['query'] ) ) ? "?{$URLComponents['query']}" : "" )
			. ( ( isset( $URLComponents['fragment'] ) ) ? "#{$URLComponents['fragment']}" : "" );
	}
	
	/*
		Επιστρέφει το τρέχον URL ολόκληρο
		
		@return ( string )
	*/
	public static function get()
	{
		return $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	}
	
	/*
	 * Επιστρέφει το url έχοντας προσθέσει/αντικαταστήσει στοιχεία στο query.
	 * @param $url ( string )
	 * @param $query ( array ) { Αν πεδίο δίνεται Null αφαιρείται από το query }
	 */
	public static function addQuery( $url, $query )
	{
		parse_str( parse_url( $url, PHP_URL_QUERY ), $url_query );
		$query_string = http_build_query( array_filter( array_merge( $url_query, $query ) ) );
		if( ( $pos = strpos( $url, '?' ) ) !== false ) { /* Έχει ήδη παραμέτρους */
			$url = substr( $url, 0, $pos );
		}
		return $url . '?' . $query_string;
	}
}
?>