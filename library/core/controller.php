<?php

/*
	Ένας controller μπορεί να έχει ένα view και ένα template.
*/
abstract class Core_Controller
{
	public function __construct()
	{
		$this->request = Core_Request::getInstance();
		$this->response = Core_Response::getInstance();
		$this->modules = array();
		
		/* View paths. */
		$controller = get_called_class();
		$reflector = new ReflectionClass( $controller );
		$path = substr( $reflector->getFileName(), 0, strpos( $reflector->getFileName(), DS . 'controller' ) );
		$this->defaultViewPath = $path . DS . 'views' . DS . strtolower( implode( DS, explode( '_', substr( $controller, strpos( $controller, 'Controller_' )+11 ) ) ) );
		/* Before action */
		$methods = $reflector->getMethods();
		foreach( $methods as $m )
		{
			if( $m->name == 'before' )
			{
				$this->before();
			}
		}
		// Mobile plugin
		if (Core_App::getConfig('library.plugins.mobile')) {
		    new Plugin_Mobile();
		}		
	}
	
	public function __destruct(){}
	
	/*
		Set / override view.
		Αν δεν δίνεται το view path χρησιμοποιεί το default.
		
		@param $path { view file path }
	*/
	protected function setView( $path = null )
	{
		if( ! $path )
		{
			$path = $this->defaultViewPath;
		}
		
		$this->view = new Core_View( $path . '.php' );
		$this->view->controller = $this;
	}
	
	/* 
		Magic method set.
		Ορίζει μία property.
		
		@param $name ( property name )
		@param $value ( property value )
		@return  ( class property )
	*/
	public function __set( $name, $value )
	{
		return $this->$name = $value;
	}
	
	/* 
		Magic method get.
		Επιστρέφει το property.
		
		@param  ( property name )
		@return  ( class property )
	*/
	public function __get( $name )
	{
		return $this->$name;
	}
}

?>