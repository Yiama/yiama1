<?php

/*
	Εκκινεί το session αν δεν είναι εκκινημένο.
	Ανακτά από την $_COOKIE της PHP και ορίζει τα cookies Με την setcookie().
*/
class Core_Cookie
{
	private static $iv; //Χρειάζεται για το encryption.

	/*
		Set cookie.

		@param $array { array(
			$key ( string )
			$value ( string )
			$encrypt ( boolean )
			$expire ( integer )
			$path ( string )
			$domain ( string )
			$secure ( boolean )
			$httponly ( boolean )
		) }
	*/
	public static function set( $array )
	{
		if( isset( $array['encrypt'] ) && $array['encrypt'] ){
			$array['value'] = self::encrypt( $array['value'] );
		}
		$ar = array(
			'key' => "",
			'value' => "",
			'expire' => 0,
			'path' => "/",
			'domain' => "",
			'secure' => false,
			'httponly' => false
		);
		$array = array_replace( $ar, $array );
		setcookie( $array['key'], $array['value'], $array['expire'], $array['path'], $array['domain'], $array['secure'], $array['httponly'] );
	}

	/*
		Get cookie.

		@param ']key ( string )
		@param $encrypted ( boolean ) { Αν είναι encrypted το cookie. }
		@param $default ( string ) { Default return value. }
		@return ( mixed ) { Αν δεν βρέθηκε και δεν έχει δοθεί $default επιστρέφει null. }
	*/
	public static function get( $key, $encrypted = false, $default = null )
	{
		if( isset( $_COOKIE[ $key ] ) )
		{
			return trim( ( $encrypted ) ? self::decrypt( $_COOKIE[ $key ] ) : $_COOKIE[ $key ] );
		}

		return $default;
	}

	/*
		Remove cookie.

		@param $key ( string )
	*/
	public static function remove( $key )
	{
		unset( $_COOKIE[ $key ] );
		setcookie( $key, null, -1, '/' );
	}

	/*
		Encrypt cookie value.
		Χρησιμοποιεί Initialization Vector (IV) το οποίο αποθηκεύεται στο cookie μαζί με την τιμή του cookie
		ώστε να μπορέσει να χρησιμοποιηθεί για το decryption.

		@param $data ( string ) { Data to encrypt. }
		@return ( string ) { Encrypted data. }
	*/
	public static function encrypt( $data )
	{
		$key = substr(Core_App::getConfig( 'security.salt' ), 0, 10);
		$ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
		$iv = openssl_random_pseudo_bytes($ivlen);
		$ciphertext_raw = openssl_encrypt($data, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
		return base64_encode( $iv.$ciphertext_raw );
	}

	/*
		Decrypt cookie value.

		@param $data ( string ) { Cookie value. }
		@return ( string ) { Decrypted data. }
	*/
	public static function decrypt( $data )
	{
		$key = substr(Core_App::getConfig( 'security.salt' ), 0, 10);
		$c = base64_decode($data);
		$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
		$iv = substr($c, 0, $ivlen);
		$ciphertext_raw = substr($c, $ivlen);
		return openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);exit;
	}
}
