<?php
abstract class Core_ModuleAbstract {
    
    /**
     * @param Core_ModuleConfig $config
     */
    public function __construct() {
		$this->request = Core_Request::getInstance();
		$this->response = Core_Response::getInstance();
        $this->defaultViewPath = PATH_ROOT.'module'.DS.'views'.DS.strtolower(
            implode(DS, explode('_', substr(get_class($this), 7)))
        );
    }
    
    /**
     * @param string $path
     */
    protected function setView($path = null) {
		$path = $path == null ? $this->defaultViewPath : $path;
		$this->view = new Core_View($path . '.php');
	}
    
    abstract public function render($args = null);
}