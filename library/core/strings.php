<?php

/*
	Strings @helper.
*/
class Core_Strings
{
	/*
		Επιστρέφει κεφαλαιοποιημένη κάθε λέξη που χωρίζεται με τον delimiter
	
		@param  ( string ) $string
		@param  ( array )  $delimiter { associative }
		@return ( mixed )  array
	*/
	public static function ucwords( $string, $delimiter ='_' ) 
	{
		$parts = explode( $delimiter, $string );
		
		foreach( $parts as &$p )
		{
			$p = ucfirst( $p );
		}

		return implode( $delimiter, $parts );
	}
	
	/*
	 * Επιστρέφει το κείμενο κομμένο αν χρειαστεί, και αν χρειαστεί βάζει στο τέλος αποσιοποιητικά
	 * 
	 * @param $str
	 * @param $max_len
	 */
	public static function short( $str, $max_len )
	{
		return ( mb_strlen( $str ) > $max_len ? mb_substr( $str, 0, mb_strpos( $str, ' ', $max_len ) ) : $str ) . ( mb_strlen( $str ) > $max_len ? '...' : '' );
	}
}
?>