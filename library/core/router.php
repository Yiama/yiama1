<?php

/*
	Router.
	Βρίσκει τον controller και το action βάσει του url path.
*/
class Core_Router
{
	/* 
		Route request.
		Επιλέγει controller και action βάσει του route pattern.
	*/
	public static function route()
	{
		$controller = Core_Request::getInstance()->controller;
		$action = Core_Request::getInstance()->action;
		
		/* 
			Autofill action arguments. 
			Η action δέχεται arguments μόνο τύπου ( string, int, array ).
		*/
		$actionParams = self::actionArgs( $controller, $action );
		
		$controller = new $controller;
		call_user_func_array( array( $controller, $action ), $actionParams );
	}
	
	/* 
		Autofill method action arguments.
		Καλούμε την ReflectionMethod αρχικά και όχι την ReflectionParameter γιατί έτσι 
		δεν χρειάζεται να υπάρχει controller instance.
		Χρησιμοποιεί τις methods της ReflectionParameter class της ReflectionMethod class της PHP
		για να βρει τις arguments της action method του controller και να τις γεμίσει
		από τις params του URL, ελέγχοντας και ποιες είναι required ( όσες δεν έχουν default value ). 
		Argument και url param πρέπει να έχουν το ίδιο όνομα.
		
		@param $controller ( string )
		@param $action ( string )
		@return ( array ) action method arguments ( value, type )
	*/
	public static function actionArgs( $controller, $action )
	{
		$reflection = new ReflectionMethod( $controller, $action );
		$objRefParam = $reflection->getParameters();
		
		$return = array();
		foreach( $objRefParam as $ref )
		{ 
			$arg = $ref->getName();
			
			/* Αναζήτηση στις url parameters για την argument. */
			if( $p = Core_Request::getInstance()->getParams( $arg ) )
			{
				$return[$arg] = $p;
			}
			
			/* Αν δεν έχουν σταλεί οι required arguments. */
			if( ! $ref->isOptional() && ! isset( $return[$ref->getName()] ) )
			{
				throw new Exception( "library_core.router.not_found_required_params_for_controller_action:$controller:$action()" );
			}
			
		}
		return $return;
	}
}
?>