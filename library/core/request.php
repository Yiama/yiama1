<?php

/*
	Request.
	Singleton class.
*/
class Core_Request
{
	/*
		Singleton instance.
	*/
	private static $instance = null;
	
	public function __construct(){}
	
	public function __destruct(){}
	
	/*
		Request type { html, ajax }.
		
		@var ( array )
	*/
	private $type;
	
	/* 
		Request method.
		
		@var ( string );
	*/
	private $method;
	
	/*
		Request parameters { GET, POST, FILE }.
		
		@var ( array )
	*/
	private $params;
	
	/*
		Request headers.
		
		@var ( array )
	*/
	private $headers;
	
	/*
		Route που αντιστιχοίθηκε.
		
		@var ( string )
	*/
	private $route;
	
	/*
		Το url path του request χωρίς τα baseDir και appName.
		
		@var ( string )
	*/
	private $urlRoutePath;
	
	/*
		Αν το URL καταλήγει σε εικονικό document.
		
		@var ( string )
	*/
	private $urlPseudoDoc;
	
	/*
		Το extension του $pseudoDoc.
		
		@var ( string )
	*/
	private $urlPseudoExt;
	
	/*
		Request URI.
		
		@var ( string )
	*/
	private $URI;
	
	/*
		Yiama base directory.
		
		@var ( string )
	*/
	private $baseDir;
	
	/*
		@var ( string )
	*/
	private $appName;
	
	/*
		Το extension του $pseudoDoc.
		
		@var ( string )
	*/
	private $pseudoExt;
	
	/*
		Όνομα controller.
		
		@var ( string )
	*/
	private $controller;
	
	/*
		Όνομα action.
		
		@var ( string )
	*/
	private $action;
	
	/*
		Κωδικοποίηση του URL.
		
		@var ( string )
	*/
	private $url_encoding;

	/*
		Get instance.
	*/
	public static function getInstance() 
	{
        if ( self::$instance == null ) 
		{
            self::$instance = new Core_Request();
		}
		
		return self::$instance;
	}
	
	/* 
		Initialize.
		Set request variables. 
	*/
	public function init()
	{
		/* Type */
		if( ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) ) {
			if( strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
				$this->type = 'ajax';
			}
		} else {
			$this->type = 'html';
		}
		
		/* Request method. */
		$this->method = $_SERVER['REQUEST_METHOD'];
		
		/* Request URI. */
		$this->URI = $_SERVER['REQUEST_URI'];
		
		/* Parameters */
		$this->params = array();
        
		switch( $this->method ) {
			case 'GET':
				$this->params = $_GET;
				break;
			case 'POST':
				$this->params = $_POST;
				break;
		}
		if( ! empty( $_FILES ) ) {
			$this->params = array_merge( $this->params, array( 'files' => $_FILES ) );
		}
		
		/* Headers */
		$this->headers = array (
			'host'             => ( ( isset( $_SERVER['HTTP_HOST']            ) ) ? $_SERVER['HTTP_HOST']            : null ),
			'connection'       => ( ( isset( $_SERVER['HTTP_CONNECTION']      ) ) ? $_SERVER['HTTP_CONNECTION']      : null ),
			'user_agent'       => ( ( isset( $_SERVER['HTTP_USER_AGENT']      ) ) ? $_SERVER['HTTP_USER_AGENT']      : null ),
			'cache_control'    => ( ( isset( $_SERVER['HTTP_CACHE_CONTROL']   ) ) ? $_SERVER['HTTP_CACHE_CONTROL']   : null ),
			'accept'           => ( ( isset( $_SERVER['HTTP_ACCEPT']          ) ) ? $_SERVER['HTTP_ACCEPT']          : null ),
			'accept_encoding'  => ( ( isset( $_SERVER['HTTP_ACCEPT_ENCODING'] ) ) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : null ),
			'accept_languages' => ( ( isset( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : null ),
			'accept_charset'   => ( ( isset( $_SERVER['HTTP_ACCEPT_CHARSET']  ) ) ? $_SERVER['HTTP_ACCEPT_CHARSET']  : null )
		);	
		
		if( ! ( $route = $this->getRoute( self::getUrlRoutePath() ) ) ) { /* Αν δεν βρεθεί route επιστρέφει Exception */
			throw new Exception( 'library_core.request.no_route_matched_for_send_url:Core_Request:init()' );
		} else {
			$matches = $route['matches'];
			/* Προσθέτουμε στις params όσα αντιστιχοίθηκαν, εκτός των module, controller και action. */
			foreach( $matches as $key => $val ) {
				if( is_string( $key ) && $key != 'module' && $key != 'controller' && $key != 'action' ) {
					$this->params[$key] = $val;
				}
			}	
		}
		$this->params = array_merge( $this->params, array_diff_key( $route['matches'], array_flip( array( 'module', 'controller', 'action' ) ) ) );
		if( isset( $matches['module'] ) ) {
			$this->controller = Core_Strings::ucwords( substr( $matches['module'], 4 ) ) . '_Controller_' . Core_Strings::ucwords( ( isset( $matches['controller'] ) ) ? $matches['controller'] : Core_App::getConfig( 'library.core.request.defaultController' ) );
		} else {
			$this->controller = 'Controller_' . Core_Strings::ucwords( ( isset( $matches['controller'] ) ) ? $matches['controller'] : Core_App::getConfig( 'library.core.request.defaultController' ) );
		}
		$this->action = ( isset( $matches['action'] ) ) ? $matches['action'] : Core_App::getConfig( 'library.core.request.defaultAction' );
	}
	
	/* 
		Get request type { html, ajax, ... } 
		
		@return ( string )
	*/
	public function getType()
	{
		return $this->type;
	}
	
	/* 
	 * Βρίσκουμε την route στην οποία αντιστοιχεί το request url και αποθηκεύουμε αν υπάρχουν	parameters
	 * @return array( 'name', 'matches' )
	 */
	public function getRoute( $path )
	{
		$matches;
		foreach( Core_App::getConfig( 'library.core.request.routes' ) as $key => $route ) {
			if( preg_match( $route, $path, $matches ) ) {
				$name = $key;
				break;
			}
		}
		$route_params = array();
		foreach( $matches as $key => $val ) {
			if( is_string( $key ) )  {
				$route_params[$key] = $val;
			}
		}
		return $matches ? array( 'name' => $name, 'matches' => $route_params ) : null;
	}
	
	/* 
		Get το τμήμα του routing από το path του URL . 
		
		@return ( string )
	*/
	public function getUrlRoutePath()
	{
		if( $this->urlRoutePath ) {
			return $this->urlRoutePath;
		}
	
		/*
		 * Urldecode για την περίπτωση που είναι urlencoded και αφαίρεση των κενών που μπορεί να έχουν προστεθεί 
		 * κατά λάθος στο url από άλλα site που κάνουν link στο url αυτό προσθέτωντας δικά τους στοιχεία ( π.χ. facebook ). 
		 */
		$path = $this->urlRoutePath = preg_replace( '/\s+/', '', urldecode( parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH ) ) );

		/* Αφαιρούμε αν υπάρχει '/' στο τέλος. */
		if( strrchr( $path, "/" ) == "/" ) {
			$path = substr( $path, 0, -1 );
		}
		
		$parts = explode( '/', $path );
		
		/* Αφαιρούμε το 1o στοιχείο το οποίο αφήνει η PHP κενό. */
		array_shift( $parts );

		/* 
			Base directory.
			Αφαιρείται.			
		*/
		$base = $this->baseDir = Core_App::getConfig('baseDir');
		if( ! empty( $base ) && strpos( $path, '/' . $base ) === 0  ) {
            $exp = explode('/', $base);
            foreach( $exp as $e ) {
                array_shift( $parts );
            }
		}
		
		/* 
			Application name.
			Ελέγχει αν μετά το basedir υπάρχει κάποιο από τα ονόματα των applications,
			αν όχι τότε ορίζουμε ως $appName το 'defaultApplication'.			
			Αφαιρείται.			
		*/
		if( in_array( current( $parts ), array_keys( Core_App::getConfig( 'applications' ) ) ) ) {
			$this->appName = array_shift( $parts );
		} else {
			$this->appName = Core_App::getConfig( 'defaultApplication' );
		}
		
		/* 
			Pseudo document.
			Αποθηκεύεται και αφαιρείται.			
		*/
		$ext = $this->urlPseudoExt = Core_App::getConfig( 'library.core.request.pseudoExt' );
		$len = strlen( $ext );
		$last = end( $parts );
		if( ! empty( $ext ) && ! empty( $last ) ) {
			if( substr_compare( $last, $ext, $len*(-1), $len ) === 0 ) {
				$this->urlPseudoDoc = substr( end( $parts ), 0, $len*(-1) );
				array_pop( $parts );
			}
		}
		
		/*
		 * Url Path Mapping
		 *Αντικαθιστούνται τμήματα του routing με αντίστοιχα αν υπάρχουν στο urlPathMap.  
		 */
		if( $urlPathMap = Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.urlPathMap' ) ){
			foreach( $urlPathMap as $key => $map ){
				if( ( $pKey = array_search( $key, $parts ) ) !== false ){
					$parts[ $pKey ] = $map;
				}
			}
		}
		
		return $this->urlRoutePath = '/' . implode( '/', $parts );
	}
	
	/* 
		Get μία ή όλες τις parameter.
		Αν δεν δίνεται $name τις επιστρέφει όλες.
		
		@param $name ( string ) { parameter name }
		@param $search_array dot_seperated string { για asscociative array parameters }
		@return ( mixed )
	*/
	public function getParams( $name = null, $search_array = null )
	{
		if( $name ) {
			if( $search_array && isset( $this->params[ $name ] ) ) {
				return Core_Array::shorthand( $search_array, $this->params[ $name ] );
			} else {
				foreach( ( array ) $this->params as $key => $val ) {
					if( $key == $name ) {	
						return $val;
					}
				}
			}
			return null;
		} else {
			return $this->params;
		}
	}
    
	public function addParams(array $params) {
        $this->params = array_merge_recursive($this->params, $params);
    }
	
	/*
	 * Αφαιρεί Parameters από τις κατάλληλες μεταβλητές του Request
	 * @param $params mixed
	 */
	public function deleteParams( $params )
	{
		$this->params = array_diff_key( $this->params, array_flip( ( array ) $params ) );
		$exp = explode( '?', $this->URI );
		$this->URI = array_shift( $exp ) . ( isset( $exp[0] ) ? '?' . http_build_query( $this->params ) : '' );
	}
	
	/* 
		Δημιουργία relative URL.
		Δημιουργεί ένα URL αντικαθιστώντας τις variable στο route με αυτές που δίνονται στην $route_params
		και στη συνέχεια ελέγχει αυτό το URL αν είναι σωστό ως προς το $route. Έτσι είμαστε σίγουροι ότι
		οι $route_params αντιστοιχούν στο $route που δίνεται και ότι τα οptional στοιχεία του $route δεν θα δημιουργήσουν πρόβλημα.
		
		@param ( string ) { Route name. }
		@param ( mixed ) { Parameters που υπάρχουν μέσα στο string του route['write']. }
		@param ( mixed ) { URL query parameters. }
		@param ( array ) { SEOUri, langCode, ... }
		@return ( string )
	*/
	public function url( $route_name, $route_params = null, $params = null, $extra = null )
	{/**Να καλείται η getRoute() και να καλεί μέσα της το plugin*/
		/* Language plugin */
        if( Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.plugins.url_language' ) ||
            Core_App::getConfig( 'library.plugins.url_language')) {
			$plugin = new Plugin_Url_Language();
			$route = Core_App::getConfig( 'library.core.request.routes.' . $plugin->getRouteName( $route_name ) );
			$route_params = $plugin->getRouteParams( $route_params );
		} else {
			$route = Core_App::getConfig( 'library.core.request.routes.' . $route_name );
		}
		/* 
			Αντικατάσταση των variables μέσα στο $route με variables που κάναμε extract
			για να δημιουργήσουμε το νέο URL και να ελέγξουμε στην συνέχεια αν το έτοιμο νέο URL αντιστοιχεί με το route.
		*/
		$returnMatches = array();
		preg_replace_callback ( 
			'/<([a-z]+)>/', 
			function ( $matches ) use ( $route_params, &$returnMatches ) { 
				extract( ( array ) $route_params ); /* Array casting γιατί μπορεί να δίνονται ως Object */
				if( isset( ${$matches[1]} ) ) {
					$returnMatches[] = Core_URL::sanitize( ${$matches[1]}, array( 'filter' => 'url_path' ) );
				} 
			}, 
			$route 
		);
		
		$checkUrl = '/' . implode( '/', $returnMatches );
		/*
			Ελέγχουμε αν το έτοιμο νέο URL αντιστοιχεί με το route.
		*/
		if( ! preg_match( $route, $checkUrl, $matches ) ) {
			throw new Exception( 'library_core.request.no_route_matched_for_new_url:Core_Request:url()' );
		}
		/*
		 * Αν υπάρχει urlPathMap τότε αντιστοιχούνται τα τμήματα του routing.
		 */
		if( $urlPathMap = Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.urlPathMap' ) ){
			foreach( $urlPathMap as $mapKey => $map ){
				if( ( $rKey = array_search( $map, $returnMatches ) ) !== false ){
					$returnMatches[ $rKey ] = $mapKey;
				}
			}
		}
			
		$url = '/' . implode( '/', $returnMatches );
		
		/* Αν δίνεται το application */
		$application = null;
		if( isset( $extra['application'] ) ){
			$application = $extra['application'];
		} else {
			/* Αν το application δεν είναι το 'defaultApplication' πρέπει να υπάρχει στο URL. */
			if( $this->appName != Core_App::getConfig( 'defaultApplication' ) ){
				$application = $this->appName;	
			}
		}
		
		if( $application ){
			$url = '/' . $application . $url;
		}
		
		/* Προσθέτουμε το base directory. */
		$url = $this->baseDir . $url;

		/* Προσθέτουμε το '/' μπροστά αν δεν υπάρχει για να λειτουργήσει ως relative URL. */
		$url = ( ( $url[0] != '/' ) ? '/' : '' ) . $url;
		
		/* Αν δίνεται το pseudoDoc προσθέτουμε μαζί και το pseudoExt. */
		if( isset( $extra['pseudoDoc'] ) ) {
			$url .= '/' . Core_URL::sanitize( $extra['pseudoDoc'], array( 'filter' => 'url_path' ) ) . $this->urlPseudoExt;
		}
		
		/* 
		 * Query parameters. Urldecode αντικαθιστά τα + με space
		 */
		$query = ( $p = $params ) ? '?' . http_build_query( ( array ) $p ) : '';
	
		return $url . $query;
	}
	
	public function urlFromPath( $url_route_path, $params = null, $extra = null )
	{
		if( strpos( $url_route_path, '/' ) !== 0 ) {
			$url_route_path = '/' . $url_route_path;
		}
		if( ! ( $route = $this->getRoute( $url_route_path ) ) ) { /* Αν δεν βρεθεί route επιστρέφει Exception */
			throw new Exception( 'library_core.request.no_route_matched_for_send_url:Core_Request:urlFromPath()' );
		}
		return $this->url( $route['name'], $route['matches'], $params, $extra );
	}
	
	/*
		@return ( string )
	*/
	public function getController()
	{
		return strtolower( substr( $this->controller, strpos( $this->controller, '_' )+1 ) );
	}
	
	/*
		@return ( string )
	*/
	public function getAction()
	{
		return $this->action;
	}
	
	/*
		@return ( string )
	*/
	public function getURI()
	{
		return empty( $this->URI ) ? $_SERVER['REQUEST_URI'] : $this->URI;
	}
	
	/*
	 *  Αφαιρεί το pseudodocument
		@return ( string )
	*/
	public function getCleanURI()
	{
		$uri = $this->getURI();
		
		/* 
			Pseudo document.
			Αποθηκεύεται και αφαιρείται.			
		*/
		$ext = $this->urlPseudoExt = Core_App::getConfig( 'library.core.request.pseudoExt' );
		if( $ext && strrpos( $uri, $ext ) == strlen( $uri )-strlen( $ext ) ) {
			$uri = substr( $uri, 0, strrpos( $uri, '/' ) );
		}
		
		return $uri;
	}
    
    public function getMethod() 
    {
        return $this->method;
    }
	
	/* 
		Magic method get.
		Επιστρέφει το property.
		
		@param  ( property name )
		@return  ( class property )
	*/
	public function __get( $name )
	{
		return $this->$name;
	}
}
?>