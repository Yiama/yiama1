<?php
class Core_Cache
{
	private $path;
	/*
	 * Αποθηκεύει τις cache Που έχουν ζητηθεί, στην μνήμη για να μην ξανανοίγονται τα αρχεία
	 * @param array
	 */
	private static $memory;
	private static $enabled;
	
	/*
	 * $param $options { array( 'path' => .., .... }
	 */
	public function __construct( $options = null )
	{
		$path = PATH_ROOT;
		$this->path = isset( $options['path'] ) ? $path . $options['path'] . DS : $path . 'cache' . DS;
		if ( ! is_dir( $this->path ) ) {
			mkdir( $this->path, 0777, true );
		}
		self::$enabled = Core_App::getConfig( 'library.core.cache.enabled' );
	}
	
	/*
	 * Prefix delimiter '.'
	 * 
	 * Prefix π.χ. 
	 * 	<prefixKey>.<samestring>[.<samestring>]...
	 * 	<samestring>[.<samestring>].<prefixKey>.<samestring>[.<samestring>]...
	 */
	private function generateFile( $key )
	{
		$parts = explode( '.', $key );
		$md5Parts = array();
		foreach( $parts as $p ) {
			$md5Parts[] = md5( $p );
		}
		
		return $this->path . implode( '.', $md5Parts );
	}
	
	/*
		Set cache key ( file name ).
		
		@param $key ( int/string )
		@param $value ( mixed )
		@param $in_memory ( boolean )
	*/
	public function set( $key, $value, $in_memory = false )
	{
		if( ! self::$enabled ) {
			return;
		}
		$file = $this->generateFile( $key );
		$content = serialize( $value );
		file_put_contents( $file, $content );
		if( $in_memory ) {
			self::$memory[ $key ] = $value;
		}
	}
	
	/*
		Get cache content.
		
		@param $key ( int/string )
		@param $from_memory ( boolean )
	*/
	public function get( $key, $from_memory = false )
	{
		if( ! self::$enabled ) {
			return;
		}
		if( $from_memory && ! empty( self::$memory[ $key ] ) ) {
			return self::$memory[ $key ];
		}
		
		$file = $this->generateFile( $key );
		if( file_exists( $file ) ) {
			$content = unserialize( file_get_contents( $file ) );
			if( $from_memory && empty( self::$memory[ $key ] ) ) {
				self::$memory[ $key ] = $content;
			}
			return $content;
		}
		
		return false;
	}
	
	/*
		Delete cache file.
		
		@param $key ( int/string )
	*/
	public function delete( $key )
	{
		$file = $this->generateFile( $key );
		@unlink( $file );
	}
	
	/*
		Delete cache files που ξεκινάνε με αυτό το Prefix.
		
		@param $keyPrefix ( int/string )
	*/
	public function deletePrefixed( $keyPrefix )
	{
		$objects = scandir( $this->path ); 
		foreach( $objects as $o ) { 
			if( $o != "." 
			&& $o != ".." 
			&& filetype( $this->path . $o ) == "file" 
			&& ( strpos( $o, md5( $keyPrefix ) . '.' ) === 0 
				|| strpos( $o, '.' . md5( $keyPrefix ) . '.' ) !== false ) ) {
				@unlink( $this->path . DS . $o );
			}
		} 
			
		reset( $objects ); return;
	}
	
	/*
		Delete όλη την cache μέσα στο folder και στα subfolder του path και των applications.
	*/
	public function flush()
	{
		$this->removedir( $this->path );
		$apps = Core_App::getConfig( 'applications' );
		foreach( $apps as $name => $val ) {
			$this->removedir( PATH_ROOT . 'applications' . DS . $name . DS . 'cache' );
		}
	}
	
	private function removedir( $dir ) 
	{ 
		$objects = scandir( $dir ); 
		foreach( $objects as $o ) { 
			if( $o != "." && $o != ".." ) { 
				if( filetype( $dir . DS . $o ) == "dir" ) {
					$this->removedir( $dir . DS . $o ); 
				}
				else {
					@unlink( $dir . DS . $o ); 
				}
			} 
		} 
		
		reset( $objects ); 
	} 
}
?>