<?php

/*
	Επιλέγει PDO και μεταφέρει τις ενέργειες στο PDO.
	Αυτή η class υπάρχει ώστε αντί να λέμε $mysqli->openCon() ή $mysql->openCon() κ.λ.π., να λέμε $db->openCon().
*/
class Core_DB
{
	/*
		PDO object { Mysqli, ... }
	*/
	private $PDO;
	
	/*
		Database object instances.
	*/
	private static $instances = array();
	
	/*
		Database name.
	*/
	private $dbName;
	
	/*
		Get instance.

		@param $dbName { config.php database name }
	*/
	public static function getInstance( $dbName ) 
	{
        if( isset( self::$instances[$dbName] ) ) 
		{
			return self::$instances[$dbName];
		}
		
		return self::$instances[$dbName] = new Core_DB( $dbName );
	}
	
	/*
		Ορίζει το database name, το PDO και κάνει την σύνδεση.
		
		@param $dbName { config.php database name }
	*/
	private function __construct( $dbName ) 
	{
		$this->dbName = $dbName;
		
		$PDO = 'DB_' . ucfirst( Core_App::getConfig( 'databases.' . $dbName . '.pdo' ) );
		$this->PDO = new $PDO();
		
		$this->openCon( $dbName );
	}
	
	/*
		Magic method call.
		Καλεί την αντίστοιχη method του PDO object περνώντας του τα arguments και επιστρέφει το αποτέλεσμα.
	*/
	public function __call( $action, $args )
	{
		return call_user_func_array( array( $this->PDO, $action ), $args );
	}
}

?>