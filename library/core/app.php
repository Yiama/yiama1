<?php 

/*
	Application Core.
	Καθορίζει πράγματα σχετικά με τις class που βρίσκονται μέσα στο library/core.
	Είναι singleton class και καλείται μέσω της μεθόδου instance().
*/

class Core_App
{
	/* 
		Configuration array.
	
		@var ( array )
	*/
	private static $config;
	
	/* 
		Error messages array.
	
		@var ( array )
	*/
	private static $errorMsgs;
	
	/* 
		Current application name.
	
		@var ( string )
	*/
	private static $appName;
	
	/* 
		Application scope variables.
	
		@var ( array )
	*/
	private static $vars;
	
	/* 
		Get configuration.
	
		@param $field ( string ) { config array field }
		@return ( mixed ) { config array field }
	*/
	public static function getConfig( $field = null)
	{
            if ($field == null) {
                return self::$config;
            }
            return Core_Array::shorthand( $field, self::$config );
	}	
	
	/* 
		Get configuration current application name.
		Αν το $appName δεν είναι αποθηκευμένο, το παίρνει από την Core_Request.
	
		@return ( string )
	*/
	public static function getAppName()
	{
		if( $n = self::$appName ) return $n;
		
		$request = Core_Request::getInstance();
		$request->getUrlRoutePath();
		return self::$appName = $request->appName;
	}	
	
	/* 
		Get error message.
	
		@param $field ( string ) { error.ini array field }
		@return ( mixed ) { error.ini array field }
	*/
	public static function getErrorMsg( $field )
	{
		return Core_Array::shorthand( strtoupper( $field ), self::$errorMsgs );
	}
	
	/*	
		Initialize 
		
		@param $config ( array ) { Configuration }		
		@param $envConfig ( array ) { Configuration by environment }
	*/
	public static function init( $config, $envConfig )
	{        
		/* Set configuration array */
		self::$config = array_merge($config, $envConfig);
                
		/* Set error messages array */
		self::$errorMsgs = parse_ini_file( PATH_ROOT . 'config' . DS . 'errors.ini', true );
		
		/* 	Register error handlers */
		
		/*	
			Register shutdown handler. 
			Επειδή η PHP δεν στέλνει στην error_handler τα Fatal Error χρησιμοποιούμε
			αυτή την μέθοδο η οποία καλείται στο τέλος της εκτέλεσης της PHP και εκεί 
			μπορούμε να ελέγξουμε αν συνέβει κάποιο fatal error.
			Manual: http://www.php.net/manual/en/function.register-shutdown-function.php 
		*/
		register_shutdown_function(  array( 'Core_Error', 'fatal_error_handler' ) );
		
		/*	
			Register error handler και set errors types που θα επιστρέφονται μόνο. 
			Manual: http://www.php.net/manual/en/function.set-error-handler.php
		*/
		$error_mode = self::getConfig( 'library.core.exception.mode' );

		/* 	
			$error_types και error_reporting() πρέπει να είναι ίδια. 
			Τα error types πρέπει να οριστούν στην set_error_handler() ως 2η parameter γιατί δυστυχώς 
			η error_reporting() δεν επιρρεάζει την callback function της set_error_handler() και αλλιώς 
			η callback function θα καλείται για όλα τα errors. Την error_reporting() την γεμίζουμε
			για να ξέρει η Core_Exception::error_handler() πια errors θέλουμε να εμφανίζονται. 
		*/
		if( $error_mode['development']['on'] )
		{
			$error_types =   $error_mode['development']['errors'];
			error_reporting( $error_types );
		}
		elseif ( $error_mode['production']['on'] )
		{
			$error_types =   $error_mode['production']['errors'];  
			error_reporting( $error_types ); 
		}
		
		set_error_handler( array( 'Core_Error', 'error_handler' ), $error_types );
		
		/*	
			Register exception handler
			Manual: http://www.php.net/manual/en/function.set-error-handler.php 
		*/
		set_exception_handler( array( 'Core_Error', 'exception_handler' ) );
		
		/* Autoload */
		Core_Autoload::addPaths( PATH_APPS . Core_App::getAppName() . DS );
		/* Load external autoloaders */
		if( $loaders = Core_App::getConfig( 'library.core.autoload.loaders' ) ) {
			Core_Autoload::external_autoloaders( $loaders );
		}
        
        /* Enable PHP session */
        $currentAppSessionEnabledKey = 'applications.' . Core_App::getAppName() . '.session.enabled';
        $sessionEnabled = self::getConfig($currentAppSessionEnabledKey);
        if (!empty($sessionEnabled)) {
            Core_Session::getInstance();
        }
	}
	
	/*
	 * Ορίζει variables για να μπορουν να χρησιμοποιηθούν οπουδήποτε μέσα στο application
	 * @param $vars array assoc
	 */
	public static function addVars( $vars )
	{
		self::$vars = array_merge( ( array ) self::$vars, $vars );
	}
	
	/*
	 * @param $var_name string
	 */
	public static function getVars( $var_name = null )
	{
		if( $var_name ) {
			return self::$vars[ $var_name ];
		}
		return self::$vars;
	}
}

?>