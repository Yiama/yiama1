<?php
class Core_ModuleConfig{
    
    /**
     * @var string
     */
    private $last_id = 0;
    
    /**
     * @var array Core_ModuleAdminInstance
     */
    private $instances = array();

    /**
     * DEPRECATED
     *
     * @var string
     */
    private $config_file;

    /**
     * DEPRECATED
     *
     * @param string $config_file
     */
//    public function __construct($config_file) {
//        $this->config_file = $config_file;
//        if (!file_exists($config_file)) {
//            @file_put_contents($config_file, '');
//        }
//    }
    
    /**
     * @param Core_ModuleInstance $instance
     */
    public function addInstance(Core_ModuleInstance $instance) {
        $this->instances[] = $instance;
    }
    
    /**
     * @return array
     */
    public function getInstances() {
        return $this->instances;
    }
    
    public function getLastId() {
        return $this->last_id;
    }
    
    public function setLastId($last_id) {
        $this->last_id = $last_id;
    }
    
    /**
     * DEPRECATED
     *
     * @param mixed $data
     * @return integer|boolean
     */
    public function write($data) {
        return file_put_contents($this->config_file, json_encode($data));
    }
    
    /**
     * @return stdClass
     */
    public function toObject() {
        $obj = new stdClass();
        $obj->last_id = $this->last_id;
        $obj->instances = array();
        foreach ($this->instances as $inst) {
            $obj->instances[] = $inst->toObject();
        }
        return $obj;
    }

    public static function fromString($config_string) {
        $new = new self();
        $object = json_decode($config_string);
        if (!empty($object)) {
            $last_id = !empty($object->last_id) || intval($object->last_id) == 0
                ? $object->last_id
                : 0;
            $new->setLastId($last_id);
            if (!empty($object->instances)) {
                foreach ($object->instances as $inst) {
                    $instance = new Core_ModuleInstance($inst->id);
                    if (!empty($inst->options)) {
                        $instance->setOptions($inst->options);
                    }
                    if (!empty($inst->params)) {
                        $instance->setParams($inst->params);
                    }
                    $new->addInstance($instance);
                }
            }
        }
        return $new;
    }

    /**
     * DEPRECATED
     */
    public static function fromFile($config_file) {
        $new = new self($config_file);
        $object = json_decode(file_get_contents($config_file));
        if (!empty($object)) {
            $last_id = !empty($object->last_id) || intval($object->last_id) == 0
                ? $object->last_id
                : 0;
                $new->setLastId($last_id);
            if (!empty($object->instances)) {
                foreach ($object->instances as $inst) {
                    $instance = new Core_ModuleInstance($inst->id);
                    if (!empty($inst->options)) {
                        $instance->setOptions($inst->options);
                    }
                    if (!empty($inst->params)) {
                        $instance->setParams($inst->params);
                    }
                    $new->addInstance($instance);
                }
            }
        }
        return $new;
    }
}