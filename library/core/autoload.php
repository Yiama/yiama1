<?php 

/*
 * Autoload classes.
 * Δεν χρειάζεται να κάνουμε include τις κλάσεις στα directory που θα ορίσουμε.
 */
class Core_Autoload
{
	/* 
		Paths for default autoloader to search.
		@var ( array )
	*/
	private static $autoloadPaths = array();
	
	/*
	 * Στους external autoloaders πρέπει να υπάρχει ο παρακάτω κώδικας:
	 * 	spl_autoload_register( function ( $class ) { <external_code> } );
	 */
	public static function init( $paths = null )
	{
		/* Load default autoloader */
		self::addPaths( $paths );
		spl_autoload_register( array( 'Core_Autoload', 'default_autoloader' ) );
	}

	public static function external_autoloaders( $loaders )
	{ 
		foreach( ( array ) $loaders as $l ) {
			if( file_exists( $l['path'] ) ) {
				require_once( $l['path'] );
			}
		}
	}
	
	public static function addPaths( $paths )
	{
		self::$autoloadPaths = array_merge( self::$autoloadPaths, ( array ) $paths );
	}
	
	/*
		Συμβάσεις: 
		_Η ονομασία της κλάσης θα ξεκινάει με το όνομα κάποιου directory από αυτά που υπάρχουν στην config.php -> array['library']['core']['autoload']['path'.
		_Θα ακολουθεί η ονομασία κάθε υποφακέλου χωρίζοντάς τες με '_'.
		_Στο τέλος θα μπαίνει κάποιο όνομα.
		_Επειδή το autoload γίνεται βάσει του όνοματος και μόνο, δύο class ΔΕΝ μπορούν να έχουν το ίδιο όνομα.
		Παραδείγματα:
			Έστω ότι καλούμε την class του controller για την applicaiton Eshop
			class: <Eshop :application_name>_Controller_Products
			dir: PATH_ROOT . 'applications' . DS . '<application_name>' . DS . 'frontend' . DS . 'controller' . DS . 'products' . EXT
			
			Έστω ότι καλούμε την class του controller για το module Header
			class: <Header :module_name>_Controller_Default
			dir: PATH_ROOT . 'modules' . DS . '<module_name>' . DS . 'controller' . DS . 'default' . EXT
	*/
	public static function default_autoloader( $className )
	{
		$parts = explode( '_', strtolower( $className ) );
		
		foreach( self::$autoloadPaths as $p )
		{
			if( file_exists( $p . implode( DS, $parts ) . EXT ) ) {			
				require_once $p . implode( DS, $parts ) . EXT;
				return;
			}
		}
	}
}

?>