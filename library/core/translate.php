<?php
/**
 * * * * * * * *
 * DEPRECATED  *
 * * * * * * * *
 */
class Core_Translate
{
    /**
     * @var array
     */
    private $translation;

    /**
     * @param array $translation
     */
    public function setTranslation(array $translation = array()) {
        $this->translation = $translation;
    }
    
    /**
     * @param string $key
     * @return string
     */
    public function _($key) {
        if (isset($this->translation[$key])) {
            return $this->translation[$key];
        }
        return $key;
    }
    
    /*
	 * @param $langcode ( string )
	 * @param $path ( string ) { Dot seperated file path }
	 * @param $index ( string ) { Dot seperated index path }
	 */
	public static function get( $langcode, $path, $index = null )
	{
		$file = PATH_ROOT . 'public' . DS . 'assets' . DS . 'languages' . DS 
            . $langcode . DS . str_replace( '.', DS, $path ) . '.php';
		
		if( file_exists( $file ) ) 
		{
			$trans = include $file;
			
			if( $index ) {
				return Core_Array::shorthand( $index, $trans );
			}
			else{
				return $trans;
			}
		}
		
		return false;
	}
}
?>