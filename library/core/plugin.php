<?php
	abstract class Core_Plugin
	{
		protected $plugin_name;
		
		public function __construct( $name )
		{
			$this->plugin_name = $name;
		}
		
		public function __destruct(){}
		
		/*
		 * Ελέγχει αν είναι ενεργοποιημένο για το τρέχον application 
		 */
		public function isEnabled()
		{
			return Core_App::getConfig( "applications." . Core_App::getAppName() . ".plugins." . $this->plugin_name );
		}
	}
?>