<?php
	class HTML_Tag
	{
		/*
			Open - close tag.
			Είναι Tag που κλείνονται ( </tag_name> ).
			@param $tag ( string )
			@param $attributes ( array )
		*/
		public static function open( $tag, $attributes = null )
		{
			$s = "<{$tag} ";
			if( isset( $attributes ) ) {
				foreach( $attributes  as $key => $val ) {
					$s .= " {$key}=\"" . trim( $val, "'\"" ) . "\"";
				}
			}
			$s .= ">";
			return $s;
		}
		
		/*
		 * @param $tag
		 */
		public static function close( $tag )
		{
			return "</{$tag}>";
		}
		
		/*
			Closed tag.
			Είναι Tag τύπου( <tag_name></tag_name> ).
			@param $tag ( string )
			@param $attributes ( array )
			@param $text ( string )
		*/
		public static function closed( $tag, $attributes = null, $text = null )
		{
			$s = "<{$tag} ";
			if( isset( $attributes ) ) {
				foreach( $attributes as $key => $val ) {
					$s .= " {$key}=\"" . trim( $val, "'\"" ) . "\"";
				}
			}
			$s .= ">";
			if( isset( $text ) ) {
				$s .= $text;
			}	
			$s .= "</{$tag}>";
			return $s;
		}
		
		/*
			Void tag.
			Είναι Tag τύπου ( <tag_name> ).
			@param $tag ( string )
			@param $attributes ( array )
		*/
		public static function void( $tag, $attributes = null )
		{
			$s = "<{$tag} ";
			if( isset( $attributes ) ) {
				foreach( $attributes  as $key => $val ) {
					$s .= " {$key}=\"" . trim( $val, "'\"" ) . "\"";
				}
			}
			$s .= ">";
			return $s;
		}
	}
?>