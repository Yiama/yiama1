<?php
	class HTML_Form_Tag
	{
		private static $n; //new line
		
		/*
			@param $array { 
				array( 
					'attributes' => array( 
						'action' => ..., 
						... 
						)
					'newLine' => ...
				) 
			}
		*/
		public static function start( $array )
		{
			$n = self::$n = ( isset( $array['newLine'] ) ) ? "{$array['newLine']}" : '';
			$s = "";
			$s .= "<form ";
			$s .= urldecode( http_build_query( $array['attributes'], '', ' ' ) );
			$s .= ">{$n}";
			
			return $s;
		}
		
		public static function end()
		{
			$n = self::$n;
			return "</form>{$n}";
		}
		
		/*
			@param $array { 
				array( 
					'attributes' => array( 
						... 
						)
					'required' => ...,
					'info' => array( 
						'attributes' => array(  
						... 
						) 
					... 
					),
					'text' => ...,
					'instructions' => ...
				) 
			}
		*/
		public static function label( $array )
		{
			$n = self::$n;
			$s = "";
			$s .= "<label ";
			if( isset( $array['attributes'] ) )
			{
				$s .= urldecode( http_build_query( $array['attributes'], '', ' ' ) );
			}
			$s .= ">";
			if( isset( $array['info'] ) )
			{
				$s .= "{$n}<span class='info' " . urldecode( http_build_query( $array['info']['attributes'], '', ' ' ) ) . "></span>{$n}";
			}
			if( isset( $array['text'] ) )
			{
				$s .= "{$array['text']}";
			}
			if( ! empty( $array['required'] ) )
			{
				$s .= "<b> *</b>";
			}
			if( isset( $array['instructions'] ) )
			{
				$s .= "{$n}<span class='instructions'>{$array['instructions']}</span>{$n}";
			}
			$s .= "</label>{$n}";
			
			return $s;
		}
		
		/*
			@param $array{ 
				array( 
					'type' => ..., 
					... 
				)
			}
		*/		
		public static function input( $array )
		{
			$n = self::$n;
			return "<input " . urldecode( http_build_query( $array, '', ' ' ) ) . " />{$n}";
		}
		
		/*
			@param $array{ 
				array( 
					'attributes' => array( 
						...
						), 
					'text' => ...
				) 
			}
		*/		
		public static function textarea( $array )
		{
			$n = self::$n;
			$s = "<textarea ";
			if( isset( $array['attributes'] ) )
			{
				$s .= urldecode( http_build_query( $array['attributes'], '', ' ' ) );
			}
			$s .= ">";
			if( isset( $array['text'] ) )
			{
				$s .= $array['text'];
			}
			$s .= "</textarea>{$n}";
			
			return $s;
		}
		
		/*
			@param $array { 
				array( 
					'attributes' => array( 
						'name' => ..., 
						... 
						), 
					'options => array( 
						'attributes' => array( 
							'value' => ...,
							...
							),
						'text' => ... 
						), 
						... 
					)
				) 
			}
		*/
		public static function select( $array )
		{
			$n = ( isset( $array['newLine'] ) ) ? "{$array['newLine']}" : '';
			$s = "";
			$s .= "<select " . ( ( isset( $array['attributes'] ) ) ? urldecode( http_build_query( $array['attributes'], '', ' ' ) ) : "" ) . ">{$n}";
			if( isset( $array['options'] ) )
			{
				foreach( $array['options'] as $op )
				{
					$s .= "  <option ";
					if( isset( $op['attributes'] ) )
					{
						$s .= urldecode( http_build_query( $op['attributes'], '', ' ' ) );
					}
					$s .= ">" . ( isset( $op['text'] ) ? $op['text'] : '' ) . "</option>{$n}";
				}
			}
			$s .= "</select>{$n}";
			
			return $s;
		}
		
		/*
			@param $array { 
				array( 
					'attributes' => array( 
						...
						), 
					...
				) 
			}
		*/		
		public static function message( $array = null )
		{
			$n = self::$n;
			$s = "<div ";
			if( isset( $array['attributes'] ) )
			{
				$s .= urldecode( http_build_query( $array['attributes'], '', ' ' ) );
			}
			if( ! isset( $array['attributes']['id'] ) )
			{
				$s .= " id='formMsg'";
			}
			$s .= " class='msg rc1'></div>{$n}";
			
			return $s;
		}
		
		/*
			@param $array { 
				array( 
					'attributes' => array( 
						...
						), 
					...
				) 
			}
		*/		
		public static function button( $array )
		{
			$n = self::$n;
			return "<button " . urldecode( http_build_query( $array['attributes'], '', ' ' ) ) . ">" . $array['text'] . "</button>{$n}";
		}
	}
?>