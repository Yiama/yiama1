<?php
	class HTML_Form_Image
	{
		/*
			Εμφανίζει button για upload.
		
			@use { Js validExtensions(), _get(), _show() }
			@param $max ( int ) { Max buttons number. }
			@param $extensions ( string ) { Valid file extensions για το javascript ( comma seperated ). }
		*/
		public static function button( $max, $extensions )
		{
			$s = "";
			$i = 0;
			while( $i++ < $max )
			{
				$s .= "<div id='imgbuttonwrapper" . $i . "' class='imgbutton" . ( ( $i == 1 ) ? " first" : "" ) . " rc1'>\n";
				$s .= "  <span class='text'>Εικόνα</span>\n";
				$s .= "  <input type='file' name='images[]' 
					onchange=\"
						if( ! validExtensions( this, '{$extensions}' ) )
						{
							return;
						}
						else
						{
							this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path show' );
							this.parentNode.getElementsByTagName( 'input' )[1].value = this.value.substr( this.value.lastIndexOf( '\\\' )+1).toLowerCase();
							this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove show' );
						}
						var nextButton = document.getElementById( 'imgbuttonwrapper" . ( $i+1 ) . "' );
						if( nextButton ) nextButton.style.display = 'block';
						\" 
					class='button'/>\n";
				$s .= "  <input type='text' disabled='disabled' class='path'/>\n";
				$s .= "  <span class='remove'
					onclick=\"
						this.parentNode.getElementsByTagName( 'input' )[0].value = '';
						this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path' );
						this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove' );
						\"></span>\n";
				$s .= "</div>\n";
			}
			
			Core_HTML::addFiles( array( 
				'form\images.css',
				'library\misc.js',
				'library\form.js'
			) );
			
			return $s;
		}
		
	/*
			Εμφανίζει button για upload.
		
			@use { Js validExtensions(), _get(), _show() }
			@param $max ( int ) { Max buttons number. }
			@param $extensions ( string ) { Valid file extensions για το javascript ( comma seperated ). }
		*/
		public static function button( $max, $extensions )
		{
			$s = "";
			$i = 0;
			while( $i++ < $max )
			{
				$s .= "<div id='imgbuttonwrapper" . $i . "' class='imgbutton" . ( ( $i == 1 ) ? " first" : "" ) . " rc1'>\n";
				$s .= "  <span class='text'>Εικόνα</span>\n";
				$s .= "  <input type='file' name='images[]' 
					onchange=\"
						if( ! validExtensions( this, '{$extensions}' ) )
						{
							return;
						}
						else
						{
							this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path show' );
							this.parentNode.getElementsByTagName( 'input' )[1].value = this.value.substr( this.value.lastIndexOf( '\\\' )+1).toLowerCase();
							this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove show' );
						}
						var nextButton = document.getElementById( 'imgbuttonwrapper" . ( $i+1 ) . "' );
						if( nextButton ) nextButton.style.display = 'block';
						\" 
					class='button'/>\n";
				$s .= "  <input type='text' disabled='disabled' class='path'/>\n";
				$s .= "  <span class='remove'
					onclick=\"
						this.parentNode.getElementsByTagName( 'input' )[0].value = '';
						this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path' );
						this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove' );
						\"></span>\n";
				$s .= "</div>\n";
			}
			
			Core_HTML::addFiles( array( 
				'form\images.css',
				'library\misc.js',
				'library\form.js'
			) );
			
			return $s;
		}
		
		/*
			Εμφανίζει τα images.
			
			@param $images ( array )
			@param $dir ( string ) { Image directory. }
		*/
		public static function picture( $images, $dir )
		{
			$s = "";
			foreach( $images as $i )
			{
				$s .= "<div class='imgpicture rc1'>\n";
				$s .= "  <input type='hidden' name='removeimages[]'/>\n";
				$s .= "  <img src='{$dir}{$i}' />\n";
				$s .= "  <span class='text'
					onclick=\"
						if( this.parentNode.getElementsByTagName( 'input' )[0].value == '' )
						{
							this.parentNode.style.backgroundColor = '#ffdddd';
							this.innerHTML = 'Ακύρωση';
							this.parentNode.getElementsByTagName( 'input' )[0].value = '{$i}';
							this.parentNode.getElementsByTagName( 'img' )[0].setAttribute( 'class', 'opac40p' );
						}
						else
						{
							this.parentNode.style.backgroundColor = '#f5f5f5';
							this.innerHTML = 'Διαγραφή';
							this.parentNode.getElementsByTagName( 'input' )[0].value = '';
							this.parentNode.getElementsByTagName( 'img' )[0].removeAttribute( 'class' );
						}
						\">Διαγραφή</span>\n";
				$s .= "</div>\n";
			}
			
			Core_HTML::addFiles( array( 
				'form\images.css',
				'library\misc.js',
				'library\form.js'
			) );
			
			return $s;
		}
	}
?>