<?php
	class HTML_Form_Map
	{
		/*
		 *  @param $options ( array ) {
		 *  	array(
		 *  		'id',
		 *  		'start' => array(
		 *  			'latitude' => ...,
		 *  			'longitude' => ...,
		 *  			'zoom'
		 *  		),
		 *  		'markers' => array( //Χρήση στην Index
		 *  			'latitude' => ...,
		 *  			'longitude' => ...,
		 *  			'text' => '...'
		 *  		),
		 *  		'marker_repositionable' => boolean //Χρήση στην Insertform
		 *  	)
		 *  }
		 */
		public static function initialize( $options )
		{
			/* Include google maps API */
			Core_HTML::addHeader( "<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDvJzUhwrE1U33QMBJRr5jlmLwTfV5jkgI'></script>" );
			
			$script = "
			<script type='text/javascript'>
			function initialize_GM() 
			{
				
				var latlng = new google.maps.LatLng( {$options['start']['latitude']},{$options['start']['longitude']} );
			
				var mapOptions = {
					center: latlng,
					zoom: " . ( isset( $options['start']['zoom'] ) ? $options['start']['zoom'] : 8 ) . "
				};
				
				map = new google.maps.Map( document.getElementById( '{$options['id']}' ), mapOptions );
			";
				
				if( isset( $options['markers'] ) )
				{
					foreach( $options['markers'] as $m )
					{
						$script .= "
							var latlng = new google.maps.LatLng( {$m['latitude']},{$m['longitude']} );
							marker_{$m['id']} = new google.maps.Marker( {
								position: latlng,
								map: map,
								title: '{$m['title']}'
							} );
							var infowindow_{$m['id']} = new google.maps.InfoWindow( {
     							 content: '<div style=\'font-size: 12pt\'>"
									. str_replace( array( PHP_EOL, '"' ), array( "", "\\'" ) , $m['text'] )
									. "</div><a href=\'" . $m['deleteLink'] . "\' class=\'marker_delete\'>Διαγραφή</a>'
  							} );
							google.maps.event.addListener( marker_{$m['id']}, 'click', function() {
    							infowindow_{$m['id']}.open( map, marker_{$m['id']} );
  							} );
						";
					}
				}
				
				if( isset( $options['marker_repositionable'] ) && $options['marker_repositionable'] )
				{
					$script .= "
						var markerRepositionable = new google.maps.Marker( {
							position: latlng,
							map: map,
							title: 'default'
						} );
						google.maps.event.addListener( map, 'click', function( event ) {
							markerRepositionable.setPosition( event.latLng );
							document.getElementById( '{$options['inputLat_id']}' ).value = event.latLng.lat();
			            	document.getElementById( '{$options['inputLng_id']}' ).value = event.latLng.lng();
						} );
					";
				}
				
			$script .= "
			}
			
			google.maps.event.addDomListener( window, 'load', initialize_GM ); 
			</script>";
			
			return $script;
		}
		
		/*
		 * @param $attributes ( array )
		 */
		public static function map( $attributes )
		{
			Core_HTML::addFiles( 'admin\maps.css' );
			
			return "<div id='{$attributes['id']}' class='map'></div>";
			
		}
	}
?>