<?php
	class HTML_Admin_Table
	{
		private static $n = "\n"; //new line
		private static $table; 
		private static $result = '';
		
		/* 
			@param $table ( array ){
				'bulkActions' => array( 'publishLink', 'unpublishLink', ... ),
				'actions' =>  ... ( boolean ),
				'localActions' => ... ( boolean ),
				'fieldHeaders' => array( 'Τίτλος', ... ),
				'rows' => array(
					array(                      //row 1
						'fieldvalues' => array( 'field_value_1', 'field_value_2' ),
						'actions' => array(
							'updateLink' => ...,
							'deleteLink' => ...,
							'mediaLink' => ...,
							'mediaCount' => ...
						),
						'checkboxes' => array(
							array(                 //checkbox 1
								'
							),
							array(                 //checkbox 2
							
							)
						)
					),
					array(                      //row2
						...
					)
				)
			}
		*/
		public static function create( $table )
		{
			self::$table = $table;
			
			$n = self::$n;
			
			self::$result .= "<div class='tbCorner rc1Top'></div>{$n}";
			self::$result .= "  <table>{$n}";
			self::$result .= self::headers();
			if( isset( $table['rows'] ) ) 
			{
				self::$result .="      <tr>{$n}";
				self::$result .= self::rows();
				self::$result .="      </tr>{$n}";
			}
			self::$result .= "  </table>{$n}";
			self::$result .= "<div class='tbCorner rc1Bottom'></div>{$n}";
			
			Core_HTML::addFiles( array(
				'admin\table.css',
				'library\basic.js',
				'library\popups.js'
			) );
			
			return self::$result;
		}
		
		public static function headers()
		{
			$n = self::$n;

			self::$result .= "    <tr class='header'>{$n}";
			if( isset( self::$table['bulkActions'] ) )
			{
				self::$result .="<script type='text/javascript'>
						_addOnLoad( \"if( _get( 'yiamamsg' ) ) setTimeout( function(){ _hide( _get( 'yiamamsg' ) ) }, 2000 );\" );
						function checkAll( elClicked ){ var inputs = document.getElementsByName( 'checkboxid' ); for( var i = 0; i < inputs.length; i++ ) if( elClicked.checked == true ) inputs[i].checked = true; else inputs[i].checked = false; }
						function selectChecked(){ var ids = new Array(); var inputs = document.getElementsByName( 'checkboxid' ); for( var i = 0; i < inputs.length; i++ ){ if( inputs[i].checked == true ){ ids.push( inputs[i].getAttribute( 'id' ) ); } } return ids.join(); }
					</script>
				";
				self::$result .="      <th class='ids'>{$n}";
				self::$result .="        <input type='checkbox' style='float: left' onclick=\"checkAll( this );\" />{$n}";
				self::$result .="        <a class='actions' onclick=\"var el = _get( 'actionsdiv' ); if( el.style.display == 'block' ) _hide( el ); else _show( el );\"></a>{$n}";
				self::$result .="        <div id='actionsdiv'><ul>{$n}";
				if( isset( self::$table['bulkActions']['publishLink'] ) && ( $link = self::$table['bulkActions']['publishLink'] ) ) {
					self::$result .="          <li><a onclick=\"_request( '{$link}" . ( ( strpos( $link, '?' ) === false ) ? '?' : '&' ) . "ids=' + selectChecked() );\">Δημοσίευση επιλεγμένων</a></li>{$n}";
				}
				if( isset( self::$table['bulkActions']['unpublishLink'] ) && ( $link = self::$table['bulkActions']['unpublishLink'] ) ) {
					self::$result .="          <li><a onclick=\"_request( '{$link}" . ( ( strpos( $link, '?' ) === false ) ? '?' : '&' ) . "ids=' + selectChecked() );\">Αποδημοσίευση επιλεγμένων</a></li>{$n}";
				}
				if( isset( self::$table['bulkActions']['deleteLink'] ) && ( $link = self::$table['bulkActions']['deleteLink'] ) ) {
					self::$result .="          <li><a onclick=\"_popupConfirm( 'Θέλετε να διαγραφούν οι επιλεγμένες εγγραφές;', '_request( \'{$link}" . ( ( strpos( $link, '?' ) === false ) ? '?' : '&' ) . "ids=\' + selectChecked() )' );\">Διαγραφή επιλεγμένων</a></li>{$n}";
				}
				self::$result .="        </ul></div>{$n}";
				self::$result .="      </th>{$n}";
			}
			
			$params = Core_Request::getInstance()->getParams();
			$search_field = ( isset( $params['search_field'] ) ) ? $params['search_field'] : '_' ;
			$search_value = ( isset( $params['search_value'] ) ) ? $params['search_value'] : '_' ;
			$order = ( isset( $params['order'] ) ) ? $params['order'] : '_' ;
	
			foreach( self::$table['fieldHeaders'] as $h )
			{	
				self::$result .= "      <th>{$n}";      
				
				if( isset( self::$table['headerOrderLink'] ) && isset( $h['order'] ) )
				{
					if( $order[0] == $h['order'][0] )	{
						self::$result .= "<a class='selected' href='" . self::$table['headerOrderLink'] . ( ( strpos( self::$table['headerOrderLink'], '?' ) === false ) ? '?' : '&' ) . "order[]={$h['order'][0]}&order[]=" . ( ( $order[1] == 'ASC' ) ? 'DESC' : 'ASC' ) . "'>{$h['value']}</a>{$n}";
					}
					else {
						self::$result .= "<a href='" . self::$table['headerOrderLink'] . ( ( strpos( self::$table['headerOrderLink'], '?' ) === false ) ? '?' : '&' ) . "order[]={$h['order'][0]}&order[]={$h['order'][1]}' class='enabled'>{$h['value']}</a>{$n}";
					}
				}
				else {
					self::$result .= "      {$h['value']}{$n}";
				}
				
				if( isset( self::$table['headerSearchLink'] ) && isset( $h['search_field'] ) )
				{
					if( isset( $h['selectOptions'] ) )
					{
						$array = array( 
							'attributes' => array( 
								'name' => "'search_value'",
								'class' => "'rc1'",
								'onchange' => "\"window.location.href='" . self::$table['headerSearchLink'] . ( ( strpos( self::$table['headerSearchLink'], '?' ) === false ) ? '?' : '&' ) . "search_value=' + this.options[ this.selectedIndex ].value + '&search_field={$h['search_field']}';\"" 
							),
							'options' => $h['selectOptions']
						);
						self::$result .= HTML_Form_Tag::select( $array );
					}
					else
					{
						$s  = "<input ";
						$s .= "id='" . ( isset( $h['calendarId'] ) ? $h['calendarId'] : "search_{$h['value']}" ) . "' ";
						$s .= "type='text' value='" . ( ( $search_field == $h['search_field'] ) ? $search_value : "" ) . "' ";
						$s .= "name='search_value' class='rc1' />";
						$s .= "<button onclick=\" if( _get( '" . ( isset( $h['calendarId'] ) ? $h['calendarId'] : "search_{$h['value']}" ) . "' ).value ) window.location.href='" . self::$table['headerSearchLink'] . ( ( strpos( self::$table['headerSearchLink'], '?' ) === false ) ? '?' : '&' ) . "search_value=' + _get( '" . ( isset( $h['calendarId'] ) ? $h['calendarId'] : "search_{$h['value']}" ) . "' ).value + '&search_field={$h['search_field']}';\" class='rc1Right'> > </button>";
						self::$result .= $s;
					}
				}
				
				self::$result .= "      </th>{$n}";  
			}
			self::$result .= "    </tr>{$n}";
		}
		
		public static function rows()
		{
			$n = self::$n;
			
			foreach( self::$table['rows'] as $key => $row )
			{
				self::$result .= "    <tr" . ( ( $key % 2 ) ? " class='dark'" : " class='light'" ) . ">{$n}";
				
				/* fields */
				foreach( $row['fieldvalues'] as $f ) {	
					self::$result .= "      <td>{$f}</td>{$n}";
				}
				
				/* actions */
				self::$result .= "      <td class='actions'>\n";
				if( isset( $row['actions']['mediaLink'] ) ) { 
					self::$result .= "        <a title='Εικόνες' class='media'>" . ( ( $row['actions']['mediaCount'] ) ? "<span class='rc1'>{$row['actions']['mediaCount']}</span>" : "" ) . "</a>{$n}";
				}
				if( isset( $row['actions']['mapsLink'] ) ) { 
					self::$result .= "        <a href='{$row['actions']['mapsLink']}' title='Χάρτες' class='maps'>" . ( ( $row['actions']['mapsCount'] ) ? "<span class='rc1'>{$row['actions']['mapsCount']}</span>" : "" ) . "</a>{$n}";
				}
				if( isset( $row['actions']['statsLink'] ) ) { 
					if( ! empty( $row['actions']['statsLink'] ) )
					{
						self::$result .= "        <a href='{$row['actions']['statsLink']}' title='Στατιστικά' class='stats'></a>{$n}";
					}
					else
					{
						self::$result .= "        <a class='nostats'></a>{$n}";
					}
				}
				if( isset( $row['actions']['addLink'] ) ) {
					self::$result .= "        <a href='{$row['actions']['addLink']}' title='Πρόσθεση' class='add'></a>{$n}";
				}
				if( isset( $row['actions']['updateLink'] ) ) {
					self::$result .= "        <a href='{$row['actions']['updateLink']}' title='Ενημέρωση' class='update'></a>{$n}";
				}
				if( isset( $row['actions']['deleteLink'] ) ){
					self::$result .= "        <a onclick=\"window.scrollTo( 0,0 ); _popupConfirm( 'Θέλετε να γίνει διαγραφή της εγγραφής;', '_request( \'{$row['actions']['deleteLink']}\' )' );\" title='Διαγραφή' class='delete'></a>{$n}";
				}
				self::$result .= "      </td>\n";
				
				self::$result .= "    </tr>\n";
			}
		}
		
		/* 
		 * Αν δεν στέλνεται $type τότε εμφανίζει success type
		 * 
		 * @param $type ( string )
		 * @param $text ( string )
		 */
		public static function message( $type = null, $text )
		{
			$n = self::$n;
			$s = "";
			$s .="<script type='text/javascript'>_addOnLoad( \"setTimeout( function(){ _hide( _get( 'yiamamsg' ) ) }, 5000 );\" );</script>";
			$s .="<div class='message'>{$n}";
			$s .= HTML_Tag::yiamaMsg( $text, ( ! $type ) ? 'success' : 'error' );
			$s .="</div>{$n}";
			if( ! is_null( $text ) )
			{
				$s .="<script type='text/javascript'>_show('yiamamsg' );</script>";
			}
			
			Core_HTML::addFiles( array(
				'admin\table.css',
				'library\basic.js',
				'library\popups.js'
			) );
			
			return $s;
		}
	}
?>