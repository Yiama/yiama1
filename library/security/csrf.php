<?php
/**
 * CSRF attack is prevented by generating a token which is stored to Session and
 * also send with the Request. The validation is done by comparing the token 
 * from the Request and the token stored in Session.
 * A session token is produced only when there isn't one stored in session, this
 * way the same token can be used to validate in different pages. This means
 * that a client can have multiple form pages opened for submission.
 */
class Security_Csrf {
    /**
     * @var Core_Session
     */
    private $session;
    
    /**
     * @var string
     */
    private $request_token_name;
    
    /**
     * @var string
     */
    private $token;
    
    public function __construct(
        $session, 
        $request_token_name = 'csrf_token', 
        $session_token_key = 'csrf_token'
    ) {
        $this->session = $session;
        $this->request_token_name = $request_token_name;

        // If token does not exist in session then generate it
        if (!empty($session->get($session_token_key))) {
            $this->token = $session->get($session_token_key);
        } else {
            $this->token = hash('sha512', md5(uniqid(mt_rand(), true)));
        }
        $session->set($session_token_key, $this->token);
    }
    
    /**
     * @return string
     */
    public function getToken() {
        return $this->token;
    }
    
    /**
     * @return string
     */
    public function getRequestTokenName() {
        return $this->request_token_name;
    }
    
    /**
     * @param string $request_token
     * @return boolean
     */
    public function isValid($request_token) {
        return hash_equals($this->token, $request_token);
    }
}