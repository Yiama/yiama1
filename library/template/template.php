<?php
class Template_Template
{
    /**
     * @var string
     */
    private $template;
    
    /**
     * @var array 
     */
    private $params;

    /**
     * @param string $template
     */
    public function __construct($template) {
        $this->template = $template;
    }
    
    /**
     * @param array $params
     */
    public function setParams($params) {
        $this->params = $params;
    }
    
    /**
     * @return string
     */
    public function render() {
        preg_match_all('/{{+(.*?)}}/', $this->template, $matches);
        if (!empty($matches)) {
            $template = $this->template;
            foreach ($matches[0] as $key => $match) {
                $template = preg_replace(
                    '/' . $match . '/',
                    $this->params[$matches[1][$key]],
                    $template
                );
            }
        }
        return $template;
    }
}