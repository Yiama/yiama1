<?php
	class DB_ActiveRecord_Model
	{
		protected static $app_name;
		protected static $db_name;
		public static $db_model; /* DB_Model instance */
		protected static $DB_AR_Array; /* DB_ActiveRecord_Array instance */
		public static $cache;
		
		protected function __construct()
		{
			self::init();
		}	
	
		public function __destruct(){}
	
		public static function init()
		{
			self::$app_name = Core_App::getAppName(); 
			$db_config_name = Core_App::getConfig( "applications." . self::$app_name . ".database" );
			self::$db_name = Core_App::getConfig( 'databases.' . $db_config_name . '.connection.dbname' );
			self::$db_model = new DB_Model( $db_config_name );
			self::$DB_AR_Array = new DB_ActiveRecord_Array();
			self::$cache = new Core_Cache( array( 'path' => 'applications' . DS . self::$app_name . DS . 'cache' ) );
		}
		
		public function __set( $property, $value )
		{
			$this->$property = $value;
		}
		
		public function getClassName()
		{
			return get_class( $this );
		}
		
		public function getTableName()
		{
			$model_class_name = $this->getClassName();
			return $model_class_name::$table_name;
		}
		
		public function getTableColumns()
		{
			$model_class_name = $this->getClassName();
			$table_name = $model_class_name::$table_name;
			return self::$db_model
				->table( 'INFORMATION_SCHEMA.COLUMNS' )
				->select()
				->search( array(
					"TABLE_NAME = '{$table_name}'",
					"TABLE_SCHEMA = '" . self::$db_name . "'"
				) )
				->execute( ':COLUMN_NAME' );
		}
		
		public function getPrimaryKey()
		{
			$model_class_name = $this->getClassName();
			return $model_class_name::$primary_key;
		}
		
		public function setLastInsertedId( $value )
		{
			$model_class_name = $this->getClassName();
			$model_class_name::$last_inserted_id = $value;
		}
		
		public function getLastInsertedId()
		{
			$model_class_name = $this->getClassName();
			if( empty( $model_class_name::$last_inserted_id ) ) {
				$max_id = current( self::$db_model
					->table( $model_class_name::$table_name )
					->select( '@func:IFNULL( MAX( id ), 0 )' )
					->execute( 'one' )
				);
				$this->setLastInsertedId( $max_id );
			}
			return $model_class_name::$last_inserted_id;
		}
		
		public function getRelations()
		{
			$model_class_name = $this->getClassName();
			return ! empty( $model_class_name::$relations ) ? $model_class_name::$relations : null;
		}
		
		public function getRequired()
		{
			$model_class_name = $this->getClassName();
			return ! empty( $model_class_name::$required ) ? $model_class_name::$required : array();
		}
		
		/*
		 * Overridden method
		 * Default query αν δεν έχει οριστεί στην child class.
		 * Χρησιμοποιείται για την δημιουργία των properties τύπου property => value 
		 */
		public function getDBModelStateSelect()
		{
			$model_class_name = $this->getClassName();
			return self::$db_model
				->table( $model_class_name::$table_name )
				->select();
		}
		
		/*
		 * Αν δεν έχει οριστεί τύπος για το δεδομένο property επιστρέφει true
		 * @param string $property object property
		 * @param mixed $value object property value
		 */
		public function isValidType( $property, $value ) 
		{
			if( ! empty( $this->key_types ) ) {
				foreach( $this->key_types as $key => $type ) {
					if( $key == $property ) {	
						if( is_array( $type ) ) {
							$type = $type[0];
						}
						switch( $type ) {
							case 'int':
								return is_int( $value );
								break;
							case 'float':
								return is_float( $value );
								break;
							case 'boolean':
								return is_bool( $value );
						}
					}
				}
			}
			return true;
		}
		
		/*
		 * Ελέγχει αν έχουν οριστεί συγκεκριμένοι τύποι που πρέπει να δοθούν σε ορισμένες properties.
		 * @param string $property object property
		 * @param mixed $value object property value
		 */
		public function typeCastValue( $property, $value ) 
		{
			if( ! empty( $this->key_types ) ) {
				foreach( $this->key_types as $key => $type ) {
					if( $key == $property ) {	
						if( is_array( $type ) ) {
							$type = $type[0];
							$extra = $type[1];
						}
						switch( $type ) {
							case 'int':
								return intval( $value );
								break;
							case 'float':
								return round( $value, $extra );
								break;
							case 'boolean':
								return boolval( $value );
						}
					}
				}
			}
			return $value;
		}
		
		public function getCacheKey( $cache_postfix = null )
		{
			$model_class_name = $this->getClassName();
			return $cache_key = self::$app_name . '.' . $model_class_name::$table_name . ( ! empty( $this->cache_postfix ) ? '.' . $this->cache_postfix : '' ) . ( ! empty( $cache_postfix ) ? '.' . $cache_postfix : '' );
		}
		
		/*
		 * @param string cache_postfix
		 */
		public function setCache( $cache_postfix, $content )
		{
			self::$cache->set( $this->getCacheKey( $cache_postfix ), $content, true );
		}
		
		/*
		 * @param string cache_postfix
		 */
		public function getCache( $cache_postfix )
		{
			if( ( $cache = self::$cache->get( $this->getCacheKey( $cache_postfix ), true ) ) !== false ) {
				return $cache;
			}
			return false;
		}
		
		/*
		 * Συντομεύσεις query
		 */
		public function query()
		{
			return new DB_ActiveRecord_Query( $this );	
		}
		
		public function insert()
		{
			return $this->query()->insert();	
		}
		
		public function update()
		{
			return $this->query()->update();	
		}
		
		public function delete()
		{
			return $this->query()->delete();	
		}
		
		public function findAll()
		{
			return $this->query()->find();
		}
		
		public function findAttr( $attribute )
		{
			return $this->query()->find( $attribute );
		}
		
		/*
		 * @param $key_value mixed
		 */
		public function findByKey( $key_value )
		{
			$prKey = $this->getPrimaryKey();
			if( is_array( $prKey ) ) {
				$search = array();
				foreach( $prKey as $key => $pk ) {
					$search[] = "{$pk} = '{$key_value[ $key ]}'";
				}
			} else {
				$search = "{$prKey} = '{$key_value}'";
			}
			return $this->query()->search( $search )->find( ':first' );
		}
	}
?>