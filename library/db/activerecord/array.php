<?php
	/*
	 * Χρησιμοποιείται για την δημιουργία πίνακα με Objects
	 * βάσει ενός associative πίνακα
	 */
	class DB_ActiveRecord_Array
	{
		/*
		 * @param array[ associative ] $initial_array
		 * @param string $model_class_name 
		 * 		DB_AcriveRecord_Model classname
		 */
		public static function create( $initial_array, $model_class_name )
		{
			if( empty( $initial_array ) ) {
				return array();
			}
			$return_array = array();
			foreach( $initial_array as $row ) {
				$object = new $model_class_name(); 
				foreach( $row as $row_key => $row_val ) {
					$object->$row_key = $object->typeCastValue( $row_key, $row_val );
				}
				$return_array[] = $object;
			}
			return $return_array;
		}
		
		/*
		 * @param array[ objects ] $array
		 * @param array $conditions
		 */
		public static function queryArray( $array, $conditions )
		{
			if( empty( $array ) ) {
				return array();
			}
			if( ! empty( $conditions['search'] ) ) {
				$array = self::search( $array, $conditions['search'] );
			}
			if( ! empty( $conditions['order'] ) ) {
				foreach( $conditions['order'] as $property => $direction ) {
					$array = self::sort( $array, $property, $direction );
				}
			}
			if( ! empty( $conditions['limit'] ) ) {
				$limit = explode( ',', $conditions['limit'] );
				if( ! isset( $limit[1] ) ) {
					$array = array_slice( $array, 0, $limit[0] );
				}
				else {
					$array = array_slice( $array, $limit[0], $limit[1] );
				}
			}
			return $array;
		}
		
		public static function trim( $value )
		{
			return trim( trim( $value, "'" ), "`" );
		}
		
		/*
		 * @param array[ DB_ActiveRecord_Model instances ] $objectsArray
		 * @param array $conditions 
		 * 		array( array( '<col_name>', '<operator>', '<value>' ), ... )
		 */
		public static function search( $objectsArray, $conditions )
		{
			$result = array();
			$total_conditions = count( $conditions );
		
			foreach( $objectsArray as $obj ) {
				$total_found = 0;	
				foreach( $conditions as $condition ) {
					if( $condition[1] == '=' && is_array( $condition[2] ) ) { /* Περίπτωση αντίστοιχη της IN() στην SQL  */
						foreach( $condition[2] as $cond ) {
							if( self::trim( $cond ) == self::trim( $obj->$condition[0] ) ) {
								$total_found++;
								break;
							}
						}
					}
					else {
						$property = self::trim( $condition[0] );
						$condition[2] = self::trim( $condition[2] );
						switch( $condition[1] ) {
							case "=":
								if( $obj->$property == $condition[2] ) {
									$total_found++;
								}
								break;
						 	case "<>": 
						 		if( $obj->$property != $condition[2] ) {
									$total_found++;
								}
								break;
						 	case ">":
								if( $obj->$property > $condition[2] ) {
									$total_found++;
								}
								break;
						 	case "<":
								if( $obj->$property < $condition[2] ) {
									$total_found++;
								}
								break;
						 	case ">=":
								if( $obj->$property >= $condition[2] ) {
									$total_found++;
								}
								break;
						 	case "<=":
								if( $obj->$property <= $condition[2] ) {
									$total_found++;
								}
								break;
						}
					}
				}
				if( $total_found == $total_conditions ) {
					$result[] = $obj;
				}
			}
			
			return $result;
		}
		
		/*
		 * Usort compare function
		 */
		public static function sortCompare( $object1, $object2 )
		{
			$property = self::$sort_property;
			
			if( $object1->$property == $object2->$property )
			{
				return 0;
			}
			
			if( self::$sort_direction === 'asc' )
			{
		    	return ( $object1->$property < $object2->$property ) ? -1 : 1;
			}
			else
			{
		    	return ( $object1->$property > $object2->$property ) ? 1 : -1;
			}
		}
		
		/*
		 * @param $array Objects array
		 * @param $property 
		 * @param $direction { sorting direction [ 1: asc, 0: desc ] }
		 */
		public static function sort( $array, $property, $direction = 'asc' )
		{
			self::$sort_property = $property;
			self::$sort_direction = strtolower( $direction );
			
			usort( $array, array( 'DB_ActiveRecord_Query', 'sortCompare' ) );
			
			return array_reverse( $array ); 
		}
		
		/*
		 * Δημιουργεί μία δενδρική ιεράρχηση βάσει ενός field που χρησιμοποιείται για την ιεράρχιση των rows. 
		 * $objectsArray @param ( mixed )  
		 * $initial_value@param ( string ) 
		 * $value_field { field που χρησιμοποιείται για τον έλεγχο στο δεύτερο πέρασμα }
		 * @param ( string ) $hierarchy_field { field που χρησιμοποιείται για την ιεράρχιση στο πρώτο πέρασμα }
		 * @return ( array )
		 */
		public static function tree( $objectsArray, $initial_value = null, $value_field, $hierarchy_field, $tree_level = 1 )
		{
			$branch = array();
			foreach( $objectsArray as $ar ) {
				if( $ar->$hierarchy_field == $initial_value || is_null( $initial_value ) ) {
					$ar->tree_level = $tree_level;
					$ar->total_children = 0;
					$children = self::tree( $objectsArray, $ar->$value_field, $value_field, $hierarchy_field, ($tree_level+1) );
					if( ! empty( $children ) ) {
						$ar->children = $children;
						$ar->total_children = count( $children );
					}
					$branch[] = $ar;
				}
			}
			return $branch;
		}
	
		/*
		 * Flatten object array φτιαγμένο με την $tree().
		 * @param $objectsArray
		 * @param $field ( string ) { Property ως προς την οποία θα γίνεται το flatten, αυτή δηλαδή θα περιέχει τα subobjects. }
		 */ 
		public static function flatten( $objectsArray, $field ) 
		{
			$return = array();
			foreach( $objectsArray as $ar ) {
				$r = $ar;
				$return[] = $r;
				if( isset( $ar->$field ) ) {
					$subreturn = self::flatten( $ar->$field, $field );
					unset( $return[ count( $return )-1 ]->$field );
					$return = array_merge( $return, $subreturn );
				}
			}
			return $return;
		}
	}
?>