<?php
	/*
	 * Χρησιμοποιείται ώστε να μπορούν να γίνουν απλά query στην Βάση
	 */
	class DB_ActiveRecord_Query
	{
		/*
		 * DB_ActiveRecord_Model instance
		 */
		protected $ar_model_instance;
		
		/*
		 * @param array 
		 * 	Array( 
		 * 		'where' => array( array( '<col_name>', '<operator>', '<value>' ), ... ),
		 * 		'limit' => '<offset>,<length>',
		 * 		'order' => array( '<col_name>' => '<direction>', ... )
		 * 	)
		 */
		private $conditions;
		
		private $from_cache = null;
		
		public function __construct( $model_instance )
		{
			$this->ar_model_instance = $model_instance;
		}
		
		public function __destruct(){}
		
		/*
		 * Ορίζει αν θα παίρνει από την cache
		 * @param string Cache postfix name
		 */
		public function cache( $cache_postfix )
		{
			$this->from_cache = $cache_postfix;
			return $this;
		}
		
		/*
		 * Τα ονόματα των relations που θα συνδεθούν
		 * @param array $combine
		 */
		public function combine( $combine )
		{
			$this->conditions['combine'] = ( array ) $combine;
			return $this;
		}
		
		/*
		 * @param array $alias ( string )
		 */
		public function alias( $alias )
		{
			$this->conditions['alias'] = $alias;
			return $this;
		}
		
		/*
		 * @param array $select ( mixed )
		 */
		public function select( $select )
		{
			$this->conditions['select'] = $select;
			return $this;
		}
		
		/*
		 * @param array $seach 
		 * 		? για queryArray(): array( array( '<col_name>', '<operator>', '<value>' ), ... )
		 * 		? για queryDb(): array( '<col_name> <operator> <value>, ...' )
		 */
		public function search( $search )
		{
			$this->conditions['search'] = $search;
			return $this;
		}
		
		/*
		 * @param array $join
		 */
		public function join ( $table, $condition, $direction = null )
		{
			if( ! isset( $this->conditions['join'] ) ) {
				$this->conditions['join'] = array();
			}
			$this->conditions['join'][] = array( $table, $condition, $direction );
			return $this;
		}
		
		/*
		 * @param array $limit 
		 * 		'<integer>' ή '<offset>,<length>'
		 */
		public function limit( $limit )
		{
			$this->conditions['limit'] = is_int( $limit ) ? "0,{$limit}" : $limit;
			return $this;
		}
		
		/*
		 * @param array $group 
		 */
		public function group( $group )
		{
			$this->conditions['group'] = $group;
			return $this;
		}
		
		/*
		 * @param array $having
		 */
		public function having( $having )
		{
			$this->conditions['having'] = $having;
			return $this;
		}
		
		/*
		 * @param array $order 
		 * 		? για queryArray(): array( '<col_name>' => '<direction>', ... )
		 * 		? για queryDb(): '<col_name> <direction>, ...'
		 */
		public function order( $order )
		{
			$this->conditions['order'] = $order;
			return $this;
		}
		
		/*
		 * Κάνει query στην Cache
		 */
		public function queryCache( $cache_postfix = null )
		{
			$result = $this->ar_model_instance->getCache( $cache_postfix );
			if( ! empty( $result ) ) {
				return DB_ActiveRecord_Array::queryArray( array( $result ), $this->conditions );
			}
		}
			
		/*
		 * Κάνει query στην Βάση
		 * @param object[ DB_MODEL ] $db_model
		 */
		public function queryDB( $db_model )
		{
			if( ! empty( $this->conditions['alias'] ) ) {
				$db_model->select( $this->conditions['alias'] );
			}
			if( ! empty( $this->conditions['select'] ) ) {
				$db_model->select( $this->conditions['select'] );
			}
			if( ! empty( $this->conditions['search'] ) ) {
				$db_model->search( $this->conditions['search'] );
			}
			if( ! empty( $this->conditions['join'] ) ) {
				foreach( $this->conditions['join'] as $key => $join ) {
					$db_model->join( $join[0], $join[1], isset( $join[2] ) ? $join[2] : null );
				}
				$this->conditions['join'] = array(); /* !!! Ξαναορίζουμε το join για να γεμίζει από την αρχή */
			}
			if( ! empty( $this->conditions['group'] ) ) {
				$db_model->group( $this->conditions['group'] );
			}
			if( ! empty( $this->conditions['having'] ) ) {
				$db_model->having( $this->conditions['having'] );
			}		
			if( ! empty( $this->conditions['order'] ) ) { 
				$db_model->order( $this->conditions['order'] );
			}
			if( ! empty( $this->conditions['limit'] ) ) {
				$db_model->limit( $this->conditions['limit'] );
			}					
			return $db_model->execute();
		}
		
		private function relations( $result )
		{
			if( empty( $result[0] ) ) {
				return $result;
			}
			$model_instance = $this->ar_model_instance;
			$relations = $model_instance->getRelations();
			if( ! empty( $this->conditions['combine'] ) && ! empty( $relations ) ) {
				foreach( $this->conditions['combine'] as $com ) {
					if( empty( $relations[ $com ] ) ) {
						continue;
					}
					$relation = $relations[ $com ];
					$model = new $relation['model_name']();
					$query = $model->query();
					foreach( $result as $r ) {
						if( ! empty( $relation['select'] ) ) {
							$query->select( $relation['select'] );
						}
						$search = array();
						if( ! empty( $relation['where'] ) ) {
							$search[] = $relation['where'];
						}
						foreach( $relation['foreign_key'] as $from_key => $to_key ) {
							$search[] = "{$to_key} = '{$r->$from_key}'";
						}
						$query->search( $search );
						if( ! empty( $relation['join'] ) ) {
							foreach( $relation['join'] as $join ) {
								$query->join( $join[0], $join[1], isset( $join[2] ) ? $join[2] : null );
							}
						}
						if( ! empty( $relation['group'] ) ) {
							$query->group( $relation['group'] );
						}
						if( ! empty( $relation['having'] ) ) {
							$query->order( $relation['having'] );
						}
						if( ! empty( $relation['order'] ) ) {
							$query->order( $relation['order'] );
						}
						if( ! empty( $relation['combine'] ) ) {
							$query->combine( $relation['combine'] );
						}
						if( $relation['type'] == 'many_to_one' || $relation['type'] == 'one_to_one' ) {
							$r->$com = $query->find( ':first' );
						}
						elseif ( $relation['type'] == 'one_to_many' || $relation['type'] == 'many_to_many' ) {
							$r->$com = $query->find();
						}
					}
				}
			}
			$this->conditions = null;
			return $result;
		}
		
		/*
		 * Επιστρέφει πίνακα από objects που ικανοποιούν τις συνθήκες.
		 * @param mixed $option 
		 * 		':first', ':last', '<col_name>', <index>
		 */
		public function find( $option = null )
		{
			$model_instance = $this->ar_model_instance;
			/* Αν θέλει από την cache και αυτή υπάρχει την παίρνουμε  */
			if( ! is_null( $this->from_cache ) 
				&& ( $result = $this->ar_model_instance->getCache( $this->from_cache ) ) !== false ) { 
			}
			/* 
			 * Παίρνουμε το αποτέλεσμα και αν θέλει από την cache την δημιουργούμε  
			 */
			else {
				$result = DB_ActiveRecord_Array::create( 
					$this->queryDB( $model_instance->getDBModelStateSelect() ),
					$model_instance->getClassName()				
				);
				if( $option == ':first' ) {
					$result = $this->relations( array( reset( $result ) ) );
					$result = reset( $result );
				}
				elseif ( $option == ':last' ) {
					$result = $this->relations( array( end( $result ) ) );
					$result = end( $result );
				}
				elseif ( ! empty( $option ) && is_int( $option ) ) { /* Επιστρέφει το n-οστό κελί του πίνακα */
					$result = $this->relations( array( $result[ $option ] ) );
					$result = $result[ $option ];
				}
				elseif ( ! empty( $option ) ) { /* Επιστρέφει μία στήλη */
					$return = array();
					foreach( $this->relations( $result ) as $r ) {
						$return[] = $r->$option;
					}
					$result = $return;
				}
				else{
					$result = $this->relations( $result );
				}
				if( ! is_null( $this->from_cache ) ) {
					$this->ar_model_instance->setCache( $this->from_cache, $result );
				}
			}
			return $result;
		}
		
		/*
		 * Αν όλα τα primary keys εκτός του id και όλα τα required properties έχουν τεθεί και εφόσον έχουν σωστό τύπο προχωράμε.
		 */
		public function prepare()
		{
			$model_instance = $this->ar_model_instance;
			
			/*
			 * Primary key properties
			 */
			foreach( ( array ) $model_instance->getPrimaryKey() as $property ) {
				if( $property != 'id' && ! isset( $model_instance->$property ) ) { 
					throw new Exception( "Can't save object of class " . $model_instance->getClassName() . "! Missing primary key {$property}"  );
				}
			}
			/*
			 * Required properties
			 */
			foreach( $model_instance->getRequired() as $property ) {
				if( ! isset( $model_instance->$property ) ) { 
					throw new Exception( "Can't save object of class " . $model_instance->getClassName() . "! Required {$property}"  );
				}
			}
			/*
			 * Valid value type
			 */
			foreach( $model_instance as $property => $value ) {
				if( ! $model_instance->isValidType( $property, $value ) ) { 
					throw new Exception( "Can't save object of class " . $model_instance->getClassName() . "! Invalid type for {$property}"  );
				}
			}
			
			return true;
		}

		/*
		 * Εισάγει στην Βάση και ανανεώνει την Cache
		 */
		public function insert()
		{
			if( ! $this->prepare() ) {
				return false;
			}
			$model_instance = $this->ar_model_instance;
			$prKey = $model_instance->getPrimaryKey();
			if( $prKey == 'id' ) {
				$model_instance->id = $model_instance->getLastInsertedId()+1;	 
			}
			$insert_values = array();
			$columns = $model_instance->getTableColumns();
			foreach( $columns as $property ) {
				if( isset( $model_instance->$property ) ) {
					$insert_values[ $property ] = $model_instance->$property;
				}
			}
			$result = DB_ActiveRecord_Model::$db_model
				->table( $model_instance->getTableName() )
				->insert( array_keys( $insert_values ) )
				->values( array_values( $insert_values ) )
				->execute();
			if( $result && $prKey == 'id' ) {
				$model_instance->setLastInsertedId( $model_instance->id );	 
			}
			return $result;		
		}

		/*
		 * Εισάγει στην Βάση και ενημερώνει την Cache
		 */
		public function update()
		{
			if( ! $this->prepare() ) {
				return false;
			}
			$model_instance = $this->ar_model_instance;
			$prKey = $model_instance->getPrimaryKey();
			$where = array();
			foreach( ( array ) $prKey as $pk ) {
				if( is_string( $model_instance->$pk ) ) {
					$where[] = "{$pk} = '{$model_instance->$pk}'";
				}
				else {
					$where[] = "{$pk} = {$model_instance->$pk}";
				}
			}
			$update_values = array();
			$columns = $model_instance->getTableColumns();
			foreach( $columns as $property ) {
				if( isset( $model_instance->$property ) && ! in_array( $property, ( array ) $prKey ) ) {
					$update_values[ $property ] = $model_instance->$property;
				}
			}
			$result = DB_ActiveRecord_Model::$db_model
				->table( $model_instance->getTableName() )
				->update( $update_values )
				->search( $where )
				->execute();	
			return $result;
		}

		public function delete()
		{
			if( ! $this->prepare() ) {
				return false;
			}
			$model_instance = $this->ar_model_instance;
			$prKey = $model_instance->getPrimaryKey();
			$where = array();
			foreach( ( array ) $prKey as $pk ) {
				if( is_string( $model_instance->$pk ) ) {
					$where[] = "{$pk} = '{$model_instance->$pk}'";
				}
				else {
					$where[] = "{$pk} = {$model_instance->$pk}";
				}
			}
			$result = DB_ActiveRecord_Model::$db_model
				->table( $model_instance->getTableName() )
				->delete()
				->search( $where )
				->execute();	
			return $result;
		}
	}
?>