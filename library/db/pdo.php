<?php

/*
	Interface ��� ��� PDO class.
	��� �� ����� ������� ��� ���� �� PDO �� ��������� ������� ����������� methods.
*/
interface DB_PDO
{
	public function openCon( $params );
	
	public function closeCon();
	
	public function beforeQuery( $options );
	
	public function query( $query );
	
	public function result( $result );
	
	public function escape( $values );
	
	public function affected_rows();
}

?>