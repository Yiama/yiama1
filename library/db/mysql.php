<?php

class DB_Mysql implements DB_PDO
{
	private $mysql;
	
	private $options;
	
	private $result;
	
	/*
		Καλεί την method του Mysql για το database connection διαβάζοντας τις παραμέτρους σύνδεσης από το configuration.
		Ορίζει το charset της database.
		
		@param $db { database name }
	*/
	public function openCon( $db )
	{
		$params = Core_App::getConfig( 'databases.' . $db . '.connection' );
		
		/* Connect. */
		if ( ! $con = mysql_connect( $params['server'], $params['username'], $params['userpass'], $params['dbname'] ) ) 
		{
			throw new Exception( mysql_error()  );
		}
		else
		{
			
			mysql_select_db( $params['dbname'], $con ); 
		}
		
		/* 
			Set GROUP_CONCAT max length.
			Χρειάζεται αν δημιουργούνται μεγάλα string με αυτή την μέθοδο. 
		*/
		mysql_query( 'SET group_concat_max_len = 100000' );
			
		/* 
			Ο mysql optimizer από προεπιλογή ψάχνει 62 διαφορετικά σενάρια κάθε φορά 
			για να επιλέξει το καλύτερο για την εκτέλεση μίας εντόλης. Αυτό μειώνει 
			δραματικά την εκτέλεση των πολλαπλών join ακόμα και αν είναι όλα πάνω σε foreign keys.
		*/
		mysql_query( 'SET optimizer_search_depth = 0' );
		
		/* 
			Set UTF8 characters. 
			Χρειάζεται και για την real_escape_string().
		*/
		if ( ! mysql_set_charset( 'utf8' ) ) 
		{
			throw new Exception( mysql_error()  );
		}
	}
	
	/*
		Close database connection.
	*/
	public function closeCon()
	{
		if ( ! mysql_close() ) 
		{
			throw new Exception( mysql_error() );
		}
		
	}
	
	/*
		Προετοιμασία query options.
		
		@param $options ( array ) 
	*/
	public function beforeQuery( $options )
	{
		$this->options = $options;
	}
	
	/*
		Query database.
		Αν επιστρέφει mysqli result object καλεί την result() αλλιώς επιστρέφει το αποτέλεσμα (true/false). 
		[	
			Returns FALSE on failure. For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries mysqli_query() 
			will return a mysqli_result object. For other successful queries mysqli_query() will return TRUE.
		]
		
		@param $query ( string ) { Mysql query string. }
		@return ( mixed )
	*/
	public function query( $query )
	{
		if ( ! ( $result = mysql_query( $query ) ) )
		{
			throw new Exception( mysql_error() );
		}
		
		return ( is_resource( $result ) ) ? $this->result( $result ) : $result;
	}
	
	/*
		Καλείται αν επιστραφεί από το query ένα mysqli result object.
		[		
			You should always free your result with mysqli->free(), when your result object is not needed anymore.
		]
		
		@param $result ( mysqli result object )
		@return ( array )
	*/
	public function result( $result )
	{
		$return = array();
		
		if( $this->options['fetch_rows'] == 'one' || $this->options['fetch_rows'] == 'row' )
		{
			$return = mysql_fetch_array( $result, $this->options['fetch_fields'] ); 
		}
		elseif ( strpos( $this->options['fetch_rows'], ':' ) === 0 )
		{
			$column = substr( $this->options['fetch_rows'], 1 );
			while( $row = mysql_fetch_array( $result, MYSQLI_ASSOC ) ) 
			{
				$return[] = $row[ $column ];
			} 
		}
		elseif ( $this->options['fetch_rows'] == 'object' )
		{
			$return = mysql_fetch_object( $result ); 
		}
		else
		{
			while( $row = mysql_fetch_array( $result, $this->options['fetch_fields'] ) ) 
			{
				$return[] = $row;
			}
		}

		mysql_free_result( $result );
		
		return $return;
	}
	
	/*
		Escapes special characters in a string for use in a SQL statement ( PHP Manual ).
		
		@param $value ( mixed )
		@return ( string )
	*/
	public function escape( $value )
	{
		return mysql_real_escape_string( $value );
	}
	
	/*
		Number of affected rows in a previous MySQL operation.
		
		@return ( int )
	*/
	public function affected_rows()
	{
		return mysql_affected_rows();
	}
}

?>