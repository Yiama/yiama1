<?php

/*
	Ειδικές λειτουργίες. Σύνταξη @<special_action_name>:[<string>].
		Αύξηση του id column σε ένα insert statement:  
			->insert()
			->values( array( '@id:', 1, 1, 0, ... ) ).
		Search statement, ψάχνει αν υπάρχει εγγραφή με το string (@s:) 'title_updated' στο πεδίο title:
			->search( 'title=@s:title_updated' ).
		Update ή insert statement: 
			->values( 'date' => '@func:NOW()' ).
	Alias. Σύνταξη: για να ορίσουμε ένα alias στο βασικό table καλούμε την $this->alias( '<alias_string>')
		->alias( 't1' ).
*/
class DB_Model
{
	/*
		@var ( object )
	*/
	protected $db;
	
	/*
		Το id της τελευταίας εγγραφής.
	
		@var ( int )
	*/
	protected $maxId;
	
	/*
		@var ( string )
	*/
	protected $table, $from, $action, $order, $limit, $alias, $group, $having, $query;
	
	/*
		@var ( mixed )
	*/
	protected $select, $insert, $update, $search, $join, $values;
	
	/*
		@param $dbName { config.php database name }
	*/
	public function __construct( $dbName )
	{
		$this->db = Core_DB::getInstance( $dbName );
	}
	
	public function __destruct(){}
	
	/*
		Execute query.
		Πρώτα αρχικοποιεί τα properties για να είναι έτοιμα για το επόμενο query.
		Καλεί την beforeQuery() για να πρετοιμάσει το query π.χ. αν θέλουμε να πάρουμε 1 row σε 2d array.
		
		Options:
			$options['result_array']  = 1 => return 2d array, 2 => return multiD array.
			$options['result_fields'] = 1 => return associative key array, 2 => return numeric key array.
		
		@param $options ( mixed )
		@return  ( array/false/true ) { PDO query return. }
	*/
	public function execute( $options = null )
	{
		$ops = array();
		/* Default flags. */
		$ops['fetch_rows'] = 'all';
		$ops['fetch_fields'] = MYSQLI_ASSOC;
		if( $options )
		{
			foreach( ( array ) $options as $o )
			{
				if( strpos( $o, ':' ) === 0 )
				{
					$ops['fetch_rows'] = $o;
				}
				switch( $o )
				{
					case 'one':
					case 'object':
						$ops['fetch_rows'] = $o;
						break;
					case 'num':
						$ops['fetch_fields'] = MYSQLI_NUM;
						break;
				}
			}
		}
		
		$this->db->beforeQuery( $ops );
		return $this->db->query( $this->buildQuery() );		
	}
	
	/*
		Κάνει null τα properties που χρησιμοποιούνται στα queries
		για να μην χρησιμοποιηθεί κατά λάθος κάποιο με προηγούμενη τιμή
		επειδή ο χρήστης δεν θα την έχει ορίσει εκ νέου.
	*/
	public function cleanVars()
	{
		$this->action = $this->limit = $this->alias = $this->from = $this->select = $this->insert = $this->values = $this->update = $this->search = $this->order = $this->join = $this->group = $this->having = $this->query = null;
	}
	
	/*
		Δόμηση query, διαγραφή των vars.
		Μπορεί να κληθεί και από τον χρήστη για χρήση ως εμφωλευμένο query.
	*/
	public function buildQuery()
	{	
		switch( $this->action )
		{
			case 'select':
				$q[] = 'SELECT';
				$q[] = $this->select;
				$q[] = 'FROM';
				$q[] = ( $this->from ) ? $this->from : $this->table;
				$q[] = ( ( $this->alias ) ? 'AS ' : '' ) . $this->alias; 
				$q[] = ( $this->join ) ? implode( ' ', $this->join ) : '';
				$q[] = ( ( $this->search ) ? 'WHERE ' : '' ) . $this->search;
				$q[] = ( ( $this->group ) ? 'GROUP BY ' : '' ) . $this->group;
				$q[] = ( ( $this->having ) ? 'HAVING ' : '' ) . $this->having;
				$q[] = ( ( $this->order ) ? 'ORDER BY ' : '' ) . $this->order;
				$q[] = ( ( $this->limit ) ? 'LIMIT ' : '' ) . $this->limit;
				break;
			case 'insert':
				$q[] = 'INSERT INTO';
				$q[] = $this->table;
				$q[] = $this->insert;
				$q[] = 'VALUES';
				$q[] = $this->values;
				break;
			case 'insert_update':
				$q[] = 'INSERT INTO';
				$q[] = $this->table;
				$q[] = $this->insert;
				$q[] = 'VALUES';
				$q[] = $this->values;
				$q[] = 'ON DUPLICATE KEY UPDATE';
				$q[] = $this->update;
				break;
			case 'update':
				$q[] = 'UPDATE';
				$q[] = $this->table;
				$q[] = 'SET';
				$q[] = $this->update;
				$q[] = ( ( $this->search ) ? 'WHERE ' : '' ) . $this->search;
				break;
			case 'delete':
				$q[] = 'DELETE FROM';
				$q[] = $this->table;
				$q[] = ( ( $this->search ) ? 'WHERE ' : '' ) . $this->search;
				break;
			case 'query':
				$q = $this->query;
				break;
		}
		
		$this->cleanVars();
		
		return implode( ' ', ( array ) $q );
	}
	
	/*
		Max = last id.
		Αν υπάρχει ήδη το maxId το επιστρέφει αλλιώς το παίρνει με query στη database.
	*/
	public function getMaxId()
	{
		if( $i = $this->maxId ) 
		{
			return $i;
		}

		$newInstance = get_called_class();
		$newInstance = new $newInstance();
		$result = $newInstance->select( '@func:IFNULL( MAX( id ), 0 )' )->execute( 'one' );
		return array_shift( $result );
	}	
	
	/*
		Επιστρέφει τις values αντικαθιστώντας τα special action με την τιμή που επιστρέφει η αντίστοιχη ενέργεια.
		Σύνταξη special action: @<special_action_name>:[<string>].
		Ελέγχει αν υπάρχει ο χαρακτήρας " ' " και βάζει διακόπτη " \ " μπροστά του. 
		
		@param $values ( mixed )
		@return ( array )
	*/
	public function filterValues( $values )
	{
		/*	Το βάζουμε σε loop ώστε να εκτελεστούν μόνο αν έχει το κατάλληλο string στοιχείο. */
		foreach( $values as &$v )
		{
			if( preg_match( "/@(?P<action>[a-z]+):/", $v, $results ) )
			{
				$action = $results['action'];
				
				switch( $action )
				{
					/* Increment row id. */
					case 'id':
						$v = $this->getMaxId()+1;
						break;
					/* Μysql function. */
					case 'func':
						$v = str_replace( "'", "", substr( $v, strpos( $v, ':' )+1 ) );
						break;
					/* Βάζει αυτάκια ξεκινώντας μετά το ':' και μέχρι το τέλος του string. */
					case 's':
						$pos = strpos( $v, '@s:' )+3;
						$v = "'" . substr( str_replace( "'", "", $v ), $pos ) . "'";
						break;
					case 'null':
						$v = 'NULL';
						break;
				}
			}
		}
		
		return $values;
	}
	
	/*
		Chained.
		
		@param $tbName 
	*/
	public function table( $tbName )
	{
		$this->table = $tbName;
		
		return $this;
	}
	
	/*
		Where statement.
		Chained.
		
		@param $search ( mixed ) { Where statements. }
	*/
	public function search( $search )
	{
		if( ! empty( $this->search ) ) {
			$this->search .= ' AND ' . implode( ' AND ', $this->filterValues( ( array ) $search ) );
		}
		else {
			$this->search = implode( ' AND ', $this->filterValues( ( array ) $search ) );
		}
		
		return $this;
	}
	
	/*
		Set table alias.
		Αν ορίσουμε alias πρέπει να το κάνουμε πριν οποιαδήποτε άλλη εντολή του query.
		Chained.
		
		@param $alias ( mixed )
	*/
	public function alias( $alias )
	{
		$this->alias = $alias;
		
		return $this;
	}
	
	/*
		Join statement.
		Chained.
		
		@param $table ( string )
		@param $condition ( mixed ) { ON ... }
		@param $direction ( string ) { LEFT, ... }
	*/
	public function join( $table, $condition = null, $direction = null )
	{
		$q[] = $direction;
		$q[] = 'JOIN';
		$q[] = $table;
		if( $condition ) {
			$q[] = 'ON';
			$q[] = '(' . implode( ' AND ', ( array ) $condition ) . ')';
		}
		$this->join[] = implode( ' ', $q );
		
		return $this;
	}	
	
	/*
		Insert values.
		Δέχεται μόνο array με values και όχι string.
		Chained.
		
		@param $values ( mixed )
	*/
	public function values( $values )
	{
		foreach( $values as &$v )
		{
			$v = "'" . $this->db->escape( $v ) . "'";
		}
		
		$this->values = '( ' . implode( ', ', $this->filterValues( ( array ) $values ) ) . ' )';

		return $this;
	}	
	
	/*
		Order by statement.
		Chained.
		
		@param $values ( mixed )
	*/
	public function order( $order )
	{
		if( is_string( $order ) ) {
			$this->order = $order;
		} elseif ( is_array( $order ) ) {
			$ord = array();
			foreach( $order as $key => $val ) {
				$ord[] = $key . ' ' . $val;
			}
			$this->order = implode( ',', $ord );
		}

		return $this;
	}
	
	/*
		Limit statement.
		Chained.
		
		@param $values ( string )
	*/
	public function limit( $limit )
	{
		$this->limit = $limit;

		return $this;
	}
	
	/*
		Group by statement.
		Chained.
		
		@param $group ( string )
	*/
	public function group( $group )
	{
		$this->group = $group;

		return $this;
	}
	
	/*
		Having statement.
		Chained.
		
		@param $having ( string )
	*/
	public function having( $having )
	{
		$this->having = implode( ' AND ', $this->filterValues( ( array ) $having ) );

		return $this;
	}
	
	/*
		Select statement.
		Ορίζει το action.
		Chained.
		
		@param $columns ( mixed ) { Table columns. }
	*/
	public function select( $columns = '*' )
	{	
		$this->action = 'select';
	
		$this->select = implode( ', ', $this->filterValues( ( array ) $columns ) );
		
		return $this;
	}
	/*
		From statement.
		Chained.
		
		@param $from ( string ) { Table name. }
	*/
	public function from( $from )
	{	
		$this->from = $from;
		
		return $this;
	}
	
	/*
		Insert fields.
		Ορίζει το action.
		Αν θέλουμε να εισάγουμε τιμές σε όλα τα columns του table απλά δεν δίνουμε παράμετρο.
		Chained.
		
		@param $insert ( mixed )
	*/
	public function insert( $insert = null )
	{
		$this->action = 'insert';
		
		$this->insert = '( `' . implode( '`, `', $this->filterValues( ( array ) $insert ) ) . '` )';
		
		return $this;
	}
	
	/*
		Insert fields on duplicate key update.
		Ορίζει το action.
		Αν θέλουμε να εισάγουμε τιμές σε όλα τα columns του table απλά δεν δίνουμε παράμετρο.
		Chained.
		
		@param $insert ( mixed )
		@param $update ( array )
	*/
	public function insert_update( $insert, $update )
	{
		$this->action = 'insert_update';
		
		$this->insert = '( `' . implode( '`, `', $this->filterValues( ( array ) $insert ) ) . '` )';
		foreach( $update as &$v )
		{
			$v = "'" . $this->db->escape( $v ) . "'";
		}
		
		$set = array();
		foreach( $this->filterValues( ( array ) $update ) as $key => $value ) {
			$set[] = "`{$key}`={$value}";
		}
		$this->update = implode( ',', $set );
		
		return $this;
	}
	
	/*
		Update fields { περιέχει την σύνταξη SET }.
		Ορίζει το action.
		Chained.
		
		@param $update ( mixed )
	*/
	public function update( $update )
	{
		$this->action = 'update';
		
		foreach( $update as &$v )
		{
			$v = "'" . $this->db->escape( $v ) . "'";
		}
		
		$set = array();
		foreach( $this->filterValues( ( array ) $update ) as $key => $value ) {
			$set[] = "`{$key}`=" . $value;
		}
		$this->update = implode( ',', $set );

		return $this;
	}
	
	/*
		Delete fields.
		Ορίζει το action.
		Chained.
	*/
	public function delete()
	{
		$this->action = 'delete';

		return $this;
	}
	
	/*
	 * Επιστρέφει string σε json format
	 * 
	 * @param  $array { Fields }
	 * @param  $alias { Select row alias }
	 */
	public function groupconcat_json( $array, $alias )
	{
		//CONCAT( '[ ', CONCAT( '{ ', CAST( GROUP_CONCAT( DISTINCT '\"title\": \"', IF( l.title IS NULL, \"\", l.title ), '\", \"description\": \"', IF( l.description IS , \"NULL\", l.description ), '\"' SEPARATOR ' }, { ' ) AS CHAR ), ' }' ), ' ]' ) AS languages,	 
		//$s = "CONCAT( '[ ', CONCAT( '{ ', CAST( GROUP_CONCAT( DISTINCT ";
		$s = "CAST( GROUP_CONCAT( DISTINCT ";
		$keysIndex = array_keys( $array );
		foreach( $array as $key => $r )	{
			$s .= "'" . ( ( $keysIndex[0] != $key ) ? "\", " : "" ) . "\"{$key}\": \"', IF( {$r} IS NULL, \"\", {$r} ), ";
		}
		$s .= "'\"' SEPARATOR ' }, { ' ) AS CHAR ) AS {$alias}";
		//$s .= "'\"' SEPARATOR ' }, { ' ) AS CHAR ), ' }' ), ' ]' ) AS {$alias}";
		
		return $s;
	}
	
	/*
		Number of affected rows in a previous MySQL operation.
		
		@return ( int )
	*/
	public function affected_rows()
	{
		return $this->db->affected_rows();
	}
	
	/*
		Escapes special characters in a string for use in a SQL statement ( PHP Manual ).
		
		@param $string ( string ) { Query }
		@return ( string )
	*/
	public function escape( $string )
	{
		return $this->db->escape( $string );
	}
	
	/*
		Query.
		Αν θέλουμε να φτιάξουμε κατευθείαν ένα query.
		Ορίζει το action και το query string.
		Chained.
		@param $string ( string ) { Query }
	*/
	public function query( $string )
	{
		$this->action = 'query';
		
		$this->query = implode( '', $this->filterValues( ( array ) $string ) );

		return $this;
	}
	
	public function startTransaction()
	{
		$this
			->query( 'START TRANSACTION' )
			->execute();
	}
	
	public function commit()
	{
		$this
			->query( 'COMMIT' )
			->execute();
	}
	
	public function rollback()
	{
		$this
			->query( 'ROLLBACK' )
			->execute();
	}
}

?>