<?php
	class Upload_File
	{
		/*	
			@param $array ( array ) { array (
					'location' => array(
						'from' => ...,
						'to' => ...
					),
					...,
				)
			}
			@return ( boolean )
		*/
		public static function upload( $array )
		{
			return ( move_uploaded_file( $array['location']['from'], $array['location']['to'] ) === true ) ? true : false;
		}	
	}
	
?>