<?php

	class Upload_Image
	{
		private static $w;
		
		private static $h;
	
		/*	
			Δημιουργεί ένα νέο image βάσει αυτού που δίνεται. Το νέο image το αφήνει όπως είναι ή 
			του κάνει resize ή crop. Στο τέλος αποθηκεύει στον σκληρό το νέο image.
			
			@param $array ( array ) { array (
					'extension' => ...,
					'location' => array(
						'from' => ...,
						'to' => ...
					),
					'resize' => array(
						'max' => array(
							'w' => ...,
							'h' => ...
						)
					),
					'crop' => array(
						'w' => ...,
						'h' => ...
					), 
					'watermark' => array(
						'color' => array(
							'red' => ...,
							'green' => ...,
							'blue' => ...
						),
						'width' => ...,
						'height' => ...,
						'path' => ...,
						'text' => ...
					),
					...,
				)
			}
			@return ( boolean )
		*/
		public static function upload( $array )
		{
			list( self::$w, self::$h ) = getimagesize( $array['location']['from'] );
			
			if( isset( $array['resize'] ) )
			{
				return ( self::resize( $array ) !== false ) ? true : false;
			}
			elseif ( isset( $array['crop'] ) )
			{
				return ( self::crop( $array ) !== false ) ? true : false;
			}
		}
		
		/*
			Δημιουργεί και επιστρέφει ένα νέο image ως αντίγραφο ενός αρχικού.
			
			@param $location ( string ) { Location του image που θα αντιγραφεί. }
			@param $extension ( string ) { Extension του image που θα αντιγραφεί. }
			@return ( mixed )
		*/
		private static function resample( $location, $extension )
		{
			switch( $extension )
			{
				case 'png':
					return ( ( $image = imagecreatefrompng( $location ) ) !== false ) ? $image : false;
				case 'jpg':
				case 'jpeg':
				case 'pjpeg':
					return ( ( $image = imagecreatefromjpeg( $location ) ) !== false ) ? $image : false;
				case 'gif':
					return ( ( $image = imagecreatefromgif( $location ) ) !== false ) ? $image : false;
			}
		}
		
		/*
			Αποθηκεύει ένα image στον σκληρό.
			
			@param $image ( image )
			@param $location ( string ){ Location που θα αποθηκευτεί το image. }
			@param $extension ( string ) { Extension του image που θα αντιγραφεί. }
			@return ( boolean )
		*/
		private static function save( $image, $location, $extension )
		{
			switch( $extension )
			{
				case 'png':
					return ( imagepng( $image, $location, 9 ) !== false ) ? true : false;
				case 'jpg':
				case 'jpeg':
				case 'pjpeg':
					return ( imagejpeg( $image, $location ) !== false ) ? true : false;
				case 'gif':
					return ( imagegif( $image, $location ) !== false ) ? true : false;
			}
		}
		
		/*
			Προσθέτει ένα watermark στο κέντρο του image.
			
			@param $image ( image )
			@param $array ( array ) { Upload array => $array['watermark']. }
			@return ( boolean )
		*/
		private static function watermark( $image, $array )
		{
			$colorIdentifier = imagecolorallocate( $image, $array['color']['red'], $array['color']['green'], $array['color']['blue'] );
			
			return ( imagettftext( $image, 10, 0, $array['position']['x'], $array['position']['y'], $colorIdentifier, $array['path'], $array['text'] ) !== false ) ? true : false;
		}
		
		/*
			Δημιουργεί αρχικά ένα image ως αντίγραφο αυτού που γίνεται upload.
			Υπολογίζει τις νέες διαστάσεις και βάσει αυτών δημιουργεί έναν image resource identifier
 			πάνω στον οποίο θα δημιουργήσει το νέο image κάνοντας σε αυτόν copy το αντίγραφο του uploaded image.
			Τέλος αποθηκεύει στον σκληρό το νέο image.
			
			@param $array ( array )
		*/
		public static function resize( $array )
		{
			//Create sample
			if( ( $resampledImg = self::resample( $array['location']['from'], $array['extension'] ) ) === false )
			{
				return false;
			}
			
			//New image dimensions
			$max = $array['resize']['max'];
			if( $max['w'] >= self::$w && $max['h'] >= self::$h )
			{
				$newWdt = self::$w;
				$newHgt = self::$h;
			}
			else
			{
				$ratio = self::$w / self::$h;
				//landscape
				if( $ratio > 1 )
				{
					$newWdt = $max['w'];
					$newHgt = $max['w'] / $ratio;
					if( $newHgt > $max['h'] )
					{
						$newHgt = $max['h'];
						$newWdt = $max['h'] * $ratio;
					}
				}
				//portrait
				elseif ( $ratio < 1 )
				{
					$newHgt = $max['h'];
					$newWdt = $max['h'] * $ratio;
					if( $newWdt > $max['w'] )
					{
						$newWdt = $max['w'];
						$newHgt = $max['w'] / $ratio;
					}
				}
				//square
				else
				{
					$newHgt = $max['h'];
					$newWdt = $max['w'];
				}
			}
			
			//New image
			$newImg = imagecreatetruecolor( $newWdt, $newHgt );
			
			//Transparency για png
			if( $array['extension'] == 'png' )
			{
				imagealphablending( $newImg, false );
	      		imagesavealpha( $newImg, true );
			}		
		
			//Copy resampled στο new image
			imagecopyresampled( $newImg, $resampledImg, 0, 0, 0, 0, $newWdt, $newHgt, self::$w, self::$h );
		
			//Watermark
			if( isset( $array['watermark'] ) )
			{
				self::watermark( $newImg, $array['watermark'] );
			}
			
			//Save
			$save = self::save( $newImg, $array['location']['to'], $array['extension'] );
			
			//Clear memory
			imagedestroy( $newImg );
			imagedestroy( $resampledImg );
			
			return ( ! $save ) ? false : true;
		}
		
		public static function crop( $array )
		{
			//Create sample
			if( ( $resampledImg = self::resample( $array['location']['from'], $array['extension'] ) ) === false )
			{
				return false;
			}
			
			//ypologismos shmeioy ekkinhshs kopsimatos (ypologizoume to kentriko shmeio toy image)
			$min = ( self::$w < self::$h ) ? self::$w : self::$h;
			$start_crop_x = self::$w/2-$min/2;
			$start_crop_y = self::$h/2-$min/2;
			
			//Temporary image
			$tmpImg = imagecreatetruecolor( $min, $min );
			//New image
			$newImg = imagecreatetruecolor( $array['crop']['w'], $array['crop']['h'] );
			
			//Transparency για png
			if( $array['extension'] == 'png' )
			{
				imagealphablending( $tmpImg, false );
	      		imagesavealpha( $tmpImg, true );
	      		imagealphablending( $newImg, false );
	      		imagesavealpha( $newImg, true );
			}	
			
			//Copy resampled στο temporary image και το τελευταίο στο νέο image.
			imagecopy( $tmpImg, $resampledImg, 0, 0, $start_crop_x, $start_crop_y, self::$w, self::$h ); 
			imagecopyresampled( $newImg, $tmpImg, 0, 0, 0, 0, $array['crop']['w'], $array['crop']['h'], $min, $min ); 
			
			//Watermark
			if( isset( $array['watermark'] ) )
			{
				self::watermark( $newImg, $array['watermark'] );
			}
			
			//Save
			$save = self::save( $newImg, $array['location']['to'], $array['extension'] );
			
			//Clear memory
			imagedestroy( $newImg );
			imagedestroy( $tmpImg );
			imagedestroy( $resampledImg );
			
			return ( ! $save ) ? false : true;
		}	
	}
	
?>