<?php 
class Plugin_Mobile extends Core_Plugin
{
    public function __construct()
    {
        parent::__construct( 'mobile' );
        
        if (stristr($_SERVER['HTTP_USER_AGENT'],'mobi') !== FALSE) {
            $GLOBALS['is_mobile'] = true;
        }
    }
}