<?php
	/*
	 * Set No cache headers
	 */
	class Plugin_Cache_Nocache extends Core_Plugin
	{
		public function __construct()
		{
			parent::__construct( 'cache_nocache' );
		}
		
		public function __destruct(){}
		
		public function setHeaders()
		{
			if( $this->isEnabled() ) {
				Core_Response::getInstance()->setHeaders( array( 
					'Expires: Tue, 01 Jan 2000 00:00:00 GMT', 
					'Last-Modified: ' . gmdate( "D, d M Y H:i:s" ) . ' GMT',
					'Cache-Control: no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0',
					'Pragma: no-cache'
				) );
			}
		}
	}
?>