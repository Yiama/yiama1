<?php
/*
 * Απαιτεί το module assets_combine
 */
class Plugin_Assets_Bundle extends Core_Plugin
{
    public function __construct()
    {
        parent::__construct( 'assets_bundle' );
    }
    
    public function __destruct(){}
    
    public function getFiles( $files )
    {
        $combined_files = '';
        $css = array();
        $js = array();
        foreach( ( array ) $files as $key => $val ) {
            foreach( array_unique( $val ) as $v ) {
                if( php_uname( 's' ) == 'Linux' ) {
                    $v = str_replace( '\\', '/', $v );
                }
                if( $key == 'css' ) {
                    $css[] = $v . '.css';
                } elseif ( $key == 'js' ) {
                    $js[] = $v . '.js';
                }
            }
        }
        $queryCss = array( 'files' => urlencode( implode( ',', $css ) ) );
        $queryJs = array( 'files' => urlencode( implode( ',', $js ) ) );
        $css_url = Core_Request::getInstance()->url(
            'module',
            array(
                'controller' => 'module',
                'modname' => 'assets',
                'modaction' => 'combine_css'
            ),
            $queryCss,
            array('application' => '')
        );
        $js_url = Core_Request::getInstance()->url(
            'module',
            array(
                'controller' => 'module',
                'modname' => 'assets',
                'modaction' => 'combine_js'
            ),
            $queryJs,
            array('application' => '')
        );
        if( ! empty( $css ) ) {
            $combined_files .= "<link rel='stylesheet' href='{$css_url}' type='text/css' />" . PHP_EOL;
        }
        if( ! empty( $js ) ) {
            $combined_files .= "<script type='text/javascript' src='{$js_url}'></script>" . PHP_EOL;
        }
        return $combined_files;
    }
}
?>