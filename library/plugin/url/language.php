<?php
	/*
	 * Ελέγχει αν το τρέχον application είναι Multilingual και παρέχει μεθόδους για την προσαρμογή των στοιχείων του Request
	 */
	class Plugin_Url_Language extends Core_Plugin
	{
		public function __construct()
		{
			parent::__construct( 'url_language' );
		}
		
		public function __destruct(){}
		
		/*
		 * Θα πρέπει τα local route name να ξεκινάνε με το 'lang_'
		 * @param $route_name string
		 */
		public function getRouteName( $route_name )
		{
			$lang_route_name = 'lang_' . $route_name;
			if( $this->isEnabled() && Core_App::getConfig( "library.core.request.routes.{$lang_route_name}"  ) ) {
				return $lang_route_name;
			}
			return $route_name;
		}
		
		/*
		 * @param $route_params array
		 */
		public function getRouteParams( $route_params )
		{
			if( $this->isEnabled() ) {
				$language = new Model_Yiama_Language();
				return array_merge( $route_params, array( 'langcode' => $language->getCurrent()->code ) );
			}
			return $route_params;
		}
	}
?>