#### Versions
- current (1.6)
    - Added environment configuration file.
- 1.5
    - Added email SMTP support.
---

#### Install

###### Apache

* Enable **.htaccess**

        <Directory ...>
            ...
            AllowOverride All
            ....

* Enable **mod_rewrite**

        a2enmod rewrite

* Enable **www redirect**

        RewriteCond %{HTTP_HOST} !^www\. [NC]
        RewriteRule ^(.*)$ http://www.%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

###### PHP

* extensions (if missing):

        sudo apt-get install php-mbstring
        sudo phpenmod mbstring

        sudo apt-get install php-gd
        sudo phpenmod gd

###### Framework

- edit file `./environment` and set the <environment-name>, then create a file `./config/<environment-name>.conf.php` which will contain the environment based configuration (e.g. database).
- add `./public/assets/images/db/` to `.gitignore`.

