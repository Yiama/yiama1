<?php
class Module_User extends Core_ModuleAbstract {
    
	public function render($args = null) {
		$links = array();
		$links['login'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'formlogin'
			)
		);
		$links['register'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'formregister'
			)
		);
		$links['logout'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'ajaxlogout'
			)
		);
		$user = new Model_Yiama_User();
		$logged_user = $user->getLogged();		
		
		$this->setView();
		$this->view->links = $links;
		$this->view->logged_user = $logged_user;
		return $this->view->render();
	}	
}