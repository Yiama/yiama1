<?php
class Module_Google_Recaptcha extends Core_ModuleAbstract {

    protected $admin;

    public function __construct() {
        parent::__construct();
        $this->admin = new Module_Google_Recaptcha_Admin();
    }
    
	public function render($args = null) {	
	    
	    $options = null;
        $config = $this->admin->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
	    if (!empty($instances)) {
	        foreach ($instances as $inst) {
	            if ($inst->getId() == $args['id']) {
	                $options = $inst->getOptions();
	                break;
	            }
	        }
	    }
	    if (empty($options->private_key) || empty($options->public_key)) {
	        return;
	    }
        
        // View
		$this->setView();
		$this->view->addVars(array('options' => $options));
		return $this->view->render();
    }	
    
    public function validate() {
        
        // Check params
        $params = $this->request->getParams();
        if( empty( $params['g-recaptcha-response'] ) ) {
            return FALSE;
        }
        
        // Options
        $options = null;
        $config = $this->admin->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        $options = $instances[0]->getOptions();
        if (empty($options->private_key) || empty($options->public_key)) {
            return;
        }
        
        // Request recaptcha site
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => $options->private_key,
            'response' => $params['g-recaptcha-response']
        );
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $result_decoded = json_decode($result);
        
        return $result_decoded->success;
    }
}