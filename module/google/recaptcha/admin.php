<?php
class Module_Google_Recaptcha_Admin extends Core_ModuleAdminAbstract{

    protected $name = "google_recaptcha";

    public function __construct() {
        parent::__construct();
    }
    
    public function render($args = null) {

        // Config
        $config = $this->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        
        // Buttons
        $form = Helper_Buttons::get(array('save' => null));
        
        // Form
        $form .= Helper_Form::openForm(Core_Request::getInstance()->url( 
            'adminmodule', 
            array(     
                'controller' => 'adminmodule',
                'modname' => 'google_recaptcha', 
                'modaction' => 'save'
            )
        ));
        $form .= Helper_Form::openWrapperColumnLeft();
        
        // Last id
        $form .= Helper_Form::input(
            'last_instance_id',
            '-1',
             null,
             array('type' => 'hidden')
        );
        
        // Options
        $form .= Helper_Form::openWrapperModule('Options');
        $id = 'item_0';
        $instance = !empty($instances[0]) ? $instances[0] : null;
        $options = !empty($instance) ? $instance->getOptions() : null;
        $form .= Helper_Form::input(
            "instance[$id][options][private_key]",
            !empty($options->private_key) ? $options->private_key.'' : '',
            null,
            null,
            'Private key'
        );
        $form .= Helper_Form::input(
            "instance[$id][options][public_key]",
            !empty($options->public_key) ? $options->public_key.'' : '',
            null,
            null,
            'Public key'
        );
        $form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit('Submit');
        $form .= Helper_Form::closeForm();
        
        return $form;
    }
    
	public function save() {	
        $request = Core_Request::getInstance();
        $params = $request->getParams();
		$this->checkCsrf($params);
        $warning = [];
        $error = null;

        // Save
        $config = new Core_ModuleConfig();
		if (!empty($params['instance'])) {
		    // Update
		    foreach ($params['instance'] as $id => $params_inst) {
		        
		        // Options
		        $options = new stdClass();
		        $options->private_key = $params_inst['options']['private_key'];
		        $options->public_key = $params_inst['options']['public_key'];
		        
		        // Create instance
		        $instance = new Core_ModuleInstance($id);
		        $instance->setOptions($options);
		        $config->addInstance($instance);
		    }
		}

        // Save config
        $this->saveConfig($config);
        
        // Redirect
        Core_Response::getInstance()->redirect(
            $request->url( 
                'adminmodule', 
                array(     
                    'controller' => 'adminmodule',
                    'modname' => 'google_recaptcha'
                )
            ), 
            null,
            array('insert', $error, array_filter($warning) 
        ));
    }
}