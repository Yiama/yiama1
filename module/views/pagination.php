<?php Core_HTML::addFiles( 'mod_pagination\index.css' ); ?>
<!-- pagination ( -->
<?php
	if( $this->buttons ) {
		echo"<div class='pagination'>\n";
		echo ( $this->first ) ? "    <a href='{$this->first['link']}' class='first'></a>\n" : "";
		echo ( $this->previous ) ? "    <a href='{$this->previous['link']}' class='prev'></a>\n" : "";
		foreach( $this->buttons as $b ) {	
			echo"    <a href='{$b['link']}'" . ( ( $b['current'] )  ? " class='selected'" : "" ) . ">{$b['title']}</a>\n";
		}
		echo ( $this->next ) ? "    <a href='{$this->next['link']}' class='next'></a>\n" : "";
		echo ( $this->last ) ? "    <a href='{$this->last['link']}' class='last'></a>\n" : "";
		echo"</div>\n";
	}
?>
<!-- ) pagination -->