<?php
    Core_HTML::addFullHeaders( 
          "<script type='text/javascript'"
        . " src='https://www.google.com/recaptcha/api.js'></script>" 
    );
?>
<style>
    iframe {
        width: auto;
        height: auto;
    }
</style>
<div class="g-recaptcha" 
     data-sitekey="<?php echo $this->options->public_key; ?>">
</div>