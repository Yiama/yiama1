<?php
Core_HTML::addFullHeaders("
<script async defer
  src='https://maps.googleapis.com/maps/api/js'>
</script>"
);
?>
<div id='google-map'></div>
<script type="text/javascript">
	var ymMap = {};
    	ymMap.options = JSON.parse('<?php echo str_replace(array("\\r\\n", "\\n"), "<br />", json_encode($this->options)); ?>');
    	ymMap.markers = JSON.parse('<?php echo str_replace(array("\\r\\n", "\\n"), "<br />", json_encode($this->markers)); ?>');
    var map = new google.maps.Map(document.getElementById('google-map'), {
        zoom: ymMap.options.zoom_level == '' ? 8 : parseInt(ymMap.options.zoom_level),
        center: new google.maps.LatLng(ymMap.options.center_lat, ymMap.options.center_long),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: ymMap.options.style == '' ? [] : ymMap.options.style
    });
        
    var infowindow = new google.maps.InfoWindow({ 
        size: new google.maps.Size(250,150)
    });
    
    var marker, i;
    
    for (i = 0; i < ymMap.markers.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(ymMap.markers[i].latitude, ymMap.markers[i].longitude),
            map: map,
            ymLabel: ymMap.markers[i].label
        });
            
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(marker.ymLabel);
                infowindow.open(marker.get('map'), marker);
            }
        })(marker, i));
    }
</script>