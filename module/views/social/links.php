<?php 
	Core_HTML::addFiles('module\social\links.css'); 
?>
<div class='social_links'>
	<?php foreach((array) $this->links as $link ): ?>
	<a 
		class='<?php echo $link->network.' '.$this->options->style; ?>' 
		href='<?php echo $link->url; ?>'
		target='_blank'
	></a>
	<?php endforeach; ?>
</div>