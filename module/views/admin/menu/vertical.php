<?php 
Core_HTML::addFiles('admin\mod_menu\vertical.css');
/**
 * Note: Shows only 2 menu levels
 */
$request = Core_Request::getInstance();
?>
<div class='mod_menu_vertical'>
    <div class="user">
        <img src="<?php echo $this->user->getImage(); ?>" />
        <div class="info"><?php echo $this->user->username; ?></div>
        <div class="links">
            <a  class="settings"
                href="<?php echo $this->user->link_edit; ?>"></a>
            <a  class="logout"
                href="<?php echo $this->user->link_logout; ?>"></a>
        </div>
    </div>
    <div class="menu">
        <?php foreach($this->items as $key => $item): ?>
        <div class="item level1 
            <?php echo !empty($item->children) ? ' parent' :''; ?>
            <?php echo $item->is_current_parent || $item->is_current ? ' active' : ''; ?>">
        <?php if(!empty($item->children)): ?>
            <a>
                <?php echo $item->title; ?>
            </a>
            <div class="children">
                <?php foreach($item->children as $child): ?>
                <div class="item level2 <?php echo $child->is_current ? 'active' : ''; ?>">
                    <a  href="<?php echo $request->urlFromPath('/' . $child->url); ?>">
                        <?php echo $child->title; ?>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <a  href="<?php echo $request->urlFromPath('/' . $item->url); ?>">
                <?php echo $item->title; ?>
            </a>
        <?php endif; ?>
        </div>
        <?php endforeach; ?>
    </div>
</div>