<?php 
	Core_HTML::addFiles( 'mod_menu\categories.css' ); 
?>
<li>
    <div id='modMenuCategories'>
        <?php foreach( $this->categories as $cat ): ?>
        <div class='item'>
            <div class="img"
                <?php $img = $cat->getImage()->normal; ?>
                 style="background-image: url( '<?php echo $img; ?>' )"
            ></div>
            <a href="<?php echo$cat->link; ?>"><?php echo $cat->title; ?></a>
        </div>
        <?php endforeach; ?>
    </div>
</li>
<script type='text/javascript'>
    /*
    window.addEventListener( 'load', function() {
        var wrapper = document.getElementsByClassName( 'outerWrapper menu' )[0]
                .children[0];
        var el = document.getElementById( 'modMenuCategories' );
            el.style.width = wrapper.offsetWidth + "px";
    } );
    window.addEventListener( 'resize', function() {
        var wrapper = document.getElementsByClassName( 'outerWrapper menu' )[0]
                .children[0];
        var el = document.getElementById( 'modMenuCategories' );
            el.style.width = wrapper.offsetWidth + "px";
    } );
    */
</script>


