<?php 
	Core_HTML::addFiles( 'module/menu/footer.css' ); 
?>
<div class='mod-menu-footer'>
    <ul>
        <?php foreach ($this->pages as $page): ?>
		<?php $is_current1 = !empty($this->current) && $page->id == $this->current->id; ?>
        <li>
            <?php if (!empty($page->url)): ?>
            <?php $url = Core_Request::getInstance()->urlFromPath($page->url); ?>
            <a 
            	href="<?php echo $url; ?>"
            	<?php echo $is_current1 ? " class='active'" : ''; ?>
            	><?php echo $page->title; ?></a>
            <?php else: ?>
            <span><?php echo $page->title; ?></span>
            <?php endif; ?>
            <?php if (!empty($parent->is_parent)): ?>
            <ul>
        		<?php foreach ($page->children as $child): ?>
        		<?php $is_current2 = !empty($this->current) && $child->id == $this->current->id; ?>
                <li>
                    <?php if (!empty($page->url)): ?>
            		<?php $url = Core_Request::getInstance()->urlFromPath($child->url); ?>
                    <a 
                    	href="<?php echo $url; ?>"
                    	<?php echo $is_current2 ? " class='active'" : ''; ?>
                    	><?php echo $page->title; ?></a>
                    <?php else: ?>
                    <span><?php echo $page->title; ?></span>
                    <?php endif; ?>
                </li>
    			<?php endforeach;?>
            </ul>
            <?php endif; ?>
        </li>
    <?php endforeach;?>
    </ul>
</div>

