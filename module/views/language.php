<div class="mod-languages">
    <?php if(!empty($this->languages)): ?>
        <?php foreach ($this->languages as $language): ?>
            <div class="item">
                <a href="<?php echo $language->link; ?>"><?php echo $language->code; ?></a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>