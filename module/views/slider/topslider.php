<?php 
Core_HTML::addFiles(array(
    'module/topslider.css',
	'module/slider/core.js',
    'module/slider/transition.js',
    'module/slider/behavior.js'
)); 
?>
<div class='outerWrapper slider top'>
	<div class='slider-button left' onclick="theSlider.previous();"></div>
	<div class='slider-button right' onclick="theSlider.next();"></div>
	<?php foreach($this->slides as $key => $slide): ?>	
		<?php if ($slide->show_title || $slide->show_text): ?>
    		<?php 
    		if ($slide->use_article_link) {
    		    $link = Core_Request::getInstance()->urlFromPath($slide->article->url);
    		} else {
    		    $link = $slide->link;
    		}
    		?>
        	<div class='slider-text'>
    			<?php if ($slide->show_title): ?>
                <div class="title">
                    <?php echo $slide->article->title; ?>
                </div>
    			<?php endif;?>
    			<?php if ($slide->show_text): ?>
    			<br />
                <div class="description">
                    <?php echo $slide->article->description; ?>
                </div>
    			<?php endif;?>
    			<?php if ($slide->show_button && !empty($link)): ?>
    			<br /><a 
    				class="button"
    				href="<?php echo $link; ?>" 
    			>ΠΕΡΙΣΣΟΤΕΡΑ</a>
    			<?php endif;?>
    		</div>
		<?php endif;?>
	<?php endforeach; ?>
	<div id='slider' class='slider-wrapper'>
	<?php foreach( $this->slides as $slide ): ?>
		<?php if (($img = $slide->article->getDefaultImage())): ?>
		<div class='slider-slide-opacity'
             style="background-image: url( '<?php echo $img->getNormal(); ?>' );"
             <?php if ($slide->use_link_body && !empty($link)): ?>
		     onclick="window.location.href='<?php echo $link; ?>'"
    	     <?php endif; ?>
        ></div>
        <?php endif; ?>
	<?php endforeach; ?>
	</div>
</div>
<script type="text/javascript">
    /**
     * ----------
     * Top slider
     * ----------
     */
     var theSlider = new slider( {
     	'wrapper': document.getElementById( 'slider' )
     } );
     theSlider.setTransition( new sliderTransition( theSlider, {
         effect: 'opacity',
         duration: <?php echo !empty($this->options->transition_duration) ? $this->options->transition_duration : '1000'; ?>
     } ) );
     theSlider.registerEvent( 'onTransitionStart', function( slider ) {
         var texts = document.getElementsByClassName( 'slider-text' );
         for( var i = 0; i < texts.length; i++ ) {
             if( i == slider.currentIndex ) {
                 texts[i].style.left = '-100%';
             }
         }
     } );
     theSlider.registerEvent( 'onTransitionComplete', function( slider ) {
         var texts = document.getElementsByClassName( 'slider-text' );
         for( var i = 0; i < texts.length; i++ ) {
             if( i == slider.currentIndex ) {
                 texts[i].style.left = '50%';
                 texts[i].style.zIndex = 10;
             } else {
                 texts[i].style.left = '-100%';
                 texts[i].style.zIndex = 9;
         	}
         }
     } );
     sliderBehaviors( theSlider, {
     autoplay: {
     sleep : <?php echo !empty($this->options->sleep_duration) ? $this->options->sleep_duration : '5000'; ?>
     },
     responsive: {
     sizeRatio: 0.3
     }
     } );
     theSlider.start();
 </script>