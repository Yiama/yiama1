<?php 
Core_HTML::addFiles( array(
//    'module\form\contact.css',
//    'library/jquery/jquery-3.3.1.min.js'
) ); 
?>
<div class="form-contact">
    <h1><?php echo $this->form_result->title; ?></h1>
    <div class="text"><?php echo $this->form_result->text; ?></div>
    <div class="form"><?php echo $this->form; ?></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.form-contact form').on('submit', function(e) {
			e.preventDefault();
			$.ajax({
           		type: "POST",
           		url: $('.form-contact form').attr('action'),
           		data: $('.form-contact form').serialize(),
           		success: function(data) {
               		if (data.status) {
                   		$('.form-message').addClass('success');
               		} else {
                   		$('.form-message').addClass('error');
               		}
                    $('.form-message')
                        .addClass('show')
                        .html(data.message);
                    window.scrollTo(0, 0);
					window.formMessageTimeout = setTimeout(function() {
           				$('.form-message')
           					.removeClass('show')
           					.removeClass('success')
           					.removeClass('error')
           					.html("");
           				clearTimeout(window.formMessageTimeout);
					}, 5000);
	           	}
         	});
		});
	});
</script>