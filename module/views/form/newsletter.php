<?php
    Core_HTML::addFiles('library\jquery\jquery-3.3.1.min.js');
?>
<?php echo $this->form; ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#formNewsletter').on('submit', function(e) {
			e.preventDefault();
			$.ajax({
           		type: "POST",
           		url: $('#formNewsletter').attr('action'),
           		data: $('#formNewsletter').serialize(),
           		success: function(json) {
           		    data = JSON.parse(json);
               		if (data.status) {
                   		$('.form-message').addClass('success');
               		} else {
                   		$('.form-message').addClass('error');
               		}
                    $('.form-message')
                        .addClass('show')
                        .html(data.message);
					window.formMessageTimeout = setTimeout(function() {
           				$('.form-message')
           					.removeClass('show')
           					.removeClass('success')
           					.removeClass('error')
           					.html("");
           				clearTimeout(window.formMessageTimeout);
					}, 5000);
	           	}
         	});
		});
	});
</script>