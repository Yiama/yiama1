<?php 
	Core_HTML::addFiles( array(
		'mod_user\index.css',
		'library\ajax.js'
	) ); 
	$language = new Model_Yiama_Language();
	$translate = Core_Translate::get( $language->getCurrent()->code, 'mod_user' );
?>
<!-- module user ( -->
<?php if( $this->logged_user ): ?>
<a id='logoutButton' title='<?php echo $translate['logout']; ?>' class='button-logout'><?php echo $this->logged_user->username; ?></a>
<?php else: ?>
<a href='<?php echo $this->links['login']; ?>' class='button-login'><?php echo $translate['login']; ?></a>
<?php endif; ?>
<script type='text/javascript'>
	if( document.getElementById( 'logoutButton' ) ) {
		document.getElementById( 'logoutButton' ).addEventListener( 'click', function() {
			_ajaxRequest( {
				method        : 'post',
				url           : '<?php echo $this->links['logout']; ?>',
				successhandler: function( responseText ){
					response = JSON.parse( responseText );
					if( response.status ) {
						window.location.href = '<?php echo Core_Request::getInstance()->getURI(); ?>';
					}
				}
			} );
		} );
	}
</script>
<!-- ) module user -->