<?php Core_HTML::addFiles(array(
    'library/jquery/jquery-3.3.1.min.js',
    'module/parallax.js',
    'module/parallax.css'  
)); ?>
<div id="<?php echo $this->dom_element_id; ?>">
    <div></div>
    <div class="inner-section">
    	<h1><?php echo $this->article->title; ?></h1>
    	<div><?php echo $this->article->description; ?></div>
    	<a 
    		class="button"
    		href="<?php echo Core_Request::getInstance()->urlFromPath('article/about'); ?>"
    		>ΠΕΡΙΣΣΟΤΕΡΑ</a>
    </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#<?php echo $this->dom_element_id; ?>').ymParallax();
	});
</script>