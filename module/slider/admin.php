<?php
class Module_Slider_Admin extends Core_ModuleAdminAbstract{

    protected $name = "slider";

	public function __construct() {
	    parent::__construct();
    }
    
    public function render($args = null) {	
        
	    // HTML
	    Core_HTML::addStyle(
	        '#main_form .element label{width: 100px;}'
	        .'#main_form .element{padding: 0;}'
	        .'#main_form .element select{display: inline-block; width: 100px;}'
	        .'#main_form .element:last-child{padding-bottom: 30px;}'
	        .'#main_form .element:last-child select{display: block; width: 100%;}'
	        .'#main_form .element.new-instance{display: inline-block; width: auto; padding: 10px; border: 2px solid #F1586C;}'
	        .'#main_form .element.new-instance label{display: inline-block; font-size: 14pt;}'
        );
		
        // Config
        $config = $this->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        
        // Buttons
        $form = Helper_Buttons::get(array('save' => null));
        
        // Tabs
        $tabs = array();
        foreach ($instances as $inst) {
            $tabs[$inst->getId()] = $inst->getId();
        }
        $form .= Helper_Form::tabMenu($tabs);
        
        // Form
        $form .= Helper_Form::openForm(Core_Request::getInstance()->url( 
            'adminmodule', 
            array(     
                'controller' => 'adminmodule',
                'modname' => 'slider', 
                'modaction' => 'save'
            )
        ));
		$form .= Helper_Form::openWrapperColumnLeft();
		
		// New instance
		$form .= Helper_Form::checkbox(
		    'new_instance',
		    null,
		    null,
		    null,
		    'New Slider',
		    array('class' => 'new-instance')
	    );
		
		// Last id
		$form .= Helper_Form::input(
		    'last_instance_id',
            empty($config) || empty($config->getLastId()) ? '-1' : $config->getLastId(),
		    null,
		    array('type' => 'hidden')
	    );
		
		// Existed instances
		$model_article = new Model_Yiama_Article();
		$articles = $model_article->query()
            ->order('local.title')
            ->find();
        $index = 0;
        foreach ($instances as $instance) {
            $id = $instance->getId();
            $form .= Helper_Form::openTabItem($id, !$index++);
            
            // Options
            $form .= Helper_Form::openWrapperModule('Options');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][delete]",
                0,
                null,
                '<b>Delete slider</b>',
                null,
                null,
                array('no_empty' => true)
            );
            $options = $instance->getOptions();
            $form .= Helper_Form::input(
                "instance[$id][options][transition_duration]",
                !empty($options->transition_duration) ? $options->transition_duration.'' : '',
                null,
                null,
                'Transition duration'
                );
            $form .= Helper_Form::input(
                "instance[$id][options][sleep_duration]",
                !empty($options->sleep_duration) ? $options->sleep_duration.'' : '',
                null,
                null,
                'Sleep duration'
            );
            $form .= Helper_Form::closeWrapperModule();
            
            // New slide
            $form .= Helper_Form::openWrapperModule('New Slide');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][slide]",
                0,
                null,
                '<b>New slide</b>',
                null,
                null,
                array('no_empty' => true)
            );
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][show_title]",
                0,
                null,
                'Show title',
                null,
                null,
                array('no_empty' => true)
            );
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][show_text]",
                0,
                null,
                'Show text',
                null,
                null,
                array('no_empty' => true)
                );
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][show_button]",
                0,
                null,
                'Show button',
                null,
                null,
                array('no_empty' => true)
            );
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][use_article_link]",
                0,
                null,
                'Use article link',
                null,
                null,
                array('no_empty' => true)
                );
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][use_link_body]",
                0,
                null,
                'Link body',
                null,
                null,
                array('no_empty' => true)
                );
            $form .= Helper_Form::input(
                "instance[$id][new][link]",
                '',
                null,
                null,
                'Link'
                );
            $form .= Helper_Form::select(
                "instance[$id][new][article_id]",
                $articles,
                'id',
                'title',
                null,
                null,
                null,
                'Article'
                );
            $form .= Helper_Form::closeWrapperModule();
            
            // Existed slides
            if (!empty($instance->getParams())) {
                foreach ($instance->getParams() as $key => $item) {
    		        $form .= Helper_Form::openWrapperModule('Slide: <strong>'.($key+1).'</strong>');
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][delete]",
    		            0,
    		            null,
    		            '<b>Delete</b>',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][show_title]",
    		            $item->show_title.'',
    		            null,
    		            'Show title',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][show_text]",
    		            $item->show_text.'',
    		            null,
    		            'Show text',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][show_button]",
    		            $item->show_button.'',
    		            null,
    		            'Show button',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][use_article_link]",
    		            $item->use_article_link.'',
    		            null,
    		            'Use article link',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][slides][$key][use_link_body]",
    		            $item->use_link_body.'',
    		            null,
    		            'Link body',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		            );
                    $form .= Helper_Form::input(
                        "instance[$id][slides][$key][link]",
                        $item->link,
                        null,
                        null,
                        'Link'
                    );
                    $form .= Helper_Form::select(
                        "instance[$id][slides][$key][article_id]", 
                        $articles, 
                        'id', 
                        'title',
                        'id',
                        $item->article_id ? $item->article_id : null,
                        null,
                        'Article'
                    );
                    $form .= Helper_Form::closeWrapperModule();
    		    }
            }
            $form .= Helper_Form::closeTabItem();
		}
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit('Submit');
        $form .= Helper_Form::closeForm();
        
        return $form;
    }
    
	public function save() {	
        $request = Core_Request::getInstance();
        $params = $request->getParams();
		$this->checkCsrf($params);
        $warning = [];
        $error = null;
		
        // Save
		$config = new Core_ModuleConfig();
		if (!empty($params['instance'])) {
            // Update
		    foreach ($params['instance'] as $id => $params_inst) {
		        if (empty($params_inst['delete'])) {
		            
		            // Options
		            $options = new stdClass();
		            $options->transition_duration = $params_inst['options']['transition_duration'];
		            $options->sleep_duration = $params_inst['options']['sleep_duration'];
		            
		            // Params
                    $slides = array();
                    if (!empty($params_inst['slides'])) {
                        foreach ($params_inst['slides'] as $key => $slide) {
                            if (empty($params_inst['slides'][$key]['delete'])) {
                                $slide = new stdClass();
                                $slide->show_title = !empty($params_inst['slides'][$key]['show_title']);
                                $slide->show_text = !empty($params_inst['slides'][$key]['show_text']);
                                $slide->show_button = !empty($params_inst['slides'][$key]['show_button']);
                                $slide->use_article_link = !empty($params_inst['slides'][$key]['use_article_link']);
                                $slide->use_link_body = !empty($params_inst['slides'][$key]['use_link_body']);
                                $slide->link = $params_inst['slides'][$key]['link'];
                                $slide->article_id = !empty($params_inst['slides'][$key]['article_id'])
                                    ? $params_inst['slides'][$key]['article_id']
                                    : '';
                                $slides[] = $slide; 
                            }
                        }
                    }
                    
                    // New
                    if (!empty($params_inst['new']['slide'])) {
                        $slide = new stdClass();
                        $slide->show_title = !empty($params_inst['new']['show_title']);
                        $slide->show_text = !empty($params_inst['new']['show_text']);
                        $slide->show_button = !empty($params_inst['new']['show_button']);
                        $slide->use_article_link = !empty($params_inst['new']['use_article_link']);
                        $slide->use_link_body = !empty($params_inst['new']['use_link_body']);
                        $slide->link = $params_inst['new']['link'];
                        $slide->article_id = !empty($params_inst['new']['article_id'])
                            ? $params_inst['new']['article_id']
                            : '';
                        $slides[] = $slide; 
                    }
                    
                    // Create instance
                    $instance = new Core_ModuleInstance($id);
                    $instance->setParams($slides);
                    $instance->setOptions($options);
                    $config->addInstance($instance);
		        }
            }
        }
        if (!empty($params['new_instance'])) {
            if ($params['last_instance_id'] == '-1') {
                $params['last_instance_id'] = 0;
            }
            $last_id = intval($params['last_instance_id'])+1;
            $config->addInstance(new Core_ModuleInstance('item_'.$last_id));
            $config->setLastId($last_id);
        }

        // Save config
        $this->saveConfig($config);

        // Redirect
        Core_Response::getInstance()->redirect(
            $request->url( 
                'adminmodule', 
                array(     
                    'controller' => 'adminmodule',
                    'modname' => 'slider'
                )
            ), 
            null,
            array('insert', $error, array_filter($warning))
        );
    }
}