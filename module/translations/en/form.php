<?php
	return array(
	    'submission' => 'Submit',
	    'name' => 'Name',
        'subject' => 'Subject',
	    'message' => 'Message',
		'text' => 'This email will be used for sending newsletter',
	    'no_recipients_specified' => 'No recipients specified',
	    'no_emails_send' => 'Email failed to send',
	    'recaptcha_failed' => 'Security check failed',
	    'success' => 'Send was successful',
        'messages.formFieldError' => 'Some fields are wrong',
        'messages.formSendError' => 'Already registered',
        'messages.formSendSuccess' => 'Successful registration.',
		'messages' => array(
			'formFieldError' => 'Some fields are wrong',
			'formSendError' => 'Already registered',
			'formSendSuccess' => 'Successful registration.'
		)
	);
?>