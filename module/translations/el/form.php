<?php
    return array(
        'submission' => 'Αποστολή',
        'name' => 'Όνομα',
        'subject' => 'Θέμα',
        'message' => 'Μήνυμα',
        'text' => 'Το παρακάτω email δόθηκε για την αποστολή newsletter',
        'no_recipients_specified' => 'ΔΕν έχουν οριστεί παραλήπτες',
        'no_emails_send' => 'Η αποστολή email απέτυχε',
        'recaptcha_failed' => 'Ο έλεγχος ασφαλείας απέτυχε',
        'success' => 'Η αποστολή ήταν επιτυχής',
        'messages.formFieldError' => 'Ορισμένα πεδία δεν συμπληρώθηκαν σωστά',
        'messages.formSendError' => 'Έχετε εγγραφεί ήδη',
        'messages.formSendSuccess' => 'Η εγγραφή σας ήταν επιτυχής.',
        'messages' => array(
            'formFieldError' => 'Ορισμένα πεδία δεν συμπληρώθηκαν σωστά',
            'formSendError' => 'Έχετε εγγραφεί ήδη',
            'formSendSuccess' => 'Η εγγραφή σας ήταν επιτυχής.'
        )
    );
?>