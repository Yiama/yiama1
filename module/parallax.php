<?php
class Module_Parallax extends Core_ModuleAbstract {
    
    /**
     * $args[<article_id>, <dom_element_id>]
     */
    public function render($args = null) {
        if (empty($args['article'])) {
            return;
        }
            
        // View
        $this->setView();
        $this->view->addVars(array(
            'article' => $args['article'],
            'dom_element_id' => $args['dom_element_id']
        ));
        return $this->view->render();
    }
}
        