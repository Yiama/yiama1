<?php
class Module_Pagination extends Core_ModuleAbstract {
	/*
	 * @uses pagination_total_content int { count all content } 
	 */
	public function render($args = null) {
		$request_uri = Core_Request::getInstance()->getURI();
		$config_params = Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.modules.pagination' );
		$page = ( $p = Core_Request::getInstance()->getParams( 'page' ) ) ? $p : 1;
		if(isset($args['content_per_page'])) {
            $content_per_page = $args['content_per_page'];
        } elseif ( isset( $config_params['content_per_page'] ) ) { /* Δίνεται στο config */
            $content_per_page = $config_params['content_per_page'];
        } else {
            $content_per_page = 10;
        }
		if(isset($args['max_buttons'])) { /* ! Μόνο ζυγούς αριθμούς. */
			$butMaxShow = $max_buttons;
		} elseif ( isset( $config_params['max_buttons'] ) ) {  /* Δίνεται στο config */
			$butMaxShow = $config_params['max_buttons'];
		} else {
			$butMaxShow = 10;
		}
		
		/* Buttons */
		$buttons = null;
		$first = null;
		$previous = null;
		$next = null;
		$last = null;
		if( $args['total_content'] > $content_per_page ) {
			$butCount = ceil( $args['total_content'] / $content_per_page );
			$butMin = $page - floor( $butMaxShow / 2 ); 
			$butMax = $page + floor( $butMaxShow / 2 );
			for( $i = 0; $i < $butCount; $i++ ) {
				if( $i+1 >= $butMin && $i < $butMax ) {
					$buttons[$i]['title'] = $i+1;
					$buttons[$i]['link'] = Core_URL::addQuery( $request_uri, array( 'page' => $i+1 ) );
					$buttons[$i]['current'] = ( $page == $i+1 ) ? true : false;
				}
			}
			if( $butMin > 1 ) {
				$first['link'] = Core_URL::addQuery( $request_uri, array( 'page' => 1 ) );
				$previous['link'] = Core_URL::addQuery( $request_uri, array( 'page' => $page-1 ) );
			}
			if( $butMaxShow < $butCount && $butMax < $butCount ) {
				$next['link'] = Core_URL::addQuery( $request_uri, array( 'page' => $page+1 ) );
				$last['link'] = Core_URL::addQuery( $request_uri, array( 'page' => $i ) );
			}
		}
		
		/* View */
		$this->setView();
		$this->view->addVars( array( 'buttons' => $buttons, 'first' => $first, 'previous' => $previous, 'next' => $next, 'last' => $last ) );
		return $this->view->render();
	}	
}