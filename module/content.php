<?php
class Module_Content extends Core_ModuleAbstract {

    protected $admin;

    public function __construct() {
        parent::__construct();
        $this->admin = new Module_Content_Admin();
    }

    public function render($args = null) {

        $content = array();
        $config = $this->admin->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        if (!empty($instances)) {
            foreach ($instances as $inst) {
                $options = $inst->getOptions();
                if ($inst->getId() == $args['id']) {
                    $content = $inst->getParams();
                    break;
                }
            }
        }
        if (empty($content)) {
            return;
        }

        $results = array();
        foreach ($content as $key => $item) {
            if ($content[$key]['type'] == "article") {
                $model = new Model_Yiama_Article();
                $result = $model->findByKey($content[$key]['article_id']);
                if (!empty($result)) {
                    $results[] = $result;
                }
            } elseif ($content[$key]['type'] == "category") {
                $model = new Model_Yiama_Category();
                $result = $model->findByKey($content[$key]['category_id']);
                if (!empty($result)) {
                    $results[] = $result;
                }
            }
        }

        // View
        $this->setView(__DIR__.DS."views/content/{$args['view']}");
        $this->view->addVars(array(
            'options' => $options,
            'content' => $results
        ));
        return $this->view->render();
    }
}