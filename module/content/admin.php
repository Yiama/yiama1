<?php
class Module_Content_Admin extends Core_ModuleAdminAbstract{

    protected $name = "content";

    public function __construct() {
        parent::__construct();
    }
    
    public function render($args = null) {

	    // HTML
	    Core_HTML::addStyle(
	        '#main_form .element label{width: 100px;}'
	        .'#main_form .element{padding: 0;}'
	        .'#main_form .element select{display: inline-block; width: 100px;}'
	        .'#main_form .element:last-child{padding-bottom: 30px;}'
	        .'#main_form .element:last-child select{display: block; width: 100%;}'
	        .'#main_form .element.new-instance{display: inline-block; width: auto; padding: 10px; border: 2px solid #F1586C;}'
	        .'#main_form .element.new-instance label{display: inline-block; font-size: 14pt;}'
        );

        // Config
        $config = $this->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        
        // Buttons
        $form = Helper_Buttons::get(array('save' => null));
        
        // Tabs
        $tabs = array();
        foreach ($instances as $inst) {
            $tabs[$inst->getId()] = $inst->getId();
        }
        $form .= Helper_Form::tabMenu($tabs);
        
        // Form
        $form .= Helper_Form::openForm(Core_Request::getInstance()->url( 
            'adminmodule', 
            array(     
                'controller' => 'adminmodule',
                'modname' => 'content',
                'modaction' => 'save'
            )
        ));
		$form .= Helper_Form::openWrapperColumnLeft();
		
		// New instance
		$form .= Helper_Form::checkbox(
		    'new_instance',
		    null,
		    null,
		    null,
		    'New Content Group',
		    array('class' => 'new-instance')
	    );
		
		// Last id
		$form .= Helper_Form::input(
		    'last_instance_id',
            empty($config) || empty($config->getLastId()) ? '-1' : $config->getLastId(),
		    null,
		    array('type' => 'hidden')
	    );
		
		// Existed instances
        $model_category = new Model_Yiama_Category();
        $categories = $model_category->query()
            ->order('local.title')
            ->find();
		$model_article = new Model_Yiama_Article();
		$articles = $model_article->query()
            ->order('local.title')
            ->find();
        $index = 0;
        foreach ($instances as $instance) {
            $id = $instance->getId();
            $form .= Helper_Form::openTabItem($id, !$index++);
            
            // Options
            $form .= Helper_Form::openWrapperModule('Options');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][delete]",
                0,
                null,
                '<b>Delete content group</b>',
                null,
                null,
                array('no_empty' => true)
            );
            $options = $instance->getOptions();
            $form .= Helper_Form::input(
                "instance[$id][options][option1]",
                !empty($options->option1) ? $options->option1.'' : '',
                null,
                null,
                'Option 1'
            );
            $form .= Helper_Form::input(
                "instance[$id][options][option2]",
                !empty($options->option2) ? $options->option2.'' : '',
                null,
                null,
                'Option 2'
            );
            $form .= Helper_Form::closeWrapperModule();

            $s1 = new stdClass();
            $s1->id = "article";
            $s1->title = "Article";
            $s2 = new stdClass();
            $s2->id = "category";
            $s2->title = "Category";

            // New content
            $form .= Helper_Form::openWrapperModule('New Content');
            $form .= Helper_Form::select(
                "instance[$id][new][type]",
                [$s1, $s2],
                'id',
                'title',
                null,
                null,
                null,
                'Type'
            );
            $form .= Helper_Form::select(
                "instance[$id][new][category_id]",
                $categories,
                'id',
                'title',
                null,
                null,
                null,
                'Category'
            );
            $form .= Helper_Form::select(
                "instance[$id][new][article_id]",
                $articles,
                'id',
                'title',
                null,
                null,
                null,
                'Article'
            );
            $form .= Helper_Form::closeWrapperModule();
            
            // Existed slides
            if (!empty($instance->getParams())) {
                foreach ($instance->getParams() as $key => $item) {
    		        $form .= Helper_Form::openWrapperModule('Content: <strong>'.($key+1).'</strong>');
    		        $form .= Helper_Form::selectBoolean(
    		            "instance[$id][content][$key][delete]",
    		            0,
    		            null,
    		            '<b>Delete</b>',
    		            null,
    		            null,
    		            array('no_empty' => true)
    		        );
                    $form .= Helper_Form::select(
                        "instance[$id][content][$key][type]",
                        [$s1, $s2],
                        'id',
                        'title',
                        'id',
                        $item->type ? $item->type : null,
                        null,
                        'Type'
                    );
                    $form .= Helper_Form::select(
                        "instance[$id][content][$key][category_id]",
                        $categories,
                        'id',
                        'title',
                        'id',
                        $item->category_id ? $item->category_id : null,
                        null,
                        'Category'
                    );
                    $form .= Helper_Form::select(
                        "instance[$id][content][$key][article_id]",
                        $articles, 
                        'id', 
                        'title',
                        'id',
                        $item->article_id ? $item->article_id : null,
                        null,
                        'Article'
                    );
                    $form .= Helper_Form::closeWrapperModule();
    		    }
            }
            $form .= Helper_Form::closeTabItem();
		}
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit('Submit');
        $form .= Helper_Form::closeForm();
        
        return $form;
    }
    
	public function save() {	
        $request = Core_Request::getInstance();
        $params = $request->getParams();
		$this->checkCsrf($params);
        $warning = [];
        $error = null;

        // Save
        $config = new Core_ModuleConfig();
		if (!empty($params['instance'])) {
            // Update
		    foreach ($params['instance'] as $id => $params_inst) {
		        if (empty($params_inst['delete'])) {
		            
		            // Options
		            $options = new stdClass();
		            $options->option1 = $params_inst['options']['option1'];
                    $options->option2 = $params_inst['options']['option2'];
		            
		            // Params
                    $content = array();
                    if (!empty($params_inst['content'])) {
                        foreach ($params_inst['content'] as $key => $item) {
                            if (empty($params_inst['content'][$key]['delete'])) {
                                $item = new stdClass();
                                $item->type = $params_inst['content'][$key]['type'];
                                $item->category_id = !empty($params_inst['content'][$key]['category_id'])
                                    ? $params_inst['content'][$key]['category_id']
                                    : '';
                                $item->article_id = !empty($params_inst['content'][$key]['article_id'])
                                    ? $params_inst['content'][$key]['article_id']
                                    : '';
                                $content[] = $item;
                            }
                        }
                    }
                    
                    // New
                    if (!empty($params_inst['new']['type'])) {
                        $item = new stdClass();
                        $item->type = $params_inst['new']['type'];
                        $item->category_id = !empty($params_inst['new']['category_id'])
                            ? $params_inst['new']['category_id']
                            : '';
                        $item->article_id = !empty($params_inst['new']['article_id'])
                            ? $params_inst['new']['article_id']
                            : '';
                        $content[] = $item;
                    }
                    
                    // Create instance
                    $instance = new Core_ModuleInstance($id);
                    $instance->setParams($content);
                    $instance->setOptions($options);
                    $config->addInstance($instance);
		        }
            }
        }
        if (!empty($params['new_instance'])) {
            if ($params['last_instance_id'] == '-1') {
                $params['last_instance_id'] = 0;
            }
            $last_id = intval($params['last_instance_id'])+1;
            $config->addInstance(new Core_ModuleInstance('item_'.$last_id));
            $config->setLastId($last_id);
        }

        // Save config
        $this->saveConfig($config);
        
        // Redirect
        Core_Response::getInstance()->redirect(
            $request->url( 
                'adminmodule', 
                array(     
                    'controller' => 'adminmodule',
                    'modname' => 'content'
                )
            ), 
            null,
            array('insert', $error, array_filter($warning))
        );
    }
}