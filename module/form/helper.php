<?php
class Module_Form_Helper {
    
    /**
     * @var array
     */
    private $elements;
    
    /**
     * @var integer
     */
    private $index = 0;

    private $translator;

    /**
     * @param array $elements
     */
    public function __construct(array $elements) {
        $this->elements = $elements;
    }
    
    public function reset() {
        $this->last_element = 0;
    }
    
    public function setTranslator(\Yiama\I18n\Translator $translator) {
        $this->translator = $translator;
    }
    
    /**
     * @return string|null
     */
    public function getNextElement() {
        if (!isset($this->elements[$this->index])) {
            return;
        }
        $element = $this->elements[$this->index++];
        $label = '';
        if (!empty($element->label)) {
            $label = $element->label;
            if (!empty($this->translator)) {
                $label = $this->translator->_($element->label);
            }
        }
        $sublabel = '';
        if (!empty($element->sublabel)) {
            $sublabel = $element->sublabel;
            if (!empty($this->translator)) {
                $sublabel = $this->translator->_($element->sublabel);
            }
        }
//        $label = HTML_Tag::closed(
//            'label',
//            null,
//            $label.(!empty($sublabel) ? '<span>'.$sublabel.'</span>' : '')
//        );
        switch ($element->tag) {
            case "input":
                $attrs = array(
                    'type' => $element->type,
                    'name' => $element->name,
                    'value' => $element->value,
                    'placeholder' => $label
                );
                if ($element->required) {
                    $attrs['required'] = 'required';
                }
                return HTML_Tag::closed(
                    'div', 
                    array('class' => 'group'),
                    $label.HTML_Tag::open('input',$attrs)
                ); 
            case "textarea":
                $attrs = array(
                    'name' => $element->name,
                    'value' => $element->value,
                    'placeholder' => $label
                );
                if ($element->required) {
                    $attrs['required'] = 'required';
                }
                return HTML_Tag::closed(
                    'div',
                    array('class' => 'group'),
                    $label.HTML_Tag::closed('textarea',$attrs)
                );
            case "select":
                $attrs = array();
                if ($element->required) {
                    $attrs['required'] = 'required';
                }
                return Helper_Form::select( 
                    $element->name, 
                    $element->options,
                    "value", 
                    "text",
                    "selected",
                    true,
                    $element->required, 
                    array($label, $sublabel),
                    $attrs,
                    null,
                    null,
                    ['class' => 'group']
                ); 
            case "checkbox-group":
                $content = '';
                if (!empty($element->label)) {
                    $content .= Helper_Form::label(array(
                        $label,
                        $sublabel,
                        $element->required
                    ));
                }
                if (!empty($element->options)) {
                    foreach ($element->options as $option) {
                        $attrs = array(
                            'type' => 'checkbox',
                            'value' => $option->value
                        );
                        if ($option->selected) {
                            $attrs['checked'] = 'checked';
                        }
                        $radio = HTML_Tag::void('input', $attrs);
                        if (!empty($option->text)) {
                            $radio = HTML_Tag::closed(
                                'label', 
                                null, 
                                $radio . $option->text
                            );
                        }
                        $content .= "<br />" . $radio;
                    }
                } else {
                    $content .= HTML_Tag::void('input', array(
                        'type' => 'checkbox',
                        'name' => $element->name,
                        'value' => $element->value
                    ));
                }
                return HTML_Tag::closed(
                    'div',
                    array('class' => 'group'),
                    $content
                );
            case "radio-group":
                $content = '';
                if (!empty($element->label)) {
                    $content .= Helper_Form::label(array(
                        $label,
                        $sublabel,
                        $element->required
                    ));
                }
                if (!empty($element->options)) {
                    foreach ($element->options as $option) {
                        $attrs = array(
                            'type' => 'radio',
                            'value' => $option->value
                        );
                        if ($option->selected) {
                            $attrs['checked'] = 'checked';
                        }
                        $radio = HTML_Tag::void('input', $attrs);
                        if (!empty($option->text)) {
                            $radio = HTML_Tag::closed(
                                'label', 
                                null, 
                                $radio . $option->text
                            );
                        }
                        $content .= "<br />" . $radio;
                    }
                } else {
                    $content .= HTML_Tag::void('input', array(
                        'type' => 'radio',
                        'name' => $element->name,
                        'value' => $element->value
                    ));
                }
                return HTML_Tag::closed(
                    'div',
                    array('class' => 'group'),
                    $content
                );
        }
    }
}