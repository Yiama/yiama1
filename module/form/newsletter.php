<?php
class Module_Form_Newsletter extends Core_ModuleAbstract {

    public function render($args = null) {

        $form = HTML_Tag::open(
            'form',
            array(
                'id' => 'formNewsletter',
                'action' => $this->request->url(
                    'module',
                    array(
                        'controller' => 'module',
                        'modname' => 'form_newsletter',
                        'modaction' => 'submit'
                    )
                ),
                'method' => 'POST'
            )
        );
        $form .= HTML_Tag::closed(
            'div',
            array('class' => 'form-message')
        );
        $form .= Helper_Form::input(
            'email',
            '',
            true,
            array( 'validValues' => 'valEmail', 'placeholder' => 'Newsletter' )
        );
        $form .= Helper_Form::submit("");
        $form .= Helper_Form::closeForm();
        $form .= HTML_Tag::close( 'form');

        // View
        $this->setView();
        $this->view->addVars(array(
            'form' => $form
        ));
        return $this->view->render();
    }

    public function submit() {
        // Translation
        $language = new Model_Yiama_Language();
        $locale = $language->getCurrent()->code;
        $translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $translator->setLocale($locale);
        $translator->addResource(new \Yiama\File\File(PATH_MODULES.'translations'.DS.$locale.DS.'form.php'), $locale);

        $params = $this->request->getParams();
        if(!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            echo json_encode(["status" => 0, "message" => $translator->_("messages.formFieldError")]);
            return;
        }

        $subscription = new Model_Yiama_Subscription();
        $result = $subscription->add( 'newsletter', $params['email'] );
        if ($result === 2) {
            echo json_encode(["status" => 0, "message" => $translator->_("messages.formSendError")]);
            return;
        }

        echo json_encode(["status" => 1, "message" => $translator->_("messages.formSendSuccess")]);
        return;
    }
}