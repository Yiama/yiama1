<?php
class Module_Form_Contact extends Core_ModuleAbstract {
    
    public function render($args = null) {
        
        // Data
        $form_model = new Model_Yiama_Form();
        $form_result = $form_model
            ->query()
            ->search("alias='{$args['form_alias']}'")
            ->find(':first');
        if (empty($form_result)) {
            return;
        }
        $json = json_decode($form_result->json);
        if (empty($json)) {
            return;
        }
        
        // Translation
        $language = new Model_Yiama_Language();
        $locale = $language->getCurrent()->code;
        $translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $translator->setLocale($locale);
        $translator->addResource(
                new \Yiama\File\File(PATH_MODULES.'translations'.DS.$locale.DS.'form.php'),
                $locale
        );
        
        // Form
        $id = str_replace('-', '_', $args['form_alias']);
        $form = HTML_Tag::open(
            'form',
            array(
                'id' => $id,
                'action' => $this->request->url(
                    'module',
                    array(
                        'controller' => 'module',
                        'modname' => 'form_contact',
                        'modaction' => 'submit'
                    )
                )   
            )
            );
        $form .= HTML_Tag::closed(
            'div',
            array('class' => 'form-message')
            );
        $elements = array();
        foreach ($json as $j) {
            $elements[] = $j->object;
        }
        $form_helper = new Module_Form_Helper($elements);
        $form_helper->setTranslator($translator);
        while (($element = $form_helper->getNextElement()) !== null) {
            $form .= $element;
        }
        
        // Recaptcha
        $mod_recaptcha = new Module_Google_Recaptcha();
        $form .= HTML_Tag::closed(
            'div',
            array('class' => 'group'),
            $mod_recaptcha->render(array('id' => 'item_0'))
            );
        $form .= HTML_Tag::closed(
            'div',
            array('class' => 'group'),
            HTML_Tag::closed(
                'button',
                null,
                $translator->_('submission')
                )
            );
        
        // Notification
        if (!empty($args['notification'])) {
            $form .= HTML_Tag::closed(
                    'input', 
                    [
                        'type' => 'hidden', 
                        'name' => 'notification', 
                        'value' => $args['notification']
                    ]
                );
        }
        
        $form .= HTML_Tag::close('form');
        
        // View
        $this->setView();
        $this->view->addVars(array(
            'form_result' => $form_result,
            'form' => $form
        ));
        return $this->view->render();
    }
    
    public function submit() {
        $params = $this->request->getParams();
        $error = FALSE;
        $msg = 'success';
        
        // Translation
        $language = new Model_Yiama_Language();
        $locale = $language->getCurrent()->code;
        $translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $translator->setLocale($locale);
        $translator->addResource(
            new \Yiama\File\File(PATH_MODULES.'translations'.DS.$locale.DS.'form.php'),
            $locale
           );
        
        // Check recaptcha
        $mod_recaptcha = new Module_Google_Recaptcha();
        if (!$mod_recaptcha->validate()) {
            $this->response->setJson((object)array(
                'status' => FALSE,
                'message' => $translator->_('recaptcha_failed')
            ));
            return;
        }
        
        // Add subscription if is asked
        if( !empty( $params['subscription'] ) ) {
            $subscription = new Model_Yiama_Subscription();
            $subscription->add( 'newsletter', $params['email'], array( 'name' => $params['name'] ) );
        }
        // Add recipients
        if( !empty( $params['notification'] ) ) {
            $notification = new Model_Yiama_Notification();
            $notify = $notification->query()
                ->search( "alias = '" . $params['notification'] . "'" )
                ->combine( array( 'recipients', 'template' ) )
                ->find( ':first' );
            if (!empty($notify)) {
                // Prepare message
                $message = new \Yiama\Mail\Message();
                $message->setSender(\Yiama\Mail\Message::Address($params['email']));
                $message->setSubject("Επικοινωνία χρήστη από " . Core_App::getConfig( 'domain' ));
                $recipients = [];
                foreach( $notify->recipients as $v ) {
                    $recipients[] = \Yiama\Mail\Message::Address($v->email);
                }
                $message->addRecipients($recipients);
                // Set body
                $template = new Template_Template($notify->template->text);
                $domain = Core_App::getConfig('domain');
                // Params of Array type can't be used inside template
                $template_params = array(
                    'domain' => $domain,
                    'copy' => $domain . ' ' . date( 'Y' )
                );
                $template_params = array_merge($template_params, $params);
                $template->setParams($template_params);
                $content = new \Yiama\Mail\Multipart(\Yiama\Mail\Multipart::$TYPE_MIXED);
                $contentPart = new \Yiama\Mail\Part();
                $contentPart->setBodyHtml($template->render());
                $content->addParts([$contentPart]);
                $message->setContent($content);
            } else {
                $error = TRUE;
                $msg = 'no_recipients_specified';
            }
            // Send
            if (!$error) {
                $transfer = new \Yiama\Mail\Client\Basic();
                $failed = false;
                try {
                    $transfer->send( $message );
                } catch(\Yiama\Mail\MailException $e) {
                    $failed = true;
                }
                if ($failed) {
                    $msg = 'no_emails_send';
                }
            }
        }
        
        $this->response->setJson((object)array(
            'status' => !$error,
            'message' => $translator->_($msg)
        ));
    }
}