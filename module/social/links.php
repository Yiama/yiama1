<?php
class Module_Social_Links extends Core_ModuleAbstract {

    protected $admin;
    
    public function __construct() {
        parent::__construct();
        $this->admin = new Module_Social_Links_Admin();
    }
    
    /**
     * @param array $args {slider_id: string}
     */
    public function render($args = null) {
        
        $options = null;
        $config = $this->admin->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        if (!empty($instances)) {
            foreach ($instances as $inst) {
                if ($inst->getId() == $args['id']) {
                    $options = $inst->getOptions();
                    $links = $inst->getParams();
                    break;
                }
            }
        }
        if (empty($options)) {
            return;
        }
        
        // View
        $this->setView();
        $this->view->addVars(array(
            'options' => $options,
            'links' => $links
        ));
        return $this->view->render();
    }
}