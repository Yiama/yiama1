<?php
class Module_Social_Links_Admin extends Core_ModuleAdminAbstract{

    protected $name = "social_links";

    public function __construct() {
        parent::__construct();
    }
    
    public function render($args = null) {
        
        // HTML
        Core_HTML::addStyle(
            '#main_form .element label{width: 100px;}'
            .'#main_form .element{padding: 0;}'
            .'#main_form .element select{display: inline-block; width: 100px;}'
            .'#main_form .element:last-child{padding-bottom: 30px;}'
            .'#main_form .element:last-child select{display: block; width: 100%;}'
            .'#main_form .element.new-instance{display: inline-block; width: auto; padding: 10px; border: 2px solid #F1586C;}'
            .'#main_form .element.new-instance label{display: inline-block; font-size: 14pt;}'
        );

        // Config
        $config = $this->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        
        // Buttons
        $form = Helper_Buttons::get(array('save' => null));
        
        // Tabs
        $tabs = array();
        foreach ($instances as $inst) {
            $tabs[$inst->getId()] = $inst->getId();
        }
        $form .= Helper_Form::tabMenu($tabs);
        
        // Form
        $form .= Helper_Form::openForm(Core_Request::getInstance()->url(
            'adminmodule',
            array(
                'controller' => 'adminmodule',
                'modname' => 'social_links',
                'modaction' => 'save'
            )
            ));
        $form .= Helper_Form::openWrapperColumnLeft();
        
        // New instance
        $form .= Helper_Form::checkbox(
            'new_instance',
            null,
            null,
            null,
            'New Link group',
            array('class' => 'new-instance')
            );
        
        // Last id
        $form .= Helper_Form::input(
            'last_instance_id',
            empty($config) || empty($config->getLastId()) ? '-1' : $config->getLastId(),
            null,
            array('type' => 'hidden')
            );
        
        // Existed instances
        $index = 0;
        foreach ($instances as $instance) {
            $id = $instance->getId();
            $form .= Helper_Form::openTabItem($id, !$index++);
            
            // Options
            $form .= Helper_Form::openWrapperModule('Options');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][delete]",
                0,
                null,
                '<b>Delete link group</b>',
                null,
                null,
                array('no_empty' => true)
                );
            $options = $instance->getOptions();
            $form .= $this->styles(
                "instance[$id][options][style]",
                !empty($options->style) ? $options->style.'' : null
                );
            $form .= Helper_Form::closeWrapperModule();
            
            // New link
            $form .= Helper_Form::openWrapperModule('New Link');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][item]",
                0,
                null,
                '<b>New link</b>',
                null,
                null,
                array('no_empty' => true)
                );
            $form .= $this->networks(
                "instance[$id][new][network]"
                );
            $form .= Helper_Form::input(
                "instance[$id][new][url]",
                '',
                null,
                null,
                'Url'
                );
            $form .= Helper_Form::closeWrapperModule();
            
            // Existed links
            if (!empty($instance->getParams())) {
                foreach ($instance->getParams() as $key => $item) {
                    $form .= Helper_Form::openWrapperModule('Link: <strong>'.($key+1).'</strong>');
                    $form .= Helper_Form::selectBoolean(
                        "instance[$id][items][$key][delete]",
                        0,
                        null,
                        '<b>Delete</b>',
                        null,
                        null,
                        array('no_empty' => true)
                        );
                    $form .= $this->networks(
                        "instance[$id][items][$key][network]",
                        $item->network.''
                        );
                    $form .= Helper_Form::input(
                        "instance[$id][items][$key][url]",
                        $item->url,
                        null,
                        null,
                        'Url'
                        );
                    $form .= Helper_Form::closeWrapperModule();
                }
            }
            $form .= Helper_Form::closeTabItem();
        }
        
        $form .= Helper_Form::closeWrapperColumnLeft();
        $form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit('Submit');
        $form .= Helper_Form::closeForm();
        
        return $form;
    }
    
    public function save() {
        $request = Core_Request::getInstance();
        $params = $request->getParams();
        $this->checkCsrf($params);
        $warning = [];
        $error = null;
        
        // Save
        $config = new Core_ModuleConfig();
        if (!empty($params['instance'])) {
            // Update
            foreach ($params['instance'] as $id => $params_inst) {
                if (empty($params_inst['delete'])) {
                    
                    // Options
                    $options = new stdClass();
                    $options->style = $params_inst['options']['style'];
                    
                    // Params
                    $items = array();
                    if (!empty($params_inst['items'])) {
                        foreach ($params_inst['items'] as $key => $item) {
                            if (empty($params_inst['items'][$key]['delete'])) {
                                $item = new stdClass();
                                $item->network = $params_inst['items'][$key]['network'];
                                $item->url = $params_inst['items'][$key]['url'];
                                $items[] = $item;
                            }
                        }
                    }
                    
                    // New
                    if (!empty($params_inst['new']['item'])) {
                        $item = new stdClass();
                        $item->network = $params_inst['new']['network'];
                        $item->url = $params_inst['new']['url'];
                        $items[] = $item;
                    }
                    
                    // Create instance
                    $instance = new Core_ModuleInstance($id);
                    $instance->setParams($items);
                    $instance->setOptions($options);
                    $config->addInstance($instance);
                }
            }
        }
        
        if (!empty($params['new_instance'])) {
            if ($params['last_instance_id'] == '-1') {
                $params['last_instance_id'] = 0;
            }
            $last_id = intval($params['last_instance_id'])+1;
            $config->addInstance(new Core_ModuleInstance('item_'.$last_id));
            $config->setLastId($last_id);
        }

        // Save config
        $this->saveConfig($config);
        
        // Redirect
        Core_Response::getInstance()->redirect(
            $request->url(
                'adminmodule',
                array(
                    'controller' => 'adminmodule',
                    'modname' => 'social_links'
                )
                ),
            null,
            array('insert', $error, array_filter($warning))
            );
    }
    
    private function networks($name, $selected_val = null) {
        return Helper_Form::select(
            $name,
            array(
                (object)array('value' => 'facebook', 'title' => 'Facebook'),
                (object)array('value' => 'twitter', 'title' => 'Twitter'),
                (object)array('value' => 'googleplus', 'title' => 'Google Plus'),
                (object)array('value' => 'pinterest', 'title' => 'Pinterest') ,
                (object)array('value' => 'linkedin', 'title' => 'LinkedIn') ,
                (object)array('value' => 'instagram', 'title' => 'Instagram'),
                (object)array('value' => 'youtube', 'title' => 'YouTube')
            ),
            'value',
            'title',
            'value',
            $selected_val,
            null,
            'Network',
            null,
            null,
            array('no_empty' => true)
            );
    }
    
    private function styles($name, $selected_val = null) {
        return Helper_Form::select(
            $name,
            array(
                (object)array('value' => 'round', 'title' => 'Round'),
                (object)array('value' => 'rectangle', 'title' => 'Rectangle'),
                (object)array('value' => 'no_frame', 'title' => 'No frame')
            ),
            'value',
            'title',
            'value',
            $selected_val,
            null,
            'Style',
            null,
            null,
            array('no_empty' => true)
            );
    }
}