<?php

class Module_Menu_Vertical extends Core_ModuleAbstract {
    
    public function render($args = null)
	{
		$this->setView();
		$type_alias = $args[0];
		$pages = Module_Menu_Helper_Pages::getPages( $type_alias );
        if (!empty($pages)) {
            $this->view->nav = $this->getNav( 1, $pages, Module_Menu_Helper_Pages::getCurrent( $type_alias ) );
        }
        return $this->view->render();
	}
	
	private function getNav( $id, $pages, $current )
	{
		$view = '';
		$items = DB_ActiveRecord_Array::search( $pages, array( array( 'parent_id', '=', $id ) ) );
		foreach( $items as $v ) {
			if( ! $v->is_published ) {
				continue;
			}
			$v->tree_level -= 1; /* Αφαιρείται τo level της Root */
			$view .= "<li level='{$v->tree_level}' onclick='menuV( this, {$v->tree_level} )'>";
			if( $v->total_children ) {
				$view .= "<span" . ( $v->is_parent ? " class='active'" : "" ) . ">{$v->title} <small>&#xf10e;</small></span>";
			} elseif ( $current && $v->id == $current->id ) {
				$view .= "<span class='level{$v->tree_level} current'>&#xf10b; {$v->title}</span>";
			} else {
				$view .= "<a href='" . $this->request->urlFromPath( $v->url ) . "' class='level{$v->tree_level}" . ( $v->is_parent ? " active" : "" ) . "'>{$v->title}</a>";
			}
			$view .= "</li>";
			if( $v->total_children ) {
				$view .= "<ul class='sub" . ( $v->is_parent ? " show" : "" ) . "'>" . $this->getNav( $v->id, $pages, $current ) . "</ul>";
			}
		}
		
		return $view;
	}
}

?>