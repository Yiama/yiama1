<?php
class Module_Menu_Horizontal extends Core_ModuleAbstract {
    
    public function render($args = null) {
		$this->plugin_url = new Plugin_Url_Language();
		
		$this->setView();
		$type_alias = $args[0];
		$pages = Module_Menu_Helper_Pages::getPages( $type_alias );
        if (!empty($pages))  {
            $this->view->nav = $this->getNav( 1, $pages, Module_Menu_Helper_Pages::getCurrent( $type_alias ) );
        }
		return $this->view->render();
	}
	
	public function getNav( $id, $pages, $current ) {
		$view = '';
		$items = DB_ActiveRecord_Array::search( $pages, array( array( 'parent_id', '=', $id ) ) );
		$view .= "  <ul>";
		foreach( $items as $v ) {
			if( ! $v->is_published ) {
				continue;
			}
			$class = array();
            if( ! empty( $v->module )
                &&  $view .= Module_Menu_Helper_Module::renderModule( $v->module ) ) {
                continue;
            }
			if( $v->is_parent || ( $this->request->urlFromPath( $v->url ) == urldecode( $this->request->getCleanURI() ) && $v->url != '' ) || ( $this->request->getController() == 'index' && $v->url == '/' ) ) { /* Parent item ή ολόκληρο το Path ή Home Page */
				$class[] = "active";
			}
			if( $current && $v->id == $current->id ) {
				$class[] = "current";
			}
			if( $v->total_children ) {
				$class[] = "has-sub";
			}
			$view .= "<li class='" . implode( ' ', $class ) . "'>";
			if( empty($v->url) ) {
				$view .= "<a>{$v->title}</a>";
			} else {
			    $view .= "<a href='" . $this->request->urlFromPath( $v->url, null, Module_Menu_Helper_Url::getExtra( $v->url ) ) . "'>{$v->title}</a>";
			}
			if( $v->total_children ) {
				$view .= $this->getNav( $v->id, $pages, $current );
			}
			$view .= "  </li>";
		}
		$view .= "  </ul>";
		
		return $view;
	}
}