<?php

class Module_Menu_Helper_Url
{
	public static function getPseudoDoc( $path )
	{
	    $match_models = array();
	    $matchesConfig = Core_App::getConfig( 'modules.menu.match_route_path_with_model' );
        if (!empty($matchesConfig)) {
            $match_models = $matchesConfig;
        }
		if( ( $route = Core_Request::getInstance()->getRoute( '/' . $path ) )
		&& in_array( 'id', array_keys( $route['matches'] ) ) ) {
			$model = $match_models[ $route['matches']['controller'] ];
			$model = new $model();
			return $model->findByKey( $route['matches']['id'] )->alias;
		}
		return null;
	}

	public static function getExtra( $path )
	{
		$extra = array();
		if( $pseudoDoc = self::getPseudoDoc( $path ) ) {
			$extra['pseudoDoc'] = $pseudoDoc;
		}
		return $extra;
	}
}

?>