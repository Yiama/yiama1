<?php

class Module_Menu_Helper_Module
{
	public static function renderModule( $options )
	{
        $opts = json_decode( $options );
        if( ( $mod = new Core_Module( $opts->name, $opts->controller ) ) ) {
            return $mod->render( $opts );
        }
	}
}