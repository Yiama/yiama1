<?php

class Module_Menu_Helper_Pages
{
	private static $current;
	
	public static function getPages( $type_alias )
	{
		$page = new Model_Yiama_Page();
		$pages = $page->getByTypeAlias( $type_alias, false );
		$published = $page->query()	
			->search( 'is_published = 1' )
            ->order( 'ordered ASC' )
			->find();
		$published = DB_ActiveRecord_Array::flatten( DB_ActiveRecord_Array::tree( $published, 1, 'id', 'parent_id' ), 'children' );
		$current_page_parents = null;
		$current = self::getCurrent( $type_alias );
		if( $current_page = $current ) {
			$current_page_parents = $current_page->getParents();
		}
        if (!empty($pages)) {
            foreach( $pages as &$v ) {
                foreach( $published as $v2 ) {
                    if( $v->id == $v2->id ) {
                        $v = $v2;
                    }
                }
                $v->is_parent = false;
                foreach( ( array ) $current_page_parents as $parent ) {
                    if( $v->id == $parent->id ) {
                        $v->is_parent = true;
                        break;
                    }
                }
            }
        }
		return $pages;
	}
	
	public static function getCurrent( $type_alias )
	{
		$page = new Model_Yiama_Page();
        if( ! empty( self::$current[ $type_alias ] ) ) {
            return self::$current[ $type_alias ];
        }
		return self::$current[ $type_alias ] = $page->getCurrent( $type_alias );
	}
}

?>