<?php

class Module_Menu_Footer extends Core_ModuleAbstract {
    
	public function render($args = null)
	{
		$this->plugin_url = new Plugin_Url_Language();
		
		$type_alias = $args[0];
        $page = new Model_Yiama_Page();
        $pages = (array) Module_Menu_Helper_Pages::getPages( $type_alias );
        foreach($pages as $page ) {
            $page->link = $this->request->urlFromPath( $page->url );
        }
        $current = Module_Menu_Helper_Pages::getCurrent( $type_alias );
        
        // View
		$this->setView();
        $this->view->addVars( array( 
            'pages' => $pages, 
            'current' => $current 
        ) );
		return $this->view->render();
	}
    
    protected function getPages( $parent_id )
    {
        $page = new Model_Yiama_Page();
        return $page->query()	
        ->search( 'EXISTS( SELECT 1 FROM ym_pages AS parent"'
                . " WHERE parent.id = " . $parent_id
                . " AND parent.id = ym_pages.parent_id"
                . " AND ym_pages.is_published = 1' )" )
        ->find();
    }
}

?>