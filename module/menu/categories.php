<?php

class Module_Menu_Categories extends Core_ModuleAbstract {
    
	public function render($args = null)
	{
		$model_cat = new Model_Yiama_Category();
        $categories = $model_cat
            ->query()
            ->search( "parent_id = " . $args->params->id )
            ->combine( "images" )
            ->find();
		foreach( $categories as $cat ) {
            $cat->link = $this->request->urlFromPath( 
                $cat->url, 
                null, 
                array( 'pseudoDoc' => $cat->title )
            );
        }
        
		$this->setView();
        $this->view->addVars( array( 'categories' => $categories ) );
		return $this->view->render();
	}
}

?>