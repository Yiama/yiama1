<?php
class Module_Admin_Navigation extends Core_ModuleAbstract {
    
	public function render($args = null) {
        $page = new Model_Yiama_Page();
        $request = Core_Request::getInstance();
        $action = $request->getAction();
        if( $action == 'insertform' ) {
            $title_appendix = ' Προσθήκη νέου';
        }
        $page = $page->getCurrent( 'admin' );
        $page_parents = $page->getParents();
        $page_nav_items = array();
        foreach( array_reverse( $page_parents ) as $key => $parent ) {
            $page_nav_items[] = $key && $parent->url 
                ? "<a href='" 
                    . Core_Request::getInstance()->urlFromPath( $parent->url ) 
                    . "'><i>{$parent->title}</i></a>" 
                : $parent->title;
        }
        $page_nav_items[] = $page->title;
        $page_nav = implode( ' / ', $page_nav_items );
        return HTML_Tag::closed( 
            'div', 
            array( 'class' => 'navigation' ), 
            $page_nav . ( ! empty( $title_appendix ) 
                ? ' / ' . implode( ' <small>/</small> ', ( array ) $title_appendix ) 
                : '' 
            ) 
        );
	}	
}