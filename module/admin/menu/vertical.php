<?php
class Module_Admin_Menu_Vertical extends Core_ModuleAbstract {
    
	public function render($args = null) {
		$page = new Model_Yiama_Page();
		$pages = $page->getByTypeAlias( 'admin', false );
		$published = $page->query()	
			->search( 'is_published = 1' )
            ->order('ordered')
			->find();
		$published = DB_ActiveRecord_Array::flatten( DB_ActiveRecord_Array::tree( $published, 1, 'id', 'parent_id' ), 'children' );
		$current_page_parents = null;
		$current = $page->getCurrent( 'admin' );
		if( $this->current_page = $current ) {
			$current_page_parents = $this->current_page->getParents();
		}
		foreach( $pages as &$v ) {
			foreach( $published as $v2 ) {
				if( $v->id == $v2->id ) {
					$v = $v2;
				}
			}
			$v->is_current_parent = false;
			foreach( ( array ) $current_page_parents as $parent ) {
				if( $v->id == $parent->id ) {
					$v->is_current_parent = true;
					break;
				}
			}
		}
        $user = new Model_Yiama_User();
        $logged_user = $user->getLogged();
        $logged_user->link_edit = $this->request->urlFromPath( 
            "/yiama_users/updateform/" . $logged_user->id
        );
        $logged_user->link_logout = $this->request->urlFromPath( 
            "/user/logout"
        );
		
		/* View */
		$this->setView();
		$this->view->addVars(array(
            'user' => $logged_user,
            'items' => $this->getItems(1, $pages)
        ));
		return $this->view->render();
	}
    
    private function getItems($id, $pages) {
        $items = DB_ActiveRecord_Array::search($pages, array(
            array('parent_id', '=', $id),
            array('is_published', '=', 1) 
        )); 
        if (!empty($items)) {
            foreach ($items as $item) {
                $item->is_current = false;
                if (    !empty($this->current_page) 
                    &&  $item->id == $this->current_page->id) {
                    $item->is_current = true;
                }
                if (!empty($item->total_children)) {
                    $item->children = $this->getItems($item->id, $pages);
                }
            }
        }
        return $items;
    }
	
    /*
	private function getNav( $id, $pages )
	{
		$view = '';
		$items = DB_ActiveRecord_Array::search( $pages, array( array( 'parent_id', '=', $id ) ) ); 
		if (!empty($items)) {
            foreach( $items as $v ) {
                if( ! $v->is_published ) {
                    continue;
                }
                $v->tree_level -= 1; // Αφαιρείται τo level της Root
                $view .= "<li level='{$v->tree_level}' onclick='menuV( this, {$v->tree_level} )'><h" . ( $v->tree_level+1 ) . ">";
                if( $v->total_children ) {
                    $view .= "<span" . ( $v->is_parent ? " class='active'" : "" ) . ">{$v->title} <small>&#xf10e;</small></span>";
                } elseif ( $this->current_page && $v->id == $this->current_page->id ) {
                    $view .= "<span class='level{$v->tree_level} current'>&#xf10b; {$v->title}</span>";
                } else {
                    $view .= "<a href='" . $this->request->urlFromPath( $v->url ) . "' class='level{$v->tree_level}" . ( $v->is_parent ? " active" : "" ) . "'>{$v->title}</a>";
                }
                $view .= "</h" . ( $v->tree_level+1 ) . "></li>";
                if( $v->total_children ) {
                    $view .= "<ul class='sub" . ( $v->is_parent ? " show" : "" ) . "'>" . $this->getNav( $v->id, $pages ) . "</ul>";
                }
            }
        }
        
		return $view;
	}
    */
}