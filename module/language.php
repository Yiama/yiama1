<?php
class Module_Language extends Core_ModuleAbstract {

    public function render($args = null) {
        $language = new Model_Yiama_Language();
        $languages = $language->query()
            ->search("is_published")
            ->find();

        $request = Core_Request::getInstance();
        $links = [];
        foreach ($languages as $language) {
            $l = new stdClass();
            $l->code = $language->code;
            $l->link = "/" . $request->code;
            $links[] = $l;
        }

        $this->setView();
        $this->view->addVars(["languages" => $links]);
        return $this->view->render();
    }
}