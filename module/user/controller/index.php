<?php
class User_Controller_Index extends Core_Controller
{
	public function render()
	{
		$links = array();
		$links['login'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'formlogin'
			)
		);
		$links['register'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'formregister'
			)
		);
		$links['logout'] = $this->request->url(
			'cont_act_id',
			array(
				'controller' => 'user',
				'action' => 'ajaxlogout'
			)
		);
		$user = new Model_Yiama_User();
		$logged_user = $user->getLogged();		
		
		$this->setView();
		$this->view->links = $links;
		$this->view->logged_user = $logged_user;
		return $this->view->render();
	}	
}

?>