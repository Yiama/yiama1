<?php
class Module_Slider extends Core_ModuleAbstract {

    protected $admin;
    
    public function __construct() {
        parent::__construct();
        $this->admin = new Module_Slider_Admin();
    }
    
    /**
     * @param array $args {slider_id: string, view: string} 
     */
    public function render($args = null) {
        
        $slides = array();
        $config = $this->admin->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        if (!empty($instances)) {
            foreach ($instances as $inst) {
                $options = $inst->getOptions();
                if ($inst->getId() == $args['id']) {
                    $slides = $inst->getParams();
                    break;
                }
            }
        }
        if (empty($slides)) {
            return;
        }
        $article = new Model_Yiama_Article();
        foreach ($slides as $key => $slide) {
            $result = $article->query()
                ->search("id='".$slide->article_id."'")
                ->combine('images')
                ->find(':first');
            if (empty($result)) {
                unset($slides[$key]);
            } else {
                $slide->article = $result;
            }
        }
        
        // View
        $this->setView(__DIR__.DS."views/slider/{$args['view']}");
        $this->view->addVars(array(
            'options' => $options,
            'slides' => $slides
        ));
        return $this->view->render();
    }
}