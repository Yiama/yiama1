<?php
class Module_Text_Admin extends Core_ModuleAdminAbstract{

    protected $name = "text";

    public function __construct() {
        parent::__construct();
    }
    
    public function render($args = null) {
        
        // HTML
        Core_HTML::addStyle(
            '#main_form .element label{width: 100px;}'
            .'#main_form .element{padding: 0;}'
            .'#main_form .element select{display: inline-block; width: 100px;}'
            .'#main_form .element:last-child{padding-bottom: 30px;}'
            .'#main_form .element:last-child select{display: block; width: 100%;}'
            .'#main_form .element.new-instance{display: inline-block; width: auto; padding: 10px; border: 2px solid #F1586C;}'
            .'#main_form .element.new-instance label{display: inline-block; font-size: 14pt;}'
        );

        // Config
        $config = $this->getConfig();
        $instances = [];
        if ($config != null) {
            $instances = $config->getInstances();
        }
        
        // Buttons
        $form = Helper_Buttons::get(array('save' => null));
        
        // Tabs
        $tabs = array();
        foreach ($instances as $inst) {
            $tabs[$inst->getId()] = $inst->getId();
        }
        $form .= Helper_Form::tabMenu($tabs);
        
        // Form
        $form .= Helper_Form::openForm(Core_Request::getInstance()->url(
            'adminmodule',
            array(
                'controller' => 'adminmodule',
                'modname' => 'text',
                'modaction' => 'save'
            )
            ));
        $form .= Helper_Form::openWrapperColumnLeft();
        
        // New instance
        $form .= Helper_Form::checkbox(
            'new_instance',
            null,
            null,
            null,
            'New Text group',
            array('class' => 'new-instance')
            );
        
        // Last id
        $form .= Helper_Form::input(
            'last_instance_id',
            empty($config) || empty($config->getLastId()) ? '-1' : $config->getLastId(),
            null,
            array('type' => 'hidden')
            );
        
        // Existed instances
        $index = 0;
        foreach ($instances as $instance) {
            $id = $instance->getId();
            $form .= Helper_Form::openTabItem($id, !$index++);
            
            // Options
            $form .= Helper_Form::openWrapperModule('Options');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][delete]",
                0,
                null,
                '<b>Delete text group</b>',
                null,
                null,
                array('no_empty' => true)
                );
            $options = $instance->getOptions();
            $form .= Helper_Form::closeWrapperModule();
            
            // New text
            $form .= Helper_Form::openWrapperModule('New Text');
            $form .= Helper_Form::selectBoolean(
                "instance[$id][new][item]",
                0,
                null,
                '<b>New text</b>',
                null,
                null,
                array('no_empty' => true)
            );
            $form .= Helper_Form::textarea(
                "instance[$id][new][text]",
                '',
                null,
                'Text',
                null
            );
            $form .= Helper_Form::closeWrapperModule();
            
            // Existed text
            if (!empty($instance->getParams())) {
                foreach ($instance->getParams() as $key => $item) {
                    $form .= Helper_Form::openWrapperModule('Text: <strong>'.($key+1).'</strong>');
                    $form .= Helper_Form::selectBoolean(
                        "instance[$id][items][$key][delete]",
                        0,
                        null,
                        '<b>Delete</b>',
                        null,
                        null,
                        array('no_empty' => true)
                    );
                    $form .= Helper_Form::textarea(
                        "instance[$id][items][$key][text]",
                        $item->text,
                        null,
                        null,
                        'Text'
                    );
                    $form .= Helper_Form::closeWrapperModule();
                }
            }
            $form .= Helper_Form::closeTabItem();
        }
        
        $form .= Helper_Form::closeWrapperColumnLeft();
        $form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit('Submit');
        $form .= Helper_Form::closeForm();
        
        return $form;
    }
    
    public function save() {
        $request = Core_Request::getInstance();
        $params = $request->getParams();
        $this->checkCsrf($params);
        $warning = [];
        $error = null;
        
        // Save
        $config = new Core_ModuleConfig();
        if (!empty($params['instance'])) {
            // Update
            foreach ($params['instance'] as $id => $params_inst) {
                if (empty($params_inst['delete'])) {
                    
                    // Options
                    $options = new stdClass();
                    
                    // Params
                    $items = array();
                    if (!empty($params_inst['items'])) {
                        foreach ($params_inst['items'] as $key => $item) {
                            if (empty($params_inst['items'][$key]['delete'])) {
                                $item = new stdClass();
                                $item->text = $params_inst['items'][$key]['text'];
                                $items[] = $item;
                            }
                        }
                    }
                    
                    // New
                    if (!empty($params_inst['new']['item'])) {
                        $item = new stdClass();
                        $item->text = $params_inst['new']['text'];
                        $items[] = $item;
                    }
                    
                    // Create instance
                    $instance = new Core_ModuleInstance($id);
                    $instance->setParams($items);
                    $instance->setOptions($options);
                    $config->addInstance($instance);
                }
            }
        }
        
        if (!empty($params['new_instance'])) {
            if ($params['last_instance_id'] == '-1') {
                $params['last_instance_id'] = 0;
            }
            $last_id = intval($params['last_instance_id'])+1;
            $config->addInstance(new Core_ModuleInstance('item_'.$last_id));
            $config->setLastId($last_id);
        }

        // Save config
        $this->saveConfig($config);
        
        // Redirect
        Core_Response::getInstance()->redirect(
            $request->url(
                'adminmodule',
                array(
                    'controller' => 'adminmodule',
                    'modname' => 'text'
                )
            ),
            null,
            array('insert', $error, array_filter($warning))
            );
    }
}