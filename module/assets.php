<?php
class Module_Assets extends Core_ModuleAbstract {
    
    public function __construct() {
        parent::__construct();
        
        
    }
        
    public function render($args = null) {
    }
    
    public function combine_css() {
        header("Content-type: text/css");
        
        $files = urldecode($this->request->getParams('files'));
        $content = '';
        foreach (explode(',', $files) as $file) {
            $content .= @file_get_contents(PATH_ASSETS.'css'.DS.trim($file, "/"));
        }
        
        // Replace relative image paths with absolute
        $content = str_replace(
            array(
                '../../../../../../../../../../',
                '../../../../../../../../../',
                '../../../../../../../../',
                '../../../../../../../',
                '../../../../../../',
                '../../../../../',
                '../../../../',
                '../../../',
                '../../',
                '../',
                '',
            ),
            PATH_REL_ASSETS.'/',
            $content
        );
        // Remove comments
        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
        // Remove space after colons
        $content = str_replace(': ', ':', $content);
        // Remove whitespace
        $content = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $content);
        
        Core_Response::getInstance()->setBody($content);
    }
    
    public function combine_js() {
        header("Content-type: text/css");
        
        $files = urldecode($this->request->getParams('files'));
        $content = '';
        foreach (explode(',', $files) as $file) {
            $content .= @file_get_contents(PATH_ASSETS.'js'.DS.trim($file, "/"));
        }
        
        Core_Response::getInstance()->setBody($content);
    }
}