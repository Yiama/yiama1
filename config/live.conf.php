<?php
return [
    'databases' => [
        'default' => [
            'pdo' => 'mysqli',
            'connection' => [
                'server' => '',
                'username' => '',
                'userpass' => '',
                'dbname' => ''
            ]
        ]
    ]
];