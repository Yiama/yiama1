<?php

/**
 * Admin
 * user: yiama
 * pass: rZy2Y5fc
 */

	/* Ακολουθεί τη διαδρομή των directory. */
	return array(
		'domain' => '',
		/*
			Αν το framework το έχουμε σε κάποιο folder mέσα στο htdocs τότε πρέπει να ορίσουμε το baseDir αν δεν έχουμε ορίσει
			base directory στο .htaccess. Αν το baseDir δεν οριστεί θα πρέπει να περιέχει κενό string.
		*/
		'baseDir' => '',
		'library' => array(
			'core' => array(
				'exception' => array(
					'mode' => array(
						'development' => array(
							'errors' => ( E_ALL | E_STRICT ) ^ ( E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR ),
						    'on' => true
						),
						'production' => array(
							'errors' => E_USER_WARNING | E_USER_NOTICE | E_USER_ERROR | E_NOTICE | E_DEPRECATED,
						    'on' => false
						)
					),
					'page' => '{ "routename": "lang_cont_act_id", "routevalues": { "langcode": "xx", "controller": "error", "action": "notfound" } }'
				),
				'request' => array(
					/*
						Τα preg expression πρέπει να έχουν προτεραιότητα ξεκινώντας από αυτά που είναι
						πιο συγκεκριμένα και καταλήγοντας σε αυτά που είναι πιο γενικά και από αυτά που έχουν λιγότερα optional
						στοιχεία σε αυτά που έχουν περισσότερα.
						Ο controller πρέπει να είναι πάνω από 2 χαρακτήρες μήκος ώστε κάτω από 2 χαρακτήρες
						να μπορούμε να περνάμε πρώτο το language code.
						το:
							/el/controller/action/4/asdsdasdasd-asdasd-asd-asd.html
						κάνει το ίδιο με:
							/controller/action/4/asdsdasdasd-asdasd-asd-asd.html
					*/
					'routes' => array(
						//'lang_cont_id'       => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})/(?P<id>\d+)(/(?P<action>[a-z_]+))?$~',
						//'lang_cont_act_id'   => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})(/(?P<action>[a-z_]+)(/(?P<id>\d+))?)?$~',
					    'adminmodule'        => '~^/(?P<controller>\badminmodule\b)/(?P<modname>[a-z_]+)(/(?P<modaction>[a-z_]+))?$~',
					    'module'             => '~^/(?P<controller>\bmodule\b)/(?P<modname>[a-z_]+)(/(?P<modaction>[a-z_]+))?$~',
					    'cont_id'            => '~^/(?P<controller>[a-z_]{3,})/(?P<id>\d+)$~',
						'lang_cont_id'       => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})/(?P<id>\d+)$~',
						'cont_id_act'        => '~^/(?P<controller>[a-z_]{3,})/(?P<id>\d+)/(?P<action>[a-z_]+)$~',
						'lang_cont_id_act'   => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})/(?P<id>\d+)/(?P<action>[a-z_]+)$~',
						'cont_act_id'        => '~^/(?P<controller>[a-z_]{3,})(/(?P<action>[a-z_]+)(/(?P<id>.*))?)?$~',
						'lang_cont_act_id'   => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})(/(?P<action>[a-z_]+)(/(?P<id>.*))?)?$~',
						'cont_cat_cont'      => '~^/(?P<controller>[a-z_]{3,})/(?P<category>[\p{L}\d-]+)/(?P<content>[\p{L}\d\.;!?,-]+)$~u',
						'lang_cont_cat_cont' => '~^/(?P<langcode>[a-z]{2})/(?P<controller>[a-z_]{3,})/(?P<category>[\p{L}\d-]+)/(?P<content>[\p{L}\d\.;!?,-]+)$~u',
						'default'            => '~^/$~',
						'lang_default'       => '~^/(?P<langcode>[a-z]{2})$~'
					),
					/* Εδώ ορίζεται η default page. */
					'defaultController' => 'index',
					/* Default action για όλους τους controllers. */
					'defaultAction' => 'index',
					/*
						Αν δεν είναι κενό string τότε η url class δεν λαμβάνει υπόψη της το τελευταίο
						directory του url path όταν αυτό τελειώνει με αυτό ακριβώς το extension.
					*/
					'pseudoExt' => '.html'
				),
				'cache' => array(
					'enabled' => false
				),
				'autoload' => array(
                    'loaders' => array(
                        'yiama' => array(
                            'path' => PATH_ROOT . 'library' . DS . 'vendor' . DS . 'yiama' . DS . 'autoload.php'
                        )
                    )
				),
			    'html' => array(
			        'title_postfix' => 'customweb'
			    )
			),
			'plugins' => array(
				/*'url_language' => true,*/
			    'assets_bundle' => true,
			    'cache_nocache' => true/*,
			    'mobile' => true*/
			)
		),
		/*
			Πρέπει να ορίζεται.
		*/
		'defaultApplication' => 'default',
		/*
		 * Τα applications πρέπει να δίνονται στο URL εκτός του 'defaultApplication'.
		 * Μπορούμε να αντιστοιχίσουμε στα μονογλωσσικά site ένα μέρος του path στο url με ένα στοιχείο
		 * του route. Π.χ. το μικρές-αγγελίες αντιστοιχίζεται με το aggelies που
		 * είναι το όνομα ενός controller, urlPathMap => array( 'μικρές-αγγελίες' => 'aggelies' )
		 * Όταν υπάρχει κάποιο από τα keys του urlPathMap στο url path, θα αντικατασταθεί
		 * με το αντίστοιχο value -ΠΡΙΝ- ελεγθεί αν είναι σωστό το route.
		 */
		'applications' => array(
			'admin' => array(
                'session' => array(
                    'enabled' => false
                ),
				'database' => 'default',
				'backupFolder' => PATH_ROOT . 'backup',
				'defaultpage' => 'dashboard',
				'plugins' => array(
					'cache_nocache' => true
				),
				'modules' => array(
					'pagination' => array(
						'content_per_page' => 20,
						'max_buttons' => 20
					)
				),
                "user" => array(
                    "login" => array(
                        "interval" => 2*60*60
                    )
                )
			),
			'default' => array(
				'database' => 'default',
				'plugins' => array(
					/*'url_language' => true*/
				),
				'modules' => array(
					'pagination' => array(
						'content_per_page' => 9,
						'max_buttons' => 6
					)
				),
                "user" => array(
                    "login" => array(
                        "interval" => 2*60*60
                    )
                )
            ),
            'install' => array(
                'database' => 'default'
            )
		),
		'databases' => array(
			'default' => array(
				'pdo' => 'mysqli',
                'connection' => [
                    'server' => '',
                    'username' => '',
                    'userpass' => '',
                    'dbname' => ''
                ]
			)
        ),
		/* Change in every site */
		'security' => array(
			'salt' => 'U92vWZyn'
		),
	    'mail' => array(
	        'sender' => '',
	        'use_smtp' => false,
	        'smtp' => array(
	            'host' => '',
	            'port' => 465,
	            'username' => '',
	            'password' => ''
	        )
	    ),
        'modules' => array(
            'menu' => array(
                 'match_route_path_with_model' => array(
                     'article' => 'Model_Yiama_Article',
                     'product' => 'Model_Yiama_Article',
                     'articles' => 'Model_Yiama_Category'
                 ),
            )
        )
	);
