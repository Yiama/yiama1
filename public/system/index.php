<?php

/* Define directory separator */
define('DS', DIRECTORY_SEPARATOR);

/* Define resource files extension */
define('EXT', '.php');

/* Define document site root path */
define('PATH_ROOT', realpath(dirname(dirname(dirname(__FILE__ )))) . DS);

/* Define document site base directory */
define('PATH_BASEDIR', ltrim(dirname(dirname(dirname($_SERVER['SCRIPT_NAME']))), '/'));

require PATH_ROOT . 'start' . EXT;
