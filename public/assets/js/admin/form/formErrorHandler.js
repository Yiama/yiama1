/*
	@param formObj ( object )
	@param ar ( fieldnames array )
*/
function handleError( formObj, ar )
{
	if( but = formObj.getElementsByTagName( 'button' )[0] ) {
		_removeClass( but, 'loading' );
	}
	if( but = _get( 'main_form_right_top_button' ) ) {
		_removeClass( but, 'loading' );
	}
	message( 'error', 'Ορισμένα πεδία δεν συμπληρώθηκαν σωστά!' );
	window.scrollTo( 0, 0 );
	for( var i = 0; i < ar.length; i++ ){
		for( var k = 0; k < formObj.elements.length; k++ ){
			if( formObj.elements[k].name == ar[i] ){
				highlightTab( formObj.elements[k] );
				_addClass( formObj.elements[k], 'highlighted' );
			}
		}
	}
}

function highlightTab( element )
{
	if( element.parentNode.tagName == 'FORM' ) {
		return;
	} else {
		if( _hasClass( element.parentNode, 'form_tab_item' ) ) {
			_addClass( _get( 'tab_button_' + element.parentNode.id ), 'highlighted' );
		} else {
			return highlightTab( element.parentNode );
		}
	}
}