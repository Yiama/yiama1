/** 
 * @param Object params
 * @returns Function
 */
class ymFormBuilder {
    
    constructor(
        jsonGroups, 
        jsonStoredGroups, 
        domPanelButton, 
        domPanelEdit, 
        domPanelPreview
    ) {
        this.jsonGroups = jsonGroups;
        this.jsonStoredGroups = typeof jsonStoredGroups !== "undefined"
            ? jsonStoredGroups
            : [];
        this.previewItemCollection = [];
        this.domPanelButton = domPanelButton;
        this.PanelEdit = new ymFormBuilderPanelEdit(domPanelEdit);
        this.domPanelPreview = domPanelPreview;

        // Panel Buttons
        for (var i = 0; i < this.jsonGroups.length; i++) {
            var button = new ymFormBuilderPanelButton(this.jsonGroups[i]);
                button.addClickListener(this);
            this.domPanelButton.appendChild(button.render());
        }
        
        // Render Stored groups
        for (var i = 0; i < this.jsonStoredGroups.length; i++) {
            // Because render method is not saved with JSON.stringify, bind
            // it in storedGroups
            for (var k = 0; k < this.jsonGroups.length; k++) {
                if (this.jsonGroups[k].id == this.jsonStoredGroups[i].id) {
                    this.jsonStoredGroups[i].toObject = jsonGroups[k].toObject;
                    this.jsonStoredGroups[i].render = jsonGroups[k].render;
                    break;
                }
            }
            this.createPreviewItem(this.jsonStoredGroups[i]);
        }
        
        // Render save button
        var instance = this;
        var button = document.createElement("div");
            button.id = "formBuilderButtonSave";
            button.innerHTML = "Save";
            button.onclick = function() {
                var json = [];
                for (var i = 0; i < instance.previewItemCollection.length; i++) {
                    var el = instance.previewItemCollection[i]
                            .getJsonElement();
                        el.object = el.toObject();
                    json.push(el);
                }
                document.getElementById("formJsonInput").value = JSON.stringify(json);
                instance.saveButtonState(true); 
            }
        this.domPanelPreview.previousSibling.appendChild(button);
        // Save current json at start up
        button.click();
    }
    
    /**
     * @param ymFormBuilderPanelPreviewItem previewItem
     * @returns undefined
     */
    addPreviewItem(previewItem) {
        this.previewItemCollection.push(previewItem);
    }
    
    /**
     * @param ymFormBuilderPanelPreviewItem previewItem
     * @returns undefined
     */
    removePreviewItem(previewItem) {
        var newCollection = [];
        for (var i = 0; i < this.previewItemCollection.length; i++) {
            if (this.previewItemCollection[i] !== previewItem) {
                newCollection.push(this.previewItemCollection[i]);
            }
        }
        this.previewItemCollection = newCollection;
    }
    
    /**
     * @param boolean state
     * @returns undefined
     */
    saveButtonState(state) { 
        // If false show that current state is not saved
        var button = document.getElementById("formBuilderButtonSave");
        if (state) { 
            button.className = "saved";
        } else {
            button.className = "";
        }
    }
    
    /**
     * @param JSONObject jsonElement
     * @returns undefined
     */
    createPreviewItem(jsonElement) {
        // Create
        var previewItem = new ymFormBuilderPanelPreviewItem();
        // Add item to collection
        this.addPreviewItem(previewItem);
        // Render preview item
        this.domPanelPreview.appendChild(previewItem.render());
        // Render preview item's content
        previewItem.renderContent(jsonElement);
        // Subscribe PanelEdit to preview item's edit
        previewItem.addEditListener(this.PanelEdit);
        // Subscribe PanelEdit to preview item's delete
        previewItem.addDeleteListener(this.PanelEdit);
        // Call preview item's edit event
        previewItem.eventClickEdit();
        // Subscribe this on preview items' remove 
        previewItem.addDeleteListener(this);
    }

    /**
     * Create and render a new preview item, update PanelEdit
     * 
     * @param ymFormBuilderPanelButton Button
     * @returns undefined
     */
    onPanelButtonClick(Button) {
        // Create preview item
        this.createPreviewItem(Button.getJsonElement());
        // Subscribe this on PanelEdit click
        this.PanelEdit.addClickListener(this);
        // Set save button's state
        this.saveButtonState(false);
    }
    
    /**
     * @param ymFormBuilderPanelEdit PanelEdit
     * @returns undefined
     */
    onPanelEditClick(PanelEdit) {
        // Show that current state is not saved
        this.saveButtonState(false);
    }
    
    /**
     * @param ymFormBuilderPanelPreviewItem previewItem
     * @returns undefined
     */
    onPanelPreviewItemDeleteClick(previewItem) {
        // Remove item from collection
        this.removePreviewItem(previewItem);
        // Show that current state is not saved
        this.saveButtonState(false);
    }
}

/**
 * @param JSONObject jsonElement
 * @returns ymFormBuilderPanelButton
 */
class ymFormBuilderPanelButton {
    
    constructor(jsonElement) {
        this.jsonElement = _clone(jsonElement);
        this.clickListeners = [];
    }
    
    /**
     * @returns JSONObject
     */
    getJsonElement() {
        return this.jsonElement;
    }

    /**
     * @returns HTMLDOMElement
     */
    render() {
        var instance = this;
        var span = document.createElement("span");
            span.innerHTML = this.jsonElement.button.innerHTML;
        var button = document.createElement("div");
            button.appendChild(span); 
            button.className = "button " + this.jsonElement.button.class;
            button.onclick = function() {
                instance.eventClick();
            }
        return button;
    }    

    /**
     * @param Object listener
     * @returns undefined
     */
    addClickListener(listener) {
        // Remove any 
        this.clickListeners.push(listener);
    }

    /**
     * @returns undefined
     */
    eventClick() {
        for (var i = 0; i < this.clickListeners.length; i++) {
            this.clickListeners[i].onPanelButtonClick(this);
        }
    }
}

/**
 * @param HHTMLDOMElement domPanelEdit
 * @returns ymFormBuilderPanelEdit
 */
class ymFormBuilderPanelEdit {
    
    constructor(domPanelEdit) {
        this.jsonElement;
        this.clickListeners = [];
        this.domPanelEdit = domPanelEdit;
    }
    
    /**
     * @returns JSONObject
     */
    getJsonElement() {
        return this.jsonElement;
    }
    
    /**
     * @param JSONObject jsonElement
     * @returns undefined
     */
    render(jsonElement) {
        this.jsonElement = _clone(jsonElement);
        
        var instance = this;
        // Clear contents
        this.domPanelEdit.innerHTML = "";
        // Render content wrapper
        var content = document.createElement("div");
            content.className = "content";
        this.domPanelEdit.appendChild(content);
        // Render button 
        var button = document.createElement("div");
            button.className = "button";
            button.innerHTML = "OK";
            button.onclick = function() {
                instance.eventClick();
            }
        this.domPanelEdit.appendChild(button);
        // Render elements
        for (var i = 0; i < jsonElement.elements.length; i++) {
            // Value
            var element = document.createElement(jsonElement.elements[i].tag);
                element.value = jsonElement.elements[i].value;
            // Attributes
            for (var key in jsonElement.elements[i].attributes) {
                element.setAttribute(key, jsonElement.elements[i].attributes[key]);
            }
            // Checked, if property exists
            if (typeof jsonElement.elements[i].checked !== "undefined") {
                element.checked = this.jsonElement.elements[i].checked;
            }
            // Options
            if (typeof jsonElement.elements[i].options !== "undefined") {
                var o = jsonElement.elements[i].options;
                var ymOptions = new ymFormBuilderOptions(o, element);
                ymOptions.render();
            }
            content.appendChild(element);
        }
    }
    
    /**
     * @param Object listener
     * @returns undefined
     */
    addClickListener(listener) {
        this.clickListeners.push(listener);
    }
    
    /**
     * @param String listenerType
     * @returns undefined
     */
    removeClickListenerByClassName(className) {
        var newClickListeners = [];
        for (var i = 0; i < this.clickListeners.length; i++) {
            if (this.clickListeners[i].constructor.name != className) {
                newClickListeners.push(this.clickListeners[i]);
            }
        }
        this.clickListeners = newClickListeners;
    }
    
    /**
     * @returns undefined
     */
    eventClick() {
        // Update jsonElement
        var elements = this.domPanelEdit.getElementsByClassName("content")[0]
                .children;
        for (var i = 0; i < elements.length; i++) {
            this.jsonElement.elements[i].tag = elements[i].tagName.toLowerCase();
            // Value
            this.jsonElement.elements[i].value = elements[i].value;
            // Attributes
            for (var k = 0; k < elements[i].attributes.length; k++) {
                var name = elements[i].attributes[k].name;
                var value = elements[i].attributes[k].value;
                this.jsonElement.elements[i].attributes[name] = value;
            }
            // Checked, only if property exists
            if (typeof elements[i].checked !== "undefined") {
                this.jsonElement.elements[i].checked = elements[i].checked;
            }
            // Options
            if (typeof this.jsonElement.elements[i].options !== "undefined") {
                var options = elements[i]
                    .getElementsByClassName("content")[0]
                    .getElementsByClassName("item-wrapper");
                this.jsonElement.elements[i].options = [];
                for (var k = 0; k < options.length; k++) {
                    this.jsonElement.elements[i].options[k] = {
                        name: options[k].children[1].name,
                        text: options[k].children[1].value,
                        value: options[k].children[2].value,
                        selected: options[k].children[3].checked === true 
                    };
                }
            }
        }
        
        for (var i = 0; i < this.clickListeners.length; i++) {
            this.clickListeners[i].onPanelEditClick(this);
        }
    }
    
    /**
     * Update edit panel
     * 
     * @param ymFormBuilderPanelPreviewItem PreviewItem
     * @returns undefined
     */
    onPanelPreviewItemEditClick(PreviewItem) {
        // Unsubscribe any registered PreviewItem and subscribe this one
        this.removeClickListenerByClassName("ymFormBuilderPanelPreviewItem");
        this.addClickListener(PreviewItem);
        
        this.render(PreviewItem.getJsonElement());
    }
    
    /**
     * Update edit panel
     * 
     * @param ymFormBuilderPanelPreviewItem PreviewItem
     * @returns undefined
     */
    onPanelPreviewItemDeleteClick(PreviewItem) {
        // Unsubscribe any registered PreviewItem and subscribe this one
        this.removeClickListenerByClassName("ymFormBuilderPanelPreviewItem");
        this.domPanelEdit.innerHTML = "";
    }
}

/**
 * @param JSONObject jsonElement
 * @returns ymFormBuilderPanelPreviewItem
 */
class ymFormBuilderPanelPreviewItem {
    
    constructor() {
        this.jsonElement;
        this.editListeners = [];
        this.deleteListeners = [];
        this.domWrapper;
        this.domContent;
    }
    
    /**
     * @returns JSONObject
     */
    getJsonElement() {
        return this.jsonElement;
    }
    
    /**
     * @returns undefined
     */
    render() {
        var instance = this;
        var editButton = document.createElement("div");
            editButton.className = "button edit";
            editButton.onclick = function() {
                instance.eventClickEdit();
            }
        var removeButton = document.createElement("div");
            removeButton.className = "button remove";
            removeButton.onclick = function() {
                instance.eventClickDelete();
            }
        var panel = document.createElement("div");
            panel.appendChild(editButton);
            panel.appendChild(removeButton);
            panel.className = "panel";
        instance.domContent = document.createElement("div");
        instance.domContent.className = "content";
        instance.domWrapper = document.createElement("div");
        instance.domWrapper.appendChild(panel);
        instance.domWrapper.appendChild(instance.domContent);
        instance.domWrapper.className = "form-builder-preview-item";
        return instance.domWrapper;
    }
    
    /**
     * @param JSONObject jsonElement
     * @returns undefined
     */
    renderContent(jsonElement) {
        this.jsonElement = _clone(jsonElement);
        this.domContent.innerHTML = "";
        this.domContent.appendChild(jsonElement.render());
    }
    
    /**
     * @returns undefined
     */
    remove() {
        this.domWrapper.parentNode.removeChild(this.domWrapper);
    }
    
    /**
     * @param Object listener
     * @returns undefined
     */
    addEditListener(listener) {
        this.editListeners.push(listener);
    }
    
    /**
     * @param Object listener
     * @returns undefined
     */
    addDeleteListener(listener) {
        this.deleteListeners.push(listener);
    }
    
    /**
     * @returns undefined
     */
    eventClickEdit() {        
        for (var i = 0; i < this.editListeners.length; i++) {
            this.editListeners[i].onPanelPreviewItemEditClick(this);
        }
    }
    
    /**
     * @returns undefined
     */
    eventClickDelete() {
        for (var i = 0; i < this.deleteListeners.length; i++) {
            this.deleteListeners[i].onPanelPreviewItemDeleteClick(this);
        }
        // Remove from DOM
        this.domWrapper.parentNode.removeChild(this.domWrapper);
    }
    
    /**
     * Update content
     * 
     * @param ymFormBuilderPanelEdit PanelEdit
     * @returns undefined
     */
    onPanelEditClick(PanelEdit) {
        this.renderContent(PanelEdit.getJsonElement());
    }
}

class ymFormBuilderOptions {
    
    constructor(jsonElementOptions, domWrapper) {
        this.jsonElementOptions = _clone(jsonElementOptions);
        this.domWrapper = domWrapper;
        this.domButtons;
        this.domContent;
    }
    
    /**
     * @returns undefined
     */
    render() {
        var instance = this;
        var label = document.createElement("label");
            label.innerHTML = "Options";
        this.domWrapper.appendChild(label);
        var addButton = document.createElement("div");
            addButton.className = "button add";
            addButton.onclick = function() {
                instance.domContent.appendChild(instance.renderOption());
            }
        var removeButton = document.createElement("div");
            removeButton.className = "button remove";
            removeButton.onclick = function() {
                var items = instance.domContent
                        .getElementsByClassName("item-wrapper");
                if (items.length) {
                    instance.domContent.removeChild(items[items.length-1]);
                }
            }
        this.domButtons = document.createElement("div");
        this.domButtons.appendChild(addButton);
        this.domButtons.appendChild(removeButton);
        this.domButtons.className = "buttons";
        this.domContent = document.createElement("div");
        this.domContent.className = "content";
        if (typeof this.jsonElementOptions !== "undefined") {
            for (var k = 0; k < this.jsonElementOptions.length; k++) {
                this.domContent.appendChild(this.renderOption(
                    this.jsonElementOptions[k]
                ));
            }
        }
        this.domWrapper.appendChild(this.domButtons);
        this.domWrapper.appendChild(this.domContent);
    }
    
    /**
     * @param JSONObject jsonElementOption
     * @returns HTMLDOMElement
     */
    renderOption(jsonElementOption) {
        var label = document.createElement("label");
            label.innerHTML = "Option";
        var text = document.createElement("input");
            text.setAttribute("placeholder", "{Text}");
        var value = document.createElement("input");
            value.setAttribute("placeholder", "{Value}");
        var selected = document.createElement("input");
            selected.type = "checkbox";
        if (typeof jsonElementOption !== "undefined") {
            text.value = jsonElementOption.text;
            value.value = jsonElementOption.value;
            selected.checked = jsonElementOption.selected;
        }
        var wrapper = document.createElement("div");
            wrapper.className = "item-wrapper";
            wrapper.appendChild(label);
            wrapper.appendChild(text);
            wrapper.appendChild(value);
            wrapper.appendChild(selected);
        return wrapper;
    }
}