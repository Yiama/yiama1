/**
 * @type Array
 */
var ymFormBuilderJsonGroups = [
    {   // Simple input
        id: "inputsimple",
        button: {
            class: "text-input",
            innerHTML: "Text Input"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Value
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Value}"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "input";
                object.type = "text";
                object.name = this.elements[2].value;
                object.value = this.elements[4].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            var input = document.createElement("input");
                input.value = this.elements[4].value;
            wrapper.appendChild(input);
            return wrapper;
        }
    }, {// Email input
        id: "inputemail",
        button: {
            class: "email-input",
            innerHTML: "Email Input"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Value
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Value}"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "input";
                object.type = "email";
                object.name = this.elements[2].value;
                object.value = this.elements[4].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            var input = document.createElement("input");
                input.type = "email";
                input.value = this.elements[4].value;
            wrapper.appendChild(input);
            return wrapper;
        }
    }, {// Textarea
        id: "textarea",
        button: {
            class: "textarea",
            innerHTML: "Textarea"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Text
                tag: "textarea",
                value: "",
                attributes: {
                    placeHolder: "{Text}"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "textarea";
                object.name = this.elements[2].value;
                object.value = this.elements[4].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            var textarea = document.createElement("textarea");
                textarea.value = this.elements[4].value;
            wrapper.appendChild(textarea);
            return wrapper;
        }
    }, {// Select
        id: "select",
        button: {
            class: "select",
            innerHTML: "Dropdown"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Select
                tag: "div",
                options: [
                ],
                attributes: {
                    class: "options-wrapper"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "select";
                object.name = this.elements[2].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
                object.options = [];
            // Options
            for (var i = 0; i < this.elements[4].options.length; i++) {
                object.options.push({
                    text: this.elements[4].options[i].text,
                    value: this.elements[4].options[i].value,
                    selected: this.elements[4].options[i].selected
                });
            }
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            var select = document.createElement("select");
            for (var i = 0; i < this.elements[4].options.length; i++) {
                var option = new Option(
                    this.elements[4].options[i].text,
                    this.elements[4].options[i].value,
                    this.elements[4].options[i].selected,
                    this.elements[4].options[i].selected
                );
                select.add(option);
            }
            wrapper.appendChild(select);
            return wrapper;
        }
    }, {// Checkbox
        id: "checkbox",
        button: {
            class: "checkbox",
            innerHTML: "Checkboxes"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Select
                tag: "div",
                options: [
                ],
                attributes: {
                    class: "options-wrapper"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "checkbox-group";
                object.name = this.elements[2].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
                object.options = [];
            // Options
            for (var i = 0; i < this.elements[4].options.length; i++) {
                object.options.push({
                    type: "checkbox",
                    text: this.elements[4].options[i].text,
                    value: this.elements[4].options[i].value,
                    selected: this.elements[4].options[i].selected
                });
            }
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            for (var i = 0; i < this.elements[4].options.length; i++) {
                var checkbox = document.createElement("input");
                    checkbox.type = "checkbox";
                    checkbox.value = this.elements[4].options[i].value;
                    checkbox.checked = this.elements[4].options[i].selected;
                var label = document.createElement("label");
                    label.innerHTML = this.elements[4].options[i].text;
                    label.appendChild(checkbox);
                wrapper.appendChild(label);
            }
            return wrapper;
        }
    }, {// Radio
        id: "radio",
        button: {
            class: "radio",
            innerHTML: "Radio buttons"
        },
        elements: [
            {   // Label
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Label}"
                }
            }, {// Sublabel
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Sublabel}"
                }
            }, {// Name
                tag: "input",
                value: "",
                attributes: {
                    placeHolder: "{Name}"
                }
            }, {// Required
                tag: "input",
                value: "",
                attributes: {
                    type: "checkbox",
                    title: "Required"
                }
            }, {// Select
                tag: "div",
                options: [
                ],
                attributes: {
                    class: "options-wrapper"
                }
            }
        ],
        toObject: function() {
            var object = {};
                object.tag = "radio-group";
                object.name = this.elements[2].value;
                object.required = this.elements[3].checked === true;
                object.label = this.elements[0].value;
                object.sublabel = this.elements[1].value;
                object.options = [];
            // Options
            for (var i = 0; i < this.elements[4].options.length; i++) {
                object.options.push({
                    type: "radio",
                    text: this.elements[4].options[i].text,
                    value: this.elements[4].options[i].value,
                    selected: this.elements[4].options[i].selected
                });
            }
            return object;
        },
        render: function() {
            var wrapper = document.createElement("div");
                wrapper.className = "group";
            var label = document.createElement("label");
                label.innerHTML = this.elements[0].value
                    + (this.elements[3].checked === true
                        ? "<span class='required'></span>"
                        : "");
            wrapper.appendChild(label);
            var sublabel = document.createElement("div");
                sublabel.className = "sublabel";
                sublabel.innerHTML = this.elements[1].value;
            wrapper.appendChild(sublabel);
            for (var i = 0; i < this.elements[4].options.length; i++) {
                var radio = document.createElement("input");
                    radio.type = "radio";
                    radio.value = this.elements[4].options[i].value;
                    radio.checked = this.elements[4].options[i].selected;
                var label = document.createElement("label");
                    label.innerHTML = this.elements[4].options[i].text;
                    label.appendChild(radio);
                wrapper.appendChild(label);
            }
            return wrapper;
        }
    }
];