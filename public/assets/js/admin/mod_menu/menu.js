function menuV( element, level )
{
	var parentElements = element.parentNode.getElementsByTagName( '*' );
	var Ul_Li_Elements = [];
	for( var i = 0; i < parentElements.length; i++ )
	{
		if( parentElements[i].nodeName == 'UL' || parentElements[i].nodeName == 'LI' )
		{
			Ul_Li_Elements.push( parentElements[i] );
		}
	}
	
	for( var i = 0; i < Ul_Li_Elements.length; i++ )
	{
		if( typeof Ul_Li_Elements[i+1] !== 'undefined' && Ul_Li_Elements[i] == element )
		{
			var nextEl = Ul_Li_Elements[i+1];
			if( nextEl.nodeName == 'UL' )
			{
				if( nextEl.style.maxHeight == '10000px' )
				{
					nextEl.style.maxHeight = 0;
				}
				else
				{
					nextEl.style.maxHeight = '10000px';
				}
			}
		}
		else if( typeof Ul_Li_Elements[i+1] !== 'undefined' && Ul_Li_Elements[i].getAttribute( 'level' ) == element.getAttribute( 'level' ) )
		{
			if( Ul_Li_Elements[i+1].nodeName == 'UL' )
			{
				Ul_Li_Elements[i+1].style.maxHeight = 0;
			}
		}
	}
}