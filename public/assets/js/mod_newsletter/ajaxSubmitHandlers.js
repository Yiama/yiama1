/*
@param formObj ( object )
@param responseText ( ajax response text )
*/
function ajaxSuccessNewsletterHandler( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', 'rc1' );
	
	var divMsg = _get( 'formNewsletterMsg' );
	var objResponse = JSON.parse( responseText );
	
	if( objResponse.status )
	{
		divMsg.innerHTML = objResponse.messages.formSendSuccess;
		divMsg.setAttribute( 'class', 'success' );
	}
	else
	{
		divMsg.innerHTML = objResponse.messages.formSendError + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}
	
	divMsg.style.display = 'block';
}

/*
@param formObj ( object )
*/
function errorHandler( formObj, errorMsg )
{
	alert( errorMsg );
}