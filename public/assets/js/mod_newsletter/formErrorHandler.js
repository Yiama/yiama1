/*
@param formObj ( object )
@param ar ( fieldnames array )
@param errorMsg
*/
function handleNewsletterError( formObj, ar, errorMsg )
{
	var divMsg = document.getElementById( 'formNewsletterMsg' );
	
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', 'rc1' );
	
	divMsg.innerHTML = errorMsg;
	divMsg.setAttribute( 'class', 'error' );
	divMsg.style.display = 'block';
	
	for( var i = 0; i < ar.length; i++ )
	{
		for( var k = 0; k < formObj.elements.length; k++ )
		{
			if( formObj.elements[k].name == ar[i] )
			{
				formObj.elements[k].setAttribute( 'class', formObj.elements[k].className + ' highlighted' );
			}
		}
	}
}

function onFocusHideNewsletterMsg( element, classname )
{
	_get( 'formNewsletterMsg' ).style.display = 'none';
	element.className = classname;
}