/*
	@param formObj ( object )
	@param responseText ( ajax response text )
*/

function ajaxSuccessHandler( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', '' );
	
	var divMsg = _get( 'formMsg' );

	var objResponse = JSON.parse( responseText );
	if( objResponse.redirect )
	{
		window.location.href = decodeURIComponent( objResponse.redirect );
		return;
	}

	if( objResponse.status === 1 && typeof objResponse.messages.formSuccess !== 'undefined' )
	{
		divMsg.innerHTML = objResponse.messages.formSuccess;
		divMsg.setAttribute( 'class', 'success' );
	}
	else if( objResponse.status === 0 && typeof objResponse.messages.formFailure !== 'undefined'  )
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAnc';
}


/***** USER [[ ****/
function ajaxSuccessHandlerLogin( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', '' );
	
	var divMsg = _get( 'formMsgLogin' );
	
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === true )
	{
		divMsg.innerHTML = objResponse.messages.formLoginsuccess;
		divMsg.setAttribute( 'class', 'success' );
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
		window.location.href = decodeURIComponent( objResponse.redirect );
		return;
	}
	else if( objResponse.status === 1 )
	{
		divMsg.innerHTML = objResponse.messages.formLoginprivileges;
		divMsg.setAttribute( 'class', 'error' );
	}
	else if( objResponse.status === false )
	{
		divMsg.innerHTML = objResponse.messages.formLoginfailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAncLogin';
}

/*
function ajaxSuccessHandlerRegister( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', '' );
	
	var divMsg = _get( 'formMsg' );
	
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === 1 )
	{
		/* Ορίζεται στο view */
/*
		popupRegister.pop();
		window.scrollTo( 0, 0 );
	}
	else if( objResponse.status === 2 )
	{
		divMsg.innerHTML = objResponse.messages.formRegisterUserExists;
		divMsg.setAttribute( 'class', 'error' );
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
	}
	else if( objResponse.status === 3 )
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
	}
}
*/

function ajaxSuccessHandlerRegister( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', '' );
	
	var divMsg = _get( 'formMsg' );
	
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === 1 )
	{
		divMsg.innerHTML = objResponse.messages.formRegisterSuccess;
		divMsg.setAttribute( 'class', 'success' );
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
		if( objResponse.redirect )
		{
			window.location.href = decodeURIComponent( objResponse.redirect );
			return;
		}
	}
	else if( objResponse.status === 2 )
	{
		divMsg.innerHTML = objResponse.messages.formRegisterUserExists;
		divMsg.setAttribute( 'class', 'error' );
	}
	else if( objResponse.status === 3 )
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAnc';
}

function ajaxSuccessHandlerRequestNewpassword( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', 'small' );
	
	var divMsg = _get( 'formMsgRequestNewpassword' );
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === 1 )
	{
		divMsg.innerHTML = objResponse.messages.formRequestNewpasswordSuccess;
		divMsg.setAttribute( 'class', 'success' );
	}
	else if( objResponse.status === 2 )
	{
		divMsg.innerHTML = objResponse.messages.formLoginfailure;
		divMsg.setAttribute( 'class', 'error' );
	}
	else if( objResponse.status === 3 )
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAncRequestNewpassword';
}

function ajaxSuccessHandlerResetpassword( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', 'small' );
	
	var divMsg = _get( 'formMsg' );
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === 1 )
	{
		divMsg.innerHTML = objResponse.messages.formResetpasswordSuccess;
		divMsg.setAttribute( 'class', 'success' );
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
		window.location.href = decodeURIComponent( objResponse.redirect );
		return;
	}
	else
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAnc';
}

function ajaxSuccessHandlerChangeinfo( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', 'small' );
	
	var divMsg = _get( 'formMsg' );
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === 0 )
	{
		divMsg.innerHTML = objResponse.messages.formChangeinfoSuccess;
		divMsg.setAttribute( 'class', 'success' );
	}
	else if( objResponse.status === 1 )
	{
		divMsg.innerHTML = objResponse.messages.formChangeinfoPasswordFailure;
		divMsg.setAttribute( 'class', 'error' );
	}
	else if( objResponse.status === 2 )
	{
		divMsg.innerHTML = objResponse.messages.formFailure + '<big>!</big>';
		divMsg.setAttribute( 'class', 'error' );
	}

	divMsg.style.display = 'block';
	window.location.href = '#msgAnc';
}

/***** ]] USER  ****/


/***** RATING  ****/
function ajaxSuccessHandlerRating( formObj, responseText )
{
	formObj.getElementsByTagName( 'button' )[0].setAttribute( 'class', '' );
	
	var divMsg = _get( 'formMsg' );
	
	var objResponse = JSON.parse( responseText );
	if( objResponse.status === true )
	{
		window.location.reload(); 
	}
	else
	{
		divMsg.style.display = 'block';
		window.location.href = '#msgAnc';
	}
}

/*
@param formObj ( object )
*/
function ajaxErrorHandler( formObj )
{
	alert( 'Ajax error!' );
}