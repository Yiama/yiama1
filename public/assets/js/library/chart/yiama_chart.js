function line_chart( canvasId, data, options )
{
	var ctx = document.getElementById( canvasId ).getContext( "2d" );
	
	var chartOptions = {
		// Boolean - whether or not the chart should be responsive and resize when the browser does.
	    "responsive": true,
	    
	    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	    "maintainAspectRatio": false,
		
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	    "scaleBeginAtZero": false,
		    
		//Boolean - Whether grid lines are shown across the chart
	    "scaleShowGridLines": true,
	    
	    //String - Colour of the grid lines
	    "scaleGridLineColor": "rgba(0,0,0,.05)",
	
	    //Number - Width of the grid lines
	    "scaleGridLineWidth": 1,
	
	    //Boolean - Whether to show horizontal lines (except X axis)
	    "scaleShowHorizontalLines": true,
	
	    //Boolean - Whether to show vertical lines (except Y axis)
	    "scaleShowVerticalLines": true,
	
	    //Boolean - Whether the line is curved between points
	    "bezierCurve": true,
	
	    //Number - Tension of the bezier curve between points
	    "bezierCurveTension": 0.4,
	
	    //Boolean - Whether to show a dot for each point
	    "pointDot": true,
	
	    //Number - Radius of each point dot in pixels
	    "pointDotRadius": 4,
	
	    //Number - Pixel width of point dot stroke
	    "pointDotStrokeWidth": 1,
	
	    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	    "pointHitDetectionRadius": 20,
	
	    //Boolean - Whether to show a stroke for datasets
	    "datasetStroke": true,
	
	    //Number - Pixel width of dataset stroke
	    "datasetStrokeWidth": 2,
	
	    //Boolean - Whether to fill the dataset with a colour
	    "datasetFill": true
	
	    //String - A legend template
	    //"legendTemplate": "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-co"lor":<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	}
	
	if( options !== 'undefined' ) {
		for( var key in options )  {
			if( options.hasOwnProperty( key ) )  {
				chartOptions[ key ] = options[ key ];
			}
		}
	}
	
	var lineChart = new Chart(ctx).Line( data, chartOptions );
	
	if( typeof chartOptions['legendTemplate'] !== 'undefined' ) {
		document.getElementById( chartOptions['legendTemplateHTMLElementId'] ).innerHTML = lineChart.generateLegend();
	}
}

function bar_chart( canvasId, data, options )
{
	var ctx = document.getElementById( canvasId ).getContext( "2d" );
	
	var chartOptions = {
		// Boolean - whether or not the chart should be responsive and resize when the browser does.
	    "responsive": true,
	    
	    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	    "maintainAspectRatio": false,
		    
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	    "scaleBeginAtZero": true,
	
	    //Boolean - Whether grid lines are shown across the chart
	    "scaleShowGridLines": true,
	
	    //String - Colour of the grid lines
	    "scaleGridLineColor": "rgba(0,0,0,.05)",
	
	    //Number - Width of the grid lines
	    "scaleGridLineWidth": 1,
	
	    //Boolean - Whether to show horizontal lines (except X axis)
	    "scaleShowHorizontalLines": true,
	
	    //Boolean - Whether to show vertical lines (except Y axis)
	    "scaleShowVerticalLines": true,
	
	    //Boolean - If there is a stroke on each bar
	    "barShowStroke": true,
	
	    //Number - Pixel width of the bar stroke
	    "barStrokeWidth": 2,
	
	    //Number - Spacing between each of the X value sets
	    "barValueSpacing": 5,
	
	    //Number - Spacing between data sets within X values
	    "barDatasetSpacing": 1
	
	    //String - A legend template
	    //"legendTemplate": "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	}
	
	if( options !== 'undefined' ) {
		for( var key in options )  {
			if( options.hasOwnProperty( key ) )  {
				chartOptions[ key ] = options[ key ];
			}
		}
	}
	
	var barChart = new Chart(ctx).Bar( data, chartOptions );
}

var chartJsColorArray = new Array();
function chartJsMakeColors( noOfColors ) 
{
	var frequency = 10 / noOfColors; 
	for( var i = 0; i < noOfColors; ++i ) {
        r = Math.sin(frequency * i + 0) * (127) + 128;
        g = Math.sin(frequency * i + 1) * (127) + 128;
        b = Math.sin(frequency * i + 3) * (127) + 128;
        chartJsColorArray.push( "rgba( " + Math.floor(r) + ", " + Math.floor(g) + ", " + Math.floor(b) + ", 0.4 )" );
    }
}

function chartJsGetColor( i )
{
	return chartJsColorArray[i];
}