/*
scroll_to	@param func ( string ) { Function }
	@param event ( string ) { Event => 'onresize' || 'onscroll' }
*/
var onloadArray = new Array();
var onresizeArray = new Array();
var onscrollArray = new Array();
var onclickArray = new Array();

function _addOnLoad( func, event )
{
	if( typeof( event ) === 'undefined' )
	{
		onloadArray[ onloadArray.length ] = func;
	}
	else if( event.indexOf( 'onresize' ) === 0 )
	{
		onresizeArray[ onresizeArray.length ] = func;
	}
	else if( event.indexOf( 'onscroll' ) === 0 )
	{
		onscrollArray[ onscrollArray.length ] = func;
	}
	else if( event.indexOf( 'onclick' ) === 0 )
	{
		onclickArray[ onclickArray.length ] = func;
	}
}

/*
	Καλείται στο onload event του body.
*/
function _onLoad()
{
	var s = '';
	if( onloadArray.length )
	{
		s += onloadArray.join( ';' );
	}
	if( onresizeArray.length )
	{
		s += '; window.onresize = function(){' + onresizeArray.join( ';' ) + ';}';
	}
	if( onscrollArray.length )
	{
		s += '; window.onscroll = function(){' + onscrollArray.join( ';' ) + ';}';
	}
	if( onclickArray.length )
	{
		s += '; window.onclick = function(){' + onclickArray.join( ';' ) + ';}';
	}
	
	var f = new Function( s );
	
	f();
}

/* 
	Get element by id. 
	
	@id id ( int )
*/
function _get( id )
{
	if( id.indexOf( '.' ) === 0 ) {
		id = id.substr( 1 );
		var id_classes = id.split( ' ' );
		var cnt = id_classes.length;
		var els = document.getElementsByTagName( '*' );
		var found_els = [];
		for( i = 0; i < els.length; i++ ) {
			if( typeof els[i].className !== 'undefined' ) {
				var classes = els[i].className.split( ' ' );
				var found = 0;
				for( k = 0; k < classes.length; k++ ) {
					for( l = 0; l < cnt; l++ ) {
						if( id_classes[l] == classes[k] ) {
							found++;
							break;
						}
					}
				}
				if( found == cnt ) {
					found_els.push( els[i] );
				}
			}
		}
		return found_els;
	}
	var el = document.getElementById( id );
	return typeof el !== 'undefined' ? el : false;
}

/* 
	Get elements by id prefix. 
	
	@id id ( int )
*/
function _getAll( idPrefix )
{
	var elements = document.getElementsByTagName( '*' );
	var returnArray = new Array();
	for( var i = 0; i < elements.length; i++ )
	{
		if( elements[i].hasAttribute('id') )
		{
			if( elements[i].id.indexOf( idPrefix ) === 0 )
			{
				returnArray.push( _get( elements[i].id ) );
			}
		}
	}
	
	return returnArray;
}

/* 
	Get <select> selected option ( option object ). 
	
	@param param ( mixed ){ <select> id || <select> object instance. }
*/
function _selectedOption( param )
{
	if( typeof param === 'object' )
	{
		return param.options[param.selectedIndex];
	}

	var el = document.getElementById( param );
	return el.options[el.selectedIndex];
}

/* 
	Set <select> selected option ( option object ). 
	
	@param param ( mixed ){ <select> id || <select> object instance. }
	@param index ( int ){ <select> option index. }
*/
function _setSelectedOption( param, index )
{
	if( typeof param === 'object' )
	{
		param.selectedIndex = index;
		return;
	}
	
	document.getElementById( param ).selectedIndex = index;
}

/* 
	Send request. 
		
	@param link ( string )
*/
function _request( link )
{
	window.location.href=link;
}

/* 
	Display none. 
	
	@param param ( mixed ){ <element> id || <element> object instance. }
*/
function _hide( param )
{
	_setAttributes( param, { 'style': 'display: none' } );
}

/* 
	Display none. 
	
	@param param ( mixed ){ <element> ids Array || <element> id string offset. }
*/
function _hideAll( param )
{
	if( typeof param === 'object' )
	{
		for( var i = 0; i < param.length; i++ )
		{
			_hide( _get( param[i] ) );
		}
		return;
	}
	
	var elements = document.getElementsByTagName( '*' );
	for( var i = 0; i < elements.length; i++ )
	{
		if( elements[i].hasAttribute('id') )
		{
			if( elements[i].id.indexOf( param ) === 0 )
			{
				_hide( _get( elements[i].id ) );
			}
		}
	}
}

/* 
	Display block. 
	
	@param param ( mixed ){ <element> id || <element> object instance. }
	@param display ( string ) { Block, inline, inline-block. }
*/
function _show( param, displayType )
{
	if (typeof displayType === 'undefined') 
	{ 
		displayType = 'block'; 
	}
	
	_setAttributes( param, { 'style': 'display: ' + displayType } );
}

/* 
Display block. 

@param param ( mixed ){ <element> ids Array || <element> id string offset. }
@param displayType 
*/
function _showAll( param, displayType )
{
	if( typeof param === 'object' )
	{
		for( var i = 0; i < param.length; i++ )
		{
			_show( _get( param[i] ) );
		}
		return;
	}
	
	var elements = document.getElementsByTagName( '*' );
	for( var i = 0; i < elements.length; i++ )
	{
		if( elements[i].hasAttribute('id') )
		{
			if( elements[i].id.indexOf( param ) === 0 )
			{
				_show( _get( elements[i].id ), displayType );
			}
		}
	}
}

/* 
Set attributes για ένα element. 

@param param ( mixed ){ <element> id || <element> object instance. }
@param attributesObj ( string ) { Attributes in object format }
{ 'disabled': 'disabled', 'class': 'font-size: 10pt; font-weight: bold' }
*/
function _setAttributes( param, attributesObj )
{
	var element = typeof param === 'object' ? param : document.getElementById( param );
		
	for( var key in attributesObj ) 
	{
	  	if( attributesObj.hasOwnProperty( key ) )
	  	{
	  		element.setAttribute( key, attributesObj[ key ] );
		}
	}
}

/* 
Set attributes για πολλά elements. 

@param array ( mixed ){ <elements> { comma seperated ids } || <elements> object instances. }
@param attributesObj ( string ) { Attributes in object format }
{ 'disabled': 'disabled', 'class': 'font-size: 10pt; font-weight: bold' }
*/
function _setAttributesAll( array, attributesObj )
{
	var elements = new Array();
	if( typeof array === 'string' )
	{
		var ids = array.split( ',' );
		for( var i = 0; i < ids.length; i++ )
		{
			elements.push( document.getElementById( ids[i] ) );
		}
	}
	else
	{
		elements = array;
	}
		
	for( var key in attributesObj ) 
	{
	  	if( attributesObj.hasOwnProperty( key ) )
	  	{
	  		for( var i = 0; i < elements.length; i++ )
	  		{
	  			elements[i].setAttribute( key, attributesObj[ key ] );
	  		}
		}
	}
}

/* 
Remove attributes για πολλά elements. 

@param array ( mixed ){ <elements> { comma seperated ids } || <elements> object instance. }
@param attributesString ( string ) { Attributes comma seperated: 'disabled,class' }
*/
function _removeAttributesAll( array, attributesString )
{
	var elements = new Array();
	if( typeof array === 'string' )
	{
		var ids = array.split( ',' );
		for( var i = 0; i < ids.length; i++ )
		{
			elements.push( document.getElementById( ids[i] ) );
		}
	}
	else
	{
		elements = array;
	}
		
	var attributes = attributesString.split( ',' );
	for( var i = 0; i < attributes.length; i++ ) 
	{
	  	for( var k = 0; k < elements.length; k++ )
  		{
  			elements[k].removeAttribute( attributes[i] );
  		}
	}
}

/* 
	Remove element από το DOM. 
	
	@param param ( mixed ){ <element> id || <element> object instance. }
*/
function _remove( param )
{
	if( typeof param === 'object' )
	{
		param.parentNode.removeChild( param );
		return;
	}
	
	document.getElementById( param ).parentNode.removeChild( document.getElementById( param ) );
}

/*
	@param id ( int )
	@param html ( string )
	@param onclk ( boolean ) { Αν θα εξαφανίζεται με το onclick event. }
	@param attrs ( array ) { Attributes array. }
*/
function _popup( id, html, attrs )
{
	if( ( win = document.getElementById( id ) ) != null )
	{
		win.style.display = 'block';
		return; 
	}
	
	var win = document.createElement( 'div' );
	
	win.setAttribute( 'id', id );
	if( attrs )
	{
		for( var i = 0; i < attrs.length; i++ )
		{
			var ar = attrs[i].split( '~' );
			win.setAttribute( ar[0], ar[1] );
		}
	}
	win.innerHTML = html;

	document.body.appendChild( win );
}

/*
	@return ( int ) { Window width. }
*/
function _winWdt()
{
	return Math.max( document.documentElement.clientWidth, window.innerWidth || 0 );
}

/*
	@return ( int ) { Window height. }
*/
function _winHgt()
{
	if( document.body && document.body.offsetWidth ) 
	{
		var winH = document.body.offsetHeight;
	}
	if( document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) 
	{
		var winH = document.documentElement.offsetHeight;
	}
	if( window.innerWidth && window.innerHeight ) 
	{
		var winH = window.innerHeight;
	}
	
	return winH;	
}

/*
	@return ( int ) { Document height. }
*/
function _docHgt()
{
	var D = document;
    return Math.max
	(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );	
}

/*
	@param str ( string )
	@return ( string ) { Escaped HTML tags. }
*/
function _escapeHTMLTags( str ) 
{
    return str.replace( /&/g, '&amp;' ).replace( /</g, '&lt;' ).replace( />/g, '&gt;' );
}

/*
	@param str ( string ) { Escaped HTML tags. }
	@return ( string ) { Unescaped HTML tags. }
*/
function _unescapeHTMLTags( str ) 
{
    return str.replace( '/&amp;/g', '&' ).replace( '/&lt;/g', '<' ).replace( '/&gt;/g', '>' );
}

/*
	Αντίστοιχη της PHP in_array.
	Ελέγχει αν υπάρχει needle στον array ( haystack ).
	
	@param needle ( int/string )
	@param haystack ( array )
*/
function _inArray( needle, haystack ) 
{
    if( haystack.length )
    {
		for( var i = 0; i < haystack.length; i++ ) 
		{
			if( haystack[i] == needle ) 
			{
				return true;
			}
		}
	}
	
    return false;
}

/*
	Αντίστοιχη της PHP array_diff.
	
	@param array1 ( array )
	@param array2 ( array )
*/
function _arrayDiff( array1, array2 ) 
{
	if( array1.length )
    {
		for( var i = 0; i < array1.length; i++ ) 
		{
			for( var k = 0; k < array2.length; k++ ) 
			{
				if( array1[i] == array2[k] ) 
				{
					array1.splice( i, 1 );
				}
			}
		}
	}
    return array1;
}

/*
	Include JSON code για IE<8
*/
function _includeJSON()
{
	if( typeof JSON == 'undefined' )
	{
		var js = document.createElement("script");
	
		js.type = "text/javascript";
		js.src = "cdnjs.cloudflare.com/ajax/libs/json3/3.3.2/json3.min.js";
	
		document.body.appendChild(js);
	}
}

/*
	Προσθέτει επιπλέον class σε ένα html element
	
	@param name ( className για πρόσθεση )
	@param element ( object )
*/
function _addClass( element, name )
{
	var exists = false;
	if( element.className ) {
		var classes = element.className.split( ' ' );
		for( var i = 0; i < classes.length; i++ ) {
			if( classes[i] == name ) {
				exists = true;
				break;
			}
		}
	} else {
		var classes = [];
	}
	if( ! exists ) {
		classes.push(name);
		element.className = classes.join( ' ' );
	}
}

/*
	Αφαιρεί μία class από ένα html element
	
	@param name ( className για πρόσθεση )
	@param element ( object )
*/
function _removeClass( element, name )
{
	element.className = element.className.replace( name, '' ).replace( / +(?= )/g, ' ' ).trim();
}

function _hasClass( element, className ) 
{
    return element.className && new RegExp( "(^|\\s)" + className + "(\\s|$)" ).test( element.className );
}

/*
 * Επιστρέφει τα Pixel που μετατοπίστηκε το window κατά το scroll
 */
function _getScrollValue()
{
	if(typeof window.pageYOffset != "undefined")
	{
		var scroll = window.pageYOffset;
	}
	else if(typeof document.documentElement.scrollTop != "undefined")
	{
		var scroll = document.documentElement.scrollTop;
	}
	else
	{
		var scroll = document.body.scrollTop;
	}
	
	return parseFloat( scroll );
}

/* 
 * Merge javascript 2 objects 
 * @params obj1 Αρχικό Object
 * @params obj2 Object που θα εννοποιηθεί
 * @param overwrite Merge same attributes / default value = false
 */
function _objMerge( obj1, obj2, overwrite ) {
	overwrite = typeof overwrite !== 'undefined' ? overwrite : false; /* Αν θα γίνεται αντικατάσταση των ίδιων attributes */
    for( var key in obj2 ) {
        if( obj2.hasOwnProperty( key ) ) {
        	 if( obj1.hasOwnProperty( key ) && overwrite ) {
    			 obj1[ key ] = obj2[ key ];
    		 } else if( !obj1.hasOwnProperty( key ) ) {
    			 obj1[ key ] = obj2[ key ];
    		 }
        }
    }
    return obj1;
}

/*
 * reload page
 */
function _reload()
{
	window.location.reload( true );
}

/*
 * @param json ( string ) '{"option_value":"option_text",...}'
 */
function _selectOptions( json, select_id ) 
{
	var json_options = JSON.parse( json );
	var select = _get( select_id );
	select.options.length = 0;
	var i = 0;
	select.options[ i++ ] = new Option( '', '' );
	for( var key in json_options ) {
			if( json_options.hasOwnProperty( key ) ) {
				select.options[ i++ ] = new Option( json_options[ key ], key );
			}
		}
}

function _getMultipleSelectedOptionValues( select_element ) 
{
	var values = [];
	for( i = 0; i < select_element.options.length; i++ ) {
		if( select_element.options[i].selected ) {
			values.push( select_element.options[i].value );
		}		
	}
	return values;
}
function _addEvent( el, event, func )
{
	if( document.addEventListener ) {    
		// For all major browsers, except IE 8 and earlier
		el.addEventListener( event, func );
	} else if( document.attachEvent ) {  
		// For IE 8 and earlier versions
		el.attachEvent( 'on' + event, func );
	}
}

/**
 * !!!Foreign code to clone objects
 * 
 * @param Object obj
 * @returns Object
 */
function _clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = _clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = _clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}