/*
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	ΠΡΟΣΟΧΗ: 
	Η method POST στον IE δεν στέλνει πάντα με την send() το querystring,
	πράγμα που οδηγεί στο μονόδρομο της χρήσης της method GET.
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	@param params ( data structure - associative array ){
		var params = {
			key1: 'value1',  
			key2: [ 'value1', value2 ], 
			key3: [ myStruct{ key: 'val1', 'vl2', ... }, function(){ ... } ], 
			... 
		}; 
	}
	Αν το key ονομαστεί με κάποια reserved word απαιτούνται quotes.
	Το value μπορεί να είναι οποιοδήποτε object { string, function, data structure, ... }
	
	Παράδειγμα:
		_ajaxRequest(
			{
				method: 'post',
				url: '/asdasd/asdads',
				querystring: 'x=4&y=2', { Χρήση με POST method. Αν στέλνουμε με GET method το querystring γράφεται μαζί με το URL 'domain/page?x=5&y=2'. }
				async: true,
				//username: '',
				//userpassword: '',
				headers: [ ['headername1', 'headervalue1'], ['headername2', 'headervalue2'], ... ], { 2D Array }
				successhandler: function( responseText ){ ... responseText ... },
				//errorhandler: function(){ ... }
			}
		);
*/
function _ajaxRequest( params )
{	
	/* XMLHttpRequest object */ 
	if( window.XMLHttpRequest ) {
		var xmlHttp = new XMLHttpRequest(); /* ( ελέγχεται πρώτο ), IE7+, Firefox, Chrome, Opera, Safari */
	}
	else {
		var xmlHttp = new ActiveXObject( 'Microsoft.XMLHTTP' ); /* code for IE6, IE5 */
	}
	if( ! xmlHttp ) {
		alert( 'Το object XMLHttpRequest δεν δημιουργήθηκε!!!' );
		return;
	}
	
	/* Open connection */
	xmlHttp.open( 
		params['method'], 
		params['url'], 
		( typeof params['async'] !== ' undefined' ) ? params['async'] : true, 
		( typeof params['username'] !== 'undefined' ) ? params['username'] : '', 
		( typeof params['userpassword'] !== 'undefined' ) ? params['userpassword'] : '' 
	);
	
	/* Headers */
	/* Πλέον δημιουργεί προβλήματα σε μερικούς browser
	if( typeof( params['querystring'] ) !== 'undefined' ) {
		xmlHttp.setRequestHeader( "Content-length", params['querystring'].length );
	}
	*/
	xmlHttp.setRequestHeader( "X-Requested-With", "XMLHttpRequest" ); /* Το ελέγχουμε στον server για να αναγνωρίσουμε ότι είναι ajax request. */
    // If is formdata we dont send the header on post
	if( params['method'] == 'post' && typeof params['formData'] === 'undefined' ) {
		xmlHttp.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	}
	if( typeof( params['headers'] ) !== 'undefined' ) {
		for( var i = 0; i < params['headers'].length; i++ ) {
			xmlHttp.setRequestHeader( params['headers'][i][0], params['headers'][i][1] );
		}	
	}
	
	/* Ready state */
	xmlHttp.onreadystatechange = function(){		
		switch( xmlHttp.readyState ) {
			case 0: { /* uninitialized */
				if( typeof( params['errorhandler'] ) !== 'undefined' ) {
					params['errorhandler'];
				}
			}
			case 4: { /* complete */
                if ( xmlHttp.status == 200 ) {
					params['successhandler'].call( this, xmlHttp.responseText );
				}
			}
		}
	}
	
	/* Send */
	if( params['method'] == 'post' && typeof params['formData'] !== 'undefined' ) {
        xmlHttp.send( params['formData'] );
	} else if( params['method'] == 'post' ) {
        xmlHttp.send( typeof params['querystring'] !== 'undefined' ? params['querystring'] : null );
    } else {
		xmlHttp.send();
	}
}