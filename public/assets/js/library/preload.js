/*
	@param paths ( string ) { Comma seperated image paths. }
*/
function _preloadImg( paths )
{
	var paths = paths.split( ',' );
	
	for( var i = 0; i < paths.length; i++)
	{
		var img = new Image();
		img.src = paths[i];
	}
}