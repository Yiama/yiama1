/*
	Ελέγχει όλα τα form elements αν έχουν το attribute 'isRequired' και αυτό αν έχει την τιμή 'yes'.
	
	@param formObj ( object )
	@return ( mixed ) { Form element names / boolean }
*/
function checkRequiredFields( formObj ) 
{
	var emptyFields = new Array();
	
	for( var i = 0; i < formObj.elements.length; i++ )
	{
		if( ( attr = formObj.elements[i].getAttribute( 'isRequired' ) ) != null )
		{
			if( attr == 'yes' && formObj.elements[i].value.length == 0 )
			{
				emptyFields[emptyFields.length] = new String( formObj.elements[i].name );
			}
		}
	}
	
	if( emptyFields.length ) 
		return emptyFields;

	return true;
}

/*
	@param formObj ( object )
	@return ( mixed ) { Form element names / boolean }
*/
function checkFieldValue( formObj )
{
	/*
	 * min: ...,
	 * max: ...,
	 * exact: ...
	 */
	var valInteger = "1234567890";
	var valGreek = "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωςΆΈΉΊΌΎΏάέήίόύώ,.;! ";
	var valLatin = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,.?! ";
	var valSymbols = "~`@#$%^&*_+=|/\\[]{}\"'€®¥©²³£§¶¤¦°±½«»'¬";
	var valSymbolsSubset = "~`$^/\\\"'€®¥©²³£§¶¤¦°±½¬";
	var valComma = ",";
	var wrongFields = new Array();
	
	for( var i = 0; i < formObj.elements.length; i++ )
	{
		var val = formObj.elements[i].value;
		
		/* Valid values */
		if( ( valids = formObj.elements[i].getAttribute( 'validValues' ) ) != null )
		{
			var validValues = valids.split( ',' );
			
			var search = "";
			for( var k = 0; k < validValues.length; k++ )
			{
				switch( validValues[k] )
				{
					case 'valInteger':
						search += valInteger;
						continue;
					case 'valFloat':
						search += valInteger + '.' + ',';
						continue;
					case 'valComma':
						search += valComma;
						continue;
					case 'valGreek':
						search += valGreek;
						continue;
					case 'valLatin':
						search += valLatin;
						continue;
					case 'valEmail':
						continue;
				}
				
				search += validValues[k];
			}
			
			if( validValues[0] == 'valEmail' && val !== '' )
			{
				if( ! checkEmail( val ) )
				{
					wrongFields[wrongFields.length] = new String( formObj.elements[i].name );
				}
			}
			else
			{
				for( var k = 0; k < val.length; k++ )
				{
					if( search.indexOf( val.charAt( k ) ) < 0 )
					{
						wrongFields[wrongFields.length] = new String( formObj.elements[i].name );
					}
				}
			}
		}
		
		/* Invalid values */
		if( ( invalids = formObj.elements[i].getAttribute( 'invalidValues' ) ) != null )
		{
			var invalidValues = invalids.split( ',' );
			
			var search = "";
			for( var k = 0; k < invalidValues.length; k++ )
			{
				switch( invalidValues[k] )
				{
					case 'valSymbols':
						search += valSymbols;
						continue;
					case 'valSymbolsSubset':
						search += valSymbolsSubset;
						continue;
					case 'valComma':
						search += valComma;
						continue;
				}
				
				search += invalidValues[k];
			}
			
			for( var k = 0; k < val.length; k++ )
			{
				if( search.indexOf( val.charAt( k ) ) >= 0 )
				{
					wrongFields[wrongFields.length] = new String( formObj.elements[i].name );
				}
			}
		}
		
		/* 
		 * Value length
		 * 
		 * Ελέγχουμε να μην έχει κενό value γιατί θα επιστρέψει false ακόμα 
		 * και αν ο χρήστης δεν θέλει να στείλει το πεδίο αν δεν είναι required
		 */
		if( ( valLength = formObj.elements[i].getAttribute( 'valueLength' ) ) != null && formObj.elements[i].value.length > 0 )
		{
			var statements = valLength.split( ',' );
			
			for( var k = 0; k < statements.length; k++ )
			{
				var parts = statements[k].split( ':' );
				var wrong = false;
				
				switch( parts[0] )
				{
					case 'min':
						if( val.length < parseInt( parts[1] ) )
						{
							wrong = true;
						}
						break;
					case 'max':
						if( val.length > parseInt( parts[1] ) )
						{
							wrong = true;
						}
						break;
					case 'exact':
						if( val.length != parseInt( parts[1] ) )
						{
							wrong = true;
						}
						break;
				}
				
				if( wrong )
				{
					wrongFields[wrongFields.length] = new String( formObj.elements[i].name );
				}
			}
		}
		
		/* Valid value against another field value */
		if( ( compareWithName = formObj.elements[i].getAttribute( 'compareWith' ) ) != null )
		{
			if( formObj.elements[i].value != formObj.elements[compareWithName].value )
			{
				wrongFields[wrongFields.length] = new String( formObj.elements[i].name );
			}
		}
	}
	
	if( wrongFields.length ) 
		return wrongFields;
	
	return true;

}

function checkEmail(ad)
{	
	if((ad.indexOf('@') < 0) || (ad.indexOf('.') < 0) || (ad.indexOf('@') > ad.lastIndexOf('.')) || (ad.indexOf('.') == 0) || (ad.lastIndexOf('.') == ad.length-1))
	{	return false; } 
	var i;
	var m=0;
	for(i=0;i<ad.length;i++)
	{	
		if(ad[i]=='@')
			m++;	
		if(m > 1)
		{	return false; }
	}
			
	for(i=0;i<ad.length-1;i++)
	{
		if((ad[i]=='.') && (ad[i+1]=='.'))
		{	return false;  }
	}			
	
	adLocal=ad.split('@');
			
	//////////////elegxos local address
			
	if(adLocal[0].indexOf('.') == adLocal[0].length-1)
	{	return false; }
	var c='1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.-_';
	for(i=0;i<adLocal[0].length;i++)
	{
		if(c.indexOf(adLocal[0].charAt(i))<0)
		{	return false; }
	}

	adDom=adLocal[1].substr(0,adLocal[1].lastIndexOf('.'));
	adTLD=adLocal[1].substring(adLocal[1].lastIndexOf('.')+1);
			
	//////////////elegxos domain address

	if((adDom.length < 1) || (adDom.indexOf('.') == 0))
	{	return false; }

	c='1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-';
	for(i=0;i<adDom.length;i++)
	{
		if(c.indexOf(adDom.charAt(i))<0)
		{	return false; }
	}
 
	//////////////elegxos TLD address

	if((adTLD.length < 2))
	{	return false; }
	c='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	for(i=0;i<adTLD.length;i++)
	{
		if(c.indexOf(adTLD.charAt(i))<0)
		{	return false;  }
	}
			
	return true;
}

/*
	Αν δεν περιορίζονται οι προεκτάσεις δίνουμε extensions = '*'

	@param objInput ( input )
	@param extensions ( string ) { Comma seperated file extensions. }
	@return ( boolean )
*/
function validExtensions( objInput, extensions )
{
	if( extensions == '*' )
	{
		return true;
	}
	
	valid = extensions.split( ',' );
	var extension = objInput.value.substring( objInput.value.lastIndexOf( '.' )+1 ).toLowerCase();
	for( var i = 0; i < valid.length; i++ )
	{
		if( valid[i] == extension )
		{
			break;
		}
	}
	if( i == valid.length )
	{
		objInput.value = '';
		return false;
	}
	
	return true;
}


/*
	@param formObj ( object )
	@param errorHandler ( function ) { Error handler function σε εξωτερικό αρχείο. }
*/
function submitForm( formObj, errorHandler, errorMsg )
{
	/* Required fields. */
	var check = checkRequiredFields( formObj );
	if( check instanceof Array  )
	{ 
		errorHandler( formObj, check, errorMsg );
		return false;
	}
	
	/* Valid field values. */
	var check = checkFieldValue( formObj );
	if( check instanceof Array  )
	{ 
		errorHandler( formObj, check, errorMsg );
		return false;
	}
	
	return true;
}

/* 
 * Ajax submit
 * 
 * @param formObj { Form object }
 * @param handler1 { Ajax submit success handler }
 * @param handler2 { Ajax submit error handler }
 * @param handler3 { Form field error handler }
 */
function ajaxSubmitForm( formObj, handler1, handler2, handler3, errorMsg )
{
	/* Required fields. */
	var check = checkRequiredFields( formObj );
	if( check instanceof Array  )
	{ 
		handler3( formObj, check, errorMsg );
		return false;
	}
	
	/* Valid field values. */
	var check = checkFieldValue( formObj );
	if( check instanceof Array  )
	{ 
		handler3( formObj, check, errorMsg );
		return false;
	}
	
	/* 
	 * Το response είναι συνήθως σε json array όμως οι IE<8 δεν υποστηρίζουν json
	 * οπότε κάνουμε include το αρχείο με το JSON3.
	 */
	_includeJSON();
	
    // Set basic params
    var params = {
		method: 'post',
		url: formObj.action,
		async: true,
		successhandler: function( responseText ){ handler1( formObj, responseText ) },
		errorhandler: function(){ handler2( formObj ) }
	};
    
    // Find if has type='file' elements
    var hasFileElements = false;
	for( var i = 0; i < formObj.elements.length; i++ ) {
	   var e = formObj.elements[i];
       if( e.type == 'file' ) {
           hasFileElements = true;
       }
	}
    
    // In Case form has type='file' inputs we build formData
    if( hasFileElements === true ) {
        params.formData = new FormData( formObj );
        var els = formObj.elements;
        params.formData = new FormData();
        for( var i = 0; i < els.length; i++ ) {
            // Only if element has 'name' attribute
            if( els[i].hasAttribute( 'name' ) ) {
                // File type input
                if( els[i].hasAttribute( 'type' )
                &&  els[i].getAttribute( 'type' ) == 'file'
                &&  typeof els[i].files[0] !== 'undefined' ) {
                    var filename = els[i].value.split( /(\\|\/)/g ).pop();
                    params.formData.append( els[i].name, els[i].files[0], filename );
                } // Any other input
                else {
                    params.formData.append( els[i].name, els[i].value );
                }
            }
        }
    } // We build querystring
    else {
        var els = formObj.elements;
        var queryAr = [];
        for( var i = 0; i < els.length; i++ ) {
            if( ( els[i].type == 'checkbox' 
                 || els[i].type == 'radio' ) 
                     && els[i].checked != true ) {
                continue;
            } else {
                queryAr.push( encodeURIComponent( els[i].name ) + "=" + encodeURIComponent( els[i].value ) );
            }
        }
        params.querystring = queryAr.join("&");
    }
    
	_ajaxRequest( params );
	
	return false;
}
