window.scrollHandler = function( scroll ) {
    var els = _get( '.parallax' );
    for( i = 0; i < els.length; i++ ) {
        var bgPosY = parseInt( window.getComputedStyle( els[i] )
                    .getPropertyValue( 'background-position' )
                    .split( ' ' )[1] );
        if( scroll + _winHgt() > els[i].offsetTop 
        &&  scroll < els[i].offsetTop + els[i].offsetHeight ) {
            var step = 15;
            bgPosY = ( 
                scroll 
              + _winHgt() 
              - els[i].offsetTop 
              - els[i].offsetHeight 
            )*step;
            // Background position is 50% it's wrapper height
            bgPosY = bgPosY/els[i].offsetHeight+50;
            els[i].style.backgroundPosition = 'center ' + bgPosY + '%';
        }
    }
}