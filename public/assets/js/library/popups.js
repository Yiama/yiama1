/*
	@require _docHgt()
	@param action ( string ) { Javascript code για να εκτελεστεί αν πατηθεί 'ΟΚ' }
	@param message ( string )
*/
function _popupConfirm( message, action )
{
	/* Σκουραίνουμε το background */
	var bg = document.createElement( 'div' );
	bg.setAttribute( 'style', 'z-index: 1000; position: absolute; top: 0; left: 0; width: ' + document.body.offsetWidth + 'px; height: ' + _docHgt() + 'px; background-color: #000' );
	bg.setAttribute( 'class', 'opac40p' );
	bg.setAttribute( 'id', 'popupBg' );
	bg.onclick = function(){ this.style.display = 'none'; };
	document.body.appendChild( bg );
	
	/* Wrapper */
	var wrapper = document.createElement( 'div' );
	wrapper.setAttribute( 'style', 'position: absolute; z-index: 1001; top: 200px; left: 50%; margin-left: -150px; overflow: hidden; width: 300px; background-color: #eee; border: 1px solid #666' );
	wrapper.setAttribute( 'class', 'rc2' );
	
	/* Message */
	var msg = document.createElement( 'div' );
	msg.setAttribute( 'style', 'width: 80%; overflow: hidden; padding: 20px 10%; font-size: 10pt; font-weight: bold; color: #666' );
	var text = document.createTextNode( message );
	msg.appendChild( text );
	wrapper.appendChild( msg );
	
	/* Button OK */
	var butOK = document.createElement( 'div' );
	butOK.setAttribute( 'style', 'float: left; margin: 10px 0 10px 25px; width: 100px; height: 30px; border: 1px solid #666; background-color: #fff; line-height: 30px; text-align: center; font-size: 10pt; color: #666; cursor: default' );
	butOK.onclick = function(){ var f = new Function( action ); f(); };
	var text = document.createTextNode( 'OK' );
	butOK.appendChild( text );
	wrapper.appendChild( butOK );
	
	/* Button cancel */
	var butCancel= document.createElement( 'div' );
	butCancel.setAttribute( 'style', 'float: right; margin: 10px 25px 10px 0; width: 100px; height: 30px; border: 1px solid #666; background-color: #fff; line-height: 30px; text-align: center; font-size: 10pt; color: #666; cursor: default' );
	butCancel.onclick = function(){ document.body.removeChild( this.parentNode ); document.body.removeChild( document.getElementById( 'popupBg' ) ); };
	var text = document.createTextNode( 'Ακύρωση' );
	butCancel.appendChild( text );
	wrapper.appendChild( butCancel );
	
	document.body.appendChild( wrapper );
}

/*
	@require _docHgt()
	@param path ( string )
*/
function _popupImage( path )
{
	var shadow = document.createElement('div');
	shadow.setAttribute('id', 'poupImageShadow');
	shadow.setAttribute('class', 'poupImageShadow');
	
	var wrapper = document.createElement('div');
	wrapper.setAttribute('class', 'poupImageWrapper rc2');
	wrapper.setAttribute('id', 'poupImageWrapper');
	
	var im = document.createElement('img');
	im.setAttribute('src', path);
	im.setAttribute('id', 'poupImageImg');
	im.onload = function()
	{
		//to width tou wrapper den xepernaei pote to 100% (max-width: 100%) tou width tou window 
		//to bazoume sto kentro an einai mikrotero apo to width tou window
		var wrapW = this.parentNode.offsetWidth;
		var winW = window.innerWidth;
		if(wrapW < winW)
		{
			this.parentNode.style.left = '50%';
			this.parentNode.style.marginLeft = (Math.ceil(wrapW / 2) * -1) + 'px';
		}
		//diorthwsh thesis an exei ginei scrolldown
		//---CONFIGURE apostash epipleon apo panw
		var topMore = 50;
		var scrollOffsetTop = window.pageYOffset ? window.pageYOffset : document.documentElement.scrollTop;
		this.parentNode.style.top = (scrollOffsetTop + topMore) + 'px';
	};
	
	var an = document.createElement('a');
	an.appendChild(document.createTextNode('X'));
	an.setAttribute('class', 'rc1');
	an.onclick = function(){document.body.removeChild(document.getElementById('poupImageShadow')); document.body.removeChild(document.getElementById('poupImageWrapper'));};
	
	wrapper.appendChild(im);
	wrapper.appendChild(an);
	document.body.appendChild(shadow);
	document.body.appendChild(wrapper);
}