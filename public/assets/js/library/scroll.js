function scroll()
{
	var anchors = new Array();
	var interval;
	var timeout;
	var scroll_step = 10;
	var speed = 5;
	var scrolled_to;
	var active = false;
	var margin_top = 0;
	
	if( typeof scroll_instance !== 'undefined' ) { /* Κάτι σαν singleton για να έχουμε μόνο ένα Instance πάντα */
		return scroll_instance;
	}
	
	this.initialize = function( params )
	{
		scroll_instance = this;
		
		if( document.readyState != 'loaded' && document.readyState != 'interactive ' && document.readyState != 'complete' ) { /* Για να βρει τα anchors πρεπει να εχει φορτώσει τα elements το document */
			var inst = this;
			timeout = setTimeout( function(){ inst.initialize( params ), 200 } );
			return;
		} else {
			clearTimeout( timeout );
		}
		
		if( params ) { /* Set params */
			margin_top = typeof params.margin_top !== 'undefined' ? params.margin_top : margin_top;
			speed = typeof params.speed !== 'undefined' ? params.speed : speed;
		}
		
		var elements = document.getElementsByTagName( '*' ); /* Συλλογή των σημείων άγκυρας */
		for( var i = 0; i < elements.length; i++ ) {
			if( elements[i].getAttribute( 'data-scroll' ) ) {
				anchors.push( elements[i] );
				anchors[ anchors.length-1 ].setAttribute( 'offset', this.getOffset( elements[i] ) );
			}
		}
	}
	
	this.getOffset = function( element ) { /* Άθροιση των Offset όλων των Parent για να βρεθεί το συνολικό Offset του element */
		if( element.parentNode == document.body ) {
			return parseInt( element.offsetTop );
		}
		return parseInt( element.offsetTop ) + this.getOffset( element.parentNode );
	}
	
	this.scrollTo = function( data_scroll ) {
		if( active ) {
			return;
		}
		active = true;
		var scrolled = scrolled_to = _getScrollValue();
		var offset;
		for( var i = 0; i < anchors.length; i++ ) { /* Αναζήτηση άγκυρας */
			if( anchors[i].getAttribute( 'data-scroll' ) == data_scroll ) {
				offset = anchors[i].getAttribute( 'offset' );
				break;
			}
		}
		
		interval = setInterval( function(){ offset > scrolled ? scroll.down( offset ) : scroll.up( offset ) }, speed ); /* Κατεύθυνση scroll */
	}
	
	var scroll = {
		factor: 1000, /* Ο συντελεστής είναι το 1000 γιατί τα elements έχουν σε pixel offset πολλαπλάσιο του 1000 */
		down: function( offset ) {
			var new_scroll_step = scroll_step + ( offset - scrolled_to ) / scroll.factor * scroll_step;
			if( offset > scrolled_to+new_scroll_step+margin_top ) {
				window.scrollTo( 0, scrolled_to+new_scroll_step );
				scrolled_to += new_scroll_step;
			} else {
				clearInterval( interval );
				active = false;
			}
		},
		up: function( offset ) {
			var new_scroll_step = scroll_step + ( scrolled_to - offset ) / scroll.factor * scroll_step;
			if( offset < scrolled_to-new_scroll_step+margin_top ) {
				window.scrollTo( 0, scrolled_to-new_scroll_step );
				scrolled_to -= new_scroll_step;
			} else {
				clearInterval( interval );
				active = false;
			}
		}
	}
}