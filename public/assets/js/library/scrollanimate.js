/*
	Ελέγχει αν ένα element φτάνει viewport που θέλουμε κατά το scroll της σελίδας και τότε εφαρμόζει μία function σε αυτό.
	@param fullView { Αν θέλουμε να ελέγχει αν το Height του element έχει μπει όλο στο viewport }
*/
function isScrolledIntoView( elem, fullView )
{
    var elem = elem;
    var fullView = typeof fullView !== 'undefined' ? fullView : false;

    var docViewTop = _getScrollValue();
    var docViewBottom = docViewTop + _winHgt();
	// If elem is absolute positioned we take its relative positioned parents offsetTop
    var elemTop = ! elem.offsetTop ? elem.parentNode.offsetTop : elem.offsetTop;
	
    if( fullView ) {
    	var elemBottom = elemTop + elem.offsetHeight;
    }
    else {
    	var elemBottom = elemTop;
    }
	
    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function scrollAnimate( params )
{
	for( var i = 0; i< params.length; i++  ) {
		if( isScrolledIntoView( params[i]['object'], typeof params[i]['fullView'] !== 'undefined' ? params[i]['fullView'] : false ) ) {
			params[i]['func']( params[i]['object'] );
		}
	}
}