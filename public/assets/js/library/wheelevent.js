/*
 * Υπολογίζει το delta του scroll σε θετικό ή αρνητικό και το δίνει σε μία handler function
 * @uses window.wheelHandler { Η function 'handler' πρέπει να οριστεί μέσα στο script ως attribute του window δηλαδή window.wheelHandler = <function_name> }
 */
/** This is high-level function.
 * It must react to delta being more/less than zero.
 */
/** Event handler for mouse wheel event.
 */
function _wheelEvent(event){
        var delta = 0;
        if (!event) /* For IE. */
                event = window.event;
        if (event.wheelDelta) { /* IE/Opera. */
                delta = event.wheelDelta/120;
        } else if (event.detail) { /** Mozilla case. */
                /** In Mozilla, sign of delta is different than in IE.
                 * Also, delta is multiple of 3.
                 */
                delta = -event.detail/3;
        }
        /** If delta is nonzero, handle it.
         * Basically, delta is now positive if wheel was scrolled up,
         * and negative, if wheel was scrolled down.
         */
        if (delta)
        	window.wheelHandler(delta);
        /** Prevent default actions caused by mouse wheel.
         * That might be ugly, but we handle scrolls somehow
         * anyway, so don't bother here..
         */
        if (event.preventDefault)
                event.preventDefault();
	event.returnValue = false;
}

/** Initialization code. 
 * If you use your own event management code, change it as required.
 */
if (window.addEventListener)
        /** DOMMouseScroll is for mozilla. */
        window.addEventListener('DOMMouseScroll', _wheelEvent, false);
/** IE/Opera. */
window.onmousewheel = document.onmousewheel = _wheelEvent;