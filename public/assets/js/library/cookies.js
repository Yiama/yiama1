function _setCookie( name, value, millisecs, location )
{
	if( typeof location === 'undefined' ) {
		location = '/';
	}
	if( typeof millisecs === 'undefined' ) {
		document.cookie = name + "=" + value + ";path=" + location;
	} else {	
		var date = new Date();
		date.setTime( date.getTime() + millisecs );
		document.cookie = name + "=" + value + ";expires=" + date.toGMTString() + ";path=" + location;
	}
		
}

function _unsetCookie( name )
{
	_setCookie( name, "", -1, "/" );
}

function _getCookie( name )
{
	var cookiesAr = document.cookie.split( ";" );
	
	for( var i = 0; i < cookiesAr.length; i++ ) {
		var cookiesResultAr = new Array();
		cookieResultAr = cookiesAr[i].split( "=" );
		if( cookieResultAr[0].replace( /\s/, "" ) == name )//xrhsimopoioye thn replace() giati mporei h split() na balei keno sthn arxh
			return cookieResultAr[1];
	}
	
	return false;
}