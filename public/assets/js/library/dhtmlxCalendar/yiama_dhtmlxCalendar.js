dhtmlXCalendarObject.prototype.langData["el"] = {
	monthesFNames: [
		"Ιανουάριος ", "Φεβρουάριος ", "Μάρτιος ", "Απρίλιος ", "Μάϊος ", "Ιούνιος ", "Ιούλιος ",
		"Αύγουστος ", "Σεπτέμβριος ", "Οκτώμβριος ", "Νοέμβριος ", "Δεκέμβριος "
	],
	monthesSNames: [
		"Ιαν", "Φεβ", "Μάρ", "Απρ", "Μάϊ", "Ιούν",
		"Ιούλ", "Αύγ", "Σεπ", "Οκτ", "Νοέ", "Δεκ"
	],
	daysFNames: [
		"Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη",
		"Πέμπτη", "Παρασκευή", "Σαββάτο"
	],
	daysSNames: ["Κυ", "Δε", "Τρ", "Τε", "Πε", "Πα", "Σα"],
	weekstart: 1,
	weekname: "w"
};

function yiama_dhtmlXCalendar( options ) 
{
	/* make custom language default */
	dhtmlXCalendarObject.prototype.lang = ( typeof options.langCode !== 'undefined' ) ? options.langCode : "el";		

	/* initialization */
	return new dhtmlXCalendarObject( options.HTMLElementIds );
}