/*
 * Υπολογίζει το scroll σε θετικό ή αρνητικό και το δίνει σε μία handler function
 * @uses window.wheelHandler { Η function 'handler' πρέπει να οριστεί μέσα στο script ως attribute του window δηλαδή window.scrollHandler = <function_name> }
 */
/** Event handler for window scroll event.
 */
function _scrollEvent( event ){
	var scroll = document.body.scrollTop || document.documentElement.scrollTop;
    window.scrollHandler( scroll );
    if( event.preventDefault ) {
    	event.preventDefault();
    }
	event.returnValue = false;
}
if( document.addEventListener ) {    
	// For all major browsers, except IE 8 and earlier
	window.addEventListener( 'scroll', _scrollEvent );
} else if( document.attachEvent ) {  
	// For IE 8 and earlier versions
	window.attachEvent( 'onscroll', _scrollEvent );
}
window.onscroll = _scrollEvent;