function tinymce_decodeHTML( html ) {
	var txt = document.createElement( "textarea" );
    txt.innerHTML = html;
    return txt.value;
}

tinymce.init( {
	language: "el",
	mode : "specific_textareas",
	editor_selector : "editorIdentifyClass",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    image_dimensions: false, /* Δεν εμφανίζει επιλογή ορισμού σταθερών διαστάσεων στην εισαγωγή image */
    apply_source_formatting: false,
    cleanup_on_startup: false,
    trim_span_elements: false,
    cleanup: false,
    convert_urls: false,
    force_br_newlines: true,
    force_p_newlines: false,
    remove_linebreaks: false,
    convert_newlines_to_brs: false,
    forced_root_block: '',
    inline_styles : true,
    entity_encoding: 'raw', /* Δεν κάνει encoding στα HTML tags και στο utf-8 κείμενο */
    verify_html: false,
    //forced_root_block: false,
    validate_children: false,
    remove_redundant_brs: false,
    fix_table_elements: false,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    
    setup : function( editor ) { /* Αλλάζουμε το content με το που το φορτώνει ώστε να αποφευχθεί το encoding στα html entities */
        editor.on( 'LoadContent', function( e ) {
        	var new_content = tinymce_decodeHTML( tinyMCE.activeEditor.getContent() );
        	tinyMCE.activeEditor.setContent( new_content );
        } );
    }
} );