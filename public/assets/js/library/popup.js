/*
	@require basic.js
	
	***************************
	var options = {
		~ Required ~
		bg: {  
			attributes: {
				'class': '...',
				'id': '...',
				...
			},
			events: [ { 
					'event': 'onclick',
					'func': 'function body...' 
				}, { 
					'event': 'onmouseover',
					'func': 'function body...' 
				}
			]
		},
		
		~ Required ~
		wrapper: ...,
		
		~ Required ~
		message: {
			attributes: { ... },
			text: ''
		},
		
		~ Optional ~
		but1: {
			attributes: { ... },
			events: [ ... ]
			text: ''
		},
		
		~ Optional ~
		...
	}
	***************************
*/
function popup( options )
{
	var instance = this;
	
	/* Background */
	var bg = document.createElement( 'div' );
	bg.setAttribute( 'style', 'width: ' + _winWdt() + 'px; height: ' + _winHgt() + 'px' );
	_setAttributes( bg, options.bg.attributes );
	if( typeof options.bg.events !== 'undefined' )
	{
		for( var i = 0; i < options.bg.events.length; i++  ) 
		{
			this.bgEvents = options.bg.events[i];
			bg.addEventListener(
				this.bgEvents.event,
				function(){ f = new Function( instance.bgEvents.func ); f(); },
				false
			);
		}
	}
	
	/* Wrapper */
	var wrapper = document.createElement( 'div' );
	_setAttributes( wrapper, options.wrapper.attributes );
	if( typeof options.wrapper.events !== 'undefined' )
	{
		for( var i = 0; i < options.wrapper.events.length; i++  ) 
		{
			this.wrapperEvents = options.wrapper.events[i];
			wrapper.addEventListener(
				this.wrapperEvents.event,
				function(){ f = new Function( instance.wrapperEvents.func ); f(); },
				false
			);
		}
	}
	
	/* Header */
	if( typeof options.header !== 'undefined' ) {
		var header = document.createElement( 'div' );
		_setAttributes( header, options.header.attributes );
		header.innerHTML = options.header.text;
		wrapper.appendChild( header );
	}
	
	/* Message */
	var msg = document.createElement( 'div' );
	_setAttributes( msg, options.message.attributes );
	msg.innerHTML = options.message.text;
	wrapper.appendChild( msg );
	
	/* Button 1 */
	if( typeof options.button1 !== 'undefined' )
	{
		var but1 = document.createElement( 'div' );
		_setAttributes( but1, options.button1.attributes );
		but1.appendChild( document.createTextNode( options.button1.text ) );
		if( typeof options.button1.events !== 'undefined' )
		{
			for( var i = 0; i < options.button1.events.length; i++  ) 
			{
				this.button1Events = options.button1.events[i];
				but1.addEventListener(
					this.button1Events.event,
					function(){ f = new Function( instance.button1Events.func ); f(); },
					false
				);
			}
		}
		wrapper.appendChild( but1 );
	}
	
	/* Button 2 */
	if( typeof options.button2 !== 'undefined' )
	{
		var but2 = document.createElement( 'div' );
		_setAttributes( but2, options.button2.attributes );
		but2.appendChild( document.createTextNode( options.button2.text ) );
		if( typeof options.button2.events !== 'undefined' )
		{
			for( var i = 0; i < options.button2.events.length; i++  ) 
			{
				this.button2Events = options.button2.events[i];
				but2.addEventListener(
					this.button2Events.event,
					function(){ f = new Function( instance.button2Events.func ); f(); },
					false
				);
			}
		}
		wrapper.appendChild( but2 );
	}
	
	/* Methods */
	this.getBg = function(){ return bg; }
	this.getWrapper = function(){ return wrapper; }
	this.getMsg = function(){ return msg; }
	this.pop = function(){
		document.body.appendChild( instance.getBg() );
		document.body.appendChild( instance.getWrapper() );
		window.scrollTo( 0,0 );
	}
	this.unpop = function(){
		document.body.removeChild( instance.getBg() );
		document.body.removeChild( instance.getWrapper() );
	}
}