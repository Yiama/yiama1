/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Το Url πρέπει να είναι του τύπου ../../<parent_option_value>
 * Το responseText αν δεν επιστρέφει options πρέπει να είναι K E N O  S T R I N G <''>
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * 
 * @uses _ajaxRequest()
 * @uses _includeJSON()
 */
function ajaxSelectFiller()
{
	/*
	 * @params select ( object ) { Το select στο οποίο θα αλλάξουμε τα options }
	 * @params textField ( string ) { Πιο πεδίο από τον επιστρεφόμενο πίνακα θα χρησιμοποιηθεί για το option text }
	 * @params valueField ( string ) { Πιο πεδίο από τον επιστρεφόμενο πίνακα θα χρησιμοποιηθεί για το option value }
	 */
	this.request = function( select, textField, valueField, url, queryString )
	{
		var instance = this;
		
		_ajaxRequest( {
			method: 'post',
			url: url,
			querystring: queryString,
			async: true,
			successhandler: function( responseText ){ instance.successHandler( select, textField, valueField, responseText ); },
			errorhandler: function(){ instance.errorHandler(); }
		} );
	}
	
	this.successHandler = function( select, textField, valueField, responseText )
	{
		/*
		 * Αφαιρούνται όλα τα options πρώτα
		 */
		select.options.length = 0;

		if( responseText == '' )
		{
			return;
		}
		else
		{
			_includeJSON();
			
			var newOptions = JSON.parse( responseText );
			
			for( var key in newOptions )
			{
			   if( newOptions.hasOwnProperty( key ) )
			   {
			      var obj = newOptions[key];
			      select.options[ select.options.length ] = new Option( obj[ textField ], obj[ valueField ] );
			   }
			}
		}
	}
	
	this.errorHandler = function()
	{
		alert( 'Ajax Response Error' );
	}
}