function menu( el )
{
	if( _winWdt() > 768 ) {
		return;
	}
	var divs = document.getElementsByTagName( 'div' );
	for( var i = 0; i < divs.length; i++ ) {
		if( divs[i].hasAttribute( 'id' ) ) {
			var subId = divs[i].id.replace( 'menuItem', 'submenu' );
			if( divs[i].id.indexOf( 'menuItem_' ) === 0 && divs[i].id != el.id && _get( subId ) ) {
				_hide( subId );
			}
		}
	}
	var subId = el.id.replace( 'menuItem', 'submenu' );
	if( _get( subId ) ){
		if( _get( subId ).style.display === 'block' ) {
			_hide( subId );
		}
		else {
			_show( subId );
		}
	}
}