jQuery.fn.ymParallax = function() {	

	this._element = jQuery(this).get(0);
	this._scroll = 0;
	this._marginTop;
	this.addClass('ym-parallax');
	this.find('div:first-child').addClass('ym-parallax-bg');
	this._safetyDistance = Math.ceil(this.find('div:first-child').height()*0.1);
    this._move = function move(direction, scroll) {
        var scrollFactor = Math.round(scroll / 5);
        var step = 0;
        if (direction) {
            step += (scrollFactor - this._marginTop) * -1;
        } else {
            step -= scrollFactor - this._marginTop;
        }
        this.find('.ym-parallax-bg').css("top", step);
    }
    
	this._marginTop = parseInt(this.find('.ym-parallax-bg').css("top"));
	var that = this;
    jQuery(window).scroll(function() {
    	var scroll = jQuery(window).scrollTop();
    	var offset = jQuery(that).find('.ym-parallax-bg').offset();
    	var parentOffset = jQuery(that).offset();
    	var height = jQuery(that).find('.ym-parallax-bg').height();
    	var parentHeight = jQuery(that).height();
    	// Check if is out of view when scrolling down
    	if (that._scroll < scroll
		&&	offset.top+height-that._safetyDistance <= parentOffset.top+parentHeight) {
        	return;
        }  // Check if is out of view when scrolling up
        else if (that._scroll > scroll
		&&	offset.top+this._safetyDistance >= parentOffset.top) {
        	return;
        }
        var direction = that._scroll < scroll;
        that._scroll = scroll;
        that._move(that._scroll - scroll > 0, scroll);
    });
}