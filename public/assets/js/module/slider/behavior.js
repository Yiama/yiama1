function sliderBehaviors( slider, behaviors ) {
    for( var funcName in behaviors ) {
        if( behaviors.hasOwnProperty( funcName ) ) {

            // Set function name
            var funcFullName = 'sliderBehavior'
                + funcName.charAt( 0 ).toUpperCase()
                + funcName.slice( 1 );

            // Call function
            if( typeof window[ funcFullName ] === 'function' ) {
                new window[ funcFullName ]( slider, behaviors[ funcName ] );
            }
        }
    }
}

/**
 * --------
 * AUTOPLAY
 * --------
 */
function sliderBehaviorAutoplay( s, p ) {
    var slider = s;
    var params = p;
    window.sliderAutoplayInterval = setInterval( function() {
        slider.next();
    }, params.sleep );
}

/**
 * ----------
 * RESPONSIVE
 * ----------
 */
function sliderBehaviorResponsive( s, p ) {
    var slider = s;
    var params = p;
    var inst = this;

    this.resize = function() {
        var wrapperWidth = slider
            .params
            .wrapper
            .offsetWidth;
        var newHeight = Math.ceil( wrapperWidth * params.sizeRatio );
        slider
            .params
            .wrapper
            .style
            .height = newHeight + "px";
    }
    // Set initial size
    window.addEventListener( 'load', function() {
        inst.initialWidth = parseInt(slider
            .params
            .wrapper
            .offsetWidth);
        inst.initialHeight = Math.ceil( inst.initialWidth * slider.params.sizeRatio );

        slider
            .params
            .wrapper
            .style
            .height = inst.initialHeight  + "px";

        inst.resize();
    } );

    // Resize
    window.addEventListener( 'resize', function() {
        inst.resize();
    } );
}
