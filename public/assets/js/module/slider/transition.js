function sliderTransition( s, params, callback )
{
	var slider = s;
    this.durationSafetyLatency = 200;
	
	// Prepare
	this.prepare = function( slides ) {
		switch( params.effect ) {
			case 'horizontal':
				// Get width
				var wh = slides[0].element.offsetWidth;
				for( var i = 0; i < slides.length; i++ ) {
				}
				break;
			case 'opacity':
				// Set opacity
				for( var i = 0; i < slides.length; i++ ) {
					slides[i].element.style.cssText 
						+=" opacity: 0;"
						+ " -webkit-transition: opacity 1s ease-in-out;"
						+ " -moz-transition: opacity 1s ease-in-out;"
						+ " -ms-transition: opacity 1s ease-in-out;"
						+ " -o-transition: opacity 1s ease-in-out;"
						+ " transition: opacity 1s ease-in-out;";
				}
				break;
			case 'horizontalCarousel':
				// Get width
				var wh = slides[0].element.offsetWidth;
				for( var i = 0; i < slides.length; i++ ) {
				}
				break;
		}
	}
	
	// Start
	this.start = function( nextSlide, currentSlide, direction ) {
		
		// Call effect
		switch( params.effect ) {
			case 'horizontal':
				sliderTransitionHorizontal( this, nextSlide, currentSlide, direction, params.duration );
				break;
			case 'opacity':
				sliderTransitionOpacity( this, nextSlide, currentSlide, params.duration );
				break;
			case 'horizontalCarousel':
				sliderTransitionHorizontalCarousel( this, nextSlide, currentSlide, direction, params.duration );
				break;
		}
		
		// Callback function
		if( typeof callback == 'function' ) {
			callback( slider );
		}
	}
	
	// Stop
	this.stop = function( nextSlide, currentSlide ) {
		
	}
	
	// Complete
	this.complete = function() {
		// Call event on complete
		slider.triggerEvent( 'onTransitionComplete' );
	}
}

/**
 * -------
 * EFFECTS
 * -------
 */
function sliderTransitionHorizontal( transition, nextSlide, currentSlide, direction, duration )
{
	var tn = transition;
	var direction = typeof direction === 'undefined' ? 1 : direction;
	var wh = currentSlide.element.offsetWidth;
	
	// Next slide
	// Remove transition
	nextSlide.element.style.transition = 'none';
	nextSlide.element.style.WebkitTransition = 'none';
	nextSlide.element.style.MozTransition = 'none';
	// Move at start position
	nextSlide.element.style.left = direction ? wh + 'px' : ( wh*-1 ) + 'px';
	// Delay next commands so that <style.left> can be executed
	setTimeout( function() {
		// Add transition
		nextSlide.element.style.transition = 'all ' + duration + 'ms';
		nextSlide.element.style.WebkitTransition = 'all ' + duration + 'ms';
		nextSlide.element.style.MozTransition = 'all ' + duration + 'ms';
		// Move at end position
		nextSlide.element.style.left = 0;
		
		// Current slide
		// Move at end position
		currentSlide.element.style.left = direction ? ( wh*-1 ) + 'px' : wh + 'px';
	
        // Finalize
        setTimeout( function() { 
            // Call complete
            tn.complete();
        }, duration + transition.durationSafetyLatency );
	}, transition.durationSafetyLatency );
}

function sliderTransitionOpacity( transition, nextSlide, currentSlide, duration )
{
	var tn = transition;
	
	// Start position
	nextSlide.element.style.opacity = 1;
	// Delay next commands so that <style.opacity> can be executed
	setTimeout( function() {
		// Add transition
		nextSlide.element.style.transition = 'opacity ' + duration + 'ms';
		nextSlide.element.style.WebkitTransition = 'opacity ' + duration + 'ms';
		nextSlide.element.style.MozTransition = 'opacity ' + duration + 'ms';
		// Move at end position
		nextSlide.element.style.opacity = 1;
		
		// Current slide
		// Move at end position
		currentSlide.element.style.opacity = 0;
	
        // Finalize
        setTimeout( function() { 
            // Call complete
            tn.complete();
        }, duration + transition.durationSafetyLatency );
	}, transition.durationSafetyLatency );
}

function sliderTransitionHorizontalCarousel( transition, nextSlide, currentSlide, direction, duration )
{
	var tn = transition;
	var direction = typeof direction === 'undefined' ? 1 : direction;
	var wh = currentSlide.element.offsetWidth;
	
	// Next slide
    // Add transition
    nextSlide.element.style.transition = 'all ' + duration + 'ms';
    nextSlide.element.style.WebkitTransition = 'all ' + duration + 'ms';
    nextSlide.element.style.MozTransition = 'all ' + duration + 'ms';
	// Delay next commands so that <style.marignLeft> can be executed
	setTimeout( function() {
		// Current slide
        // Move at end position
        currentSlide.element.style.marginLeft = direction ? ( wh*-1 ) + 'px' : wh + 'px';
	
        // Finalize
        setTimeout( function() { 
            // Clone current slide and move to the end of the tail
            var currentSlideElementParentNode = currentSlide.element.parentNode;
            var currentSlideElement = JSON.parse( JSON.stringify( currentSlide.element ) );
            // Remove current slide element from DOM
            currentSlideElementParentNode.removeChild( currentSlide.element );
            currentSlide.element.style.marginLeft = '0px';
            // Append copy of current slide to parent node
            currentSlideElementParentNode.appendChild( currentSlide.element );

            // Call complete
            tn.complete();
        }, duration + transition.durationSafetyLatency );
	}, transition.durationSafetyLatency );
}