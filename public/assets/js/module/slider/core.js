/**
 * params: { wrapper: <object> }
 */
function slider( p )
{
	
	/**
	 * --------------
	 * INITIALIZATION 
	 * --------------
	 */
	var events = {};
	this.slides = [];
	var transition;
	this.currentIndex;
	this.nextIndex;
	this.transitionStarted;
        this.params = p;
	
	/**
	 * -----
	 * EVENT
	 * -----
	 */
	// Register event
	this.registerEvent = function( event, callback ) {
		if( ! events.hasOwnProperty( event ) ) {
			events[ event ] = [];
		} 
		events[ event ].push( callback );
	}
	
	// Trigger event
	this.triggerEvent = function( event ) {
		if( events.hasOwnProperty( event ) ) {
			for( i = 0; i < events[ event ].length; i++ ) {
				events[ event ][i]( this );
			}
		}
	}
	
	/**
	 * ----------
	 * TRANSITION 
	 * ----------
	 */	
	// Set transition
	this.setTransition = function( trans ) {
		transition = trans;
	}
	
	// Start transition
	this.startTransition = function( direction ) {
		
		// Trigger event
		this.triggerEvent( 'onTransitionStart' );
		
		// Start
		transition.start( this.slides[ this.nextIndex ], this.slides[ this.currentIndex ], direction );
	}
	
	// Stop transition
	this.stopTransition = function() {
		
		// Stop
		transition.stop( this.slides[ this.nextIndex ], this.slides[ this.currentIndex ] );
		
		// Trigger event
		this.triggerEvent( 'onTransitionStop' );  
	}
	
	// Event start
	this.registerEvent( 'onTransitionStart', function( inst ) {
		
		// Set started
		inst.transitionStarted = true; 
	} );
	
	// Event complete
	this.registerEvent( 'onTransitionComplete', function( inst ) {
		
		// Set started
		inst.transitionStarted = false;
		
		// Update current index
		inst.currentIndex = inst.nextIndex;
	} );
	
	// Event stop
	this.registerEvent( 'onTransitionStop', function( inst ) {
		
		// Set started
		inst.transitionStarted = false;
		
		// Clear autoplay interval
		if( typeof inst.autoplayInterval !== 'undefined' ) {
			clearInterval( inst.autoplayInterval );
		}
	} );
	
	/**
	 * ------
	 * ACTION 
	 * ------
	 */	
	// Call next slide
	this.next = function() {
		
		// Check not running
		if( this.transitionStarted ) return false;
		 
		// Set next index
		this.nextIndex = this.currentIndex < this.slides.length-1 ? this.currentIndex+1 : 0;
		
		// Start transition
		this.startTransition( 1 );
		
		return true;
	}
	
	// Call previous slide
	this.previous = function() {
		
		// Check not running
		if( this.transitionStarted ) return false;
		
		// Set next index
		this.nextIndex = this.currentIndex > 0 ? this.currentIndex-1 : this.slides.length-1;
		
		// Start transition
		this.startTransition( 0 );
		
		return true;
	}
	
	// Call goto specific index
	this.goto = function( index ) {
		
		// Check that goto index is not the same as current index
		if( this.currentIndex == index ) {
			return;
		}
		
		// Check not running
		if( this.transitionStarted ) return false;
		
		// Set next index
		this.nextIndex = index;
		
		// Start transition
		this.startTransition( 1 );
		
		return true;
	}
	
	// Stop
	this.stop = function() {
		
		// Stop transition
		this.stopTransition();
	}
	
	/**
	 * --------
	 * START UP
	 * --------
	 */
	// Set slides
	var wrapperChildren = this.params.wrapper.children;
	for( i = 0; i < wrapperChildren.length; i++ ) {
		var slide = {};
		slide.element = wrapperChildren[i];	
		this.slides.push( slide );
	}
	
	// Start
	this.start = function() {

		// Initialize
		this.currentIndex =  this.slides.length-1;
		this.nextIndex =  0;
		
		// Trigger event
		this.triggerEvent( 'onTransitionStart' );
		
		// Prepare transition
		transition.prepare( this.slides );
		
		// Start transition
		transition.start( this.slides[ this.nextIndex ], this.slides[ this.currentIndex ] );
	}
}