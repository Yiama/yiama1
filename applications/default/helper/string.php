﻿<?php
class Helper_String
{
	public static function shorten( $string, $max )
	{
        $shortened = mb_substr( $string, 0, $max );
        $shortened = mb_substr( $shortened, 0, mb_strrpos( $shortened, ' ' ) );
        return $shortened . ( mb_strlen( $string ) > $max ? '...' : '' );
    }
}
?>