<?php
	class Helper_Ajax
	{		
		public static function responseForm( $status, $message_path, $message_index )
		{
			$params = Core_Request::getInstance()->getParams();
			Core_Response::getInstance()->setHeaders( 'content-type: application/json; charset="utf-8"' );
			$language = new Model_Yiama_Language();
			$response = array(
				'status' => $status,
				'message' => Core_Translate::get( $language->getCurrent()->code, $message_path, $message_index )
			);
			if( isset( $params['return_url'] ) ) {
				$response['redirect'] = Core_Request::getInstance()->getParams( 'return_url' );
			}
			echo json_encode( $response );
		}
	}
?>