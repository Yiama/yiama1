﻿<?php
class Helper_Form
{
	private static $calendar_initialized = false;
	private static $form_id = null;
	private static $lang_code = null;
	private static $form_init = null;
	private static $form_ajax_init = null;
	private static $fillConnectedSelect_init = false;
	private static $message_init = null;
	private static $submit_init = null;
	
	public static function openForm( $id, $action, $ajax = false, $user_attrs = null )
	{
		self::$form_id = 'form_' . $id;
		$language = new Model_Yiama_Language();
		self::$lang_code = $language->getCurrent()->code;
		$s = self::message( self::$form_id . '_message' );
		if( ! self::$form_init ){
			self::$form_init = true;
			Core_HTML::addFiles( array(
				'library\basic.js',	
				'library\form.js'
			) ); 
			Core_HTML::addJs( "
				function handleError( formObj, ar )
				{
					_removeClass( formObj.getElementsByTagName( 'button' )[0], 'loading' );
					formMessage( formObj.id + '_message', 'error', '" . Core_Translate::get( self::$lang_code, 'form.common', 'messages.field_error' ) . "' );
					window.location.hash = formObj.id + '_message_anchor';
					for( var i = 0; i < ar.length; i++ ){
						for( var k = 0; k < formObj.elements.length; k++ ){
							if( formObj.elements[k].name == ar[i] ){
								_addClass( formObj.elements[k], 'highlighted' );
							}
						}
					}
				}
	
				function formElementOnClick() {
					var forms = document.forms; 
					for( var x = 0; x < forms.length; x++ ) {
						var elements = forms[x].elements;
						for( var i = 0; i < elements.length; i++ ) {
							if( elements[i].nodeName == 'INPUT' 
							|| elements[i].nodeName == 'SELECT'
							|| elements[i].nodeName == 'TEXTAREA' ) {
								elements[i].setAttribute( 'data-form_id', forms[x].id );
								elements[i].onclick = function(){
									_hide( this.getAttribute( 'data-form_id' ) + '_message' ); 
									_removeClass( this, 'highlighted' );
								}
							}
						}
					}
				}
				_addOnLoad( 'formElementOnClick()' );
			" );
		}
		$attrs = array(
			'enctype' => 'multipart/form-data',
			'method' => 'post',
			'name' => 'form',
			'id' => self::$form_id,
			'action' => $action,
			'onsubmit' => "return submitForm( this, handleError )"
		);
		if( $ajax ) {
			$attrs['onsubmit'] = "return ajaxSubmitForm( this, ajaxSuccessHandler, ajaxErrorHandler, handleError, '!Error from yiama form helper' )";
		}
		return $s .= HTML_Tag::open( 'form', array_merge( $attrs, ( array ) $user_attrs ) );
	}
	
	public static function openFormAjax( $id, $action, $on_response_code = null, $attrs = null )
	{
		if( ! self::$form_ajax_init ){
			self::$form_ajax_init = true;
			Core_HTML::addFiles( 'library\ajax.js' ); 
			Core_HTML::addJs( "		
				function ajaxErrorHandler( formObj )
				{
					alert( 'Ajax error!' );
				}
				
				function ajaxSuccessHandler( formObj, responseText )
				{
					_removeClass( formObj.getElementsByTagName( 'button' )[0], 'loading' );
					var objResponse = JSON.parse( responseText );
					if( typeof objResponse.redirect !== 'undefined' ) {
						window.location.href = decodeURIComponent( objResponse.redirect );
					} else { "
					. ( ( $on_response_code ) 
						? $on_response_code
						: " formMessage( formObj.id + '_message', objResponse.status ? 'success' : 'error', objResponse.message );" )	
					. "}
				}
			" );
		}
		return self::openForm( $id, $action, true, $attrs );
	}
	
	public static function closeForm()
	{
		self::$form_id = null;
		self::$lang_code = null;
		return HTML_Tag::close( 'form' );
	}
	public static function openWrapperModule( $header = null )
	{
		$s = $header ? HTML_Tag::closed( 'div', array( 'class' => 'header' ), $header ) : '';
		return "<fieldset>" . self::element( $s );
	}
	
	public static function closeWrapperModule() 
	{
		return "</fieldset>";
	}
	
	public static function message( $id )
	{
		if( ! self::$message_init ){
			self::$message_init = true;
			Core_HTML::addJs( "
				var message_timeout;
				function formMessage( id, type, text )
				{
					clearTimeout( message_timeout );
					removeMessageItems( id );
					var msgEl = _get( id );
					var item = document.createElement( 'div' );
					item.innerHTML = text;
					item.setAttribute( 'class', 'item ' + type );
					msgEl.appendChild( item );
					_show( msgEl );		
					window.location.href = '#' + id + '_anchor';
					message_timeout = setTimeout( function(){ removeMessageItems( id ) }, 5000 );
				}
			" );
		}
		$s = '';
		$s .= "<a name='{$id}_anchor'></a>";
		$s .= HTML_Tag::closed( 'div', array(
				'id' => $id,
				'class' => 'form-message'
			), ''
		);
		return $s;
	}
	
	public static function element( $content, $attrs = null )
	{
		return HTML_Tag::closed( 'div', array_merge( ( array ) $attrs, array( 'class' => 'element' . ( isset( $attrs['class'] ) ? ' ' . $attrs['class'] : '' ) ) ), $content );
	}
	
	public static function submit( $label = null )
	{
		if( ! self::$submit_init ){
			self::$submit_init = true;
			Core_HTML::addFiles( array(
				'library\basic.js',  
				'library\preload.js' 
			) );
			Core_HTML::addJs( " _addOnLoad( '_preloadImg( \'" . PATH_REL_IMAGES . "form/but.loading.gif\' )' ); " );
		}
		return self::element( HTML_Tag::closed( 'button', array(
			'id' => self::$form_id . '_submit',
			'onclick' => "_addClass( this, 'loading' );"
			), $label
		) );
	}
	
	public static function hiddenReturn( $return_url )
	{
		$params = Core_Request::getInstance()->getParams();
		return HTML_Tag::void( 'input', array( 
			'type' => 'hidden',
			'name' => 'return_url',
			'value' => $return_url
		) );
	}
	
	public static function seperator()
	{
		return HTML_Tag::closed( 'div', array( 'class' => 'seperator' ) );
	}
	
	public static function linkButton( $url, $text )
	{
		return HTML_Tag::closed( 'a', array( 'href' => $url, 'class' => 'link_button' ), $text );
	}
	
	/*
	 * @param $options = string[title]/array( <title>, <comments>, <instructions>, <attributes[array()]>, <link[array( <title> => <link>, ... )]> )
	 * @param $required boolean
	 */
	public static function label( $options, $required = false )
	{
		if( is_array( $options ) ) {
			if( $required ) {
				$options[0] = ' <b>*</b> ' . $options[0];
			}
			$l = '';
			if( isset( $options[4] ) ) {
				foreach( $options[4] as $title => $link ) {
					$l .= HTML_Tag::closed( 'a', array( 
							'class' => 'label_link',
							'href' => $link
						), $title 
					);
				}
			}
			$s = HTML_Tag::closed( 'label', 
				isset( $options[3] ) ? $options[3] : null, 
				$options[0] . ( ! empty( $options[1] ) ? '<br/>' . HTML_Tag::closed( 'small', null, $options[1] ) : '' ) . $l 
			);
			return $s;
		}
		if( $required ) {
			$options = ' <b>*</b> ' . $options;
		}
		return HTML_Tag::closed( 'label', null, $options );
	}
	
	public static function input( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		return self::element( $s . HTML_Tag::void( 'input', array_merge( array( 
				'type' => 'text', 
				'name' => $name,
				'value' => $val ? htmlspecialchars( $val ) : '',
				'isRequired' => $required ? 'yes' : 'no'
			), ( array ) $attrs
		) ), $wrapper_attrs );
	}
	
	public static function file( $name, $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		return self::element( $s . HTML_Tag::void( 'input', array_merge( array( 
				'type' => 'file', 
				'name' => $name,
				'isRequired' => $required ? 'yes' : 'no'
			), ( array ) $attrs
		) ), $wrapper_attrs );
	}
	
	public static function date( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		Core_HTML::addFiles( array(
			'library\dhtmlxCalendar\codebase\dhtmlxcalendar.js',
			'library\dhtmlxCalendar\yiama_dhtmlxCalendar.js',
			'js\library\dhtmlxcalendar.css'
		) );
		if( ! self::$calendar_initialized ) {
			self::$calendar_initialized = true;
			Core_HTML::addJs( "
				var dhtmlXCalendarOptions = {
					langCode: '" . self::$lang_code . "',
					HTMLElementIds: [ '{$name}_calendar' ]
				}
				_addOnLoad( 'var calendarOrders = yiama_dhtmlXCalendar( dhtmlXCalendarOptions )' );
			" );
		} else {
			Core_HTML::addJs( "
				dhtmlXCalendarOptions['HTMLElementIds'].push( '{$name}_calendar' );
				_addOnLoad( 'var calendarOrders = yiama_dhtmlXCalendar( dhtmlXCalendarOptions )' );
			" );
		}
		return self::input( $name, $val, $required, array_merge( array( 'id' => "{$name}_calendar", 'class' => 'small' ), ( array ) $attrs ), $label, $wrapper_attrs );
	}
	
	public static function checkbox( 
        $name, 
        $value = null, 
        $checked = false, 
        $required = false, 
        $label = null, 
        $attrs = null, 
        $wrapper_attrs = null 
    ) {
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$attrs = array_merge( ( array ) $attrs, array( 
			'type' => 'checkbox', 
			'name' => $name,
            'value' => $value,
			'isRequired' => $required ? 'yes' : 'no'
		) );
		if( $checked ) {
			$attrs['checked'] = 'checked';
		}
		return self::element( HTML_Tag::void( 'input', $attrs ) . $s, $wrapper_attrs );
	}
	
	public static function radio( 
        $name, 
        $value = '', 
        $required = false, 
        $label = null,
        $attrs = array(),  
        $wrapper_attrs = null 
    ) {
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$attrs = array_merge($attrs, array( 
			'type' => 'radio', 
			'name' => $name,
            'value' => $value,
			'isRequired' => $required ? 'yes' : 'no'
		) );
		if( $checked ) {
			$attrs['checked'] = 'checked';
		}
		return self::element( HTML_Tag::void( 'input', $attrs ) . $s, $wrapper_attrs );
	}
	
	public static function textarea( 
        $name, 
        $val = '', 
        $required = false, 
        $label = null,
        $attrs = array(), 
        $wrapper_attrs = null 
    ) {
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		return self::element( $s . HTML_Tag::closed( 'textarea', array_merge( ( array ) $attrs, array( 
				'name' => $name,
				'isRequired' => $required ? 'yes' : 'no'
			) ), $val ? htmlspecialchars( $val ) : '' 
		), $wrapper_attrs );
	}
	
	public static function select( $name, $content, $value_prop, $value_text, $selected_prop = null, $selected_val = null, $required = false, $label = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		if( ! self::$fillConnectedSelect_init ) { /* επειδη μπορει η form να επιστρεφεται απο ajax θα πρεπει να υπαρχει το javascript στην σελιδα που κλαεσε το ajax */
			self::$fillConnectedSelect_init = true;
			Core_HTML::addJs( "
				function fillConnectedSelect( parent_el, json_content ) {
					var parent_value = parent_el.options[parent_el.selectedIndex].value;
					var selects = parent_el.parentNode.parentNode.getElementsByTagName( 'select' ); 
					for( var i = 0; i < selects.length; i++ ) {
						if( selects[i] == parent_el ) {
							var child_el = selects[i+1];
							break;
						}
					}
				    var content = JSON.parse( json_content );
			        var i = 0;
				    child_el.options.length = 0;
			        for( var key in content ) {
	  					if( content.hasOwnProperty( key ) && key == parent_value ) {
	  						var child_content = content[key];
	  						if( child_content ) {
	  							child_el.options[i++] = new Option( 'Επιλογή...', '' );  
	  						}
			        		for( var key2 in child_content ) {
	  							if( child_content.hasOwnProperty( key2 ) ) {
				            		child_el.options[i++] = new Option( child_content[key2], key2 );
				            	}
				            }
				        }
			        }
				}
			" );
		}
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$options = array();
		foreach( $content as $cont ) { 
			$attrs = array( 'value' => $cont->$value_prop );
			if( $option_attrs ) {
				foreach( $option_attrs as $key => $a ) {
					if( isset( $cont->$a ) ) {
						$attrs[ $key ] = $cont->$a;
					} else {
						$attrs[ $key ] = $a;
					}
				}
			}
			if( $selected_prop && $cont->$selected_prop === $selected_val ) {
				$attrs['selected'] = 'selected';
			}
			$options[] = HTML_Tag::closed( 'option', $attrs, $cont->$value_text ); 
		}
		$attrs = array_merge( array( 'name' => $name, 'isRequired' => $required ? 'yes' : 'no' ), ( array ) $select_attrs );
		if( isset( $extra['connected_select'] ) ) {
			$onchange = htmlspecialchars( " fillConnectedSelect( this, '{$extra['connected_select']['json_content']}' );" );
			$attrs['onchange'] = ( ! empty( $attrs['onchange'] ) ? $attrs['onchange'] . ';' : '' ) . $onchange;
		}
		return self::element( $s . HTML_Tag::closed( 'select', $attrs, implode( '', $options ) ), $wrapper_attrs );
	}
	
	public static function selectBoolean( $name, $selected_val = null, $required = false, $label = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		return self::select( $name, array( ( object ) array( 'value' => '0', 'title' => 'Όχι' ), ( object ) array( 'value' => '1', 'title' => 'Ναι' ) ), 'value', 'title', 'value', $selected_val, $required, $label, $select_attrs, $option_attrs, $extra, $wrapper_attrs );
	}
	
	public static function selectLocation( $names, $selected_ids = null, $required = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		$country = new Model_Geo_Country();
		$countries = $country->query()->search( 'is_published=1' )->find();
		$region = new Model_Geo_Region();
		$regions = $region->findAll();
		$select_content = array();
		foreach( $countries as $c ) { /* child select content */
			$select_content[ $c->id ] = null;
			foreach( $regions as $r ) {
				if( $r->geo_countries_id == $c->id ) {
					$select_content[ $c->id ][ $r->id ] = $r->title;
				}
			}
		}
		return 
			self::select( $names[0], $countries, 'id', 'title', 'id', isset( $selected_ids[0] ) ? $selected_ids[0] : null, isset( $required[0] ) ? $required[0] : null, 'Χώρα', $select_attrs, null, array( 'connected_select' => array( 'json_content' => json_encode( $select_content ) ) ), $wrapper_attrs ) .
			self::select( $names[1], $regions, 'id', 'title', 'id', isset( $selected_ids[1] ) ? $selected_ids[1] : null, isset( $required[1] ) ? $required[1] : null, 'Περιοχή', $select_attrs, null, null, $wrapper_attrs );
	}
	
	
}
?>