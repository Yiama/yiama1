<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<?php
Core_HTML::addFiles(array(
    'default\global.css',
    'default\fonts.css',
    'default\layout\default.css',
    'library\basic.js',
    'default\main.js'
));
$language = new Model_Yiama_Language();
$lang_code = $language->getCurrent()->code;

?>
<html 
	xmlns='http://www.w3.org/1999/xhtml' 
	xml:lang='<?php echo $lang_code; ?>' 
	lang='<?php echo $lang_code; ?>'>
	<head>
	<?php echo Core_HTML::getHeaders(); ?>
	<link rel='shortcut icon' href='<?php echo PATH_REL_ASSETS.'images'.DS.'favicon.png'; ?>'/>
	<?php echo Core_HTML::getFiles(); ?>
	<?php echo Core_HTML::getScripts(); ?>
	<?php echo Core_HTML::getStyles(); ?>
	</head>
	<body>
		<div class="main">
			<?php echo $this->view; ?>
		</div>
	</body>
</html>