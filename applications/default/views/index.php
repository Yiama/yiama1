<?php 
	Core_HTML::addFiles(array(
		'library/jquery/jquery-3.3.1.min.js'
	)); 
	if (!empty($this->home)) {
	    Core_HTML::addToTitle($this->home->meta_title);
	    Core_HTML::addToDescription($this->home->meta_description);
	}
?>
<strong style="display: block; padding: 10vw; text-align: center; font-size: 16pt; "><?php echo $this->text; ?></strong>