<div class='section contact'>
	<?php 
	$mod_form_contact = new Module_Form_Contact();
    echo Helper_Message::get();
    echo $mod_form_contact->render(array(
        'form_alias' => 'form-contact',
        'notification' => 'user-contact'
    )); 
    ?>
</div>