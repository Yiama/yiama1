<?php 
	$language = new Model_Yiama_Language();
	$trans_meta = Core_Translate::get(  $language->getCurrent()->code, 'default.meta', 'user_change_info' );
	$trans_user = Core_Translate::get(  $language->getCurrent()->code, 'default.user' );
	Core_HTML::addHeaders( array( 'title' => $trans_meta ) );	
	Core_HTML::addFiles( array(
		'default\common\form.css',
		'default\user\index.css'
	) ); 
?>
<!-- user_login ( -->
<div class='form'>
  <div class='header'><?php echo $trans_user['form']['headers']['change_info']; ?></div><?php echo $this->form; ?>
</div>
<!-- ) user_login -->