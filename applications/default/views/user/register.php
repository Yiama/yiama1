<?php 
	$language = new Model_Yiama_Language();
	$trans_meta = Core_Translate::get(  $language->getCurrent()->code, 'default.meta', 'user_register' );
	$trans_user = Core_Translate::get(  $language->getCurrent()->code, 'default.user' );
	Core_HTML::addHeaders( array( 'title' => $trans_meta ) );	
	Core_HTML::addFiles( array(
		'default\common\form.css',
		'default\user\index.css',
		'library\popup.js'
	) ); 
?>
<!-- user_register ( -->
<script type='text/javascript'>
	var popupRegister = new popup( {
		bg: {  
			attributes: {
				'class': 'popupBg'
			},
			events: [ { 
				'event': 'click',
				'func': 'popupRegister.unpop();'
			} ]
		},
		
		wrapper: {
			attributes: {
				'class': 'popupWrapper'
			}
		},
		
		message: {
			attributes: {
				'class': 'popupMessage'
			},
			text: "<?php echo str_replace( array( "\n", "\r" ), '', $trans_user['form']['info']['user_register'] ); ?>"
		},
		
		button1: {
			attributes: {
				'class': 'popupButton1'
			},
			events: [ { 
				'event': 'click',
				'func': 'popupRegister.unpop()'
			} ],
			text: '<?php echo $trans_user['form']['popup']['close']; ?>'
		}
	} );
</script>
<div class='form register'>
  <div class='header'><?php echo $trans_user['form']['headers']['register']; ?><span onclick='popupRegister.pop(); window.scrollTo( 0,0 )'>i</span></div>
  <?php echo $this->form; ?>
</div>
<!-- ) user_register -->