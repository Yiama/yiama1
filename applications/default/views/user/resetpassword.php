<?php 
	$language = new Model_Yiama_Language();
	$trans_meta = Core_Translate::get(  $language->getCurrent()->code, 'default.meta', 'user_reset_password' );
	$trans_user = Core_Translate::get(  $language->getCurrent()->code, 'default.user' );	
	Core_HTML::addFiles( array(
		'default\common\form.css',
		'default\user\index.css'
	) ); 
?>
<!-- user_resetpassword ( -->
<div class='form'><div class='header'><?php echo $this->header; ?></div><?php echo $this->form; ?></div>
<!-- ) user_resetpassword -->