<?php 
	$language = new Model_Yiama_Language();
	$trans_meta = Core_Translate::get(  $language->getCurrent()->code, 'default.meta', 'user_change_info' );
	$trans_user = Core_Translate::get(  $language->getCurrent()->code, 'default.user' );
	Core_HTML::addHeaders( array( 'title' => $trans_meta ) );
	Core_HTML::addFiles( array(
		'default\common\form.css',
		'default\user\index.css',
		'library\popup.js'
	) ); 
?>
<!-- user_login ( -->
<script type='text/javascript'>
	var popupPassword = new popup( {
		bg: {  
			attributes: {
				'class': 'popupBg'
			},
			events: [ { 
				'event': 'click',
				'func': 'popupPassword.unpop();'
			} ]
		},
		
		wrapper: {
			attributes: {
				'class': 'popupWrapper'
			}
		},
		
		message: {
			attributes: {
				'class': 'popupMessage'
			},
			text: "<?php echo str_replace( array( "\n", "\r" ), '', $trans_user['form']['info']['remind_password'] ); ?>"
		},
		
		button1: {
			attributes: {
				'class': 'popupButton1'
			},
			events: [ { 
				'event': 'click',
				'func': 'popupPassword.unpop()'
			} ],
			text: '<?php echo $trans_user['form']['popup']['close']; ?>'
		}
	} );
</script>
<div class='outerWrapper'>
	<div class='innerWrapper'>
		<div class='form login'>
		  <div class='header'><?php echo $trans_user['form']['headers']['login']; ?></div>
		  <?php echo $this->forms['login']; ?>
		</div>
		<div class='form reset'>
		  <div class='header'>
		  	<?php echo $trans_user['form']['headers']['request_new_password']; ?><br/>
		  	<span onclick='popupPassword.pop(); window.scrollTo( 0,0 )'>i</span>
		  </div>
		  <?php echo $this->forms['reset_password']; ?>
		  <div class='subheader'>
		  	<a href='<?php echo $this->register_link; ?>'><?php echo $trans_user['form']['headers']['register']; ?></a>
		  </div>
		</div>
	</div>
</div>
<!-- ) user_login -->