<?php 
	Core_HTML::addFiles( 'default\error\error.css' );
	Core_HTML::addToTitle( $this->controller->translate['html_headers']['user']['title'] );
?>
<!-- error ( -->
<div class='error'>
	<img src='<?php echo PATH_REL_IMAGES . "error/user.png'/"; ?>'><br/><br/>
	<?php echo $this->controller->translator->_('user.text'); ?><br/>
	<a href='<?php echo $this->controller->request->urlFromPath( '/' ); ?>'><?php echo $this->controller->translator->_('user.link'); ?></a>	
</div>
<!-- ) error -->