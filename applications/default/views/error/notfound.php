<?php 
	Core_HTML::addFiles( 'default\error.css' );
?>
<div class='error not-found'>
	<div class="bg"></div>
	<?php echo $this->controller->translator->_('notfound'); ?><br/>
	<a href='<?php echo $this->controller->request->urlFromPath( '/' ); ?>'>
		<span><?php echo Core_App::getConfig('domain'); ?></span>
	</a>	
</div>