<?php
class Controller_Index extends Core_Controller {

	public function index() {
		$this->setView();
		$this->view->addVars(array(
		    'text' => "Yiama Framework"
        ));
		$view = $this->view->render();
		$this->setView(dirname(__DIR__) . DS . 'views' . DS . "layout" . DS . 'default');
        $this->view->addVars( array( 'view' => $view ) );
		$this->response->setBody( $this->view->render() );
	}
}