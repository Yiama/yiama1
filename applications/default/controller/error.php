<?php

class Controller_Error extends Core_Controller
{
	public function before()
	{
		$language = new Model_Yiama_Language();
		$this->translate = Core_Translate::get( $language->getCurrent()->code, 'error' );
	}
	
	public function user()
	{		
		$this->response->setHeaders( 'HTTP/1.0 404 Not Found' );
		
		/* View */
		$this->setView( dirname( __DIR__ ) . DS . 'views' . DS . 'error' . DS . 'user' );
		$view = $this->view->render();
		$this->setView( dirname(__DIR__) . DS . 'views' . DS . "layout" . DS . 'default' );
		$this->response->setBody( $this->view->addVars( array( 'view' => $view ) )->render() );
	}	
	
	public function notfound()
	{		
		$this->response->setHeaders( 'HTTP/1.0 404 Not Found' );

		/* View */
		$this->setView( dirname( __DIR__ ) . DS . 'views' . DS . 'error' . DS . 'notfound' );
		$view = $this->view->render();
		$this->setView( dirname(__DIR__) . DS . 'views' . DS . "layout" . DS . 'default' );
		$this->response->setBody( $this->view->addVars( array( 'view' => $view ) )->render() );
	}
}

?>