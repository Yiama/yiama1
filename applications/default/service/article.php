<?php
class Service_Article {
    
    public function getArticleById($id) {
        
        $article = new Model_Yiama_Article();
        return $article->query()
            ->search('id=' . $id)
            ->find(':first');
    }
    
    public function getArticlesById(array $ids) {
        
        $article = new Model_Yiama_Article();
        return $article->query()
            ->search('id IN('.implode(',', $ids).')')
            ->order('ordered')
            ->find();
    }
    
    public function getArticlesByTagId($id) {
        
        $article = new Model_Yiama_Article();
        return $article->query()
            ->join('ym_articles_tags at', 'at.ym_articles_id = id' 
                .' AND ym_tags_id='.$id)
            ->order('ordered')
            ->find();
    }
    
    public function getArticlesByCategoryId($id) {
        
        $article = new Model_Yiama_Article();
        return $article->query()
            ->search("ym_categories_id=" . $id)
            ->combine("images")
            ->order('ordered')
            ->find();
    }
}