<?php
	return array(
		'html_meta' => array(
			'products' => 'Προϊόντα'
		),
		'products' => array(
			'available_from' => 'Διαθέσιμο από'
		),
		'product' => array(
			'product_code' => 'Κωδικός προϊόντος',
			'available_from' => 'Διαθέσιμο από',
			'description' => 'Περιγραφή'
		),
		'basket' => array(
			'button_refresh' => 'ανανέωση',
			'button_return' => 'επιστροφή',
			'button_order' => 'παραγγελία'
		),
		'favorites' => 'Δείτε τα αγαπημένα σας',
		'brands' => 'Μάρκες',
		'customer' => 'Στοιχεία πελάτη',
		'orderlogin' => array(
			'button_submit' => 'συνέχεια',
			'form' => array(
				'order_as_guest' => 'Παραγγελία ως επισκέπτης',
				'order_as_user' => 'Παραγγελία με σύνδεση',
				'password' => 'Κωδικός'
			),
			'ajax' => array(
				'user_tried_login_as_guest' => 'Το email που δώσατε είναι υπάρχοντος χρήστη, παρακαλούμε συνδεθείτε',
				'user_login_credentials_failed' => 'Τα στοιχεία που δώσατε δεν είναι σωστά',
				'unknown' => 'Προέκυψε κάποιο σφάλμα παρακαλούμε ξαναπροσπαθήστε'
			)
		),
		'order' => array(
			'button_basket' => 'καλάθι',
			'button_order' => 'παραγγελία',
			'button_submit' => 'καταχώρηση',
			'basket_price_includes_tax' => '(με ΦΠΑ)',
			'basket_price_doesnt_include_tax' => '(χωρίς ΦΠΑ)',
			'subtotal' => 'Υποσύνολο',
			'tax' => 'Φόρος',
			'discount' => 'Έκπτωση',
			'delivery' => 'Μεταφορικά',
			'payment' => 'Επιβάρυνση πληρωμής',
			'total' => 'Σύνολο',
			'form' => array(
				'customer_info' => 'Στοιχεία τιμολόγησης',
				'more_info' => 'Λοιπά στοιχεία',
				'select_customer' => 'Επιλογή στοιχείων','receipt' => 'Απόδειξη',
				'delivery_info' => 'Θέλω να αποσταλεί σε διαφορετικά στοιχεία από αυτά της τιμολόγησης',
				'invoice' => 'Τιμολόγιο',
				'name' => 'Όνομα',
				'secondname' => 'Επώνυμο',
				'country' => 'Χώρα',
				'region' => 'Νομός',
				'municipality' => 'Δήμος',
				'address' => 'Διεύθυνση',
				'zipcode' => 'Ταχ. κώδικας',
				'phone1' => 'Τηλέφωνο 1',
				'phone2' => 'Τηλέφωνο 2',
				'company_name' => 'Όνομα εταιρίας',
				'activity' => 'Δραστηριότητα',
				'tax_office' => 'Δ.Ο.Υ.',
				'tax_id' => 'Α.Φ.Μ.',
				'comments' => 'Σχόλια',
				'insert_receipt_customer_info' => 'Δημιουργία νέου πελάτη με Απόδειξη',
				'insert_invoice_customer_info' => 'Δημιουργία νέου πελάτη με Τιμολόγιο',
				'other_customer_info' => 'Θέλω να δώσω άλλα στοιχεία',
				'update_customer_info' => 'Ενημέρωση στοιχείων',
				'insert_customer_info' => 'Εισαγωγή νέων',
				'update_customerinfo' => 'Αποθήκευση αλλαγών στοιχείων',
				'insert_customerinfo' => 'Αποθήκευση στοιχείων',
				'invoice' => 'Είδος παραστατικού',
				'delivery' => 'Τρόπος παραλαβής',
				'payment' => 'Τρόπος πληρωμής',
				'submit' => 'Καταχώρηση',
			),
			'submit' => array(
				'errors' => array(
					'failed' => 'Η εισαγωγή της παραγγελίας απέτυχε'
				)
			)
		),
		'orderchangepaymentmethod' => array(
			'form' => array(
				'header' => 'Επιλέξτε ξανά τρόπο πληρωμής',
				'submit' => 'Αλλαγή',
			),
			'ajax' => array( 
				'update_failed' => 'Η αλλαγή του τρόπου πληρωμής απέτυχε παρακαλούμε ξαναπροσπαθήστε' 
			)
			
		),
		'orderstatus' => array(
			'body' => 'Σας έχουν αποσταλλεί τα στοιχεία της παραγγελίας σας στο email',
			'subject' => 'Επιβεβαίωση παραγγελίας',
			'sir' => 'Αγαπητέ/ή κύριε/α',
			'header' => 'σας ευχαριστούμε που προτιμήσατε εμάς για τις αγορές σας.',
			'order' => 'Παραγγελία',
			'delivery_method' => 'Τρόπος παραλαβής',
			'delivery_cost' => 'Έξοδα τρόπου παραλαβής',
			'payment_method' => 'Τρόπος πληρωμής',
			'payment_cost' => 'Έξοδα τρόπου πληρωμής',
			'subtotal' => 'Υποσύνολο',
			'total' => 'Σύνολο',
			'product_title' => 'Προϊόν',
			'product_quantity' => 'Ποσότητα',
			'product_price' => 'Τιμή',
			'billing_info' => 'Στοιχεία τιμολόγησης',
			'shipping_info' => 'Στοιχεία αποστολής'
		),
		'favorites' => array(
			'title' => 'Αγαπημένα',
			'added' => 'Το προϊόν προστέθηκε στα αγαπημένα',
			'removed' => 'Το προϊόν αφαιρέθηκε από τα αγαπημένα'
		)
	);
?>
