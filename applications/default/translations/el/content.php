<?php
	return array(
		'basket' => 'Δείτε το καλάθι σας',
		'favorites' => 'Δείτε τα αγαπημένα σας',
		'contact' => 'Επικοινωνία',
		'user_login' => 'Σύνδεση χρήστη',
		'user_register' => 'Εγγραφή χρήστη',
		'user_reset_password' => 'Αλλαγή κωδικού',
		'user_change_info' => 'Αλλαγή στοιχείων χρήστη',
		'user_index' => 'Σύνδεση χρήστη',
		'customer' => 'Στοιχεία πελάτη',
		'order' => 'Παραγγελία'
	)
?>