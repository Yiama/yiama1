<?php
	class Model_Yiama_Subscription extends DB_ActiveRecord_Model
	{		
		protected static $table_name = 'ym_subscribers_subscriptiontypes';
		protected static $primary_key = array( 'ym_subscribers_id', 'ym_subscriptiontypes_id' );
		protected static $relations = array(
			'type' => array( 
				'type' => 'one_to_one',
				'model_name' => 'Model_Yiama_Subscriptiontype',
				'foreign_key' => array( 'ym_subscriptiontypes_id' => 'id' )
			),
			'subscriber' => array( 
				'type' => 'one_to_one',
				'model_name' => 'Model_Yiama_Subscriber',
				'foreign_key' => array( 'ym_subscribers_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		/*
		 * Αν δεν υπάρχει subscriber με ίδιο email τον εισάγει,
		 * αν δεν υπάρχει ίδιος subscriber με ίδιο subscriptiontype τον εισάγει στον subscriptions
		 * @param $type_alias string
		 * @param $email string
		 * @param $info array
		 */
		public function add( $type_alias, $email, $info = null, $update_if_exists = false )
		{
			$type = new Model_Yiama_Subscriptiontype();
			$type = $type->query()
				->search( "alias = '{$type_alias}'" )
				->find( ':first' );
			if( ! $type ) { /* Δεν υπάρχει το ζητούμενο subscriptiontype { π.χ. έχει διαγραφεί από τον διαχειριστή } */
				return;
			}
			$existed_subscription = $this->query()
				->join( 'ym_subscribers AS s', "s.id = ym_subscribers_subscriptiontypes.ym_subscribers_id AND s.email = '{$email}'" )
				->join( 'ym_subscriptiontypes AS st', "st.id = {$type->id}" )
				->group( 'ym_subscribers_subscriptiontypes.ym_subscribers_id, ym_subscribers_subscriptiontypes.ym_subscriptiontypes_id' )
				->find( ':first' );
			if( $existed_subscription ) { /* Αν υπάρχει ήδη η subscription την ενημερώνουμε αν ζητείται αυτό*/
				if( $update_if_exists ) {
					$existed_subscription->info = json_encode( $info );
					$existed_subscription->update();
                    return 1;
				} else {
				    return 2;
                }
			}
			$subscriber = new Model_Yiama_Subscriber();
			$existed_subscriber = $subscriber->query()
				->search( "email = '{$email}'" )
				->find( ':first' );
			if( ! $existed_subscriber ) { /* Αν ο subscriber δεν υπάρχει τον εισάγουμε */ 
				$subscriber->email = $email;
				$subscriber->insert();
				$this->ym_subscribers_id = $subscriber->id;
			} else {
				$this->ym_subscribers_id = $existed_subscriber->id;
			}
			$this->ym_subscriptiontypes_id = $type->id;
			$this->info = json_encode( $info );
			$this->insert();

			return 1;
		}
	}
?>