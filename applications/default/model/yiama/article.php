<?php
	class Model_Yiama_Article extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_articles';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'category' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Yiama_Category',
				'foreign_key' => array( 'ym_categories_id' => 'id' )
			), 
			'images' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Articleimage',
				'foreign_key' => array( 'id' => 'ym_articles_id' )
			), 
			'attributes' => array( 
				'type' => 'many_to_many',
				'model_name' => 'Model_Yiama_Articleattribute',
				'foreign_key' => array( 'id' => 'ym_articles_id' ),
				'combine' => 'type'
			), 
			'attributevalues' => array( 
				'type' => 'many_to_many',
				'model_name' => 'Model_Yiama_Articleattributevalue',
				'foreign_key' => array( 'id' => 'ym_articles_id' ),
				'combine' => 'value'
			),
			'tags' => array(
				'type' => 'many_to_many',
				'model_name' => 'Model_Yiama_Articletag',
				'foreign_key' => array( 'id' => 'ym_articles_id' )
			),
		);
		
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$language = $model_language->getCurrent();
			$this->lang_id = $language->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
		
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( self::$table_name . '_local AS local', 'local.' . self::$table_name . '_id = ' . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" );
				
		}
		
		public function getDefaultImage() /* Require combined 'images' */
		{
			if( empty( $this->images[0] ) ) {
				return;
			}
			$def_img = $this->images[0];
			foreach( $this->images as $img ) {
				if( $img->is_default ) {
					$def_img = $img;
					break;
				}
			}
			return $def_img;
		}
		
		public function getPublishedImages( $not_default = false ) /* Require combined 'images' */
		{
			if( empty( $this->images[0] ) ) {
				return;
			}
			$published = array();
			foreach( $this->images as $img ) {
				if( $not_default && $img->is_default ) {
					continue;
				}
				if( $img->is_published ) {
					$published[] = $img;
				}
			}
			return $published;
		}
		
		/*
		 * combine $attribute ( combine $type )
		 */
		public function getAttributeByTypeAlias( $type_alias )
		{
			if( ! isset( $this->attributes ) ) {
				return;
			}
			foreach( $this->attributes as $a ) {
				if( $a->type->alias == $type_alias ) {
					return $a;
				}
			}
		}
	}
?>
