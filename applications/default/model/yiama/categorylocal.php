<?php
	class Model_Yiama_Categorylocal extends DB_ActiveRecord_Model
	{		
		protected static $table_name = 'ym_categories_local';
		protected static $primary_key = array( 'ym_categories_id', 'ym_languages_id' );
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>
