<?php
	class Model_Yiama_Notification extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_notifications';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'recipients' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Notificationrecipient',
				'foreign_key' => array( 'id' => 'ym_notifications_id' )
			), 
			'template' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Yiama_Template',
				'foreign_key' => array( 'ym_templates_id' => 'id' )
			) 
		);
		protected static $lang;
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getLang()
		{
			if( empty( self::$lang ) ) {
				$option = new Model_Yiama_Option();
				self::$lang = $option->getByKey( 'notifications_language_id' );
			}
			return self::$lang;
		}
	}
?>