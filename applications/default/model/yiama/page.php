<?php
	class Model_Yiama_Page extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_pages';
		protected static $primary_key = 'id';
		
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$language = $model_language->getCurrent();
			$this->lang_id = $language->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( self::$table_name . '_local AS local', 'local.' . self::$table_name . '_id = ' . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" )
				->order( 'local.title ASC' );
		}
		
		/*
		 * @para $remove_parts_num Αφαίρεση μερών του route path, από το τέλος
		 */
		public function getRoutePath( $remove_parts_num = null )
		{ 
			$route_path = Core_Request::getInstance()->getUrlRoutePath();
			$parts = array_filter( explode( '/', $route_path ) );
			if( ( $is_multilingual = Core_App::getConfig( "applications." . Core_App::getAppName() . ".multilingual" ) ) && $is_multilingual ) { /* Αν είναι Multilingual το πρώτο μέρος θα είναι το Langcode */
				array_shift( $parts );
			}
			if( intval( $remove_parts_num ) == count( $parts ) ) {
				return null;
			}
			if( $remove_parts_num ) {
				$parts = array_slice( $parts, 0, count( $parts )-$remove_parts_num );
			}
			return implode( '/', $parts );
		}
		
		/*
		 * Δίνουμε το type alias γιατί pages από διαφορετικά menu types μπορεί να έχουν το ίδiο url
		 */
		public function getCurrent( $page_type_alias, $remove_parts_num = 0 )
		{
			$result = null;
			if( $route_path = $this->getRoutePath( $remove_parts_num ) ) { /* Αν έχουν μείνει route path parts να ελέγξουμε */
				$result = $this->query()
					->select( 'ym_pages.*, local.*' )
					->join( 'ym_pagetypes AS t', 't.id = ym_pages.ym_pagetypes_id' )
					->search( array( 
						"t.alias = '{$page_type_alias}'",
						"url = '{$route_path}'" 
					) )
					->find( ':first' );
			}
			if( empty( $result ) && $route_path ) {
				return $this->getCurrent( $page_type_alias, $remove_parts_num+1 );
			} else {
				return $result;
			}
		}
		
		public function getParents( $parent_id = null, $all = null )
		{
			$result = array();
			if( ! $parent_id ) {
				$parent_id = $this->parent_id;
				$all = $this->findAll();
			}
			if( $parent_id > 1 ) {
				foreach( $all as $v ) {
					if( $v->id == $parent_id ) {
						$result = $v;
					}
				}
				$result = array_merge( array( $result ), $this->getParents( $result->parent_id, $all ) );
			}
			return $result;
		}
		
		public function getByTypeAlias( $alias, $published = true )
		{
			$type = new Model_Yiama_Pagetype();
			$type = $type->query()
				->search( "alias = '{$alias}'" )
				->find( ':first' );
            if( !empty($type)) {
                $search = array();
                $search[] = 'ym_pagetypes_id = ' . $type->id;
                if( $published ) {
                    $search[] = 'is_published = 1';
                }
                return $this->query()	
                    ->search( $search )
                    ->order( 'ordered ASC' )
                    ->find();
            }
		}
	}
?>
