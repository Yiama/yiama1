<?php
	class Model_Yiama_Usergroup extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_usergroups';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'users' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_User',
				'foreign_key' => array( 'id' => 'ym_usergroups_id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>