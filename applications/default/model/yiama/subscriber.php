<?php
	class Model_Yiama_Subscriber extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_subscribers';
		protected static $primary_key = 'id';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>