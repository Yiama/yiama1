<?php
	class Model_Yiama_Articleimage extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_articles_images';
		protected static $primary_key = 'id';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getNormal()
		{
			return PATH_REL_IMAGES . 'db/ym_articles/' . $this->name;
		}
		
		public function getThumbnail()
		{
			return PATH_REL_IMAGES . 'db/ym_articles/thumbnails/thumb_' . $this->name;
		}
	}
?>
