<?php
	class Model_Yiama_Articleattribute extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_articles_attributes';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'type' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Yiama_Attribute',
				'foreign_key' => array( 'ym_attributes_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>
