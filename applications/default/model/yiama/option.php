<?php
	class Model_Yiama_Option extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_options';
		protected static $primary_key = 'id';
		
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getByKey( $key, $assoc = null )
		{
			$result = $this->query()
				->search( "`key` = '{$key}'"  )
				->find( ':first' );
            if (empty($result->value)) {
                return;
            }
			if( strpos( ltrim ( $result->value ), '{' ) === 0 ) {
				return json_decode( $result->value, $assoc );
			}
			return $value;
		}
	}
?>

