<?php
	class Model_Yiama_Language extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_languages';
		protected static $primary_key = 'id';
		protected static $current;
		protected static $default;
		protected static $site_default;
			
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->order( 'is_default DESC, is_sites_default DESC, is_published DESC' );
		}
			
		public function getCurrent()
		{
			if( ! empty( self::$current ) ) {
				return self::$current;
			}
			$params = Core_Request::getInstance()->getParams();
			return self::$current = isset( $params['langcode'] ) ? $this->query()->search( "code = '{$params['langcode']}'" )->find( ':first' ) : $this->getDefault();
		}	
				
		public function getDefault()
		{
			if( ! empty( self::$site_default ) ) {
				return self::$site_default;
			}
			return self::$site_default = $this->query()
				->search( "is_sites_default = 1" )
				->find( ':first' );
		}
		
		public function getImage()
		{
			$this->image->normal = PATH_REL_IMAGES . 'db/ym_languages/' . $this->image->name;
			$this->image->thumbnail = PATH_REL_IMAGES . 'db/ym_languages/thumbnails/thumb_' . $this->image->name;
			return $this->image;
		}
	}
?>