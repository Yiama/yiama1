<?php
	class Model_Geo_Countrylanguage extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'geo_countries_languages';
		protected static $primary_key = array( 'country_code', 'language_code' );
		protected static $relations = array(
			'country' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Geo_Country',
				'foreign_key' => array( 'geo_countries_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>