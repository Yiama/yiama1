<?php
	class Model_Geo_Region extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'geo_regions';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'country' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Geo_Country',
				'foreign_key' => array( 'geo_countries_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->order( 'title ASC' );	
		}
	}
?>