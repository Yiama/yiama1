<?php
	class Model_Helper_Query
	{
		public static function search()
		{
			$search = array();
			if( $param_search = Core_Request::getInstance()->getParams( 'search' ) ) {
				foreach( $param_search as $key => $val ) {
					if( is_array( $val ) ) {
						$key_val = key( $val );
						if( isset( $val['from'] ) ) {
							$search[] = "{$key} >= '{$val['from']}'";
						}
						if( isset( $val['to'] ) ) {
							$search[] = "{$key} <= '{$val['to']}'";
						}
						if( isset( $val['date_from'] ) ) {
							$search[] = "DATE({$key}) >= DATE('{$val['date_from']}')";
						}
						if( isset( $val['date_to'] ) ) {
							$search[] = "DATE({$key}) <= DATE('{$val['date_to']}')";
						}
						if( isset( $val['like'] ) ) {
							$search[] = "{$key} LIKE '%{$val['like']}%'";
						}
					} else {
						$search[] = "{$key} = '{$val}'";
					}
				}
			}
			return $search;
		}
		
		public static function limit( $limit = null )
		{
			$page = ( $p = Core_Request::getInstance()->getParams( 'page' ) ) ? $p : 1;
			if( is_null( $limit ) ) {
				$config_params = Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.modules.pagination' );
				$limit = $config_params['content_per_page'];
			}
			return ( $page-1 )*$limit . ',' . $limit;
		}
	}
?>