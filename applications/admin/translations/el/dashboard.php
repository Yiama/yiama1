<?php
return array(
	'section1.header' => 'Άρθρα',
    'section1.total' => 'σύνολο',
    'section1.visits' => 'επισκέψεις',
    'section1.active' => 'ενεργά',
    'section1.from' => 'από',
    'section2.header' => 'Google Analytics',
    'section3.latest' => 'Πρόσφατα άρθρα',
    'section3.popular' => 'Δημοφιλή Άρθρα',
    'section4.latest' => 'Πρόσφατα Μηνύματα/Ειδοποιήσεις'
);