<?php
return array(
    'button.return' => 'Επιστροφή',
    'button.save' => 'Αποθήκευση',
    'button.add' => 'Προσθήκη νέου',
    'button.page_types' => 'Τύποι σελίδων',
    'button.regions' => 'Περιοχές',
    'button.countries' => 'Χώρες',
    'button.export_to_csv' => 'Εξαγωγή σε αρχείο .csv'
);