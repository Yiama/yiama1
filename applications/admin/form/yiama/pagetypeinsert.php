<?php
class Form_Yiama_PagetypeInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form  = Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::title(null, true);
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias(null, null, 'Alias');
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}