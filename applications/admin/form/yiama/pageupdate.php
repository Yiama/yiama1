<?php
class Form_Yiama_PageUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::openForm( Helper_Link::update( $data ) );
        $data_parent = $data
            ->query()
            ->search('is_admin IS NULL OR is_admin <> 1')
            ->find();
		$pages = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree( $data_parent, 0, 'id', 'parent_id' ),
            'children' 
        );
        // Disable selection if it has articles or is the current category
        foreach( $pages as $p ) {
            $p->tree_disabled = $p->id == $data->id ? true : false;
		}
        $form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Pagelocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
			} else {
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();	
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'url', $data->url, null, null, 'URL' );
		$form .= Helper_Form::textarea( 'module', $data->module, null, null, 'Module' );
		$type = new Model_Yiama_Pagetype();
        $types = $type  
            ->query()
            ->search('is_admin IS NULL OR is_admin <> 1')
            ->find();
		$form .= Helper_Form::select( 
            'type_id', 
            $types, 
            'id', 
            'title', 
            'id', 
            $data->ym_pagetypes_id, 
            true, 
            array($t->_('label.type'), null, null, null, array( 
                $t->_('label.create_new') => Helper_Link::insertform( 
                    'yiama_pagetypes', urlencode($request->getURI()) 
                    ) 
                ) 
            ) 
        );
		$form .= Helper_Form::selectTree( $pages, 'parent_id', $data->parent_id, true, array( 'Υπερσελίδα', null, null, null, array( 'Δημιουργία νέας' => Helper_Link::insertform( 'yiama_pages', urlencode( $request->getURI() ) ) ) ) );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias( $data->alias, null, true );
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}