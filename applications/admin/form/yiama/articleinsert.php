<?php
class Form_Yiama_ArticleInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
		$form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.images'), 
                'tab3' => $t->_('tab.attributes') 
            ), 
            array(
                'required' => array('tab1'),
                'disabled' => array('tab3')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		$default_lang = $language->getDefault();
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::shortDescription( null, null, $l );
				$form .= Helper_Form::description( null, null, $l );
				$form .= Helper_Form::text( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$category = new Model_Yiama_Category();
		$categories = DB_ActiveRecord_Array::flatten(
            DB_ActiveRecord_Array::tree($category->findAll(), 1, 'id', 'parent_id'), 
            'children' 
        );
		foreach( $categories as $c ) {
			$c->tree_disabled = ( $c->total_children ) ? true : false;
		}
		$form .= Helper_Form::selectTree( 
            $categories, 
            'cat_id', 
            null, 
            true, 
            array( 
                $t->_('label.category'), 
                $t->_('label.only_with_one_without_subcategories'), 
                null, 
                null, 
                array( 
                    $t->_('label.create_new') => Helper_Link::insertform( 
                        'yiama_categories', 
                        urlencode($request->getURI())
                    ) 
                ) 
            ) 
        );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::inputAjax(
            'tags', 
            'tags[]', 
            '/' . Core_App::getConfig('baseDir') . '/admin/yiama_tags/ajax',
            null,
            null,
            null,
            array('Tags', $t->_('label.value_must_be_in_default_language_translate_from_administration'))
        );
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::images();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab3' );
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule($t->_('label.meta_info'));
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::meta( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper('Alias');
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::alias( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish();
		$form .= Helper_Form::closeSubWrapper();
        $form .= Helper_Form::openSubWrapper();
        $form .= Helper_Form::favorite();
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}