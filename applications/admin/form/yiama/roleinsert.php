<?php
class Form_Yiama_RoleInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.permissions')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'title', null, true, null, $t->_('label.title') );
		$form .= Helper_Form::input( 'description', null, true, null, $t->_('label.description') );
        $role = new Model_Yiama_Role();
		$form .= Helper_Form::select( 
            'parent_id', 
            $role->findAll(), 
            'id',
            'title',
            null, 
            null, 
            true, 
            $t->_('label.superrole')
        );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$permission = new Model_Yiama_Permission();
		$form .= Helper_Form::addButton( 
            Helper_Form::select( 
                'permissions[]', 
                $permission->findAll(), 
                'id', 
                'title', 
                null, 
                null, 
                true,
                $t->_('label.permission')
            ) .
            Helper_Form::select( 
                'permission_states[]', 
                array(
                    (object)array('id' => -1, 'title' => $t->_('label.restricted')),
                    (object)array('id' => 1, 'title' => $t->_('label.allowed')),
                    (object)array('id' => 0, 'title' => $t->_('label.inherited'))
                ), 
                'id', 
                'title', 
                null, 
                null, 
                true,
                $t->_('label.state') 
            )  
        );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}