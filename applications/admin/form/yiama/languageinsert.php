<?php
class Form_Yiama_LanguageInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form  = Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 
            'code', 
            null, 
            true, 
            array( 'valueLength' => 'exact:2' ), 
            $t->_('label.shortcode') 
        );
		$form .= Helper_Form::title( null, true );
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}