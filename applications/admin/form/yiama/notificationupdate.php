<?php
class Form_Yiama_NotificationUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.recepient_emails')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$template = new Model_Yiama_Template();
		$form .= Helper_Form::select( 
            'temp_id', 
            $template->findAll(), 
            'id', 
            'title', 
            'id', 
            $data->ym_templates_id, 
            true, 
            'Πρότυπο' 
        );
		$form .= Helper_Form::title( $data->title, true );
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::alias( $data->alias, null, 'Alias' );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$recipient = new Model_Yiama_Notificationrecipient();
		$recipients = $recipient->query()
			->search( "ym_notifications_id = {$data->id}" )
			->order( Model_Helper_Query::order() )
			->find();
		Helper_Catalogue::setHeaders( array( 'Email' => 'email' ) );
		Helper_Catalogue::setContent( 'ym_notificationrecipients', $recipients, array( array( 'text' => 'email' ) ) );
		Helper_Catalogue::setContentActions( $recipients, 'delete', 'yiama_notificationrecipients' );
		$form .= Helper_Catalogue::get();
		$form .= Helper_Form::addButton(Helper_Form::input( 
            'recipients[]', 
            null, 
            true, 
            array( 'validValues' => 'valEmail' ), 
            'Email παραλήπτη' 
        ));
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}