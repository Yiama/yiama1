<?php
class Form_Yiama_CategoryUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $tb_name = 'ym_categories';
        $form  = Helper_Form::openForm(  Helper_Link::update( $data )  );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Categorylocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::description( $data_local->description, null, $l );
                $form .= Helper_Form::text( $data_local->text, $l );
			} else {
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::description( null, null, $l );
				$form .= Helper_Form::text( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();	
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();		
		$all = $data->query()
			->select( 'ym_categories.*, local.*, COUNT( a.id ) AS total_articles' )
			->join( 'ym_articles AS a', 'a.ym_categories_id = ym_categories.id', 'LEFT' )
			->group( 'ym_categories.id')
			->find();
		$categories = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree( $all, 0, 'id', 'parent_id' ), 
            'children' 
        );
		// Disable selection if it has articles or is the current category
        foreach( $categories as $c ) {
            if ($c->total_articles || $c->id == $data->id) {
                $c->tree_disabled = true;
            }
		}
		$form .= Helper_Form::selectTree( $categories, 'parent_id', $data->parent_id, true, 'Κατηγορία που ανήκει' );
		$form .= Helper_Form::closeSubWrapper();	
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule($t->_('label.meta_info'));
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Categorylocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::meta( array( 'title' => $data_local->meta_title, 'description' => $data_local->meta_description, 'keywords' => $data_local->meta_keywords ), $l );
			} else {
				$form .= Helper_Form::meta( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();	
		$form .= Helper_Form::image( $tb_name, $data->image );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper('Alias');
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Categorylocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::alias( $data_local->alias, $l );
            } else {
				$form .= Helper_Form::alias( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();	
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'url', $data->url, null, null, 'URL' );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}