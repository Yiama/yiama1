<?php
class Form_Yiama_NotificationInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.recepient_emails')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$template = new Model_Yiama_Template();
		$form .= Helper_Form::select( 
            'temp_id', 
            $template->findAll(), 
            'id', 
            'title', 
            null, 
            null, 
            true, 
            $t->_('label.template') 
        );
		$form .= Helper_Form::title( null, true );
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::alias('', null, 'Alias');
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::addButton(Helper_Form::input( 
            'recipients[]', 
            null, 
            true, 
            array( 'validValues' => 'valEmail' ), 
            $t->_('label.recepient_email') 
        ));
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}