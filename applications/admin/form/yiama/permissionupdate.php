<?php
class Form_Yiama_PermissionUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form  = Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input('title', $data->title, true, null, $t->_('label.title'));
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}