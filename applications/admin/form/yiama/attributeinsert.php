<?php
class Form_Yiama_AttributeInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
		$form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.values'), 
            ), 
            array(
                'required' => array('tab1'),
                'disabled' => array('tab3')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		$default_lang = $language->getDefault();
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'description', null, true, null, $t->_('label.short_description') );
		$form .= Helper_Form::checkbox( 'is_multiselect', false, false, null, $t->_('label.multiple_values_can_be_selected') );
		$form .= Helper_Form::select( 'type', array( 
            ( object ) array( 'value' => 'string', 'title' => $t->_('label.simple_value') ), 
            ( object ) array( 'value' => 'float', 'title' => $t->_('label.decimal') ), 
            ( object ) array( 'value' => 'date', 'title' => $t->_('label.date') ), 
            ( object ) array( 'value' => 'boolean', 'title' => $t->_('label.logical_value') ) 
        ), 'value', 'title', null, null, true, $t->_('label.type') );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$category = new Model_Yiama_Category();
		$categories = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree(
                $category->findAll(), 
                1, 
                'id', 
                'parent_id' 
            ), 
            'children' 
        );
		$form .= Helper_Form::selectTree( 
            $categories, 
            'cat_ids[]', 
            null, 
            true, 
            array( 
                $t->_('label.category'), 
                null, 
                null, 
                null, 
                array($t->_('label.create_new') => Helper_Link::insertform( 
                    'yiama_categories', 
                    urlencode( $request->getURI() ) 
                )) 
            ), 
            array('multiple' => 'multiple') 
        );
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule($t->_('label.attribute_insert_values_info'));
        $form .= Helper_Form::addButton( Helper_Form::input( 'values_titles[]' ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias('', null, true);
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}