<?php
class Form_Yiama_CategoryInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form = Helper_Form::openForm(Helper_Link::insert());
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		$default_lang = $language->getDefault();
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::description( null, null, $l );
				$form .= Helper_Form::text( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$data = new Model_Yiama_Category();
		$all = $data->query()
			->select( 'ym_categories.*, local.*, COUNT( a.id ) AS total_articles' )
			->join( 'ym_articles AS a', 'a.ym_categories_id = ym_categories.id', 'LEFT' )
			->group( 'ym_categories.id')
			->find();
		$categories = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree( $all, 0, 'id', 'parent_id' ), 
            'children' 
        );
		foreach( $categories as $c ) {
			$c->tree_disabled = ( $c->total_articles ) ? true : false;
		}
		$form .= Helper_Form::selectTree( 
            $categories, 
            'parent_id', 
            null, 
            true, 
            $t->_('label.supercategory')
        );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule($t->_('label.meta_info'));
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::meta( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper('Alias');
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
            $form .= Helper_Form::alias( null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::closeSubWrapper();;
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}