<?php
class Form_Yiama_UserInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.informations'), 
                'tab2' => $t->_('tab.roles')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'username', null, true, null, 'Username' );
		$form .= Helper_Form::input( 'password', null, true, null, 'Password' );
		$form .= Helper_Form::input( 'email', null, true, array( 'validValues' => 'valEmail' ), 'Email' );
		$form .= Helper_Form::element(Helper_Form::imageButton('jpg,jpeg,png'));
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$role = new Model_Yiama_Role();
		$form .= Helper_Form::addButton( Helper_Form::select( 'roles[]', $role->findAll(), 'id', 'title', null, null, true ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}