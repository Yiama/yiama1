<?php
class Form_Yiama_FileUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $t) {
        $data->dirPath = dirname($data->path);
        $data->name = basename($data->path);
        $directories = Controller_Yiama_Files::directories();
        if (!$data->isFile) {
            foreach ($directories as $key => $directory) {
                if ($directory->value == $data->dirPath . DS . $data->name) {
                    unset($directories[$key]);
                }
            }
        }

        $form = Helper_Form::openForm(Helper_Link::update() . "?path=" . $data->path);
        $form .= Helper_Form::openWrapperColumnLeft();
        $form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::select("directory", $directories, "value", "text",  "value", $data->dirPath, false, $data->isFile ? "Φάκελος" : "Υπερφάκελος");
        $form .= Helper_Form::input("name", $data->name, false, null, $data->isFile ? "Όνομα αρχείου" : "Όνομα φακέλου");
        if ($data->isFile) {
            $form .= Helper_Form::input(null, $data->urlPath, false, ["disabled" => "disabled"], "Διαδρομή URL");
        }
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeWrapperColumnLeft();
        $form .= Helper_Form::input("current_path", $data->path, false, ["type" => "hidden"]);
        $form .= Helper_Form::submit();
        $form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}