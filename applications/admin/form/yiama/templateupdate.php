<?php
class Form_Yiama_TemplateUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form  = Helper_Form::openForm(  Helper_Link::update( $data )  );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::title( $data->title, true );
		$form .= Helper_Form::text( $data->text );
        $form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias( $data->alias, null, 'Alias' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}