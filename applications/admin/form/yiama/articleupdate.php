<?php
class Form_Yiama_ArticleUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $tb_name = 'ym_articles';
		$form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.images'), 
                'tab3' => $t->_('tab.attributes')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm(  Helper_Link::update( $data )  );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Articlelocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::shortDescription( $data_local->short_description, null, $l );
				$form .= Helper_Form::description( $data_local->description, null, $l );
				$form .= Helper_Form::text( $data_local->text, $l );
			} else {
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::shortDescription( null, null, $l );
				$form .= Helper_Form::description( null, null, $l );
				$form .= Helper_Form::text( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();	
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();	
		$category = new Model_Yiama_Category();
		$categories = DB_ActiveRecord_Array::flatten( DB_ActiveRecord_Array::tree( $category->findAll(), 1, 'id', 'parent_id' ), 'children' );
		foreach( $categories as $c ) {
			$c->tree_disabled = ( $c->total_children ) ? true : false;
		}
		$form .= Helper_Form::selectTree( 
            $categories, 
            'cat_id', 
            $data->ym_categories_id, 
            true, 
            array( 
                'Κατηγορία', 'μόνο με κάποια που δεν έχει υποκατηγορία', 
                null, 
                null, 
                array( 
                    'Δημιουργία νέας' => Helper_Link::insertform( 
                        'yiama_categories', 
                        urlencode($request->getURI())
                    ) 
                ) 
            ) 
        );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
        $tag = new Model_Yiama_Tag();
        $tags = $tag
            ->query()
            ->join('ym_articles_tags at', 'at.ym_tags_id = ym_tags.id AND at.ym_articles_id=' . $data->id, 'inner')
            ->join('ym_tags_local tl', 'tl.ym_tags_id = ym_tags.id AND tl.ym_languages_id = ' . $language->getDefault()->id, 'inner')
            ->find();
        $tag_titles = array();
        foreach($tags as $tt) {
            $tag_titles[] = $tt->title;
        }
		$form .= Helper_Form::inputAjax(
            'tags', 
            'tags[]', 
            '/' . Core_App::getConfig('baseDir') . '/admin/yiama_tags/ajax',
            $tag_titles,
            null,
            null,
            array('Tags', $t->_('label.value_must_be_in_default_language_translate_from_administration'))
        );
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$image = new Model_Yiama_Articleimage();
		$form .= Helper_Form::images( $tb_name, $image->query()->search( "ym_articles_id = {$data->id}" )->find() );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab3' );
		$form .= Helper_Form::openWrapperModule();
		// Non multivalue attributes
		$article_attribute = new Model_Yiama_Articleattribute();
		$article_attributes = $article_attribute->query()
			->select( 'ym_articles_attributes.*, IF( attr_type = "date", DATE_FORMAT( val, \'%d/%m/%Y\'), val ) AS val' )
			->search( array( 'ym_articles_id = ' . $data->id,  ) )
			->join( 'ym_attributes AS attr', 'attr.id = ym_attributes_id AND ( attr.is_multivalue IS NULL OR attr.is_multivalue = 0 )' )
			->combine( 'attribute' )
			->find();
		foreach( $article_attributes as $val ) {
			$val->attr_title = $val->attribute->title;
			$val->attr_description = $val->attribute->description;
		}
		Helper_Catalogue::setHeaders(array(
            $t->_('header.attribute') => null, 
            $t->_('header.description') => null, 
            $t->_('header.value') => null 
        ));
		Helper_Catalogue::setContent( 'ym_articles_attributes', $article_attributes, array( array( 'text' => 'attr_title' ), array( 'text' => 'attr_description' ), array( 'text' => 'val' ) ) );
		Helper_Catalogue::setContentActions( $article_attributes, 'delete', 'yiama_articleattributes' );
		$form .= Helper_Catalogue::get();
		// Multivalue attributes
		$article_attributevalue = new Model_Yiama_Articleattributevalue();
		$article_attributevalues = $article_attributevalue->query()
			->select( 'ym_articles_attributes_values.*' )
			->search( 'ym_articles_id = ' . $data->id )
			->join( 'ym_attributes_values AS attr_val', 'attr_val.id = ym_articles_attributes_values.ym_attributes_values_id' )
			->join( 'ym_attributes AS attr', 'attr.id = attr_val.ym_attributes_id AND attr.is_multivalue = 1' )
			->combine( 'value' )
			->find();
		foreach( $article_attributevalues as $val ) {
			$val->attr_title = $val->value->attribute->title;
			$val->attr_description = $val->value->attribute->description;
			$val->attr_value = $val->value->title;
		}
		Helper_Catalogue::setHeaders(array(
            $t->_('header.attribute') => null, 
            $t->_('header.description') => null, 
            $t->_('header.value') => null 
        ));
		Helper_Catalogue::setContent(
            'ym_articles_attributes_values', 
            $article_attributevalues, 
            array( 
                array( 'text' => 'attr_title' ), 
                array( 'text' => 'attr_description' ), 
                array( 'text' => 'attr_value' )
            ) 
        );
		Helper_Catalogue::setContentActions( 
            $article_attributevalues, 
            'delete', 
            'yiama_articleattributevalues' 
        );
		$form .= Helper_Catalogue::get();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::openWrapperModule();
        // Show only attributes that are connected with article's category or 
        // with one from it's category's parents tree
        $category = new Model_Yiama_Category();
        $category_parent_ids = array($data->ym_categories_id);
        $category_parent_ids = $category->getParentIds($data->ym_categories_id);
		$attribute = new Model_Yiama_Attribute();
		// Non multivalued attributes
		$attributes = $attribute
			->query()
			->search( array(
                'is_multivalue IS NULL OR is_multivalue = 0',
                'ac.ym_categories_id IN(' . implode(',', $category_parent_ids) . ')'
            ) )
            ->join('ym_attributes_categories ac', 'ac.ym_attributes_id = ym_attributes.id', 'inner')
			->find();
		foreach( $attributes as $attr ) {
			switch( $attr->attr_type ) {
				case 'float': $form .= Helper_Form::input( "attributes[{$attr->id}]", null, null, array( 'validValues' => 'valFloat' ), array( $attr->title, $attr->description ) ); break;
				case 'boolean': $form .= Helper_Form::selectBoolean( "attributes[{$attr->id}]", null, null, array( $attr->title, $attr->description ) ); break;
				case 'date': $form .= Helper_Form::date( "attributes[{$attr->id}]", null, null, null, array( $attr->title, $attr->description ) ); break;
				default: $form .= Helper_Form::input( "attributes[{$attr->id}]", null, null, null, array( $attr->title, $attr->description ) );
			}
		}
		// Multivalued non multiselect attributes
		$attributes = $attribute
			->query()
			->search( array(
                'is_multivalue = 1 AND ( is_multiselect IS NULL OR is_multiselect = 0 )' ,
                'ac.ym_categories_id IN(' . implode(',', $category_parent_ids) . ')'
            ) )
            ->join('ym_attributes_categories ac', 'ac.ym_attributes_id = ym_attributes.id', 'inner')
			->combine( 'values' )
			->find();
		foreach( $attributes as $attr ) {
			$form .= Helper_Form::select( "multi_attributes[{$attr->id}][]", $attr->values, 'id', 'title', null, null, null, array( $attr->title, $attr->description ) );
		}
		// Multivalued multiselect attributes
		$attributes = $attribute
			->query()
			->search( array(
                'is_multivalue = 1 AND is_multiselect = 1',
                'ac.ym_categories_id IN(' . implode(',', $category_parent_ids) . ')'
            ) )
            ->join('ym_attributes_categories ac', 'ac.ym_attributes_id = ym_attributes.id', 'inner')
			->combine( 'values' )
			->find();
		foreach( $attributes as $attr ) {
			$form .= Helper_Form::select( "multi_attributes[{$attr->id}][]", $attr->values, 'id', 'title', null, null, null, array( $attr->title, $attr->description ), array( "multiple" => "multiple" ) );
		}
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule($t->_('label.meta_info'));
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Articlelocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::meta( array( 'title' => $data_local->meta_title, 'description' => $data_local->meta_description, 'keywords' => $data_local->meta_keywords ), $l );
			} else {
				$form .= Helper_Form::meta( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$image = new Model_Yiama_Articleimage();
		$form .= Helper_Form::openSubWrapper('Alias');
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Articlelocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::alias( $data_local->alias, $l );
            } else {
				$form .= Helper_Form::alias( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();
        $form .= Helper_Form::openSubWrapper();
        $form .= Helper_Form::favorite( $data->is_favorite );
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'url', $data->url, null, null, 'URL' );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}