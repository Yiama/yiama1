<?php
class Form_Yiama_AttributeValueUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t)
    {
        $tb_name = 'ym_attributes_values';
        $tab_menu = array('tab1' => $t->_('tab.basic'));
        $form = Helper_Form::tabMenu(
            $tab_menu,
            array(
                'required' => array('tab1')
            )
        );

        // Query
        $form .= Helper_Form::openForm(Helper_Link::update($data));
        $form .= Helper_Form::openWrapperColumnLeft();
        $form .= Helper_Form::openTabItem( 'tab1', true );
        $form .= Helper_Form::openWrapperModule();
        $language = new Model_Yiama_Language();
        $languages = $language->findAll();
        $form .= Helper_Form::openWrapperLocal($languages);
        $default_lang = $language->getDefault();
        foreach ($languages as $l) {
            $form .= Helper_Form::openWrapperLocalItem($l->id);
            $data_local = new Model_Yiama_Attributevaluelocal();
            if (($data_local = $data_local->findByKey(array($data->id, $l->id)))) {
                $form .= Helper_Form::title($data_local->title, $l->id == $language->getDefault()->id ? true : null, $l);
            } else {
                $form .= Helper_Form::title(null, $l->id == $language->getDefault()->id ? true : null, $l);
            }
            $form .= Helper_Form::closeWrapperLocalItem();
        }
        $form .= Helper_Form::closeWrapperLocal();
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeTabItem();
        $form .= Helper_Form::closeWrapperColumnLeft();
        $form .= Helper_Form::openWrapperColumnRight();
        $form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::image($tb_name, $data->image);
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeWrapperColumnRight();
        $form .= Helper_Form::hiddenReturn();
        $form .= Helper_Form::submit();
        $form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}