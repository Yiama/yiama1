<?php
class Form_Yiama_PageInsert implements Form_Interface{
    
    private $form;

    public function __construct($request, $t) {
        $form  = Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$data_model = new Model_Yiama_Page();
        $data = $data_model
            ->query()
            ->search('is_admin IS NULL OR is_admin <> 1')
            ->find();
		$pages = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree( $data, 0, 'id', 'parent_id' ),
            'children' 
        );
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		$default_lang = $language->getDefault();
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'url', null, null, null, 'URL' );
		$form .= Helper_Form::textarea( 'module', null, null, null, 'Module' );
		$type = new Model_Yiama_Pagetype();
        $types = $type  
            ->query()
            ->search('is_admin IS NULL OR is_admin <> 1')
            ->find();
		$form .= Helper_Form::select( 
            'type_id', 
            $types, 
            'id', 
            'title', 
            null, 
            null, 
            true, 
            array($t->_('label.type'), null, null, null, array( 
                $t->_('label.create_new') => Helper_Link::insertform( 
                    'yiama_pagetypes', urlencode( $request->getURI()))
                )
            )
        );
		$form .= Helper_Form::selectTree( 
            $pages, 
            'parent_id', 
            null, true, 
            array($t->_('label.superpage'), null, null, null, array( 
                $t->_('label.create_new') => Helper_Link::insertform( 
                    'yiama_pages', urlencode($request->getURI()) 
                    ) 
                ) 
            ) 
        );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias(null, null, true);
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish();
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;		
    }
    
    public function render() {
        return $this->form;
    }
}