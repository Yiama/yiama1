<?php
class Form_Yiama_AttributeUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $tb_name = 'ym_attributes';
        $tab_menu = array('tab1' => $t->_('tab.basic'));
        if( $data->is_multivalue ) {
            $tab_menu['tab2'] = $t->_('tab.values');
        }
		$form = Helper_Form::tabMenu(
            $tab_menu, 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		$default_lang = $language->getDefault();
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
				$data_local = new Model_Yiama_Attributelocal();
				if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
					$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
				} else {
					$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::input( 'description', $data->description, true, null, $t->_('label.short_description') );
		if( $data->is_multivalue ) {
            $form .= Helper_Form::checkbox( 'is_multiselect', $data->is_multiselect, false, null, $t->_('label.multiple_values_can_be_selected') );
        }
		$form .= Helper_Form::select( 'type', array( 
            ( object ) array( 'value' => 'string', 'title' => $t->_('label.simple_value') ), 
            ( object ) array( 'value' => 'float', 'title' => $t->_('label.decimal') ), 
            ( object ) array( 'value' => 'date', 'title' => $t->_('label.date') ), 
            ( object ) array( 'value' => 'boolean', 'title' => $t->_('label.logical_value') ) 
        ), 'value', 'title', 'value', $data->attr_type, true, $t->_('label.type') );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::openSubWrapper();
		$category = new Model_Yiama_Category();
		$categories = DB_ActiveRecord_Array::flatten( DB_ActiveRecord_Array::tree( $category->findAll(), 1, 'id', 'parent_id' ), 'children' );
		$attribute_category = new Model_Yiama_Attributecategory();
        $attribute_category_ids = $attribute_category
            ->query()
            ->search('ym_attributes_id=' . $data->id)
            ->find('ym_categories_id');
        $form .= Helper_Form::selectTree( 
            $categories, 
            'cat_ids[]', 
            $attribute_category_ids, 
            true, 
            array( $t->_('label.category'), 
            null, 
            null, 
            null, 
            array($t->_('label.create_new') => Helper_Link::insertform('yiama_categories', urlencode($request->getURI())))), 
            array('multiple' => 'multiple') 
        );
        $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		if( $data->is_multivalue ) {
            $form .= Helper_Form::openTabItem( 'tab2' );
            $form .= Helper_Form::openWrapperModule($t->_('label.attribute_insert_values_info'));
            $value = new Model_Yiama_Attributevalue();
            $values = $value->query()
                ->select( 'ym_attributes_values.*, local.*, '
                    . 'IF( COUNT( pfv.ym_attributes_values_id ), 0, 1 ) as allow_deletion' )
                ->join( 'ym_articles_attributes_values AS pfv', 
                        'pfv.ym_attributes_values_id = ym_attributes_values.id', 
                        'LEFT' )
                ->search( "ym_attributes_id = {$data->id}" )
                ->group( 'ym_attributes_values.id' )
                ->find();
            Helper_Catalogue::setHeaders( array( $t->_('header.title') => 'title' ) );
            Helper_Catalogue::setContent( 'ym_attributes_values', $values, array( array( 'text' => 'title' ) ) );
            Helper_Catalogue::setContentActions( $values, array( 'update', 'delete' ), 'yiama_attributevalues' );
            $form .= Helper_Catalogue::get();
            $form .= Helper_Form::addButton( Helper_Form::input( 'values_titles[]' ) );
            $form .= Helper_Form::closeWrapperModule();
            $form .= Helper_Form::closeTabItem();
        }
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::alias($data->alias, null, true);
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}