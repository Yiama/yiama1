<?php
class Form_Yiama_UserUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.informations'), 
                'tab2' => $t->_('tab.roles')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'username', $data->username, true, null, 'Username' );
		$form .= Helper_Form::input( 'password', null, null, array( 'placeHolder' => '************' ), 'Password' );
		$form .= Helper_Form::input( 'email', $data->email, true, array( 'validValues' => 'valEmail' ), 'Email' );
		if (!empty($data->image)) {
            $form .= Helper_Form::element(
                HTML_Tag::open('image', array(
                    'src' => PATH_REL_IMAGES . 'db' . DS . 'ym_users' . DS . $data->image,
                    'class' => 'user-image'
                )) . '<br />'
              . Helper_Form::checkbox('delete_image', null, false, null, $t->_('label.image_deletion'))
            );
        } else {
            $form .= Helper_Form::element(Helper_Form::imageButton('jpg,jpeg,png'));
        }
        $form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$userrole = new Model_Yiama_Userrole();
		$userroles = $userrole->query()
			->select( "ym_users_roles.*, r.title, CONCAT(" . $data->id . ", '.', r.id) id" )
			->join( 'ym_roles AS r', 'r.id = ym_users_roles.ym_roles_id' )
			->search( "ym_users_roles.ym_users_id = {$data->id}" )
			->find();
		Helper_Catalogue::setHeaders( array( $t->_('role') => 'title' ) );
		Helper_Catalogue::setContent( 'ym_users_roles', $userroles, array( array( 'text' => 'title' ) ) );
		Helper_Catalogue::setContentActions( $userroles, 'delete', 'yiama_userroles' );
		$form .= Helper_Catalogue::get();
		$role = new Model_Yiama_Role();
		$form .= Helper_Form::addButton( Helper_Form::select( 'roles[]', $role->findAll(), 'id', 'title', null, null, true ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}