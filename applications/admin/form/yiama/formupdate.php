<?php
class Form_Yiama_FormUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => 'Editor'
            ), 
            array(
                'required' => array('tab1')
            )
        );
        $lang = new Model_Yiama_Language();
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
        $form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Formlocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::description( $data_local->description, null, $l );
				$form .= Helper_Form::text( $data_local->text, $l );
			} else {
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
				$form .= Helper_Form::description( null, null, $l );
				$form .= Helper_Form::text( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();	
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
        $form .= Helper_Form::input('alias', $data->alias, null, null, 'Alias');
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
        Core_HTML::addFiles(array(
            'library/basic.js',
            'admin/formbuilder/builder.js',
            'admin/formbuilder/elements.js'
        ));
        ob_start(); 
        ?>
		<div class="form-builder-wrapper">
            <div class="top">
                <div id="formBuilderPanelButtons"></div>
                <div id="formBuilderPanelEdit"></div>
            </div>
        </div>
        <?php
        $form .= ob_get_clean();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule();
        $form .= "<div id='formBuilderPanelPreview'></div>";
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
        $form .= HTML_Tag::void("input", array(
            "type" => "hidden", 
            "name" => "form_json",
            "id" => "formJsonInput"
        ));
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        ob_start(); 
        ?>
        <script type="text/javascript">
            var formBuilder = new ymFormBuilder(
                ymFormBuilderJsonGroups,
                <?php echo !empty($data->json) ? $data->json : "''"; ?>,
                document.getElementById("formBuilderPanelButtons"),
                document.getElementById("formBuilderPanelEdit"),
                document.getElementById("formBuilderPanelPreview")
            );
        </script>;
        <?php
        $form .= ob_get_clean();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}