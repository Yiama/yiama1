<?php
class Form_Yiama_LanguageUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form  = Helper_Form::openForm(  Helper_Link::update( $data )  );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 
            'code', 
            $data->code, 
            true, 
            array( 'valueLength' => 'exact:2' ), 
            $t->_('label.shortcode')
        );
		$form .= Helper_Form::title( $data->title, true );
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}