<?php
class Form_Yiama_RoleUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::tabMenu(
            array( 
                'tab1' => $t->_('tab.basic'), 
                'tab2' => $t->_('tab.permissions')
            ), 
            array(
                'required' => array('tab1')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openTabItem( 'tab1', true );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'title', $data->title, true, null, $t->_('label.title') );
		$form .= Helper_Form::input( 'description', $data->description, true, null, $t->_('label.description') );
        $role = new Model_Yiama_Role();
		$form .= Helper_Form::select( 
            'parent_id', 
            $role->findAll(), 
            'id',
            'title',
            'id', 
            $data->parent_id, 
            true, 
            $t->_('label.superrole') 
        );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::openTabItem( 'tab2' );
		$form .= Helper_Form::openWrapperModule();
		$rolepermission = new Model_Yiama_Rolepermission();
		$rolepermissions = $rolepermission->query()
			->select( "ym_roles_permissions.*, p.title, p.description, "
                    . "IF(state = -1, '" . $t->_('label.restricted')
                    . "', (IF(state = 1, '" . $t->_('label.allowed') 
                    . "', '" . $t->_('label.inherited') . "'))) state")
			->join( 'ym_permissions AS p', 'p.id = ym_roles_permissions.ym_permissions_id' )
			->search( "ym_roles_permissions.ym_roles_id = {$data->id}" )
			->find();
		Helper_Catalogue::setHeaders(array( 
            $t->_('label.permission') => 'title',
            $t->_('label.description') => 'description', 
            $t->_('label.state') => 'state'  
        ));
		Helper_Catalogue::setContent(
            'ym_roles_permissions', 
            $rolepermissions, 
            array( 
                array('text' => 'title'),
                array('text' => 'description'),
                array('text' => 'state')  
            ) 
        );
		Helper_Catalogue::setContentActions( 
            $rolepermissions, 
            array('update', 'delete'), 
            'yiama_rolepermissions' 
        );
		$form .= Helper_Catalogue::get();
		$permission = new Model_Yiama_Permission();
		$form .= Helper_Form::addButton( 
            Helper_Form::select( 
                'permissions[]', 
                $permission->findAll(), 
                'id', 
                'title', 
                null, 
                null, 
                true,
                $t->_('label.permission')
            ) .
            Helper_Form::select( 
                'permission_states[]', 
                array(
                    (object)array('id' => -1, 'title' => $t->_('label.restricted')),
                    (object)array('id' => 1, 'title' => $t->_('label.allowed')),
                    (object)array('id' => 0, 'title' => $t->_('label.inherited'))
                ), 
                'id', 
                'title', 
                null, 
                null, 
                true,
                $t->_('label.state') 
            )  
        );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}