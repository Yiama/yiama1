<?php
class Form_Yiama_TagUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t) {
        $form = Helper_Form::openForm( Helper_Link::update( $data ) );
        $form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$language = new Model_Yiama_Language();
		$languages = $language->findAll();
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Taglocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::title( $data_local->title, $l->id == $language->getDefault()->id ? true : null, $l );
			} else {
				$form .= Helper_Form::title( null, $l->id == $language->getDefault()->id ? true : null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();	
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::openWrapperColumnRight();
		$form .= Helper_Form::openWrapperModule('Alias');
		$form .= Helper_Form::openWrapperLocal( $languages );
		foreach( $languages as $l ) {
			$form .= Helper_Form::openWrapperLocalItem( $l->id );
			$data_local = new Model_Yiama_Taglocal();
			if( ( $data_local = $data_local->findByKey( array( $data->id, $l->id ) ) ) ) {
				$form .= Helper_Form::alias( $data_local->alias, $l );
            } else {
				$form .= Helper_Form::alias( null, $l );
			}
			$form .= Helper_Form::closeWrapperLocalItem();
		}
		$form .= Helper_Form::closeWrapperLocal();
		$form .= Helper_Form::openSubWrapper();
		$form .= Helper_Form::publish( $data->is_published );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnRight();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}