<?php
class Form_Yiama_FileInsert implements Form_Interface{
    
    private $form;

    public function __construct($t) {
        $directories = Controller_Yiama_Files::directories();

        $form = Helper_Form::tabMenu(
            array(
                'tab1' => $t->_('tab.file'),
                'tab2' => $t->_('tab.directory')
            )
        );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
        $form .= Helper_Form::openTabItem( 'tab1', true );
        $form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::openSubWrapper();
        $form .= Helper_Form::select("file_directory", $directories, "value", "text",  null, null, false, "Φάκελος");
        $form .= Helper_Form::file("file", null, false, null,  "Αρχείο");
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeTabItem();
        $form .= Helper_Form::openTabItem( 'tab2', true );
        $form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::openSubWrapper();
        $form .= Helper_Form::select("parent_directory", $directories, "value", "text",  null, null, false, "Υπερφάκελος");
        $form .= Helper_Form::input("dirname", "", false, null, "Ονομασία");
        $form .= Helper_Form::closeSubWrapper();
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeTabItem();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}