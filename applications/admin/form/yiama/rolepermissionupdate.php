<?php
class Form_Yiama_RolePermissionUpdate implements Form_Interface{
    
    private $form;

    public function __construct($data, $request, $t)
    {
        $tb_name = 'ym_roles_permissions';
        $tab_menu = array('tab1' => $t->_('tab.basic'));
        $form = Helper_Form::tabMenu(
            $tab_menu,
            array(
                'required' => array('tab1')
            )
        );

        // Query
        $params = $request->getParams();
        $form_link = $request->url(
            'cont_act_id',
            array(
                'controller' => 'yiama_rolepermissions',
                'action' => 'update'
            ),
            array(
                'ym_roles_id' => $params['ym_roles_id'],
                'ym_permissions_id' => $params['ym_permissions_id']
            )
        );
        $form .= Helper_Form::openForm($form_link);
        $form .= Helper_Form::openWrapperColumnLeft();
        $form .= Helper_Form::openTabItem( 'tab1', true );
        $form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::select(
            'state',
            array(
                (object)array('id' => -1, 'title' => $t->_('restricted')),
                (object)array('id' => 1, 'title' => $t->_('allowed')),
                (object)array('id' => 0, 'title' => $t->_('inherited'))
            ),
            'id',
            'title',
            'id',
            intval($data->state),
            true,
            $t->_('state')
        );
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeWrapperModule();
        $form .= Helper_Form::closeTabItem();
        $form .= Helper_Form::closeWrapperColumnLeft();
        $form .= Helper_Form::submit();
        $form .= Helper_Form::input('ym_roles_id', $params['ym_roles_id'], null, array('type' => 'hidden'));
        $form .= Helper_Form::input('ym_permissions_id', $params['ym_permissions_id'], null, array('type' => 'hidden'));
        $form .= Helper_Form::hiddenReturn($params['return_url']);
        $form .= Helper_Form::closeForm();
        $this->form = $form;
    }
    
    public function render() {
        return $this->form;
    }
}