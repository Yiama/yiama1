<?php
abstract class Module_Abstract extends Core_ModuleAbstract{
    
    protected function checkCsrf(array $params = array()) {
        $csrf = new Security_Csrf(Core_Session::getInstance());
        if (    empty($params[$csrf->getRequestTokenName()])
            ||  !$csrf->isValid($params[$csrf->getRequestTokenName()])) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'error',
                        'action' => 'notfound' 
                    )
                )
            );
        }
    }
    
    abstract public function save();
}