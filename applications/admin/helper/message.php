<?php
class Helper_Message {
    
    public function __construct($session) {
        $this->session = $session;
    }
    
    public function store($messages) {
        $msgs = array();
        $msgs['success'] = empty($messages['error'])
            ? implode(',', (array)$messages[0]) 
            : null;
        $msgs['error'] = !empty($messages['error'])
            ? implode(',', (array) $messages['error'])
            : null;
        $msgs['warning'] = !empty($messages['warning'])
            ? implode(',', (array)$messages['warning'])
            : null;
        $this->session->set('system_messages', $msgs);
    }
    
	public function get(\Yiama\I18n\Translator $t) {
        
        // Get messages from Session
        $messages = array_filter((array)$this->session->get('system_messages'));
        if (empty($messages)) {
            return;
        }
        $this->session->remove('system_messages');
        
        Core_HTML::addJs( "
            var systemMessage_timeout;
            function systemMessage( type, text )
            {
                clearTimeout( systemMessage_timeout );
                removeSystemMessageItems();
                var msgEl = _get( 'systemMessage' );
                var item = document.createElement( 'div' );
                item.innerHTML = text;
                item.setAttribute( 'class', 'item ' + type );
                msgEl.appendChild( item );
                _show( msgEl );
                systemMessage_timeout = setTimeout( function(){ removeSystemMessageItems() }, 5000 );
            }
            function removeSystemMessageItems()
            {
                var msgEl = _get( 'systemMessage' );
                var items = msgEl.getElementsByTagName( 'div' );
                if( items ) {
                    for( var i = 0; i < items.length; i++ ) {
                        msgEl.removeChild( items[i] );
                    }
                    _hide( msgEl );
                }
            }
        " );
		
		/* URL Request */
		Core_HTML::addFiles('admin/systemmessage.css');
		$msg_types = array('success', 'error', 'warning');
		$items = array();
		foreach($msg_types as $val) {
			if(!empty($messages[$val])) {
                foreach(explode( ',', $messages[$val] )as $m) {
                    $items[] = HTML_Tag::closed('div', array('class' => 'item '.$val),$t->_($val.'.'.$m));
                }
			}
		}
		$s = "<script type='text/javascript'>"
            . "window.addEventListener('load', function() {"
                . "window.systemMessage_timeout = setTimeout( function(){ removeSystemMessageItems() }, 5000 );"
            . "});"
            . "</script>";
		return $s . HTML_Tag::closed( 'div', array( 
				'class' => 'system-message',
				'id' => 'systemMessage'
			), implode( '', $items ) 
		);
	}
}