<?php
	class Helper_Stats
	{
		private static $initialized = false;
		
		public static function options( $form_action, $selects )
		{
			Core_HTML::addFiles( array( 
				'admin\stats_options.css',
				'admin\button.css' 
			) );
			$s = '';
			$s .= Helper_Form::openForm( $form_action );
			$s .= HTML_Tag::open( 'div', array( 'class' => 'stats_options' ) );	
			foreach( $selects as $sel ) {
				$s .= $sel;
			}
			$s .= Helper_Form::submit();
			$b = HTML_Tag::closed( 'a', array( 
					'class' => 'button button-blue right', 
					'id' => 'main_form_right_top_button', 
					'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
				), '<span>Αποστολή</span>' 
			);
			$s .= HTML_Tag::closed( 'div', array( 'class' => 'element' ), $b );
			$s .= HTML_Tag::close( 'div' );
			$s .= Helper_Form::closeForm();
			return $s;
		}
		
		public static function chart( $data, $type, $id, $title = null, $c_options = null, $canvas_attrs = null, $wrapper_attrs = null )
		{
			if( ! self::$initialized ) {
				self::$initialized = true;
				Core_HTML::addFiles( array(
					'admin\statistics.css',
					'library\chart\Chart.js',
					'library\chart\yiama_chart.js'
				) );
			}
		
			$js = "chartJsMakeColors( 10 );";
			$js .= "var data_{$id} = {};";
			$js .= "data_{$id}.labels = [ '" . implode( "','", $data['labels'] ) . "' ];";
			$js .= "data_{$id}.datasets = [ ";
			if( ! empty( $c_options['is_multichart'] ) && $c_options['is_multichart'] === true ) { /* Multichart */
				$sets = array();
				foreach( $data['sets'] as $key => $set ) {
					$sets[] = "{
						fillColor: chartJsGetColor( {$key} ),
			            strokeColor: '#334455',
			            pointColor: chartJsGetColor( {$key} ),
			            pointStrokeColor: '#fff',
			            pointHighlightFill: '#fff',
			            pointHighlightStroke: '#223344',
			            data: [ " . implode( ',', $set ) . " ]
					}"; 
				}
				$js .= implode( ',', $sets );
			} else {
				$js .= "{
						fillColor: '#8899aa',
			            strokeColor: '#334455',
			            pointColor: '#445566',
			            pointStrokeColor: '#fff',
			            pointHighlightFill: '#fff',
			            pointHighlightStroke: '#223344',
			            data: [ " . implode( ',', $data['sets'] ) . " ]
					}";
			}
			$js .=  " ];";
			if( $type == 'line' ) {
				$chart_options = array_merge( array(
						'scaleBeginAtZero' => true,
						'bezierCurve' => true,
						'datasetFill' => true
					), ( array ) $c_options 
				);	
				$js .= "var chart_{$id}_options = " . json_encode( $chart_options ) . ";";
				$js .= "_addOnLoad( 'line_chart( \'{$id}\', data_{$id}, chart_{$id}_options )' );";
			} elseif ( $type == 'bar' ) {
				$chart_options = array_merge( array(
						'barShowStroke' => false
					), ( array ) $c_options 
				);	
				$js .= "var chart_{$id}_options = " . json_encode( $chart_options ) . ";";
				$js .= "_addOnLoad( 'bar_chart( \'{$id}\', data_{$id}, chart_{$id}_options )' );";
			}
			Core_HTML::addJs( $js ); 
			
			$s = HTML_Tag::open( 'div', array_merge( array( 'class' => 'chart' ), ( array ) $wrapper_attrs ) );
			$s .= "<div class='header'>{$title}</div>";
			$s .= HTML_Tag::closed( 'canvas', array_merge( array( 'id' => $id ), ( array ) $canvas_attrs ) );
			$s .= HTML_Tag::close( 'div' );
			return $s;
		}
	}
?>