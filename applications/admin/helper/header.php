<?php
	class Helper_Header
	{
		public static function get( $title_appendix = null, $buttons_right = null, $buttons_left = null )
		{
			Core_HTML::addFiles( 'admin\button.css' );
			
			$page = new Model_Yiama_Page();
			$request = Core_Request::getInstance();
			$action = $request->getAction();
			if( $action == 'insertform' ) {
				$title_appendix = ' Προσθήκη νέου ' . $title_appendix;
			}
			$page = $page->getCurrent( 'admin' );
			$page_parents = $page->getParents();
			$page_nav_items = array();
			foreach( array_reverse( $page_parents ) as $key => $parent ) {
				$page_nav_items[] = $key && $parent->url ? "<a href='" . Core_Request::getInstance()->urlFromPath( $parent->url ) . "'><i>{$parent->title}</i></a>" : $parent->title;
			}
			$page_nav_items[] = $page->title;
			$page_nav = implode( ' / ', $page_nav_items );
			$s = HTML_Tag::closed( 'div', array( 'class' => 'title' ), $page_nav . ( ! empty( $title_appendix ) ? ' / ' . implode( ' <small>/</small> ', ( array ) $title_appendix ) : '' ) );
			$b = '';
			if( $buttons_right ) {
				foreach( $buttons_right as $key => $link ) {
					if( $key == 'add' ) {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button button-blue right', 
								'href' => $link 
							), '<span>Προσθήκη</span>' 
						);
					} elseif ( $key == 'save' ) {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button button-blue right', 
								'id' => 'main_form_right_top_button', 
								'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
							), '<span>Αποθήκευση</span>' 
						);
                    } else {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button button-blue right', 
								'href' => $link 
							), '<span>' . $key . '</span>' 
						);
					}
				}
			}
			if( $buttons_left ) {
				foreach( $buttons_left as $label => $link ) {
					if( $label == 'download' ) {
						$b .= HTML_Tag::closed( 'a', array(
								'class' => 'button button-silver left fonts',
								'onclick' => "document.getElementById( 'main_form_submit' ).click();",
								'title' => 'Κατέβασμα αρχείου'
							), '<span>&#xf143;</span>' 
						);
                    } elseif( $label == 'upload' ) {
						$b .= HTML_Tag::closed( 'a', array(
								'class' => 'button button-silver left fonts',
								'onclick' => "document.getElementById( 'main_form_submit' ).click();",
								'title' => 'Ανέβασμα αρχείου'
							), '<span>&#xf20c;</span>' 
						);
                    } elseif( $label == 'exportform' ) {
						 $b .= HTML_Tag::closed( 'a', array( 
                                'href' => $link,
								'class' => 'button button-silver left fonts',
								'title' => 'Εξαγωγή σε αρχείο'
							), '<span>&#xf185;</span>'
						);
					} elseif( $label == 'importform' ) {
						$b .= HTML_Tag::closed( 'a', array(
                                'href' => $link,
								'class' => 'button button-silver left fonts',
								'title' => 'Εισαγωγή από αρχείο'
							), '<span>&#xf181;</span>' 
						);
					} elseif( $label == 'mergeform' ) {
						$b .= HTML_Tag::closed( 'a', array( 
                                'href' => $link,
								'class' => 'button button-silver left fonts',
								'title' => 'Εισαγωγή από επεξεργασμένο αρχείο'
							), '<span>&#xf184;</span>' 
						);
					} elseif ( $label == 'statistics' ) {
						 $b .= HTML_Tag::closed( 'a', array( 
						 		'href' => $link, 
								'class' => 'button button-silver left fonts',
								'title' => 'Στατιστικά'
							), '<span>&#xf153;</span>'
						);
					} else {
						$b .= HTML_Tag::closed( 'a', array( 'href' => $link, 'class' => 'button button-silver left' ), '<span>' . $label . '</span>' );
					}
				}
			}
			$s .= HTML_Tag::closed( 'div', array( 'class' => 'buttons' ), $b );
			return HTML_Tag::closed( 'div', array( 'class' => 'main_header' ), $s );
		}
	}
?>