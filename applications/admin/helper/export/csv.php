<?php
class Helper_Export_Csv
{
    public static function export( $options )
    {
        $response = Core_Response::getInstance();
        $response->setHeaders( array(
            "Content-Type: application/csv;charset=UTF-8",
            "Content-Disposition: attachment; filename={$options['filename']}.csv",
            "Pragma: no-cache",
            "Expires: 0"
        ) );
        $nl = "\r\n";
        $str = self::getCsvLine( array_values( $options['fields'] ) ) . $nl;
        $fields = array_keys( $options['fields'] );
        $rows = array();
        foreach( $options['content'] as $cont ) {
            $row = array();
            foreach( $fields as $f ) {
                $row[] = self::getCsvCell( $cont, $f );
            }
            $rows[] = self::getCsvLine( $row );
        }
        $str .= implode( $nl, $rows );
        $response->setBody( $str );
    }

    private static function getCsvCell( $row, $field )
    {
        $result = str_replace( array( ',', ' ,', ', ', ' , ' ), ' | ', is_object( $row ) ? $row->$field : $row[ $field ] );
        $result = str_replace( array( "\n", "\r\n", "\r" ), '', $result );
        return $result;
    }

    private static function getCsvLine( $ar )
    {
        $sep = ",";
        $row = array();
        foreach( $ar as $a ) {
            $row[] = str_replace( '"', "'",  $a );
        }
        return implode( $sep, $row );
    }
}