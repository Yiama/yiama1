<?php
	class Helper_Redirect
	{
		/*
		 * @param $url string
		 * @param $query array
		 * @param $messages array { array( 0 => <success>, 1 => <error>, 2 => <warning> }
		 */
		public static function send( $url, $query = null, $msgs = null )
		{
			$params = Core_Request::getInstance()->getParams();
			$messages = Core_Translate::get( 'el', 'admin.error', 'messages' );
			if( empty( $url ) ) { /* Παίρνουμε το προεπιλεγμένο Url από το config */
				$url = Core_Request::getInstance()->urlFromPath( 
					 	Core_App::getConfig( 'applications.admin.defaultpage' ) 
				);
			}
			$message = array();
			$message['success'] = empty( $msgs[1] ) ? implode( ',', ( array ) $msgs[0] ) : null;
			$message['error'] = ! empty( $msgs[1] ) ? implode( ',', ( array ) $msgs[1] ) : null;
			$message['warning'] = ! empty( $msgs[2] ) ? implode( ',', ( array ) $msgs[2] ) : null;
			$url = Core_Url::addQuery( urldecode( $url ), array_merge( ( array ) $query, array( 'msg' => $message ) ) );
			
			Core_Response::getInstance()->redirect( $url );
		}
	}
?>