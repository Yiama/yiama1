<?php
	class Helper_Catalogue
	{
		private static $bulk_actions;
		private static $headers;
		private static $search;
		private static $main_search;
		private static $content;
		private static $content_actions;
		
		public static function init()
		{
			Core_HTML::addFiles( array( 
				'admin\catalogue.css',
				'admin\button.css'
			) );
			Helper_Popup::confirm( 'catalogueConfirm' );
			Helper_Popup::image( 'catalogueImage' );
		}
		
		/*
		 * @param $headers array( <title> => <order_field> )
		 */
		public static function setHeaders( $headers )
		{
			self::init();
			$order = Core_Request::getInstance()->getParams( 'order' );
			$request_uri = Core_Request::getInstance()->getURI();
			$h = array();
			$ordered = ! empty( $order ) ? $order : array("", "");
			foreach( $headers as $key => $val ) {
				if( $val ) {
					$order = array(
						$val,
						$ordered[0] == $val && $ordered[1] == 'ASC' ? 'DESC' : 'ASC'
					);
					$h[] = HTML_Tag::closed( 'a', array(
							'href' => Core_URL::addQuery( $request_uri, array( 'order' => $order, 'msg' => null ) ),
							'class' => $ordered[0] == $val 
                                ? 'header selected' . ($ordered[1] == 'ASC' ? ' up' : ' down')
                                : 'header'
						), $key
					);
				} else {
					$h[] = HTML_Tag::closed( 'a', array(
							'class' => 'header nonselectable'
						), $key
					);
				}
			}
			self::$headers = $h;
		}
		
		/*
		 * @param @params ακολουθεί την αντιστοιχία του index της $actions
		 */
		public static function setBulkActions( $tb_name, $actions, $params = null )
		{
			Core_HTML::addJs( "
				function checkAll( elClicked ){
					var inputs = document.getElementsByName( 'checkboxid' );
					for( var i = 0; i < inputs.length; i++ ) {
						if( elClicked.checked == true ) {
							inputs[i].checked = true; 
						} else { 
							inputs[i].checked = false; 
						}
					}
				}
				function selectChecked( action ) {
					var ids = new Array();
					var inputs = document.getElementsByName( 'checkboxid' );
					for( var i = 0; i < inputs.length; i++ ) {
						if( inputs[i].checked == true 
						&& ! ( action == 'delete' 
							&& typeof inputs[i].getAttribute( 'data-allow_deletion' ) !== 'undefined'
							&& inputs[i].getAttribute( 'data-allow_deletion' ) === '' ) ) {
								ids.push( inputs[i].getAttribute( 'id' ) );
						}
					}
					return ids.join(); 
				}
			" );
			$s = HTML_Tag::closed( 'option', null, 'Ενέργειες επιλεγμένων...');
			foreach( ( array ) $actions as $key => $act ) {
				if($act == 'publish') {
					$s .= HTML_Tag::closed( 
                        'option', 
                        array('url' => Helper_Link::bulkpublish($tb_name, true)), 
                        'Ενεργοποίηση'
					);
					$s .= HTML_Tag::closed( 
                        'option', 
                        array('url' => Helper_Link::bulkpublish($tb_name, false)), 
                        'Απενεργοποίηση'
					);
				}
				elseif ($act == 'update') {
					$s .= HTML_Tag::closed( 
                        'option', 
                        array('url' => Helper_Link::bulkupdateform($params[$key])), 
                        'Ενημέρωση'
					);
				}
				elseif ($act == 'delete') {
					$s .= HTML_Tag::closed(
                        'option', 
                        array(
                            'url' => Helper_Link::bulkdelete($tb_name),
                            'action' => 'delete'
                        ), 
                        'Διαγραφή'
                    );
				}
			}
			self::$bulk_actions = $s;
		}
		
		/*
		 * @param $search array(<array(text=>property)|array(value=>property)|array(date=>property)|array(select=>array(select_name{object_property},query_name,options_content_object,text)))
		 */
		public static function setSearch( $search )
		{
            /*
			Core_HTML::addFiles( array(
				'library\dhtmlxCalendar\codebase\dhtmlxcalendar.js',
				'library\dhtmlxCalendar\yiama_dhtmlxCalendar.js',
				'js\library\dhtmlxcalendar.css'
			) );
             */
			$param_search = array_merge(
                (array)Core_Request::getInstance()->getParams( 'search' ),
                (array)Core_Session::getInstance()->get('search')
            );
			$search_cols = array();
			$calendar_items = array();
			foreach ($search as $ar) {
                // Main
				if (key($ar) === 'main') {
                    self::$main_search = Helper_Form::element(
                        HTML_Tag::void(
                            'input', 
                            array(
                                'name' => 'search[' . $ar[key($ar)][0] . '][like]',
                                'value' => $ar[key($ar)][1],
                                'placeholder' => $ar[key($ar)][2],
                                'onkeypress' => 'catalogueSearch(event)'
                            )
                        ),
                        array('class' => 'catalogue-main-search')
                    );
                } // Select
				elseif (key($ar) === 'select') {
                    $search_cols[] = Helper_Form::select( 
                        $ar[key($ar)][0],
                        $ar[key($ar)][1],
                        $ar[key($ar)][2],
                        $ar[key($ar)][3],
                        $ar[key($ar)][4],
                        $ar[key($ar)][5],
                        false,
                        $ar[key($ar)][6],
                        array('onchange' => 'catalogueSearch(event)') 
                    );		
				} // Select tree
                elseif (key($ar) === 'select-tree') {
                    $search_cols[] = Helper_Form::selectTree(
                        $ar[key($ar)][0], 
                        $ar[key($ar)][1], 
                        $ar[key($ar)][2], 
                        false,
                        $ar[key($ar)][3],
                        array('onchange' => 'catalogueSearch(event)')
                    );
                }
                // Publish
                elseif (key($ar) === 'publish') {
                    $search_cols[] = Helper_Form::select( 
						'search[' . $ar[key($ar)][0] . ']', 
						array(
                            (object)array('text' => 'Ενεργά', 'value' => 1), 
                            (object)array('text' => 'Ανενεργά', 'value' => 0) 
                        ),
                        'value', 
						'text', 
						'value', 
						$ar[key($ar)][1] !== '' && $ar[key($ar)][1] !== null ? (int)$ar[key($ar)][1] : null,
                        null,
                        $ar[key($ar)][2],
                        array('onchange' => 'catalogueSearch(event)')
					);
				}
                // Favorite
                elseif (key($ar) === 'favorite') {
                    $search_cols[] = Helper_Form::select(
                        'search[' . $ar[key($ar)][0] . ']',
                        array(
                            (object)array('text' => 'Ναι', 'value' => 1),
                            (object)array('text' => 'Όχι', 'value' => 0)
                        ),
                        'value',
                        'text',
                        'value',
                        $ar[key($ar)][1] !== '' && $ar[key($ar)][1] !== null ? (int)$ar[key($ar)][1] : null,
                        null,
                        $ar[key($ar)][2],
                        array('onchange' => 'catalogueSearch(event)')
                    );
                }
                /* 
                elseif ( key( $ar ) === 'boolean' ) {
					$search_cols[] = Helper_Form::select( 
						'search[' . $ar[ key( $ar ) ] . ']', 
						array( ( object ) array( 'text' => 'Ναι', 'value' => '1' ), ( object ) array( 'text' => 'Όχι', 'value' => '0' ) ), 
						'value', 
						'text', 
						'value', 
						Core_Request::getInstance()->getParams( 'search', '[' . $ar[ key( $ar ) ] . ']' ) 
					);
				} elseif ( key( $ar ) === 'value' ) {
					$val = $ar[ key( $ar ) ];
					$from = 'από: ' . HTML_Tag::closed( 'input', array( 
						'name' => "search[{$val}][from]",
						'class' => 'small',
						'value' => isset( $param_search[ $val ]['from'] ) ? $param_search[ $val ]['from'] : ''
					) );
					$to = 'έως: ' . HTML_Tag::closed( 'input', array( 
						'name' => "search[{$val}][to]",
						'class' => 'small',
						'value' => isset( $param_search[ $val ]['to'] ) ? $param_search[ $val ]['to'] : ''
					) );
					$search_cols[] = $from . '<br/>' . $to;
				} elseif ( key( $ar ) === 'date' ) {
					$val = $ar[ key( $ar ) ];
					$calendar_item_name = 'calendar_item_' . $val;
					$from = 'από: ' . HTML_Tag::closed( 'input', array( 
						'name' => "search[{$val}][date_from]",
						'id' => "{$calendar_item_name}_from",
						'class' => 'small',
						'value' => isset( $param_search[ $val ]['date_from'] ) ? $param_search[ $val ]['date_from'] : ''
					) );
					$to = 'έως: ' . HTML_Tag::closed( 'input', array( 
						'name' => "search[{$val}][date_to]",
						'id' => "{$calendar_item_name}_to",
						'class' => 'small',
						'value' => isset( $param_search[ $val ]['date_to'] ) ? $param_search[ $val ]['date_to'] : ''
					) );
					$search_cols[] = $from . '<br/>' . $to;
					$calendar_items[] = "{$calendar_item_name}_from";
					$calendar_items[] = "{$calendar_item_name}_to";
				} elseif ( key( $ar ) === 'like' ) {
					$val = $ar[ key( $ar ) ];
					$search_cols[] = HTML_Tag::closed( 'input', array( 
						'name' => "search[{$val}][like]",
						'value' => isset( $param_search[ $val ]['like'] ) ? $param_search[ $val ]['like'] : ''
					) );
				} elseif ( key( $ar ) === 'empty' ) {
					$search_cols[] = '';
				}*/
			}
			self::$search = $search_cols;
            /*
			Core_HTML::addJs( "
				var dhtmlXCalendarOptions = {
					langCode: 'el',
					HTMLElementIds: [ '" . implode( "','", $calendar_items ) . "' ]
				}
				_addOnLoad( 'var calendarOrders = yiama_dhtmlXCalendar( dhtmlXCalendarOptions )' )
			" );
             */
		}
		
		/*
		 * @param $tb_name string
		 * @param $content object
		 * @param $row_values array(<array(text=>value)|array(date=>value)|array(publish=>null)|array(order=>null)|array(anchor=>array(<object_property>,<url_path>,<url_param_name>,<object_property_for_url_param_value>,<title>)))
		 */
		public static function setContent( $tb_name, $content, $row_values )
		{
			$content_rows = array();
			foreach( $content as $cont ) {
				$row = array();
				if( self::$bulk_actions ) {
					$row[] = HTML_Tag::closed('label', array('class' => 'checkbox'),
                        HTML_Tag::void( 'input', array(
                            'type' => 'checkbox',
                            'name' => 'checkboxid',
                            'id' => $cont->id,
                            'data-allow_deletion' => isset( $cont->allow_deletion ) && ! $cont->allow_deletion ? false : true
                        )) . "<span class='checkmark'></span>"
                    );
				}
				foreach( $row_values as $ar ) {
					if( key( $ar ) === 'time' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = HTML_Tag::closed( 'span', array( 'class' => 'value' ), date( 'd-m-Y H:i:s', strtotime( $cont->$val ) ) );
					} elseif ( key( $ar ) === 'date' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = HTML_Tag::closed( 'span', array( 'class' => 'value' ), date( 'd-m-Y', strtotime( $cont->$val ) ) );
					} elseif ( key( $ar ) === 'publish' ) {
						$attrs = array(
							'type' => 'checkbox',
							'onchange' => "window.location.href='" . Helper_Link::publish( $tb_name, $cont ) . "';"
						);
						if( $cont->is_published ) {
							$attrs['checked'] = 'checked';
						}
						$row[] = HTML_Tag::closed(
                            'label', 
                            array('class' => 'checkbox'),
                            HTML_Tag::void( 
                                'input', 
                                $attrs
                            ) . "<span class='checkmark'></span>"
                        );
					} elseif ( key( $ar ) === 'favorite' ) {
                        $attrs = array(
                            'type' => 'checkbox',
                            'onchange' => "window.location.href='" . Helper_Link::favorite( $tb_name, $cont ) . "';"
                        );
                        if( $cont->is_favorite ) {
                            $attrs['checked'] = 'checked';
                        }
                        $row[] = HTML_Tag::closed(
                            'label',
                            array('class' => 'checkbox'),
                            HTML_Tag::void(
                                'input',
                                $attrs
                            ) . "<span class='checkmark'></span>"
                        );
                    } elseif ( key( $ar ) === 'action' ) {
						$val = $ar[ key( $ar ) ];
                        $default = $val[1];
                        $request = Core_Request::getInstance();
						$attrs = array(
							'type' => 'checkbox',
							'onchange' => "window.location.href='" . $request->url(
                                'cont_act_id',
                                array( 
                                    'controller' => $request->getController(),
                                    'action' => $val[0],
                                    'id' => $cont->id
                                ),
                                array( 
                                    'tb_name' => $tb_name,
                                    'return_url' => urlencode( $request->getURI() ) 
                                ) ) . "';",
                            'class' => 'checkbox_centered'
                            );
						if( $cont->$default ) {
							$attrs['checked'] = 'checked';
						}
						$row[] = HTML_Tag::closed(
                            'label', 
                            array('class' => 'checkbox'),
                            HTML_Tag::void(
                                'input', 
                                $attrs
                            ) . "<span class='checkmark'></span>"
                        );
					} elseif ( key( $ar ) === 'order' ) {
						$up = HTML_Tag::closed( 'a', array(
								'href' => Helper_Link::order( $tb_name, $cont, 'up', $ar[ key( $ar ) ] ),
								'class' => 'order up'
							)
						);
						$down = HTML_Tag::closed( 'a', array(
								'href' => Helper_Link::order( $tb_name, $cont, 'down', $ar[ key( $ar ) ] ),
								'class' => 'order down'
							)
						);
						$row[] = "<ul class='order'><li>" . $up . '</li><li>' . $down . '</li><?ul>';
					} elseif ( key( $ar ) === 'anchor' ) {
						$val = $ar[ key( $ar ) ];
                        $title = $val[0];
                        $href = $val[3];
						$row[] = HTML_Tag::closed( 'a', array(
								'href' => Core_URL::addQuery( $val[1], array( $val[2] => $cont->$href ) ),
								'class' => 'link'
							), $cont->$title
						);
					} elseif ( key( $ar ) === 'image' ) {
                                                $property = $ar[ key( $ar ) ];
						if( $img = $cont->$property ) {
							$row[] = HTML_Tag::closed( 'span', array( 'class' => 'img' ),
								HTML_Tag::void( 'img', array( 
									'src' => PATH_REL_IMAGES . 'db/' . $tb_name . '/thumbnails/thumb_' . $img,
									'onclick' => "setCatalogueImage( '" . PATH_REL_IMAGES . 'db/' . $tb_name . '/thumbnails/thumb_' . $img . "' ); catalogueImage.pop()",
									'style' => 'cursor: pointer'
								 ) 
							) );
						} else {
							$row[] = HTML_Tag::closed( 'span', array( 'class' => 'img empty' ));
						}
					} elseif ( key( $ar ) === 'boolean' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = HTML_Tag::closed( 'span', array( 'class' => 'value' ), ( bool ) $cont->$val ? 'Ναι' : 'Όχι' );
					}  elseif ( key( $ar ) === 'json' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = implode( ',', json_decode( $cont->$val, true ) );
					} elseif ( key( $ar ) === 'value' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = HTML_Tag::closed( 'span', array( 'class' => 'value' ), $cont->$val );
					} elseif ( key( $ar ) === 'text' ) {
						$val = $ar[ key( $ar ) ];
						$row[] = $cont->$val;
					}
				}
				$content_rows[] = $row;
			}
			self::$content = $content_rows;
		}
		
		/*
		 * @param $content object
		 * @param $actions array(<add|update|delete>)
		 * @param $controller string
		 */
		public static function setContentActions( $content, $actions = null, $controller = null, $params = null )
		{
			$content_rows = array();
			$request = Core_Request::getInstance();
			Helper_Popup::confirm( 'popupConfirm' );
			foreach( $content as $cont ) {
				$row = array();
				foreach( ( array ) $actions as $act ) {
					if( $act == 'update' ) {
						$row[] = HTML_Tag::closed( 'a', array( 
								'href' => Helper_Link::updateform( $cont, $controller, urlencode( $request->getURI() ) ),
								'title' => 'Επεξεργασία',
								'class' => 'update'
							)
						);
					} elseif ( $act == 'item' ) {
						$row[] = HTML_Tag::closed( 'a', array( 
								'href' => Helper_Link::item( $cont, $controller, null, $params ),
								'title' => 'Εμφάνιση',
								'class' => 'item'
							)
						);
					} elseif ( $act == 'delete' ) {
						if( ! isset( $cont->allow_deletion ) || $cont->allow_deletion ) {
							$row[] = HTML_Tag::closed( 'a', array( 
									'title' => 'Διαγραφή',
									'class' => 'delete',
									'onclick' => "setCatalogueConfirm( 'Επιβεβαίωση διαγραφής', 'Θέλετε να γίνει διαγραφή της εγγραφής;', 'window.location.href=\'" . Helper_Link::delete( $cont, $controller ) . "\'' ); catalogueConfirm.pop();"
								)
							);
						} else {
							$row[] = HTML_Tag::closed( 'a', array( 
									'class' => 'action_disabled'
								)
							);
						}
					}
				}
				$content_rows[] = $row;
			}
			self::$content_actions = $content_rows;
		}
		
		public static function buildHeaders()
		{
			if( empty( self:: $content ) ) {
				return;
			}
            $count_headers = count(self::$headers) 
                    + intval(!empty(self:: $bulk_actions))
                    + intval(!empty(self:: $content_actions));
			$s = "<div class='row headers'>";
			if( self::$bulk_actions ) {
				$s .= "<div class='cell-header ids'>";
				$s .= HTML_Tag::closed('label', array('class' => 'checkbox'),
                    HTML_Tag::closed( 'input', array(
                        'type' => 'checkbox',
                        'onclick' => "checkAll( this );"
                        )
                    ) . "<span class='checkmark'></span>"
                );
				$s .= '</div>';
			}
            $style = 'min-width: ' . floor(100/$count_headers) . '%';
			foreach( self::$headers as $val ) {
				$s .= "<div class='cell-header' style='" . $style . "'>" . $val . '</div>';
			}
			if( self::$content_actions ) { 
				$s .= "<div class='cell-header'><a class='header'>Ενέργειες</a></div>";
			}
			return $s . '</div>';
		}
		
		public static function buildSearch()
		{
			Core_HTML::addFiles( 'library\basic.js' );
			Core_HTML::addJs( "
                function catalogueBulk(url, confirm) {
                    var checked = selectChecked();
                    if (checked != '') {
                        url += url.indexOf('?') == -1 ? '?' : '&';
                        url += 'ids=' + checked;
                        if (confirm) {
                            setCatalogueConfirm(
                                'Επιβεβαίωση ενέργειας',
                                'Θέλετε να ολοκληρωθεί η ενέργεια;',
                                'window.location.href=\'' + url + '\''
                            );
                            catalogueConfirm.pop();
                        } else {
                            _request(url);
                        }
                    }
                }
                
				function catalogueSearchClear() {
					var elements = document.getElementsByTagName( '*' );
					for( var i = 0; i < elements.length; i++ ) {
						if( elements[i].name && elements[i].name.indexOf( 'search[' ) === 0 ) {
							if( elements[i].nodeName == 'INPUT' ) {
								elements[i].value = '';
							} else if( elements[i].nodeName == 'SELECT' ) {
								elements[i].selectedIndex = 0;
							}
						}
					}
				}
                
				function catalogueSearch(event) {
                    var element = event.target;
                    var value;
                    if (    element.nodeName == 'INPUT' 
                        &&  event.keyCode == 13) {
                        value = element.value;
                    } else if (element.nodeName == 'SELECT') {
                        value = element.options[element.selectedIndex].value;
                    } else {
                        return;
                    }
                    window.location.href = '" 
                        . parse_url(
                            Core_Request::getInstance()->getURI(), 
                            PHP_URL_PATH 
                        ) . "?' 
                            + encodeURIComponent(element.name) 
                            + '=' 
                            + encodeURIComponent(value);
				}
			" );
            
			$s = '';
			if( self::$bulk_actions ) {
				$s .= Helper_Form::element( 
                    HTML_Tag::closed(
                        'select', 
                        array(
                            'class' => 'bulk ids',
                            'onchange' => "
                                var url = this.options[this.selectedIndex].getAttribute('url');
                                var action = this.options[this.selectedIndex].getAttribute('action');
                                if (typeof url !== 'undefined') {
                                    var confirm =   typeof action !== 'undefined' 
                                                &&  (action == 'delete');
                                    catalogueBulk(url, confirm);
                                }"
                        ),
                        self::$bulk_actions
                    )
                );
			}
			return HTML_Tag::closed(
                'div', 
                array('class' => 'catalogue-search'), 
                $s . (!empty(self::$search) ? implode( '', self::$search) : '')
            );
		}
		
		public static function buildContent()
		{
			$s = '';
			foreach( self::$content as $key => $val ) {
				$s .= "<div class='row list" . ( $key % 2 ? ' dark' : '' ) . "'>";
				if( self::$bulk_actions ) {
					$s .= "<div class='cell bulk'>" . implode( "</div><div class='cell'>", $val ) . '</div>';
				} else {
					$s .= "<div class='cell'>" . implode( "</div><div class='cell'>", $val ) . '</div>';
				}
				if( self::$content_actions ) { 
					$s .= "<div class='cell'><ul class='actions'><li>" . implode( '</li><li>', self::$content_actions[ $key ] ) . '</li></ul></div>';
				}
				$s .= '</div>';
			}
			return $s;
		}
		
		public static function get( $header = null )
		{
			$result = ( self::$search || self::$bulk_actions? self::buildSearch() : '' )
                . self::$main_search
                . HTML_Tag::open( 'div', array('class' => 'catalogue-table') )
				. self::buildHeaders()
				. self::buildContent()
				. HTML_Tag::close( 'div' ); 
			self::$bulk_actions = null;
			self::$headers = null;
			self::$search = null;
			self::$content = null;
			self::$content_actions = null;
			return $result;
		}
	}
?>