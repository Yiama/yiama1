<?php
	class Helper_Value
	{
		public static function price( $price, $currency )
		{
			return ! $currency->position ? ( $price ? $currency->symbol : '' ) . $price : $price . ( $price ? $currency->symbol : '' );
		}
	}
?>