<?php
	/*
	 * Για κάθε js object δίνεται ένα όνομα και χρησιμοποιείται για να κληθεί το object,
	 * ενώ επίσης δημιουργείται μία function με ονομασία το όνομα που δόθηκε ( κάνοντασ κεφαλαίο το πρώτο γράμμα ) και πρόθεμα την λέξη set
	 * π.χ. $name = popupImage, set params: setPopupImage( $params ), popup image: popupImage.pop()
	 */
	class Helper_Popup
	{
		private static $initialized = false;
		
		public static function init()
		{
			self::$initialized = true;
			Core_HTML::addFiles( array(
				'js\library\popup.css',
				'library\popup.js' 
			) );
		} 
		
		public static function confirm( $name )
		{
			if( ! self::$initialized ) {
				self::init();
			}
			Core_HTML::addJs( "
				var {$name};
				function set" . ucfirst( $name ) . "( header, message, action )
				{
					{$name} = new popup( {
						bg: {  
							attributes: {
								'class': 'popupBg'
							},
							events: [ { 
								'event': 'click',
								'func': '{$name}.unpop();'
							} ]
						},
						wrapper: {
							attributes: {
								'class': 'popupWrapper'
							}
						},
						header: {
							attributes: {
								'class': 'popupHeader'
							},
							text: header
						},
						message: {
							attributes: {
								'class': 'popupMessage'
							},
							text: message
						},
						button1: {
							attributes: {
								'class': 'popupButton yes'
							},
							events: [ { 
								'event': 'click',
								'func': action
							} ],
							text: 'Επιβεβαίωση'
						},
						button2: {
							attributes: {
								'class': 'popupButton no'
							},
							events: [ { 
								'event': 'click',
								'func': '{$name}.unpop()'
							} ],
							text: 'Ακύρωση'
						}
					} );
				}
			" );
		}
		
		public static function notify( $name )
		{
			if( ! self::$initialized ) {
				self::init();
			}
			Core_HTML::addJs( "
				var {$name};
				function set" . ucfirst( $name ) . "( header, message, action )
				{
					{$name} = new popup( {
						bg: {  
							attributes: {
								'class': 'popupBg'
							},
							events: [ { 
								'event': 'click',
								'func': '{$name}.unpop();'
							} ]
						},
						wrapper: {
							attributes: {
								'class': 'popupWrapper'
							}
						},
						header: {
							attributes: {
								'class': 'popupHeader'
							},
							text: header
						},
						message: {
							attributes: {
								'class': 'popupMessage'
							},
							text: message
						},
						button1: {
							attributes: {
								'class': 'popupButton yes'
							},
							events: [ { 
								'event': 'click',
								'func': '{$name}.unpop();'
							} ],
							text: 'ΟΚ'
						}
					} );
				}
			" );
		}
		
		public static function image( $name )
		{
			if( ! self::$initialized ) {
				self::init();
			}
			Core_HTML::addJs( "
				var {$name};
				function set" . ucfirst ( $name ) . "( src )
				{
					{$name} = new popup( {
						bg: {  
							attributes: {
								'class': 'popupBg'
							},
							events: [ { 
								'event': 'click',
								'func': '{$name}.unpop();'
							} ]
						},
						wrapper: {
							attributes: {
								'class': 'popupWrapper'
							}
						},
						message: {
							attributes: {
								'class': 'popupMessage'
							},
							text: \"<img src='\" + src + \"'/>\"
						}
					} );
				}
			" );
		}
	}
?>