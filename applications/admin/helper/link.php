<?php
	class Helper_Link
	{
		public static function catalogue( $controller = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 'controller' => ! empty( $controller ) ? $controller : $request->getController() )
			);	
		}
		
		public static function statistics( $controller = null, $id = null )
		{
			$request = Core_Request::getInstance();
			$route_params = array( 
				'controller' => ! empty( $controller ) ? $controller : $request->getController(),
				'action' => 'statistics' 
			);
			if( $id ) {
				$route_params['id'] = $id;
			}
			return $request->url( 'cont_act_id', $route_params );	
		}
		
		public static function publish( $tb_name, $content_row )
		{
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => 'task',
					'action' => 'publish',
					'id' => $content_row->id
				),
				array( 
					'tb_name' => $tb_name,
					'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken()
				)
			);
		}

        public static function favorite( $tb_name, $content_row )
        {
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
            $request = Core_Request::getInstance();
            return $request->url(
                'cont_act_id',
                array(
                    'controller' => 'task',
                    'action' => 'favorite',
                    'id' => $content_row->id
                ),
                array(
                    'tb_name' => $tb_name,
                    'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken()
                )
            );
        }
		
		public static function item( $content_row, $controller = null, $return_url = null, $params = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'item',
					'id' => $content_row->id
				),
				array_merge( array( 'return_url' => $return_url ), ( array ) $params )
			);
		}
		
		public static function bulkpublish( $tb_name, $publish = true )
		{
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => 'task',
					'action' => 'bulkpublish'
				),
				array( 
					'publish' => $publish ? 1 : 0,
					'tb_name' => $tb_name,
					'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken()
				 )
			);
		}
		
		public static function bulkdelete( $tb_name )
		{
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => 'task',
					'action' => 'bulkdelete'
				),
				array( 
					'controller' => $request->getController(),
					'tb_name' => $tb_name,
					'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken()
				)
			);
		}
		
		/*
		 * @param $direction string 'up'|'down'
		 */
		public static function order( $tb_name, $content_row, $direction, $group_field = null )
		{
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => 'task',
					'action' => 'order',
					'id' => $content_row->id
				),
				array( 
					'tb_name' => $tb_name,
					'order_dir' => $direction,
					'group_field' => $group_field,
					'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken()
				)
			);
		}
		
		public static function delete( $content_row, $controller = null )
		{
            // CSRF
            $csrf = new Security_Csrf(Core_Session::getInstance());
			$request = Core_Request::getInstance();
			$prKey = $content_row->getPrimaryKey();
			$delete_params = array();
			foreach( ( array ) $prKey as $k ) {
				$delete_params[ $k ] = $content_row->$k;
			}
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'delete'
				),
				array_merge( $delete_params, array(
                    'return_url' => urlencode( $request->getURI() ),
                    $csrf->getRequestTokenName() => $csrf->getToken() 
                ) )
			);
		}
		
		public static function insertform( $controller = null, $return_url = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'insertform'
				),
				array( 'return_url' => $return_url )
			);
		}
		
		public static function insert()
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $request->getController(),
					'action' => 'insert'
				),
				array( 'return_url' => $request->getParams( 'return_url' ) ) 
			);
		}
		
		public static function updateform( $content_row = null, $controller = null, $return_url = null )
		{
			$request = Core_Request::getInstance();
			$prKey = $content_row->getPrimaryKey();
			$update_params = array();
			foreach( ( array ) $prKey as $k ) {
				$update_params[ $k ] = $content_row->$k;
			}
            if (key($update_params) == 'id') {
                return $request->url(
                    'cont_act_id',
                    array( 
                        'controller' => $controller ? $controller : $request->getController(),
                        'action' => 'updateform',
                        'id' => ( ! $content_row ) ? null : $content_row->id
                    ),
                    array( 'return_url' => $return_url )
                );
            } else {
                return $request->url(
                    'cont_act_id',
                    array( 
                        'controller' => $controller ? $controller : $request->getController(),
                        'action' => 'updateform',
                        'id' => null
                    ),
                    array_merge($update_params, array( 'return_url' => $return_url ))
                );
            }
		}
		
		public static function update( $content_row = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $request->getController(),
					'action' => 'update',
					'id' => ( ! $content_row ) ? null : $content_row->id
				)
			);
		}
		
		public static function bulkupdateform( $params )
		{
			$request = Core_Request::getInstance();
			$query = array_merge( array( 'return_url' => urlencode( $request->getURI() ) ), $params );
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $request->getController(),
					'action' => 'bulkupdateform'
				), $query
			);
		}
		
		public static function bulkupdate( $params )
		{
			$request = Core_Request::getInstance();
			$query = array_merge( array( 'return_url' => urlencode( $request->getURI() ) ), $params );
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $request->getController(),
					'action' => 'bulkupdate'
				), $query
			);
		}
		
		public static function importform( $controller = null, $return_url = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'importform'
				),
				array( 'return_url' => $return_url )
			);
		}
		
		public static function exportform( $controller = null, $return_url = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'exportform'
				),
				array( 'return_url' => $return_url )
			);
		}
		
		public static function mergeform( $controller = null, $return_url = null )
		{
			$request = Core_Request::getInstance();
			return $request->url(
				'cont_act_id',
				array( 
					'controller' => $controller ? $controller : $request->getController(),
					'action' => 'mergeform'
				),
				array( 'return_url' => $return_url )
			);
		}
	}
?>