<?php
	class Helper_Buttons
	{
		public static function get(
            $t,
            $buttons_right = null, 
            $buttons_left = null 
        ) {
            $b_content = '';
			$b = '';
			if( $buttons_left ) {
				foreach( $buttons_left as $label => $link ) {
					if( $label == 'download' ) {
						$b .= HTML_Tag::closed( 'a', array(
								'class' => 'button',
								'onclick' => "document.getElementById( 'main_form_submit' ).click();",
								'title' => 'Κατέβασμα αρχείου'
                        ));
                    } elseif( $label == 'upload' ) {
						$b .= HTML_Tag::closed( 'a', array(
								'class' => 'button',
								'onclick' => "document.getElementById( 'main_form_submit' ).click();",
								'title' => 'Ανέβασμα αρχείου'
							), '<span>&#xf20c;</span>' 
						);
                    } elseif( $label == 'exportform' ) {
                        $b .= HTML_Tag::closed( 'a', array( 
                                'href' => $link,
								'class' => 'button',
								'title' => 'Εξαγωγή σε αρχείο'
                        ));
					} elseif( $label == 'importform' ) {
						$b .= HTML_Tag::closed( 'a', array(
                                'href' => $link,
								'class' => 'button',
								'title' => 'Εισαγωγή από αρχείο'
                        ));
					} elseif( $label == 'mergeform' ) {
						$b .= HTML_Tag::closed( 'a', array( 
                                'href' => $link,
								'class' => 'button',
								'title' => 'Εισαγωγή από επεξεργασμένο αρχείο'
                        ));
					} elseif ( $label == 'statistics' ) {
                        $b .= HTML_Tag::closed( 'a', array( 
						 		'href' => $link, 
								'class' => 'button',
								'title' => 'Στατιστικά'
                        ));
					} elseif  ( $label == 'return' ) {
						$b .= HTML_Tag::closed( 'a', array( 
                            'href' => $link, 
                            'class' => 'button return' 
                        ), '<span>' . $t->_('button.return') . '</span>' );
					} else {
						$b .= HTML_Tag::closed( 'a', array( 
                            'href' => $link, 
                            'class' => 'button' 
                        ), '<span>' . $label . '</span>' );
					}
				}
			}
            $b_content .= HTML_Tag::closed('div', array('class' => 'left'), $b);
            $b = '';
			if( $buttons_right ) {
				foreach( $buttons_right as $key => $link ) {
					if( $key == 'add' ) {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button add', 
								'href' => $link 
							), '<span>' . $t->_('button.add') . '</span>' 
						);
					} elseif ( $key == 'save' ) {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button save', 
								'id' => 'main_form_right_top_button', 
								'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
							), '<span>' . $t->_('button.save') . '</span>' 
						);
                    } else {
						$b .= HTML_Tag::closed( 'a', array( 
								'class' => 'button', 
								'href' => $link 
							), '<span>' . $key . '</span>' 
						);
					}
				}
			}
            $b_content .= HTML_Tag::closed('div', array('class' => 'right'), $b);
			return HTML_Tag::closed(
                'div', 
                array('class' => 'controller-buttons'), 
                $b_content
            );
		}
	}
?>