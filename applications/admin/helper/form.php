﻿<?php
class Helper_Form
{
	private static $calendar_initialized = false;
	private static $ajax_input_initialized = false;
	private static $local_wrapper_indx = 0;
	private static $add_button_indx = 0;
	private static $fillConnectedSelect_init = false;
	private static $discountRules_init = false;
	private static $tinyMCE_init = false;
	
	public static function openWrapperColumnLeft() 
	{
		return "<div class='form_left_col'>";
	}
	
	public static function closeWrapperColumnLeft()
	{
		return "</div>";
	}
	
	public static function openWrapperColumnRight()
	{
		return "<div class='form_right_col'>";
	}
	
	public static function closeWrapperColumnRight()
	{
		return "</div>";
	}

	public static function openForm( $action )
	{
		Core_HTML::addFiles( array(
			'admin\form.css',
			'library\basic.js',	
			'library\form.js'
		) ); 
		Core_HTML::addJs( "
            var formMessage_timeout;
            function formMessage( type, text )
            {
                clearTimeout( formMessage_timeout );
                removeFormMessageItems();
                var msgEl = _get( 'formMessage' );
                var item = document.createElement( 'div' );
                item.innerHTML = text;
                item.setAttribute( 'class', 'item ' + type );
                msgEl.appendChild( item );
                _show( msgEl );
                formMessage_timeout = setTimeout( function(){ removeFormMessageItems() }, 5000 );
            }
            function removeFormMessageItems()
            {
                var msgEl = _get( 'formMessage' );
                var items = msgEl.getElementsByTagName( 'div' );
                if( items ) {
                    for( var i = 0; i < items.length; i++ ) {
                        msgEl.removeChild( items[i] );
                    }
                    _hide( msgEl );
                }
            }
            
			function handleError( formObj, ar )
			{
				_removeClass( formObj.getElementsByTagName( 'button' )[0], 'loading' );
				_removeClass( _get( 'main_form_right_top_button' ), 'loading' );
				formMessage( 'error', 'Κάποια πεδία δεν συμπληρώθηκαν σωστά!' );
				window.scrollTo( 0, 0 );
				for( var i = 0; i < ar.length; i++ ){
					for( var k = 0; k < formObj.elements.length; k++ ){
						if( formObj.elements[k].name == ar[i] ){
							highlightTab( formObj.elements[k] );
							_addClass( formObj.elements[k], 'highlighted' );
						}
					}
				}
			}

			function highlightTab( element )
			{
				if( element.tagName == 'FORM' ) {
					return;
				} else if( _hasClass( element, 'form_tab_item' ) ) {
					_addClass( _get( 'tab_button_' + element.id ), 'highlighted' );
				} else {
					return highlightTab( element.parentNode );
				}
			}
			
			function removeHighlightTab( element )
			{
				if( element.tagName == 'FORM' ) {
					return;
				} else if( _hasClass( element, 'form_tab_item' ) ) {
					_removeClass( _get( 'tab_button_' + element.id ), 'highlighted' );
				} else {
					return removeHighlightTab( element.parentNode );
				}
			}
			
			function formElementOnClick() {
				var elements = document.forms['form'].elements;
				for( var i = 0; i < elements.length; i++ ) {
					if( elements[i].nodeName == 'INPUT' 
					|| elements[i].nodeName == 'SELECT'
					|| elements[i].nodeName == 'TEXTAREA' ) {
						elements[i].onclick = function(){ 
							_hide( 'formMessage' ); 
							_removeClass( this, 'highlighted' );
							removeHighlightTab( this );
						}
					}
				}
			}
			_addOnLoad( 'formElementOnClick()' );
            window.addEventListener('load', function() {"
                . "window.formMessage_timeout = setTimeout( function(){ removeFormMessageItems() }, 5000 );"
            . "});
		" );
		return HTML_Tag::open( 'form', array(
			'enctype' => 'multipart/form-data',
			'method' => 'post',
			'name' => 'form',
			'id' => 'main_form',
			'action' => $action,
			'onsubmit' => "return submitForm( this, handleError )"
		) ) . HTML_Tag::closed( 'div', array( 
				'class' => 'form-message',
				'id' => 'formMessage'
			));
	}
	
	public static function closeForm()
	{
        // CSRF
        $csrf = new Security_Csrf(Core_Session::getInstance());
        $csrf_tag = HTML_Tag::void('input', array(
            'type' => 'hidden',
            'name' => $csrf->getRequestTokenName(),
            'value' => $csrf->getToken()
        ));
		return $csrf_tag . HTML_Tag::close( 'form' );
	}
	public static function openWrapperModule( $header = null, $attributes = null )
	{
		$s = $header ? HTML_Tag::closed( 'div', array_merge( array( 'class' => 'header' ), ( array ) $attributes ), $header ) : '';
		return "<fieldset>" . self::element( $s );
	}
	
	public static function closeWrapperModule() 
	{
		return "</fieldset>";
	}
	
	public static function openSubWrapper( $header = null )
	{
		$s = self::seperator();
		$s .= $header ? self::element( HTML_Tag::closed( 'div', array( 'class' => 'header' ), $header ) ) : '';
		return HTML_Tag::open( 'div', array( 'class' => 'sub_wrapper' ) ) . $s;
	}
	
	public static function closeSubWrapper()
	{
		return "</div>";
	}
	
	/*
	 * @param $languages array Model_Yiama_Languages
	 */
	public static function openWrapperLocal( $languages )
	{
		if( ! self::$local_wrapper_indx ) {
			Core_HTML::addJs( "
				function formLocalTabs()
				{
					var last_fields_id;
					var last_tab_id;
					this.changeTab = function( fields_id, tab_id ) {
						if( typeof last_fields_id === 'undefined' ) {
							last_fields_id = _get( fields_id ).parentNode.getElementsByTagName( 'div' )[0].id;
						}
						_get( last_fields_id ).style.position = 'absolute';
						_get( last_fields_id ).style.visibility = 'hidden';
						_get( fields_id ).style.position = 'relative';
						_get( fields_id ).style.visibility = 'visible';
						last_fields_id = fields_id;
						if( typeof last_tab_id === 'undefined' ) {
							last_tab_id = _get( tab_id ).parentNode.getElementsByTagName( 'a' )[0].id;
						}
						_removeClass( _get( last_tab_id ), 'selected' );
						_addClass( _get( tab_id ), 'selected' );
						last_tab_id = tab_id;
					}
				}
			" );
		}
		Core_HTML::addJs( "var formLocalTabs_" . ++self::$local_wrapper_indx . " = new formLocalTabs();");
		
		$s = "<div class='local'>";
		$s .= "  <div class='tabs'>";
		foreach( $languages as $key => $l ) {
			$s .= HTML_Tag::closed( 'a', array(
					'onclick' => "formLocalTabs_" . self::$local_wrapper_indx . ".changeTab( this.getAttribute( 'data-id' ), this.id )",
					'data-id' => 'form_local_item_' . self::$local_wrapper_indx . "_{$l->id}",
					'id' => 'form_local_tab_' . self::$local_wrapper_indx . "_{$l->id}",
					'class' => 'item' . ( ! $key ? ' selected' : '' )
				), $l->title
			);
		}
		$s .= "  </div>";
		$s .= "  <div class='fields'>";
		return $s;
	}
	
	public static function closeWrapperLocal()
	{
		return "</div></div>";
	}
	public static function openWrapperLocalItem( $language_id )
	{
		return "<div id='form_local_item_" . self::$local_wrapper_indx . "_{$language_id}' class='item'>";
	}
	
	public static function closeWrapperLocalItem()
	{
		return "</div>";
	}
	
	public static function submit( $val = null, $hidden = true, $attributes = null )
	{
		Core_HTML::addFiles( array(
			'library\basic.js',  
			'library\preload.js' 
		) );
		Core_HTML::addJs( " _addOnLoad( '_preloadImg( \'" . PATH_REL_IMAGES . "yiama/assets/images/admin/form/loading.gif\' )' ); " );
		$attrs['id'] = 'main_form_submit';
		if( $hidden ) {
			$attrs['class'] = 'hidden';
		} else {
			$attrs['onclick'] = "_addClass( this, 'loading' );";
		}
		return HTML_Tag::closed( 'button', array_merge( $attrs, ( array ) $attributes ), $val );
	}
	
	public static function hiddenReturn( $return_url = null )
	{
		$params = Core_Request::getInstance()->getParams();
		return HTML_Tag::void( 'input', array( 
			'type' => 'hidden',
			'name' => 'return_url',
			'value' => $return_url ? $return_url : ( isset( $params['return_url'] ) ? $params['return_url'] : null )
		) );
	}
	
	public static function seperator()
	{
		return HTML_Tag::closed( 'div', array( 'class' => 'seperator' ) );
	}
	
	public static function linkButton( $url, $text )
	{
		return HTML_Tag::closed( 'a', array( 'href' => $url, 'class' => 'link_button' ), $text );
	}
	
	public static function tabMenu( $tabs, $options = array() )
	{
		Core_HTML::addFiles( array( 
			'library\basic.js',
			'library\cookies.js'
		) );
		$cookie_key = Core_Request::getInstance()->getController() . '.' . Core_Request::getInstance()->getAction();
		Core_HTML::addJs( "
			function showTabOnLoad()
			{
				if( cookie_val = _getCookie( 'tabMenu_{$cookie_key}' ) ) {
					showTab( _get( cookie_val ) );
				}
			}
			_addOnLoad( 'showTabOnLoad()' );
			function showTab( tab_button )
			{
				var wrapper = tab_button.parentNode;
				var items = wrapper.getElementsByTagName( 'span' );
				for( var i = 0; i < items.length; i++ ) {
					if( items[i] === tab_button ) {
						_addClass( items[i], 'selected' );
						_addClass( _get( items[i].getAttribute( 'data-id' ) ), 'visible' );
						_setCookie( 'tabMenu_{$cookie_key}', tab_button.id );
					} else {
						_removeClass( items[i], 'selected' );
						_removeClass( _get( items[i].getAttribute( 'data-id' ) ), 'visible' );
					}
				}
			}
			function setCanvasWrapperWidth()
			{
				var spans = document.getElementsByTagName( 'span' );
				for( var i = 0; i < spans.length; i++ ) {
					if( spans[i].id.indexOf( 'tab_button_' ) === 0 ) {
						var tab_item = _get( spans[i].getAttribute( 'data-id' ) );
						var canvas = tab_item.getElementsByTagName( 'canvas' );
						for( var k = 0; k < canvas.length; k++ ) {
							 canvas[k].parentNode.style.width = tab_item.parentNode.offsetWidth - tab_item.parentNode.offsetWidth*0.03 + 'px';
						}
					}
				}
			}
			_addOnLoad( 'setCanvasWrapperWidth()' );
		" );
		$t = array();
		$first_value = current( $tabs );
		foreach( $tabs as $tab_item_id => $label ) {
            $attrs = array(	'data-id' => $tab_item_id );
            if (    isset($options['disabled'])
                &&  in_array($tab_item_id, $options['disabled'])) {
                $attrs['class'] = 'disabled';
            } else {
                $attrs = array_merge( $attrs, array('onclick' => "showTab( this );" ));
                if( $label == $first_value ) {
                    $attrs['class'] = 'selected';
                }
            }
            if (    !empty($options['required']) 
                &&  in_array($label, $options['required'])) {
                $label = '° ' . $label;
            }
			$t[] = HTML_Tag::closed('span', array_merge(
                $attrs, 
                array('id' => 'tab_button_' . $tab_item_id )), 
                $label 
            );
		}
		return HTML_Tag::closed( 'div', array( 'class' => 'form_tabs' ), implode( '', $t ) );
	}
	
	public static function openTabItem( $id, $first = false )
	{
		$attrs = array( 'id' => $id, 'class' => 'form_tab_item hidden' );
		if( $first ) {
			$attrs['class'] = 'form_tab_item visible';
		}
		return HTML_Tag::open( 'div', $attrs );
	}
	
	public static function closeTabItem()
	{
		return HTML_Tag::close( 'div' );
	}
	
	public static function element( $content, $attrs = null )
	{
		return HTML_Tag::closed( 'div', array_merge( ( array ) $attrs, array( 'class' => 'element' . ( isset( $attrs['class'] ) ? ' ' . $attrs['class'] : '' ) ) ), $content );
	}
	
	/*
	 * @param $options = string[title]/array( <title>, <comments>, <instructions>, <attributes[array()]>, <link[array( <title> => <link>, ... )]> )
	 * @param $required boolean
	 */
	public static function label( $options, $required = false )
	{
		if( is_array( $options ) ) {
			if( $required ) {
				$options[0] = ' <b>&#176;</b> ' . $options[0];
			}
			$l = '';
			if( isset( $options[4] ) ) {
				foreach( $options[4] as $title => $link ) {
					$l .= HTML_Tag::closed( 'a', array( 
							'class' => 'label_link',
							'href' => $link
						), $title 
					);
				}
			}
			$s = HTML_Tag::closed( 'label', 
				isset( $options[3] ) ? $options[3] : null, 
				$options[0] . ( ! empty( $options[1] ) ? '<br/>' . HTML_Tag::closed( 'small', null, $options[1] ) : '' ) . $l 
			);
			return $s;
		}
		if( $required ) {
			$options = ' <b>&#176;</b> ' . $options;
		}
		return HTML_Tag::closed( 'label', null, $options );
	}
	
	public static function input( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		return self::element( $s . HTML_Tag::void( 'input', array_merge( array( 
				'type' => 'text', 
				'name' => $name,
				'value' => $val ? htmlspecialchars( $val ) : '',
				'isRequired' => $required ? 'yes' : 'no'
			), ( array ) $attrs
		) ), $wrapper_attrs );
	}
	
	public static function date( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		Core_HTML::addFiles( array(
			'library\dhtmlxCalendar\codebase\dhtmlxcalendar.js',
			'library\dhtmlxCalendar\yiama_dhtmlxCalendar.js',
			'js\library\dhtmlxcalendar.css'
		) );
		if( ! self::$calendar_initialized ) {
			self::$calendar_initialized = true;
			Core_HTML::addJs( "
				var dhtmlXCalendarOptions = {
					langCode: 'el',
					HTMLElementIds: [ '{$name}_calendar' ]
				}
				_addOnLoad( 'var calendarOrders = yiama_dhtmlXCalendar( dhtmlXCalendarOptions )' );
			" );
		} else {
			Core_HTML::addJs( "
				dhtmlXCalendarOptions['HTMLElementIds'].push( '{$name}_calendar' );
				_addOnLoad( 'var calendarOrders = yiama_dhtmlXCalendar( dhtmlXCalendarOptions )' );
			" );
		}
		return self::input( $name, $val, $required, array_merge( array( 'id' => "{$name}_calendar", 'class' => 'small' ), ( array ) $attrs ), $label, $wrapper_attrs );
	}
	
	public static function checkbox( $name, $checked = false, $required = false, $attrs = null, $label = null, $wrapper_attrs = null, $value = null  )
	{
		$s = '';
		if( $label ) {
			$s .= HTML_Tag::closed( 'label', array( 'class' => 'checkbox' ), ( $required ? ' <b>&#176;</b> ' : '' ) . $label );
		}
		$attrs = array_merge( ( array ) $attrs, array( 
			'type' => 'checkbox', 
			'name' => $name,
			'class' => 'checkbox',
			'isRequired' => $required ? 'yes' : 'no'
		) );
		if( $checked ) {
			$attrs['checked'] = 'checked';
		}
		if( $value ) {
		    $attrs['value'] = $value;
		}
		return self::element( HTML_Tag::void( 'input', $attrs ) . $s, $wrapper_attrs );
	}
	
	public static function radio( $name, $options, $selected_val = null, $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$indx = 0;
		foreach( $options as $key => $val ) {
			if( $indx++ ) {
				$s .= '<br/>';
			}
			$set_attrs = array_merge( array( 
				'type' => 'radio',
				'name' => $name,
				'value' => $val,
				'class' => 'radio'
			), ( array ) $attrs );
			if( ! is_null( $selected_val ) && $val == $selected_val ) {
				$set_attrs['checked'] = 'checked';
			}
			$s .= HTML_Tag::void( 'input', $set_attrs ) . HTML_Tag::closed( 'label', array( 'class' => 'radio' ), $key );
		} 
		return self::element( $s, $wrapper_attrs );
	}
	
	public static function textarea( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= HTML_Tag::closed( 'label', null, ( $required ? ' <b>&#176;</b> ' : '' ) . $label );
		}
		return self::element( $s . HTML_Tag::closed( 'textarea', array_merge( ( array ) $attrs, array( 
				'name' => $name,
				'isRequired' => $required ? 'yes' : 'no'
			) ), $val ? htmlspecialchars( $val ) : '' 
		), $wrapper_attrs );
	}
	
	public static function select( $name, $content, $value_prop, $value_text, $selected_prop = null, $selected_val = null, $required = false, $label = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$options = array();
        $l = is_array($label) ? $label[0] : $label;
        if (!isset($extra['no_empty'])) {
    		$options[] = HTML_Tag::closed(
                'option', 
                array('value' => ''),
                $label ? $l : ''
            ); 
        }
		foreach( $content as $cont ) { 
			$attrs = array( 'value' => $cont->$value_prop );
			if( $option_attrs ) {
				foreach( $option_attrs as $key => $a ) {
					if( isset( $cont->$a ) ) {
						$attrs[ $key ] = $cont->$a;
					} else {
						$attrs[ $key ] = $a;
					}
				}
			}
			if( $selected_prop && $cont->$selected_prop === $selected_val ) {
				$attrs['selected'] = 'selected';
			}
			$options[] = HTML_Tag::closed( 'option', $attrs, $cont->$value_text ); 
		}
		$attrs = array_merge( array( 'name' => $name, 'isRequired' => $required ? 'yes' : 'no' ), ( array ) $select_attrs );
		if( isset( $extra['connected_select'] ) ) {
			$onchange = htmlspecialchars( " fillConnectedSelect( this, '{$extra['connected_select']['json_content']}' );" );
			$attrs['onchange'] = ( ! empty( $attrs['onchange'] ) ? $attrs['onchange'] . ';' : '' ) . $onchange;
			if( ! self::$fillConnectedSelect_init ) {
				self::$fillConnectedSelect_init = true;
				Core_HTML::addJs( "
					function fillConnectedSelect( parent_el, json_content ) {
						var parent_value = parent_el.options[parent_el.selectedIndex].value;
						var selects = parent_el.parentNode.parentNode.getElementsByTagName( 'select' ); 
						for( var i = 0; i < selects.length; i++ ) {
							if( selects[i] == parent_el ) {
								var child_el = selects[i+1];
								break;
							}
						}
					    var content = JSON.parse( json_content );
				        var i = 0;
					    child_el.options.length = 0;
				        for( var key in content ) {
		  					if( content.hasOwnProperty( key ) && key == parent_value ) {
		  						var child_content = content[key];
		  						if( child_content ) {
		  							child_el.options[i++] = new Option( 'Επιλογή...', '' );  
		  						}
				        		for( var key2 in child_content ) {
		  							if( child_content.hasOwnProperty( key2 ) ) {
					            		child_el.options[i++] = new Option( child_content[key2], key2 );
					            	}
					            }
					        }
				        }
					}
				" );
			}
		}
		return self::element( $s . HTML_Tag::closed( 'select', $attrs, implode( '', $options ) ), $wrapper_attrs );
	}
	
	public static function selectBoolean( $name, $selected_val = null, $required = false, $label = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		return self::select( $name, array( ( object ) array( 'value' => '0', 'title' => 'Όχι' ), ( object ) array( 'value' => '1', 'title' => 'Ναι' ) ), 'value', 'title', 'value', $selected_val, $required, $label, $select_attrs, $option_attrs, $extra, $wrapper_attrs );
	}
	
	public static function selectLocation( $names, $selected_ids = null, $required = null, $select_attrs = null, $option_attrs = null, $extra = null, $wrapper_attrs = null )
	{
		$country = new Model_Geo_Country();
		$countries = $country->findAll();
		$region = new Model_Geo_Region();
		$regions = $region->findAll();
		$select_content = array();
		foreach( $countries as $c ) { /* child select content */
			$select_content[ $c->id ] = null;
			foreach( $regions as $r ) {
				if( $r->geo_countries_id == $c->id ) {
					$select_content[ $c->id ][ $r->id ] = $r->title;
				}
			}
		}
		return 
			self::select( $names[0], $countries, 'id', 'title', 'id', isset( $selected_ids[0] ) ? $selected_ids[0] : null, isset( $required[0] ) ? $required[0] : null, 'Χώρα', null, null, array( 'connected_select' => array( 'json_content' => json_encode( $select_content ) ) ), $wrapper_attrs ) .
			self::select( $names[1], $regions, 'id', 'title', 'id', isset( $selected_ids[1] ) ? $selected_ids[1] : null, isset( $required[1] ) ? $required[1] : null, 'Περιοχή' );
	}
	
	public static function publish( $val = '1', $required = true )
	{
		return self::selectBoolean( 'is_published', $val, $required, 'Ενεργό' );
	}

    public static function favorite( $val = '0', $required = false )
    {
        return self::selectBoolean( 'is_favorite', $val, $required, 'Διακεκριμένο' );
    }

	public static function alias( $val = '', $lang = null, $label = false, $required = false )
	{
        $s = '';
        if ($label) {
            $s .= HTML_Tag::closed( 'label', null, ( $required ? ' <b>&#176;</b> ' : '' ) . 'Alias' );  
        }
		return self::element( $s . HTML_Tag::closed( 'input', array( 
			'name' => ! $lang ? 'alias' : "local[{$lang->id}][alias]",
			'value' => htmlspecialchars( $val ),
            'placeholder' => 'Αν δεν δοθεί δημιουργείται αυτόματα'
		) ) );
	}
	
	public static function title( $val = '', $required = false, $lang = null )
	{
		$s  = HTML_Tag::closed( 'label', null, ( $required ? ' <b>&#176;</b> ' : '' ) . 'Τίτλος' );
		return self::element( $s . HTML_Tag::closed( 'input', array( 
			'type' => 'text', 
			'name' => ! $lang ? 'title' : "local[{$lang->id}][title]", 
			'value' => htmlspecialchars( $val ),
			'isRequired' => $required ? 'yes' : 'no'
		) ) );
	}
	
	public static function description( $val = '', $required = false, $lang = null )
	{
		$s  = HTML_Tag::closed( 'label', null, ( $required ? ' <b>&#176;</b> ' : '' ) . 'Περιγραφή' );
		return self::element( $s . HTML_Tag::closed( 'textarea', array(
				'name' => ! $lang ? 'description' : "local[{$lang->id}][description]", 
				'isRequired' => $required ? 'yes' : 'no'
			), htmlspecialchars( $val )
		) );
	}
	
	public static function shortDescription( $val = '', $required = false, $lang = null )
	{
		$s  = HTML_Tag::closed( 'label', null, ( $required ? ' <b>&#176;</b> ' : '' ) . 'Σύντομη περιγραφή' );
		return self::element( $s . HTML_Tag::closed( 'textarea', array(
				'name' => ! $lang ? 'short_description' : "local[{$lang->id}][short_description]", 
				'class' => 'small'
			), htmlspecialchars( $val )
		) );
	}
	
	public static function text( $val = '', $lang = null )
	{
		if( ! self::$tinyMCE_init ) { /* Δεν μπορεί να χρησιμοποιηθεί ως Bundle με άλλα αρχεία στο header γιαυτό γίνεται echo εδώ */
			self::$tinyMCE_init = true;
			Core_HTML::addFullHeaders( array(
				"<script type='text/javascript' src='" . PATH_REL_ASSETS . "js/library/tinymce/js/tinymce/tinymce.min.js'></script>",
				"<script type='text/javascript' src='" . PATH_REL_ASSETS . "js/library/tinymce/tinyMCEConfig.js'></script>"
			) );
		}
		return self::textarea( 
			! $lang ? 'text' : "local[{$lang->id}][text]", 
			htmlspecialchars( $val ), 
			false, 
			array(
				'class' => 'editorIdentifyClass tinyMCE', 
			), 'Κυρίως κείμενο'
		);
	}

    public static function file( $name, $val = '', $required = false, $attrs = null, $label = null, $wrapper_attrs = null )
    {
        $s = '';
        if( $label ) {
            $s .= self::label( $label, $required );
        }
        return self::element( $s . HTML_Tag::void( 'input', array_merge( array(
                'type' => 'file',
                'name' => $name,
                'value' => $val ? htmlspecialchars( $val ) : '',
                'isRequired' => $required ? 'yes' : 'no'
            ), ( array ) $attrs
            ) ), $wrapper_attrs );
    }
	
	public static function image( $tb_name = null, $image = null )
	{
		Core_HTML::addFiles( array(
			'admin\form_images.css',
			'js\library\popupImage.css',
			'js\library\popupImage.js',
			'library\popups.js'	
		) ); 
		$s = '';
		if( $tb_name && $image ) {
			$dir = PATH_REL_IMAGES . 'db/' . $tb_name;
			$s .= "<div class='img_wrapper'>";
			$s .= "  <div class='item'>";  
			$s .= HTML_Tag::void( 'img', array(
				'src' => "{$dir}/thumbnails/thumb_{$image}",
				'onclick' => "_popupImage( '{$dir}/{$image}' )",
				'class' => 'published'
			) );
			$s .= "    <div class='img_actions'>";
			$s .= '<div>';
			$s .= HTML_Tag::closed( 'label', array( 'class' => 'img_path', 'onclick' => "window.prompt( '&nbsp; Αντιγράψτε την διαδρομή της εικόνας πατώντας Ctrl+C &nbsp;', '{$dir}/{$image}' )" ), "Διαδρομή" );
			$s .= '</div>';
			$s .= '<div>';
			$s .= HTML_Tag::closed( 'input', array( 
				'type' => 'checkbox', 
				'name' => 'delete_image', 
				'class' => 'checkbox'
			) );
			$s .= HTML_Tag::closed( 'label', array( 'class' => 'checkbox' ), 'Διαγραφή' );
			$s .= '</div>';
			$s .= "    </div>";
			$s .= "  </div>";
			$s .= "</div>";		
		}
		$s .= HTML_Tag::closed( 'label', null, '<small>επιτρέπονται αρχεία png, gif, jpg, jpeg <br/>π.χ. photo.jpg</small>' );
		$option = new Model_Yiama_Option();
		$options = $option->getByKey( 'image' );
		if( isset( $options->size->normal ) && isset( $options->size->thumbnail ) ) {
			list( $normal_def_width, $normal_def_height ) = $options->size->normal;
			list( $thumb_def_width, $thumb_def_height ) = $options->size->thumbnail;
		} else {
			list( $normal_def_width, $normal_def_height ) = ( object ) array( '', '' );
			list( $thumb_def_width, $thumb_def_height ) = ( object ) array( '', '' );
		}
		$s .= self::input( "resize[normal][width]", null, null, null, array( 'Μέγιστο πλάτος μεγάλης', 'Προεπιλεγμένο: ' . $normal_def_width . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[normal][height]", null, null, null, array( 'Μέγιστο ύψος μεγάλης', 'Προεπιλεγμένο: ' . $normal_def_height . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[thumb][width]", null, null, null, array( 'Μέγιστο πλάτος μικρής', 'Προεπιλεγμένο: ' . $thumb_def_width . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[thumb][height]", null, null, null, array( 'Μέγιστο ύψος μικρής', 'Προεπιλεγμένο: ' . $thumb_def_height . 'px' ), array( 'class' => 'inline' ) );
		return self::element( $s . self::imageButton( 'png,gif,jpg,jpeg' ) );
	}
	
	public static function images( $tb_name = null, $images = array() )
	{
		Core_HTML::addFiles( array(
			'admin\form_images.css',
			'js\library\popupImage.css',
			'library\popups.js'	
		) ); 
		$s = '';
		if( $tb_name && ! empty( $images ) ) {
			$dir = PATH_REL_IMAGES . 'db/' . $tb_name;
			$s .= "<div class='img_wrapper'>";
			foreach( $images as $img )
			{
				$s .= "  <div class='item'>";  
				$s .= HTML_Tag::void( 'img', array(
					'src' => "{$dir}/thumbnails/thumb_{$img->name}",
					'onclick' => "_popupImage( '{$dir}/{$img->name}' )",
					'class' => ( $img->is_published ? ' published' : '' ) . ( $img->is_default ? ' default' : '' )
				) );
				$s .= "    <div class='img_actions'>";
				$s .= '<div>';
				$s .= HTML_Tag::void( 'input', array( 'name' => "alt_images[{$img->id}]", 'value' => $img->alt, 'class' => 'img_alt' ) );
				$s .= '</div>';
				$s .= '<div>';
				$s .= HTML_Tag::closed( 'label', array( 'class' => 'img_path', 'onclick' => "window.prompt( '&nbsp; Αντιγράψτε την διαδρομή της εικόνας πατώντας Ctrl+C &nbsp;', '{$dir}/{$img->name}' )" ), "Διαδρομή" );
				$array = array(
					'type' => 'radio', 
					'name' => 'default_image', 
					'value' => $img->id, 
					'class' => 'radio' 
				);
				if( $img->is_default ) {
					$array['checked'] = 'checked';
				}
				$s .= '</div>';
				$s .= '<div>';
				$s .= HTML_Tag::closed( 'input', $array ); 
				$s .= HTML_Tag::closed( 'label', array( 'class' => 'radio' ), 'Προεπ/μένη' );
				$array = array( 
					'type' => 'checkbox', 
					'name' => 'publish_images[]', 
					'value' => $img->id, 
					'class' => 'checkbox' 
				);
				if( $img->is_published ) {
					$array['checked'] = 'checked';
				}
				$s .= '</div>';
				$s .= '<div>';
				$s .= HTML_Tag::closed( 'input', $array ); 
				$s .= HTML_Tag::closed( 'label', array( 'class' => 'radio' ), 'Ενεργή' );
				$s .= '</div>';
				$s .= '<div>';
				$s .= HTML_Tag::closed( 'input', array( 
					'type' => 'checkbox', 
					'name' => 'delete_images[]', 
					'value' => $img->id, 
					'class' => 'checkbox' 
				) );
				$s .= HTML_Tag::closed( 'label', array( 'class' => 'checkbox' ), 'Διαγραφή' );
				$s .= '</div>';
				$s .= "    </div>";
				$s .= "  </div>";
			}
			$s .= "</div>";
		}
		$s .= HTML_Tag::closed( 'label', null, '<small>επιτρέπονται αρχεία png, gif, jpg, jpeg <br/>π.χ. photo.jpg</small>');
		$option = new Model_Yiama_Option();
		$options = $option->getByKey( 'image' );
		if( isset( $options->size->normal ) && isset( $options->size->thumbnail ) ) {
			list( $normal_def_width, $normal_def_height ) = $options->size->normal;
			list( $thumb_def_width, $thumb_def_height ) = $options->size->thumbnail;
		} else {
			list( $normal_def_width, $normal_def_height ) = ( object ) array( '', '' );
			list( $thumb_def_width, $thumb_def_height ) = ( object ) array( '', '' );
		}
		$s .= self::input( "resize[normal][width]", null, null, null, array( 'Μέγιστο πλάτος μεγάλης', 'Προεπιλεγμένο: ' . $normal_def_width . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[normal][height]", null, null, null, array( 'Μέγιστο ύψος μεγάλης', 'Προεπιλεγμένο: ' . $normal_def_height . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[thumb][width]", null, null, null, array( 'Μέγιστο πλάτος μικρής', 'Προεπιλεγμένο: ' . $thumb_def_width . 'px' ), array( 'class' => 'inline' ) );
		$s .= self::input( "resize[thumb][height]", null, null, null, array( 'Μέγιστο ύψος μικρής', 'Προεπιλεγμένο: ' . $thumb_def_height . 'px' ), array( 'class' => 'inline' ) );
		return self::element( $s . self::imageButton( 'png,gif,jpg,jpeg', true ) );
	}
	
	public static function imageButton( $extensions, $many = false )
	{	
		Core_HTML::addFiles( 'admin\form_images.css' ); 
		$s = HTML_Tag::closed( 'span', array( 'class' => 'text' ), 'Εικόνα' );
		$s .= HTML_Tag::void( 'input', array(
			'type' => 'file',
			'name' => 'images[]', 
			'class' => 'button', 
			'onchange' => "
				if( ! validExtensions( this, '{$extensions}' ) ) {
					return;
				} else {
					this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path show' );
					this.parentNode.getElementsByTagName( 'input' )[1].value = this.value.toLowerCase();
					this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove show' );
					" . ( $many ? "newImgButton( this.parentNode.parentNode );" : "" ) . "
				}"
		) );
		$s .= HTML_Tag::void( 'input', array( 'class' => 'text', 'disabled' => 'disabled', 'class' => 'path' ), '-----------' );
		$s .= HTML_Tag::closed( 'span', array(
			'class' => 'remove',
			'onclick' => "
				this.parentNode.getElementsByTagName( 'input' )[0].value = '';
				this.parentNode.getElementsByTagName( 'input' )[1].setAttribute( 'class', 'path' );
				this.parentNode.getElementsByTagName( 'span' )[1].setAttribute( 'class', 'remove' );
				removeImgButton( this.parentNode );
			" ) );
		$div = HTML_Tag::closed( 'div', array( 'class' => 'imgbutton', 'data-status' => 'static' ), $s );
		Core_HTML::addJs( "
			function newImgButton( wrapper )
			{
				var div = document.createElement( 'div' );
				div.setAttribute( 'class', 'imgbutton' );
				div.setAttribute( 'data-status', 'removable' );
				div.innerHTML = \"" . str_replace( array( '"', "\r", "\n", "\r\n" ), array( '\\"', '', '', '' ), $s ) . "\";
				wrapper.appendChild( div );
			}
			function removeImgButton( element )
			{
				if( element.getAttribute( 'data-status' ) && element.getAttribute( 'data-status' ) == 'removable' ) {
					element.parentNode.removeChild( element );
				}
			}
		" );
		return $div;
	}
	
	public static function addButton( $content, $many = false )
	{
		$add_but = HTML_Tag::closed( 'a', array( 
				'class' => 'add_button',
				'onclick' => "newAddButtonContent_" . self::$add_button_indx . "( this );"
			), '&#43; Πρόσθεση'
		);
		$remove_but = HTML_Tag::closed( 'div', 
			array( 'class' => 'add_button_header' ),
			HTML_Tag::closed( 'span', array(
				'onclick' => "this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode );"
			)
		) );
		Core_HTML::addJs( "
			function newAddButtonContent_" . self::$add_button_indx . "( element )
			{
				var div = document.createElement( 'div' ); 
				div.setAttribute( 'class', 'add_button' );
				div.innerHTML = \"" . str_replace( array( '"', "\r", "\n", "\r\n" ), array( '\\"', '', '', '' ), $remove_but . $content ) . "\";
				element.parentNode.appendChild( div );
				var a = document.createElement( 'a' ); 
				a.setAttribute( 'class', 'add_button' );
				a.setAttribute( 'onclick', 'newAddButtonContent_" . self::$add_button_indx . "( this )' );
				a.innerHTML = '&#43; Πρόσθεση';
				element.parentNode.appendChild( a );
				element.parentNode.removeChild( element );
			}
		" );
		self::$add_button_indx++;
		return self::element( $add_but );
	}
	
	public static function meta( $vals = array(), $lang = null )
	{
		$els = '';
		$s = HTML_Tag::closed( 'label', null, 'Meta τίτλος' );
		$s .= HTML_Tag::closed( 'input', array( 
			'name' => ! $lang ? 'meta_title' : "local[{$lang->id}][meta_title]", 
			'value' => isset( $vals['title'] ) ? htmlspecialchars( $vals['title'] ) : '',
			'invalidValues' => 'valSymbols'
		) );
		$els .= self::element( $s );
		$s = HTML_Tag::closed( 'label', null, 'Meta περιγραφή' );
		$s .= HTML_Tag::closed( 'input', array(
			'name' => ! $lang ? 'meta_description' : "local[{$lang->id}][meta_description]", 
			'value' => isset( $vals['description'] ) ? htmlspecialchars( $vals['description'] ) : '',
			'invalidValues' => 'valSymbols'
		) );
		$els .= self::element( $s );
		$s = HTML_Tag::closed( 'label', null, 'Meta λέξεις' );
		$s .= HTML_Tag::closed( 'input', array( 
			'name' => ! $lang ? 'meta_keywords' : "local[{$lang->id}][meta_keywords]",
			'value' => isset( $vals['keywords'] ) ? htmlspecialchars( $vals['keywords'] ) : '',
			'invalidValues' => 'valSymbols'
		) );
		$els .= self::element( $s );
		return $els;
	}
	
	/*
	 * @param $tree model array
	 * @param $name input name
	 * @param $dynamic null|1{Î½Î± Î±Ï€ÎµÎ½ÎµÏ�Î³Î¿Ï€Î¿Î¹ÎµÎ¯Ï„Î±Î¹ ÏŒÏ„Î±Î½ Î­Ï‡ÎµÎ¹ children}|0{Î½Î± Î±Ï€ÎµÎ½ÎµÏ�Î³Î¿Ï€Î¿Î¹ÎµÎ¯Ï„Î±Î¹ ÏŒÏ„Î±Î½ Î´ÎµÎ½ Î­Ï‡ÎµÎ¹ children}
	 * @param $required boolean
	 * @param $label self::label[options]
	 */
	public static function selectTree( $tree, $name, $tree_row_ids = null, $required = false, $label = null, $select_attrs = null, $wrapper_attrs = null )
	{
		$s = '';
		if( $label ) {
			$s .= self::label( $label, $required );
		}
		$options = array();
        $l = is_array($label) ? $label[0] : $label;
		$options[] = HTML_Tag::closed(
            'option', 
            array('value' => ''),
            $label ? $l : ''
        ); 
		foreach( $tree as $t ) {
			$attrs = array();
			$attrs['value'] = $t->id;
			if( isset($t->tree_disabled ) ) {
				if( ! $t->tree_disabled ) {
					$attrs['style'] = 'background-color: #fafafa; color: #44aaaa';
				} else {
					$attrs['disabled'] = 'disabled';
				}
			}
			if( !empty($tree_row_ids)) {
                foreach ((array)$tree_row_ids as $tree_row_id) {
                    if ($t->id == $tree_row_id) {
                        $attrs['style'] = 'background-color: #fafafa; color: #aa4444';
                        $attrs['selected'] = 'selected';
                    }
                }
            }
			$options[] = HTML_Tag::closed( 'option', $attrs, str_repeat( "&#8722; ", $t->tree_level-1 ) . $t->title ); 
		}
		return self::element( $s . HTML_Tag::closed( 'select', array_merge( array( 
					'name' => $name,
					'isRequired' => $required ? 'yes' : 'no'
				), ( array ) $select_attrs 
			), implode( '', $options )
		), $wrapper_attrs );
	}
    
    public static function inputAjax(
        $elementDomId, 
        $name, 
        $url,
        $vals = array(), 
        $required = false,
        $attrs = null,
        $label = null,
        $wrapper_attrs = null
    ) {
        $js_var = 'inputAjax' . $elementDomId;
		if( ! self::$ajax_input_initialized ) {
			self::$ajax_input_initialized = true;
            Core_HTML::addFiles( array(
                'library\ajax.js'
            ) );
			Core_HTML::addJs( "
				function inputAjax(domElementId, name, url) {
                    this.domElement;
                    this.domContainer;
                    this.editedItem = '';
                    this.editedItemName = name;
                    this.ajaxUrl = url;
                    var inst = this;
                    
                    this.init = function() {
                        this.domContainer = document.getElementById(domElementId 
                            + 'Container');
                        this.domElement = document.getElementById(domElementId);
                                   
                        // Start items
                        
                        
                        // Onkeypress
                        this.domElement.addEventListener('keypress', function(event) {
                            // On 'enter' keypress set  value
                            if (event.keyCode == 13) {
                                event.preventDefault();
                                // Value
                                var value = inst.domElement.previousSibling.placeholder != ''
                                    ? inst.domElement.previousSibling.placeholder
                                    : inst.domElement.value.trim()
                                // Check if there is already an input with this
                                // value or value is empty
                                if (value == '') {
                                    return;
                                }
                                var items = inst.domContainer.children;
                                for (i = 0; i < items.length; i++) {
                                    if (items[i].children[0].value == value) {
                                        return;
                                    }
                                }
                                inst.newItem(value);
                                inst.domElement.value = '';
                                inst.domElement.previousSibling.placeholder = '';
                            } // Make request
                            else {
                                inst.request(inst.domElement.value 
                                    + String.fromCharCode(event.keyCode));
                                inst.domElement.previousSibling.placeholder = '';
                            }
                            event.stopPropagation();
                        });
                    }
                    
                    // Request
                    this.request = function(value) {
                        _ajaxRequest({
                            method: 'post',
                            url: inst.ajaxUrl,
                            querystring: 'value=' + value,
                            async: true,
                            successhandler: function(response){
                                if (response != '') {
                                    var response = JSON.parse(response.trim());
                                    if (response.status) {
                                        inst.domElement.previousSibling
                                            .placeholder = response.value;
                                    }
                                }
                            }
                        });
                    }
                    
                    // New item
                    this.newItem = function(value) {
                        this.editedItem = new inputAjaxItem();
                        this.editedItem.init(
                            this.editedItemName,
                            value,
                            this.domContainer
                        );
                        this.domContainer.appendChild(this.editedItem.domElement);
                    }
				}
                
                function inputAjaxItem() {
                    this.domElement;
                    var inst = this;
                    
                    this.init = function(name, value, container) {
                        this.domElement = document.createElement('div');
                        this.domElement.className = 'ajax-input-item';
                        this.domElement.innerHTML = value;
                        var input = document.createElement('input');
                        input.name = name;
                        input.value = value;
                        input.type = 'hidden';
                        this.domElement.appendChild(input);
                        var close = document.createElement('div');
                        close.className = 'ajax-input-item-close';
                        this.domElement.appendChild(close);
                        close.onclick = function() {
                            inst.domElement.parentNode.removeChild(
                                inst.domElement
                            );
                        }
                        container.appendChild(this.domElement);
                    }
                }
            " );
        }
        Core_HTML::addJs( " 
            window.addEventListener('load', function() {
                var " . $js_var . " = new inputAjax(
                    '" . $elementDomId . "',
                    '" . $name . "',
                    '" . $url . "',
                );
                " . $js_var . ".init();
            });
        " );
        // Create startup items
        $items = array();
        if (!empty($vals)) {
            foreach ($vals as $val) {
                $items[] = HTML_Tag::closed('div', 
                    array('class' => 'ajax-input-item'),
                    $val
                  . HTML_Tag::closed('input', array(
                        'name' => $name,
                        'value' => $val,
                        'type' => 'hidden'
                    ))
                  . HTML_Tag::closed('div', array(
                        'class' => 'ajax-input-item-close',
                        'onclick' => 'this.parentNode.parentNode.removeChild(
                            this.parentNode
                        );'
                    ))
                );
            }
        }
        return 
            self::element(
                self::label((array) $label, $required)
                . HTML_Tag::closed('div', array('class' => 'ajax-input-wrapper'),
                    HTML_Tag::closed('input', array(
                        'class' => 'ajax-input-placeholder'
                    ))
                    . HTML_Tag::closed('input', array(
                        'class' => 'ajax-input',
                        'id' => $elementDomId,
                        'autocomplete' => 'off'
                    ))
                    . HTML_Tag::closed('div', array(
                        'class' => 'ajax-input-container', 
                        'id' => $elementDomId . 'Container'
                    ), implode('', $items))
                )
            );
    } 
    
	/*
	public static function discountRules( $element_name, $rule_properties, $rule_operators, $popups )
	{
		if( ! self::$discountRules_init ) {
			self::$discountRules_init = true;
			Core_HTML::addFiles( 'admin\button.css' );
			Helper_Popup::confirm( "{$element_name}Popup" );
			$addJs = "
				function {$element_name}OnClick( element ) {
					/* Briskoume to antistoixo rule_property pou exei epilegei */
					/*var prop_selects = element.form.elements['rule_properties[]'];
					if( prop_selects instanceof NodeList ) {
						for( i = 0; i < prop_selects.length; i++ ) {
							if( prop_selects[i].parentNode.parentNode == element.parentNode.parentNode ) {
								var rule_property_value = prop_selects[i].options[ prop_selects[i].selectedIndex ].value;
								element.setAttribute( 'id', 'rule_value_' + i );
								break;
							}
						}
					} else {
						element.setAttribute( 'id', 'rule_value_0' );
						var rule_property_value = prop_selects.options[ prop_selects.selectedIndex ].value;
				}
			";
			/* basei tou 'rule_property' pou einai epilegmeno, otan ginetai 'click' sto 'rule_value' emfanizetai 'popup' */
			/*foreach( $popups as $rule_property_value => $popup_body ) { 
				$addJs .= " if( rule_property_value == '{$rule_property_value}' ) {
						set" . ucfirst( $element_name ) . "Popup( '', '{$popup_body}', '_get( \'' + element.getAttribute( 'id' ) + '\' ).value=_getMultipleSelectedOptionValues( {$element_name}Popup.getWrapper().getElementsByTagName( \'select\' )[0] ).join(); {$element_name}Popup.unpop()' ); {$element_name}Popup.pop();
					}";
			}
			$addJs .= "	}
			";
			Core_HTML::addJs( $addJs );			
		}
		return Helper_Form::select( 'rule_properties[]', $rule_properties, 'value', 'text', null, null, true, 'Ιδιότητα', null, null, null, array( 'class' => 'small inline', 'style' => 'width: 20%', 'onchange' => "this.parentNode.getElementsByTagName('input')[0].value='';" ) )
		. Helper_Form::select( 'rule_operators[]', $rule_operators, 'value', 'text', null, null, true, 'Τελεστής', null, null, null, array( 'class' => 'small inline', 'style' => 'width: 20%' ) )
		. Helper_Form::input( 'rule_values[]', null, true, array( 'onclick' => "{$element_name}OnClick( this );" ), 'Τιμή', array( 'class' => 'small inline', 'style' => 'width: 20%' ) ); 	
	}*/
}
?>