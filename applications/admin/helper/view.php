<?php
class Helper_View extends Core_Controller
{
	public static function form( $controller, $vars = null )
	{
		$c = $controller;
		$c->setView( dirname( __DIR__ ) . DS . 'views' .DS . 'form' );
        $c->view->addVars($vars);
		$c->setTemplate();
        if (isset($vars['buttons'])) {
            $c->template->addVars(array('buttons' => $vars['buttons']));
        }
		return $c->template;
	}
	public static function catalogue( $controller, $vars )
	{
		$c = $controller;
		$c->setView( dirname( __DIR__ ) . DS . 'views' .DS . 'catalogue' );
        $c->view->addVars($vars);
		$c->setTemplate();
        if (isset($vars['buttons'])) {
            $c->template->addVars(array('buttons' => $vars['buttons']));
        }
		return $c->template;
	}
	public static function statistics( $controller, $vars = null )
	{
		$c = $controller;
		$c->setView( dirname( __DIR__ ) . DS . 'views' .DS . 'form' );
        $c->view->addVars($vars);
		$c->setTemplate();
        if (isset($vars['buttons'])) {
            $c->template->addVars(array('buttons' => $vars['buttons']));
        }
		return $c->template;
	}
}
?>