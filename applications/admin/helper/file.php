<?php
class Helper_File
{
    public static function subDirectoriesOf($path) {
        $paths = glob($path . '/*' , GLOB_ONLYDIR);
        if (!empty($paths)) {
            foreach ($paths as $p) {
                $paths = array_merge($paths, self::subDirectoriesOf($p));
            }
        }
        return $paths;
    }
}