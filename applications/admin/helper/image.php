<?php
 
class Helper_Image extends Core_Controller
{
	public static function upload( $tb_name, $db_names, $name, $tmp_name )
	{
		ini_set( 'max_execution_time', 300 );
		$option = new Model_Yiama_Option();
		$options = $option->getByKey( 'image' );
		if( isset( $options->size->normal ) && isset( $options->size->thumbnail ) ) {
			list( $normal_width, $normal_height ) = $options->size->normal;
			list( $thumb_width, $thumb_height ) = $options->size->thumbnail;
		} else {
			return false;
		}
		$dir = PATH_ASSETS . 'images' . DS . 'db' . DS . $tb_name;
		if( ! is_dir( $dir )  ) mkdir( $dir );
		if( ! is_dir( $dir . DS . 'thumbnails' )  ) mkdir( $dir . DS . 'thumbnails' );
		
		$ext = strtolower( pathinfo( $name, PATHINFO_EXTENSION ) );
		do { /* Create db name */
			$new_name = "";
			$i = 0;
			while( $i++ < 5 ) {
				$new_name .= chr( rand( 97, 122 ) );
			}
			$new_name .= '.' . $ext;
		} while( in_array( $new_name, $db_names ) );
		
		$upload = Upload_Image::upload( array(   /* Upload normal size */ 
			'extension' => $ext,
			'location' => array(
				'from' => $tmp_name,
				'to' => $dir . DS . $new_name
			),
			'resize' => array(
				'max' => array(
					'w' => ( $w = Core_Request::getInstance()->getParams( 'resize', 'normal.width' ) ) ? $w : $normal_width,
					'h' => ( $h = Core_Request::getInstance()->getParams( 'resize', 'normal.height' ) ) ? $h : $normal_height
				)
			)
		) );
		if( ! $upload ) {
			return false;
		} else {
			$upload = Upload_Image::upload( array( /* Upload thumbnail size */
				'extension' => $ext,
				'location' => array(
					'from' => $tmp_name,
					'to' => $dir . DS . 'thumbnails' . DS . "thumb_{$new_name}"
				),
				'resize' => array(
					'max' => array(
						'w' => ( $w = Core_Request::getInstance()->getParams( 'resize', 'thumb.width' ) ) ? $w : $thumb_width,
						'h' => ( $h = Core_Request::getInstance()->getParams( 'resize', 'thumb.height' ) ) ? $h : $thumb_height
					)
				)
			) );
		}
		return $upload ? $new_name : false;
	}
	
	public static function unlink( $tb_name, $image )
	{
		$dir = PATH_ASSETS . 'images' . DS . 'db' . DS . $tb_name . DS;
		$file = $dir . $image;
		if( ! @unlink( $file ) || file_exists( $file ) ) {
			return false;
		}
		@unlink( $dir . 'thumbnails' . DS . "thumb_{$image}" );
		return true;
	}
}

?>