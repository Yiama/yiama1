<?php
class Helper_Before {
    
    public static function checkLoggedUser() {
        $user = new Model_Yiama_User();
        if( ! ( $logged_user = $user->getLogged() )) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'user',
                        'action' => 'loginform' 
                    )
                )
            );
        }
    }
    
    /**
     * @param array $action_methods ['action' => 'method']
     */
    public static function checkRequestMethod($action_methods) {
        $action = Core_Request::getInstance()->getAction();
        $method = strtolower(Core_Request::getInstance()->getMethod());
        foreach ($action_methods as $act => $meth) {
            if (    ($act == $action || $act == '*')
                &&  $meth != $method) {
                Core_Response::getInstance()->redirect(
                    Core_Request::getInstance()->url(
                        'cont_act_id',
                        array( 
                            'controller' => 'error',
                            'action' => 'notfound' 
                        )
                    )
                );
            }
        }
    }

    /**
     * @param array $params
     */
    public static function checkCsrf(array $params) {
        $csrf = new Security_Csrf(Core_Session::getInstance());
        if (    empty($params[$csrf->getRequestTokenName()])
            ||  !$csrf->isValid($params[$csrf->getRequestTokenName()])) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'error',
                        'action' => 'notfound' 
                    )
                )
            );
        }
    }
}