<?php
	class Helper_Table
	{
		private static $header;
		private static $content;
		private static $initialized = false;
		
		public static function setHeaders( $headers )
		{
			$h = '<tr>';
			foreach( $headers as $val ) {
				$h .= '<th>' . $val . '</th>';
			}
			$h .= '</tr>';
			self::$headers = $h;
		}
		
		public static function setContent( $content )
		{
			$c = '';
			foreach( $content as $row ) {
				$c .= '<tr>';
				foreach( $row as $r ) {
					$c .= '<td>' . $r . '</td>';
				}
				$c .= '</tr>';
			}
			self::$content = $c;
		}
		
		public static function get( $header = null )
		{
			if( ! self::$initialized ) {
				Core_HTML::addFiles('admin\table.css');
				self::$initialized = true;
			}
			return HTML_Tag::open( 'table', array( 'class' => 'view' ) )
				. ( $header ? "<caption>{$header}</caption>" : "" )
				. ( ! empty( self::$headers ) ? self::$headers : '' )
				. self::$content
				. HTML_Tag::close( 'table' );
		}
	}
?>