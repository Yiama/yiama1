<?php
	class Model_Yiama_Subscription extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_subscribers_subscriptiontypes';
        protected static $primary_key = array( 'ym_subscribers_id', 'ym_subscriptiontypes_id' );
        protected static $relations = array(
            'type' => array(
                'type' => 'one_to_one',
                'model_name' => 'Model_Yiama_Subscriptiontype',
                'foreign_key' => array( 'ym_subscriptiontypes_id' => 'id' )
            ),
            'subscriber' => array(
                'type' => 'one_to_one',
                'model_name' => 'Model_Yiama_Subscriber',
                'foreign_key' => array( 'ym_subscribers_id' => 'id' )
            )
        );
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>