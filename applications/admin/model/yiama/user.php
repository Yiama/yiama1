<?php
	class Model_Yiama_User extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_users';
		protected static $primary_key = 'id';
		private static $logged_user;
		protected static $relations = array(
			'roles' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Role',
				'foreign_key' => array( 'id' => 'ym_users_roles.ym_users_id' ),
				'join' => array( array( 'ym_users_roles', 'ym_users_roles.ym_roles_id = ym_roles.id' ) )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->order( 'username ASC' );
		}
	
		/*
		 * @param $password string
		 */
		public function hash( $password )
		{
			/* Με το base64 έχει έγκυρους χαρακτήρες για να εισαχθεί σε string πεδίο στην Βάση */
			$salt = bin2hex( Core_App::getConfig( 'security.salt' ) );
			return hash( 'sha256', $password . $salt );
		}

        public function getLogged()
        {
            if( ! empty( self::$logged_user ) && self::$logged_user->login_token_expiration > time() && self::$logged_user->login_token == Core_Cookie::get( 'ym_user', true ) ) {
                return self::$logged_user;
            } else {
                return self::$logged_user = $this->query()
                    ->search( array(
                        'login_token IS NOT NULL',
                        "login_token = '" . Core_Cookie::get( 'ym_user', true ) . "'",
                        "login_token_expiration > " . time()
                    ) )
                    ->combine( 'roles' )
                    ->find( ':first' );
            }
            return false;
        }

        public function login($expiration)
        {
            $tokens = $this->query()
                ->search( "login_token IS NOT NULL" )
                ->find( 'login_token' );
            do{
                $this->login_token = hash( 'sha256', uniqid( mt_rand(), true ) );
            }
            while( in_array( $this->login_token, $tokens ) );
            $this->time_last_login = date("Y-m-d H:i:s");
            $this->time_last_logout = '@null:';
            $this->login_token_expiration = $expiration;
            if( $this->update() ) {
                self::$logged_user = $this;
                Core_Cookie::set( array(
                    'key' => 'ym_user',
                    'value' => $this->login_token,
                    'expire' => 2592000000,
                    'path' => '/',
                    'encrypt' => true
                ) );

                return true;
            }
            return false;
        }
	
		public function logout()
		{
			$this->login_token = '@null:';
            $this->time_last_logout = date("Y-m-d H:i:s");
			if( $this->update() ) {
				self::$logged_user = null;
				Core_Cookie::remove( 'ym_user' );
				return true;
			}
			return false;
		}
	
		public function hasRoles( $roles )
		{
            if (!empty($this->roles)) {
                foreach( $this->roles as $r ) {
                    if( in_array( $r->title, ( array ) $roles ) ) {
                        return true;
                    }
                }
            }
			return false;
		}
	
		public function hasPermissions($permissions)
		{
            if (!empty($this->roles)) {
                    $cnt = 0;
                foreach($this->roles as $r) {
                    $cur_role = $r
                        ->query()
                        ->search("id = " . $r->id)
                        ->combine('permissions')
                        ->find(':first');
                    while (!empty($cur_role) && $cnt++ < 20) {
                        if (!empty($cur_role->permissions)) {
                            foreach($cur_role->permissions as $p) {
                                if(     in_array($p->title, (array)$permissions)
                                    &&  (   (int)$p->state === -1
                                        ||  (int)$p->state === 1)) {
                                    return $p->state > 0;
                                }
                            }
                        }
                        $cur_role = $cur_role
                            ->query()
                            ->search("id = " . $cur_role->parent_id)
                            ->combine('permissions')
                            ->find(':first');
                    }
                }
            }
			return false;
		}
	
		/*
		 * @param $token string { Reset password token που πρέπει να ελεγχθεί }
		 */
		public function isValidResetPassToken( $token )
		{
			if( strtotime( "+1 day", strtotime( $this->reset_password_time ) ) < strtotime( "now" ) /* Αν έχει λήξει η δυνατότητα αλλαγής Password */
			|| $this->reset_password_token != $token ) {
				return false;
			}
			return true;
		}
		
		public function updateResetPassToken()
		{
			$this->reset_password_token = crypt( 'sha256', uniqid( mt_rand(), true ) );
			$this->reset_password_time = date( "Y-m-d H:i:s" );
			$this->update();
		}
        
        public function getImage()
        {
            if(empty($this->image)) {                
                return PATH_REL_ASSETS . 'images' . DS . 'admin' . DS 
                    . 'default' . DS . "user_icon.png";
            } else {
                return PATH_REL_ASSETS . 'images' . DS . 'admin'
                    . DS .  'db' . DS . 'ym_users' . DS . 'thumbnails' . DS 
                    . $this->image;
            }
        }
	}
?>