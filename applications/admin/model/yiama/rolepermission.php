<?php
	class Model_Yiama_Rolepermission extends DB_ActiveRecord_Model
	{
		protected static $table_name = 'ym_roles_permissions';
		protected static $primary_key = array( 'ym_roles_id', 'ym_permissions_id' );
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>