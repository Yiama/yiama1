<?php
	class Model_Yiama_Subscriptiontype extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_subscriptiontypes';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'subscriptions' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Subscription',
				'foreign_key' => array( 'id' => 'ym_subscriptiontypes.ym_subscriptions_types_id' ),
				'combine' => array( 'subscriber' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>