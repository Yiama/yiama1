<?php
	class Model_Yiama_Tag extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_tags';
		protected static $primary_key = 'id';
		
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$language = $model_language->getCurrent();
			$this->lang_id = $language->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
		
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( self::$table_name . '_local AS local', 'local.' . self::$table_name . '_id = ' . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" );
				
		}
	}
?>
