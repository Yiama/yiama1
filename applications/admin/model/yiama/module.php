<?php
	class Model_Yiama_Module extends DB_ActiveRecord_Model
	{
		protected static $table_name = 'ym_modules';
		protected static $primary_key = 'name';
		protected $name;

		public function __construct(string $name)
        {
            parent::__construct();
            $this->name = $name;
        }

        public function data() {
			$result = DB_ActiveRecord_Model::$db_model
				->table(self::$table_name)
				->select('data')
				->search(["name = '" . $this->name . "'"])
				->execute('one');
			if ($result != null) {
				return $result['data'];
			}
			return null;
		}

        public function insertUpdateData(string $data) {
			return DB_ActiveRecord_Model::$db_model
				->table(self::$table_name)
				->insert_update(['name', 'data'], ['data' => $data])
				->values(['name' => $this->name, 'data' => $data])
				->execute();
		}
	}
?>
