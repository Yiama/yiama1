<?php
	class Model_Yiama_Role extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_roles';
		protected static $primary_key = 'id';
		protected static $relations = array(  
			'permissions' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Permission',
				'foreign_key' => array( 'id' => 'ym_roles_permissions.ym_roles_id' ),
				'join' => array( array( 'ym_roles_permissions', 'ym_roles_permissions.ym_permissions_id = ym_permissions.id' ) )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>