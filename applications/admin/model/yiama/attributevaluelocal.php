<?php
	class Model_Yiama_Attributevaluelocal extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_attributes_values_local';
		protected static $primary_key = array( 'ym_attributes_values_id', 'ym_languages_id' );
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>
