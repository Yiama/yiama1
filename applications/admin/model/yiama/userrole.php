<?php
	class Model_Yiama_Userrole extends DB_ActiveRecord_Model
	{
		protected static $table_name = 'ym_users_roles';
		protected static $primary_key = array('ym_users_id', 'ym_roles_id');
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>