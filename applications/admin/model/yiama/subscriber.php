<?php
	class Model_Yiama_Subscriber extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_subscribers';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'subscriptiontypes' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Subscriptiontype',
				'foreign_key' => array( 'id' => 'ym_subscribers_subscriptiontypes.ym_subscribers_id' ),
				'join' => array( array( 'ym_subscribers_subscriptiontypes', 'ym_subscribers_subscriptiontypes.ym_subscriptiontypes_id = ym_subscriptiontypes.id' ) )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>