<?php
	class Model_Yiama_Articleattributevalue extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_articles_attributes_values';
		protected static $primary_key = array( 'ym_articles_id', 'ym_attributes_values_id' );
		protected static $relations = array(  
			'value' => array( 
				'type' => 'one_to_one',
				'model_name' => 'Model_Yiama_Attributevalue',
				'foreign_key' => array( 'ym_attributes_values_id' => 'id' ),
				'combine' => 'attribute'
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>