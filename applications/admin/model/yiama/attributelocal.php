<?php
	class Model_Yiama_Attributelocal extends DB_ActiveRecord_Model
	{		
		protected static $last_inserted_id;
		protected static $table_name = 'ym_attributes_local';
		protected static $primary_key = array( 'ym_attributes_id', 'ym_languages_id' );
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>
