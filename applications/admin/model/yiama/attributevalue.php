<?php
	class Model_Yiama_Attributevalue extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_attributes_values';
		protected static $primary_key = 'id';
		protected static $relations = array(  
			'attribute' => array( 
				'type' => 'many_to_one',
				'model_name' => 'Model_Yiama_Attribute',
				'foreign_key' => array( 'ym_attributes_id' => 'id' )
			)
		);
			
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$this->lang_id = $model_language->getCurrent()->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( 'ym_attributes_values_local AS local', "local.ym_attributes_values_id = " . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" )
				->order( self::$table_name . '.ordered ASC' );
		}
	}
?>