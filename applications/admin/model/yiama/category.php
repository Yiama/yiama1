<?php
	class Model_Yiama_Category extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_categories';
		protected static $primary_key = 'id';
		protected static $relations = array(  
			'articles' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Article',
				'foreign_key' => array( 'id' => 'ym_categories_id' ),
				'order' => 'ordered ASC'
			) 
		);
		
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$this->lang_id = $model_language->getCurrent()->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( self::$table_name . '_local AS local', 'local.' . self::$table_name . '_id = ' . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" )
				->order( 'local.title ASC' );
		}
        
        public function getParentIds($child_id) 
        {
            $result = array();
            if ($child_id == 0) {
                return $result;
            } else {
                $p_id = current(self::query()
                    ->search('id = ' . $child_id)
                    ->find('parent_id'));
                $result = array_merge((array) $child_id, self::getParentIds($p_id));
            }
            return $result;
        }
	}
?>
