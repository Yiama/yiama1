<?php
	class Model_Yiama_Attribute extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_attributes';
		protected static $primary_key = 'id';
		protected static $relations = array(  
			'values' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Attributevalue',
				'foreign_key' => array( 'id' => 'ym_attributes_id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
			$model_language = new Model_Yiama_Language();
			$this->lang_id = $model_language->getCurrent()->id;
			$this->cache_postfix = "lang_{$this->lang_id}";
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->join( self::$table_name . '_local AS local', 'local.' . self::$table_name . '_id = ' . self::$table_name . ".id AND local.ym_languages_id = {$this->lang_id}" )
				->order( self::$table_name . '.ordered ASC' );
		}
		
		public function validateValue( $value )
		{
			switch( $this->attr_type ) {
				case 'float': return floatval( str_replace( ',', '.', $value ) ); break;
				case 'boolean': return intval( $value ); break;
				default: return $value;
			}
		}
	}
?>