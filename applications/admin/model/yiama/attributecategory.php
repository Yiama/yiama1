<?php
	class Model_Yiama_Attributecategory extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_attributes_categories';
		protected static $primary_key = array( 'ym_categories_id', 'ym_attributes_id' );
		protected static $relations = array(   
			'attribute' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Category',
				'foreign_key' => array( 'ym_categories_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>