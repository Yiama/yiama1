<?php
	class Model_Yiama_Articletag extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_articles_tags';
		protected static $primary_key = array( 'ym_articles_id', 'ym_tags_id' );
		protected static $relations = array(   
			'tag' => array( 
				'type' => 'one_to_one',
				'model_name' => 'Model_Yiama_Tag',
				'foreign_key' => array( 'ym_tags_id' => 'id' )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
	}
?>