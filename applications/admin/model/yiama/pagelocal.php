<?php
	class Model_Yiama_Pagelocal extends DB_ActiveRecord_Model
	{		
		protected static $table_name = 'ym_pages_local';
		protected static $primary_key = array( 'ym_pages_id', 'ym_languages_id' );
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
	}
?>
