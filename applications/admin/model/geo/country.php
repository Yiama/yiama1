<?php
	class Model_Geo_Country extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'geo_countries';
		protected static $primary_key = 'id';
		protected static $relations = array(
			'regions' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Geo_Region',
				'foreign_key' => array( 'id' => 'geo_countries_id' ),
				'order' => 'title ASC'
			),
			'languages' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Geo_Countrylanguage',
				'foreign_key' => array( 'id' => 'geo_countries_id' ),
				'order' => 'language_code ASC'
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->order( 'title ASC' );	
		}
	}
?>