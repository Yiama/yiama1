<?php
	class Model_Geo_Currency extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'geo_currencies';
		protected static $primary_key = 'id';
		protected static $default;
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function __destruct(){}
		
		public function getDefault()
		{
			if( ! empty( self::$default ) ) {
				return self::$default;
			}
			return self::$default = $this->query()
				->search( "is_default = 1" )
				->find( ':first' );
		}
	}
?>