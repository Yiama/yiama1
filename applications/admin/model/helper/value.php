<?php
	class Model_Helper_Value
	{
		public static function createOrdered( $model_instance )
		{
			return intval( $model_instance->getLastInsertedId()+1 )*100;
		}
		
		/*
		 * Ελέγχει αν οι ακέραιοι δεν είναι πάνω από 2 και αφήνει μόνο 2 δεκαδικά
		 */
		public static function createPercentage( $value )
		{
			$value = str_replace( ',', '.', $value );
			if( is_numeric( $value ) ) {
				$exp = explode( '.', $value );
				if( strlen( $exp[0] ) < 3 ) {
					$val = $exp[0] . ( isset( $exp[1] ) ? '.' . $exp[1] : '' );
					return round( $val, 2 );	
				}
			}
			return 0;
		}
		
		/*
		 * Ελέγχει αν οι ακέραιοι δεν είναι πάνω από 9 και αφήνει μόνο 2 δεκαδικά
		 */
		public static function createPrice( $value )
		{
			if( is_numeric( $value ) ) {
				$exp = explode( '.', $value );
				if( strlen( $exp[0] ) < 9 ) {
					$val = $exp[0] . ( isset( $exp[1] ) ? '.' . $exp[1] : '' );
					return round( $val, 2 );	
				}
			}
			return 0;
		}
		
		public static function createDate( $value )
		{
			return date( 'Y-m-d H:i:s', strtotime( $value ) );
		}
		
		public static function createAlias( $value, $query_closure = null, $postfix = "" )
		{
            // If is NULL return
            if ($value == '@null:') {
                return $value;
            }
            // If $query_closure is set it means that alias is unique so a check must be
            // made if this value already exists
            if ($query_closure) {
                $pf = '';
                do {
                    $val = Core_URL::sanitize( mb_substr( $value . $pf, 0, 100 ), array( 'filter' => 'url_alias' ) );
                    $result = $query_closure($val);
                    $pf .= "_" . $postfix;
                } while (!empty($result));
                return $val;
            }
			return Core_URL::sanitize( substr( $value, 0, 100 ), array( 'filter' => 'url_alias' ) );
		}
		
		public static function createURL( $model_instance )
		{
			$opt = new Model_Yiama_Option();
			$urls = $opt->getByKey( 'url_types', true );
			$new_url = array();
			foreach( explode( '/', $urls[ $model_instance->getTableName() ] ) as $part ) {
				if( strpos( $part, '#' ) === 0 ) {
					$prop = substr( $part, 1 );
					if( $prop == 'id' ) {
						$new_url[] = $model_instance->getLastInsertedId()+1;
					} else {
						$new_url[] = $model_instance->$prop;
					}
				} else {
					$new_url[] = $part;
				}
			}
			return implode( '/', $new_url );
		}
	}
?>