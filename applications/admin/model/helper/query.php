<?php
	class Model_Helper_Query
	{
		public static function search($params = null, $session_field = null)
		{
            $param_search = array();
            $query_search = Core_Request::getInstance()->getParams( 'search' );
            $session_search = Core_Session::getInstance()->get('search');
			if(!empty($query_search)) {
                $session_search[$session_field] = $param_search = $query_search;
            } elseif (!empty($session_search[$session_field])) {
                $param_search = $session_search[$session_field];
            }
            Core_Session::getInstance()->set('search', $session_search);
			$search = array();
            if (!empty($param_search)) {
				foreach( $param_search as $key => $val ) {
					if( is_array( $val ) ) {
						$key_val = key( $val );
						if( !empty( $val['from'] ) ) {
							$search[] = "{$key} >= '{$val['from']}'";
						}
						if( !empty( $val['to'] ) ) {
							$search[] = "{$key} <= '{$val['to']}'";
						}
						if( !empty( $val['date_from'] ) ) {
							$search[] = "DATE({$key}) >= DATE('{$val['date_from']}')";
						}
						if( !empty( $val['date_to'] ) ) {
							$search[] = "DATE({$key}) <= DATE('{$val['date_to']}')";
						}
						if( !empty( $val['like'] ) ) {
							$search[] = "{$key} LIKE '%{$val['like']}%'";
						}
					} elseif ($val != '') {
						$search[] = "{$key} = '{$val}'";
					}
				}
			}
			if( $params ) {
				$search = array_merge( $search, ( array ) $params );
			}
			return $search;
		}
		
		public static function limit( $limit = null )
		{
			$page = ( $p = Core_Request::getInstance()->getParams( 'page' ) ) ? $p : 1;
			if( is_null( $limit ) ) {
				$config_params = Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.modules.pagination' );
				$limit = $config_params['content_per_page'];
			}
			return ( $page-1 )*$limit . ',' . $limit;
		}
		
		public static function order()
		{
			$order = Core_Request::getInstance()->getParams( 'order' );
			return ! empty( $order ) ? $order[0] . ' ' . $order[1] : '';
		}
	}
?>