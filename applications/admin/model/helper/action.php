<?php
	class Model_Helper_Action
	{
		public static function deleteLocal( $tb_name, $id )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name . '_local' )
				->delete()
				->search( $tb_name . '_id = ' . $id )
				->execute();
		}
		
		public static function deleteImages( $tb_name, $id )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name . '_images' )
				->delete()
				->search( $tb_name . '_id = ' . $id )
				->execute();
		}
		
		public static function deleteById( $tb_name, $id )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->delete()
				->search( 'id = ' . $id )
				->execute();
		}
		
		public static function setDefault( $tb_name, $id, $search )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->update( array( 'is_default' => '@func:IF( id = ' . $id . ', 1, 0 )' ) )
				->search( $search )
				->execute();
		}
		
		/*
		 * @param $ids comma_seperated
		 */
		public static function groupPublish( $tb_name, $ids, $search )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->update( array( 'is_published' => '@func:IF( id IN( ' . ( empty( $ids ) ? '-1': $ids )  . ' ), 1, 0 )' ) )
				->search( $search )
				->execute();
		}
		
		public static function publish( $tb_name, $id )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->update( array( 'is_published' => '@func:IF( is_published = 1, 0, 1 )' ) )
				->search( "id = {$id}" )
				->execute();
		}

        public static function favorite( $tb_name, $id )
        {
            return DB_ActiveRecord_Model::$db_model
                ->table( $tb_name )
                ->update( array( 'is_favorite' => '@func:IF( is_favorite = 1, 0, 1 )' ) )
                ->search( "id = {$id}" )
                ->execute();
        }
		
		public static function bulkPublish( $tb_name, $ids )
		{
			return DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->update( array( 'is_published' => Core_Request::getInstance()->getParams( 'publish' ) ) )
				->search( 'id IN( ' . implode( ',', $ids ) . ' )' )
				->execute();
		}
		
		/* @param $group_field ( string ) { Π.χ. cat_id, parent_id, ... [ Πρέπει να είναι Integer to value του πεδίου ] } */
		public static function orderUp( $tb_name, $id, $group_field = null )
		{
			$current = DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->select( '*' )
				->search( array( "id = {$id}" ) )
				->execute( 'one' );
			$smaller = current( DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->select( '@func:MAX( ordered ) AS smallerOrdered' )
				->search( array( 
					"id != {$id}", 
					"ordered < {$current['ordered']}",
					( $group_field ) ? "{$group_field} = {$current[ $group_field ]}" : "0 < 1"
				) )
				->group( 'id' )
				->order( 'ordered DESC' )
				->execute()
			);
			if( $smaller ) { /* Εφόσον υπάρχει μικρότερο */
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => $smaller['smallerOrdered'] ) )
					->search( "id = {$id}" )
					->execute();
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => '@func:ordered+1' ) )
					->search( array(
						"ordered >= {$smaller['smallerOrdered']}", 
						"id != {$id}",
						( $group_field ) ? "{$group_field} = {$current[ $group_field ]}" : "0 < 1"
					 ) )
					->execute();
				return true;
			} else {
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => '@func:ordered+1' ) )
					->search( array(
						"id != {$id}",
						"ordered >= {$current['ordered']}"
					) )
					->execute();
				return true;
			}
			return false;
		}
		
		/* @param $group_field ( string ) { Π.χ. cat_id, parent_id, ... [ Πρέπει να είναι Integer to value του πεδίου ] } */
		public static function orderDown( $tb_name, $id, $group_field = false )
		{
			$current = DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->select( '*' )
				->search( array( "id = {$id}" ) )
				->execute( 'one' );
			$larger = current( DB_ActiveRecord_Model::$db_model
				->table( $tb_name )
				->select( '@func:MIN( ordered ) AS largerOrdered' )
				->search( array( 
					"id != {$id}", 
					"ordered > {$current['ordered']}",
					( $group_field ) ? "{$group_field} = {$current[ $group_field ]}" : "0 < 1"
				) )
				->group( 'id' )
				->order( 'ordered ASC' )
				->execute()
			);
			if( $larger ) { /* Εφόσον υπάρχει μεγαλύτερο order */
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => $larger['largerOrdered'] ) )
					->search( "id = {$id}" )
					->execute();
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => '@func:ordered-1' ) )
					->search( array(
						"ordered >= {$larger['largerOrdered']}", 
						"id != {$id}",
						( $group_field ) ? "{$group_field} = {$current[ $group_field ]}" : "0 < 1"
					 ) )
					->execute();
				return true;
			} else {
				DB_ActiveRecord_Model::$db_model
					->table( $tb_name )
					->update( array( 'ordered' => '@func:IF( ordered-1 >= 0, ordered-1, 0 )' ) )
					->search( array(
						"id != {$id}",
						"ordered >= {$current['ordered']}"
					) )
					->execute();
				return true;
			}
			return false;
		}
	}
?>