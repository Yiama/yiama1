<?php
class Controller_Dashboard extends Controller_Admin {
    
	public function __construct() {
		parent::__construct();
	}
    
    public function index() {
        // User
        $user = new Model_Yiama_User();
        $logged_user = $user->getLogged();
        $logged_user->is_superadmin = $logged_user->hasRoles(array('superadmin'));
        if ($logged_user->is_superadmin) {
            $logged_user->link = $this->request->urlFromPath( 
                "yiama_users/updateform/" . $logged_user->id
            );
        }
        
        // Section 1
        $article = new Model_Yiama_Article();
        $articles['total'] = $article
            ->query()
            ->select('COUNT(*) as count')
            ->find(':first')
            ->count;
        $articles['visits'] = $article
            ->query()
            ->select('SUM(visits) as visits')
            ->find(':first')
            ->visits;
        $articles['active'] = $article
            ->query()
            ->select('COUNT(*) as count')
            ->search('is_published = 1')
            ->find(':first')
            ->count;
        $articles['users'] = $article
            ->query()
            ->select('COUNT(*) as count')
            ->search('ym_users_id_created = ' . $logged_user->id)
            ->find(':first')
            ->count;
        
        // Section 2
        $google_analytics = array();
        
        // Section 3
        $articles['latest'] = $article
            ->query()
            ->select('*')
            ->order('time_created DESC')
            ->limit('5')
            ->find();
        foreach ($articles['latest'] as $article) {
			$article->link = $this->request->urlFromPath( 
                "yiama_articles/updateform/" . $article->id
            );
        }
        $articles['popular'] = $article
            ->query()
            ->select('*')
            ->order('visits DESC')
            ->limit('5')
            ->find();
        foreach ($articles['popular'] as $article) {
			$article->link = $this->request->urlFromPath( 
                "yiama_articles/updateform/" . $article->id
            );
        }
        
        // Section 4
        $notifications = array();
		
		// Response
		$controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS
            .'dashboard.php');
		$controller_view->addVars(array( 
            'user' => $logged_user,
            'articles' => $articles,
            'notifications' => $notifications,
            'translator' => $this->translator
        ));
		$main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'
			.DS.'default.php' );
		$main_view->addVars(array(
            'controller_view' => $controller_view,
            'translator' => $this->translator
        ));
		$this->response->setBody($main_view->render());
    }
}