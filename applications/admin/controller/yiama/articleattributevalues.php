<?php
 
class Controller_Yiama_Articleattributevalues extends Core_Controller
{
	public function __construct() {
        parent::__construct();
	}
	
	public function delete()
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_articles_attributes__values';
		$data = new Model_Yiama_Articleattributevalue();
		$data = $data->findByKey( array( $params['ym_articles_id'], $params['ym_attributes_values_id'] ) );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
        
        // Response
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'delete',
            'error' => $error
        ));
        $this->response->redirect(urldecode($params['return_url']));
	}
}

?>