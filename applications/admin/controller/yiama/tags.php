<?php
 
class Controller_Yiama_Tags extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Tag();
		$query = $model->query()
			->select( 'ym_tags.*, local.*,'
			    . 'IF( COUNT( at.ym_tags_id ), 0, 1 ) as allow_deletion')
		    ->join( 'ym_articles_tags AS at', 'at.ym_tags_id = ym_tags.id', 'LEFT' ) 
			->group( 'ym_tags.id' )
			->search( Model_Helper_Query::search(null, 'yiama_tags') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Tags(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
    }
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_TagInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$tb_name = 'ym_tags';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Tag();
		$data->is_published = $params['is_published'];
        $user = new Model_Yiama_User();
        $data->ym_users_id = $user->getLogged()->id;
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Taglocal();
				$data_local->ym_tags_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] ) 
                            ? $l['title'] 
                            : 'tags_' . $key),
                    function($alias_value) use ($data_local) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_tags/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Tag();
		$form = new Form_Yiama_TagUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$tb_name = 'ym_tags';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Tag();
		$data = $data->findByKey( $id );
		$data->is_published = $params['is_published'];
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Taglocal();
				$data_local->ym_tags_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] ) 
                            ? $l['title'] 
                            : 'tags_' . $key),
                    function($alias_value) use ($data_local, $data, $key) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "NOT (ym_tags_id =" . $data->id
                                    ." AND ym_languages_id =". $key . ")",
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_tags/updateform/'.$data->id
        ));
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$tb_name = 'ym_tags';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Tag();
		$data = $data->findByKey( $id );
		if( ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false 
		|| ! $data->delete() ) { /* Delete main */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
    
	public function ajax() {
        Helper_Before::checkRequestMethod(array('*' => 'post'));
		$params = $this->request->getParams();
        $result = array();
		$result['status'] = false;
		$data = new Model_Yiama_Taglocal();
		$existed_data = $data
            ->query()	
			->search( "title = '{$params['value']}'" )
			->find(':first');
		if( $existed_data ) {
            $result['status'] = true;
            $result['value'] = $existed_data->title;
		} else {
			$proposed_data = $data->query()	
                ->search( "title LIKE '{$params['value']}%'" )
                ->find(':first');
            if( $proposed_data ) {
                $result['status'] = true;
                $result['value'] = $proposed_data->title;
            }
		}
        echo json_encode((object) $result); 
        exit;
	}
}