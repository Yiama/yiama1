<?php
 
class Controller_Yiama_Forms extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Form();
		$query = $model->query()
			->select( 'ym_forms.*, local.*' )
			->group( 'ym_forms.id' )
			->search( Model_Helper_Query::search(null, 'yiama_forms') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Forms(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
    }
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_FormInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_forms';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Form();
        $lang = new Model_Yiama_Language();
        $data->alias = Model_Helper_Value::createAlias(
            !empty( $params['alias'] ) 
                ? $params['alias'] 
                : $params['local'][$lang->getDefault()->id]['title'],
            function($alias_value) use ($data) {
                return $data
                    ->query()
                    ->search(array(
                        "alias = '" . $alias_value . "'"
                    ))
                    ->find();
            }
        );
		$data->json = $params['form_json'];
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Formlocal();
				$data_local->ym_forms_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->text = @$l['text'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_forms/updateform/'.$data->id
        ));
    }
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Form();
		$form = new Form_Yiama_FormUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
    
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_forms';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Form();
		$data = $data->findByKey( $id );
        $lang = new Model_Yiama_Language();
        $data->alias = Model_Helper_Value::createAlias(
            !empty( $params['alias'] ) 
                ? $params['alias'] 
                : $params['local'][$lang->getDefault()->id]['title'],
            function($alias_value) use ($data) {
                return $data
                    ->query()
                    ->search(array(
                        "alias = '" . $alias_value . "'",
                        "id <> " . $data->id
                    ))
                    ->find();
            }
        );
		$data->json = $params['form_json'];
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Formlocal();
				$data_local->ym_forms_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->text = @$l['text'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_forms/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_forms';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Form();
		$data = $data->findByKey( $id );
		if( ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false 
		|| ! $data->delete() ) { /* Delete main */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>