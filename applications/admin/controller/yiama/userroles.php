<?php
 
class Controller_Yiama_Userroles extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
	}
	
	public function delete()
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
		$tb_name = 'ym_users_roles';
		$data = new Model_Yiama_Userrole();
		$data = $data->findByKey( array( $params['ym_users_id'], $params['ym_roles_id'] ) );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error ) );
	}
}

?>