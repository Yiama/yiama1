<?php
 
class Controller_Yiama_Notifications extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Notification();
		$query = $model->query()
			->select( 'ym_notifications.*, IF( COUNT( r.id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_notificationrecipients AS r', 'r.ym_notifications_id = ym_notifications.id', 'LEFT' ) 
			->group( 'ym_notifications.id' )
			->search( Model_Helper_Query::search(null, 'yiama_notifications') );
        $cloned_query = clone $query;
		$data = $query
		 	->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Notifications(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
    }
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_NotificationInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_notifications';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Notification();
		$data->ym_templates_id = $params['temp_id'];
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} elseif ( isset( $params['recipients'] ) ) { /* Recipients */
			foreach( array_filter( $params['recipients'] ) as $val ) {
				$recipient = new Model_Yiama_Notificationrecipient();
				$recipient->ym_notifications_id = $data->id;
				$recipient->email = $val;
				if( ! $recipient->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_notifications/updateform/'.$data->id
        ));
    }
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Notification();
		$form = new Form_Yiama_NotificationUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_notifications';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Notification();
		$data = $data->findByKey( $id );
		$data->ym_templates_id = $params['temp_id'];
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} elseif ( isset( $params['recipients'] ) ) { /* Recipients */
			foreach( array_filter( $params['recipients'] ) as $val ) {
				$recipient = new Model_Yiama_Notificationrecipient();
				$recipient->ym_notifications_id = $data->id;
				$recipient->email = $val;
				if ( ! $recipient->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_notifications/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_notifications';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Notification();
		$data = $data->findByKey( $id );
		$del_recipients = DB_ActiveRecord_Model::$db_model
			->table( 'ym_notificationrecipients' )
			->delete()
			->search( "ym_notifications_id = {$id}" )
			->execute();
		if( ! $del_recipients 
		|| ! $data->delete() ) { /* Delete main, delete recipients */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>