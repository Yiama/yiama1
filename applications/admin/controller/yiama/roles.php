<?php
 
class Controller_Yiama_Roles extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
        $this->has_privileges = true;
        $action = $this->request->getAction();
        $user = new Model_Yiama_User();
        $logged_user = $user->getLogged();
        switch ($action) {
            case 'insert':
                $permissions = 'roles_create';
                break;
            case 'update':
                $permissions = 'roles_update';
                break;
            case 'delete':
                $permissions = 'roles_delete';
                break;
            default:
                $permissions = array(
                    'roles_create',
                    'roles_update',
                    'roles_delete'
                );
        }
        $this->has_privileges = $logged_user->hasPermissions($permissions) ? true : false;
        if ($action != 'index' && !$this->has_privileges) {
            Helper_Redirect::send(Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'dashboard',
                        'action' => 'index' 
                    )
                ), 
                null, 
                array('error', 'no_privileges')
            );
        }
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Role();
		$query = $model->query()
		    ->select( 'ym_roles.*, GROUP_CONCAT( DISTINCT p.title ) AS permissions,'
                . 'IF( COUNT( u.id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_roles_permissions AS rp', 'rp.ym_roles_id = ym_roles.id', 'LEFT' )
			->join( 'ym_permissions AS p', 'p.id = rp.ym_permissions_id', 'LEFT' )
			->join( 'ym_users_roles AS ur', 'ur.ym_roles_id = ym_roles.id', 'LEFT' )
            ->join('ym_users u', 'u.id = ur.ym_users_id', 'LEFT');
        $cloned_query = clone $query
			->group( 'ym_roles.id' )
			->search( Model_Helper_Query::search(array('ym_roles.id > 1'), 'yiama_roles') );
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Roles(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
        $buttons = '';
        if ($this->has_privileges) {
            $buttons = Helper_Buttons::get(
                $this->translator,
                array('add' => Helper_Link::insertform())
            );
        }
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_RoleInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_roles';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Role();
		$existed_data = $data->query()	
			->search( "title = '{$params['title']}'" )
			->find( ':first' );
		if( $existed_data ) {
			$error[] = 'row_exists';
            $link = Helper_Link::insertform();
		} else {
            $data->parent_id = $params['parent_id'];
			$data->title = $params['title'];
			$data->description = $params['description'];
			if( ! $data->insert() ) {
				$error[] = 'insert_row';
			} elseif ( isset( $params['permissions'] ) ) {
				foreach( array_filter( $params['permissions'] ) as $key => $val ) {
					$rolepermission = new Model_Yiama_Rolepermission();
					$rolepermission->ym_roles_id = $data->id;
					$rolepermission->ym_permissions_id = $val;
                    $rolepermission->state = $params['permission_states'][$key];
					if( ! $rolepermission->insert() ){
						$error[] = 'insert_row';
						break;
					}
				}
			}
            $link = Helper_Link::updateform($data);
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_roles/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Role();
		$form = new Form_Yiama_RoleUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_roles';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Role();
        $data = $data->findByKey( $id );
            $data->parent_id = $params['parent_id'];
        $data->title = $params['title'];
        $data->description = $params['description'];
        if( ! $data->update() ) {
            $error[] = 'update_row';
        } elseif ( isset( $params['permissions'] ) ) {
            foreach( array_filter( $params['permissions'] ) as $key => $val ) {
                $rolepermission = new Model_Yiama_Rolepermission();
                $rolepermission->ym_roles_id = $data->id;
                $rolepermission->ym_permissions_id = $val;
                $rolepermission->state = $params['permission_states'][$key];
                if( $rolepermission->findByKey( array( $data->id, $val ) ) ) {
                    if( ! $rolepermission->update() ){
                        $error[] = 'update_row';
                        break;
                    }
                } elseif( ! $rolepermission->insert() ){
                    $error[] = 'insert_row';
                    break;
                }
            }
        }
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_roles/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_roles';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Role();
		$data = $data->findByKey( $id );
		$del_roles = DB_ActiveRecord_Model::$db_model
			->table( 'ym_roles_permissions' )
			->delete()
			->search( "ym_roles_id = {$id}" )
			->execute();
		if( ! $del_roles 
		|| ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>