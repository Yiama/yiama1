<?php
 
class Controller_Yiama_Permissions extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
        $this->has_privileges = true;
        $action = $this->request->getAction();
        $user = new Model_Yiama_User();
        $logged_user = $user->getLogged();
        switch ($action) {
            case 'insert':
                $permissions = 'permissions_create';
                break;
            case 'update':
                $permissions = 'permissions_update';
                break;
            case 'delete':
                $permissions = 'permissions_delete';
                break;
            default:
                $permissions = array(
                    'permissions_create',
                    'permissions_update',
                    'permissions_delete'
                );
        }
        if ($action != 'index' && !$this->has_privileges) {
            Helper_Redirect::send(Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'dashboard',
                        'action' => 'index' 
                    )
                ), 
                null, 
                array('error', 'no_privileges')
            );
        }
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Permission();
		$query = $model->query()
		    ->select( 'ym_permissions.*,IF( COUNT( rp.ym_permissions_id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_roles_permissions AS rp', 'rp.ym_permissions_id = ym_permissions.id', 'LEFT' );
        $cloned_query = clone $query
			->group( 'ym_permissions.id' )
			->search( Model_Helper_Query::search(null, 'yiama_permissions') );
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Permissions(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator,
            $this->has_privileges
        );
        
        // Buttons
        $buttons = '';
        if ($this->has_privileges) {
            $buttons = Helper_Buttons::get(
                $this->translator,
                array('add' => Helper_Link::insertform())
            );
        }
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_PermissionInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert() {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_permissions';
		$data = new Model_Yiama_Permission();
		$existed_data = $data->query()	
			->search( "title = '{$params['title']}'" )
			->find( ':first' );
		if( $existed_data ) {
			$error[] = 'row_exists';
            $link = Helper_Link::insertform();
		} else {
			$data->title = $params['title'];
			if( ! $data->insert() ) {
				$error[] = 'insert_row';
			}
            $link = Helper_Link::updateform($data);
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_permissions/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Permission();
		$form = new Form_Yiama_PermissionUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    } 
	
	public function update($id) {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_permissions';
		$data = new Model_Yiama_Permission();
        $existed_data = $data->query()	
			->search( "title = '{$params['title']}'" )
			->find( ':first' );
		if( $existed_data ) {
			$error[] = 'row_exists';
		} else {
            $data = $data->findByKey( $id );
            $data->title = $params['title'];
            if( ! $data->update() ) {
                $error[] = 'update_row';
            }
        }
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_permissions/updateform/'.$data->id
        ));
	}
	
	public function delete($id, $redirect = true) {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_permissions';
		$data = new Model_Yiama_Permission();
        $data = $data->findByKey( $id );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>