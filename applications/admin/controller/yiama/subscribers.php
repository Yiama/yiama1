<?php
 
class Controller_Yiama_Subscribers extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Subscriber();
		$query = $model->query()
			->select( 'ym_subscribers.*, ss.*, s.title subscription_title, '
                . 'CONCAT(ym_subscribers.id, "_", s.id) subscriberid_subscriptionid, 1 as allow_deletion' )
			->join( 'ym_subscribers_subscriptiontypes ss', 
                    'ss.ym_subscribers_id = ym_subscribers.id')
			->join( 'ym_subscriptiontypes AS s', 
                    's.id = ss.ym_subscriptiontypes_id')
			->search( Model_Helper_Query::search(null, 'yiama_subscribers') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Subscribers(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
        $csrf = new Security_Csrf(Core_Session::getInstance());
        $p = array($csrf->getRequestTokenName() => $csrf->getToken());
        if (isset($params['search']['ss.ym_subscriptiontypes_id]'])) {
            $p = array_merge($p,array(
                'type_id' => $params['search']['ss.ym_subscriptiontypes_id]']
            ));
        }
        $buttons = Helper_Buttons::get( 
            $this->translator,
            array(
                $this->translator->_('button.export_to_csv') => $this->request->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'yiama_subscribers',
                        'action' => 'export'
                    ),
                    $p
                )
            ) 
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	/*
	 * @param $ids "<subscriber_id>_<subscriptiontype id>"
	 */
	public function item()
	{
        $ids = explode('_', $this->request->getParams('subscriberid_subscriptionid'));
		$model = new Model_Yiama_Subscriber();
		$data = $model->query()
			->select( 'ym_subscribers.*, s.info' )
			->search( 'ym_subscribers.id = ' . $ids[0] )
			->join( 
                'ym_subscribers_subscriptiontypes AS s', 
                's.ym_subscriptiontypes_id = ' . $ids[1]
            )
			->find( ':first' );
        
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            null,
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Catalogue
        $headers = array('Εmail' => 'email');
        $content = array(array('text' => 'email'));
        $d = $data;
		foreach((array)json_decode($data->info, true) as $key => $value) {
			$headers[$key] = null;
            $content[] = array('text' => $key);
            $d->$key = $value;
		}
        $catalogue = new Helper_Catalogue();
        $catalogue::setHeaders($headers);
		$catalogue::setContent('ym_subscribers', array($d), $content);
                
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $catalogue::get() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$data = new Model_Yiama_Subscriber();
		$data = $data->findByKey( $id );
		$del_subscriptions = DB_ActiveRecord_Model::$db_model
			->table( 'ym_subscribers_subscriptiontypes' )
			->delete()
			->search( "ym_subscribers_id = {$id}" )
			->execute();
		if( ! $del_subscriptions 
		|| ! $data->delete() ) {
			$error[] = 'delete_row';
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
	
	public function export()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		ini_set('max_execution_time', 300);
		$data = new Model_Yiama_Subscriber();
		$query = $data->query()
            ->select('ym_subscribers.*, ss.info, s.title subscriptiontype_title');
        if (!empty($params['type_id'])) {
            $query->search('s.id = ' . $params['type_id']);
        }
        $data = $query
            ->join('ym_subscribers_subscriptiontypes ss', 
                    'ss.ym_subscribers_id = ym_subscribers.id')
            ->join('ym_subscriptiontypes s', 's.id = ss.ym_subscriptiontypes_id')
			->find();
		$csv_rows = array();
		foreach( $data as $d ) {
            $row = array();
            $row[] = $d->subscriptiontype_title;
            $row[] = $d->email;
            $info = array();
            $in = (array) json_decode($d->info, true);
            foreach ($in as $key => $value) {
                $info[] = $key . ': ' . $value;
            }
            $row[] = implode("\n", $info);
            $csv_rows[] = $row;
		}
		$export = new Controller_Export();
		$export->csv( 'Εγγραφές χρηστών', null, $csv_rows );
	}
}

?>