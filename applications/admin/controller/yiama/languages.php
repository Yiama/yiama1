<?php
 
class Controller_Yiama_Languages extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Language();
		$user = new Model_Yiama_User();
		$logged_user = $user->getLogged();
        $has_privileges = $logged_user->hasRoles('superadmin') ? true : false;
		
		$query = $model->query()
			->search( Model_Helper_Query::search(null, 'yiama_languages') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Languages(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {        
        // Form
		$form = new Form_Yiama_LanguageInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_languages';	
		$data = new Model_Yiama_Language();
		$data->code = $params['code'];
		$data->title = $params['title'];
		$data->is_published = $params['is_published'];
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_languages/updateform/'.$data->id
        ));
    }
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Language();
		$form = new Form_Yiama_LanguageUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_languages';
		$data = new Model_Yiama_Language();
		$data = $data->findByKey( $id );
		$data->code = $params['code'];
		$data->title = $params['title'];
		$data->is_published = $params['is_published'];
		if( ! $data->update() ) {
			$error[] = 'update_row';
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'error' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_languages/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$error = null;
        $warning = [];
		$tb_name = 'ym_languages';
		DB_ActiveRecord_Model::init();
		$tables = DB_ActiveRecord_Model::$db_model->query( "SHOW TABLES LIKE  '%_local'" )->execute();
		foreach( $tables as $t ) {
			$result = DB_ActiveRecord_Model::$db_model
				->table( $t[ key( $t ) ] )
				->delete()
				->search( 'ym_languages_id = ' . $id )
				->execute();
			if( ! $result ) {
				$error[] = 'delete_row';
				break;
			}
		}
		if( ! Model_Helper_Action::deleteById( $tb_name, $id ) ) {
			$error[] = 'delete_row';
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
    
    public function setsitesdefault()
    {
        $params = $this->request->getParams();
        $warning = [];
		$tb_name = 'ym_languages';
        $result = DB_ActiveRecord_Model::$db_model
            ->table( $tb_name )
            ->update( array( 'is_sites_default' => '0' ) )
            ->execute();
        if($result) {
            $result = DB_ActiveRecord_Model::$db_model
                    ->table( $tb_name )
                    ->update( array( 'is_sites_default' => '1' ) )
                    ->search( "id = {$params['id']}" )
                    ->execute();
        }
		if( ! $result ){
			$warning[] = 'action';
		}
        
        // Response
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'delete',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
    }
}
?>