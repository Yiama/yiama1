<?php
class Controller_Yiama_Ajax extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
        Helper_Before::checkRequestMethod(array('*' => 'post'));
	}
	
	public function tags()
	{
		$params = $this->request->getParams();
        $result = array();
		$result['status'] = false;
		$data = new Model_Yiama_Taglocal();
		$existed_data = $data
            ->query()	
			->search( "title = '{$params['value']}'" )
			->find(':first');
		if( $existed_data ) {
            $result['status'] = true;
            $result['value'] = $existed_data->title;
		} else {
			$proposed_data = $data->query()	
                ->search( "title LIKE '{$params['value']}%'" )
                ->find(':first');
            if( $proposed_data ) {
                $result['status'] = true;
                $result['value'] = $proposed_data->title;
            }
		}
        echo json_encode((object) $result); 
        exit;
	}
}