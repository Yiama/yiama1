<?php
 
class Controller_Yiama_Subscriptiontypes extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkUser( 'admin' );
	}
	
	public function index()
	{
		$tb_name = 'ym_subscriptions_types';
		$data = new Model_Yiama_Subscriptiontype();
		$query = $data->query()
			->select( 'ym_subscriptions_types.*, IF( COUNT( s.ym_subscriptions_types_id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_subscriptions AS s', 's.ym_subscriptions_types_id = ym_subscriptions_types.id', 'LEFT' )
			->group( 'ym_subscriptions_types.id' )
			->search( Model_Helper_Query::search() );
        $cloned_query = clone $query;
		$types = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
		Helper_Catalogue::setBulkActions( $tb_name, 'delete' );
		Helper_Catalogue::setHeaders( array(
			'Τίτλος' => 'title',
			'Alias' => 'alias'
		) );
		Helper_Catalogue::setSearch( array(
			array( 'select' => Helper_Form::select( 'search[ym_subscriptions_types.id]', $data->findAll(), 'id', 'title', 'id', Core_Request::getInstance()->getParams( 'search', '[ym_subscriptions_types.id]' ) ) ),
			array( 'like' => 'alias' )
		) );
		$subscribers_url_path = Helper_Link::catalogue( 'yiama_subscribers' );
		Helper_Catalogue::setContent( 'ym_subscriptions_types', $types, array(
			array( 'anchor' => array( 'title', $subscribers_url_path, 'search[ym_subscriptions_types.id]', 'id', 'Χρήστες' ) ),
			array( 'text' => 'alias' )
		) );
		Helper_Catalogue::setContentActions( $types );
		$catalogue = Helper_Catalogue::get();
		$buttons = Helper_Buttons::get(array( 'add' => Helper_Link::insertform() ), array( 'export' => null ) );
		/* View */
		$this->response->setBody( Helper_View::catalogue( $this, array( 'buttons' => $buttons, 'catalogue' => $catalogue, 'pagination_vars' => array( 'total_content' => count( $all_search_data ) ) ) )->render() );
	}
	
	public function insertform()
	{
		$form = Helper_Buttons::get(array( 'save' => null ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule( 'Βασικά' );
		$form .= Helper_Form::title( null, true );
		$form .= Helper_Form::alias();
		$form .= Helper_Form::input( 'subscriber_fields', null, null, null, 'Πεδία χρήστη <br/><small>στα Λατινικά, χωρισμένα με κόμμα</small>' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form, 'header' => 'Εισαγωγή νέου τύπου Εγγραφής' ) )->render() );
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_subscriptions_types';
		$data = new Model_Yiama_Subscriptiontype();
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		$data->subscriber_fields = @$params['subscriber_fields'];
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		}
		Helper_Redirect::send( Helper_Link::updateform( $data ), null, array( 'insert', $error ) );
	}
	
	public function updateform( $id )
	{
		$tb_name = 'ym_subscriptions_types';
		$data = new Model_Yiama_Subscriptiontype();
		$data = $data->findByKey( $id );
		$form = Helper_Header::get( $data->title, array( 'save' => null, 'add' => Helper_Link::insertform() ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule( 'Βασικά' );
		$data = new Model_Yiama_Subscriptiontype();
		$data = $data->findByKey( $id );
		$form .= Helper_Form::title( $data->title, true );
		$form .= Helper_Form::alias( $data->alias );
		$form .= Helper_Form::input( 'subscriber_fields', $data->subscriber_fields, null, null, 'Πεδία χρήστη <br/><small>στα Λατινικά, χωρισμένα με κόμμα</small>' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form ) )->render() );
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_subscriptions_types';
		$data = new Model_Yiama_Subscriptiontype();
		$data = $data->findByKey( $id );
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		$data->subscriber_fields = @$params['subscriber_fields'];
		if( ! $data->update() ) {
			$error[] = 'update_row';
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'update', $error ) );
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_subscriptions_types';
		$data = new Model_Yiama_Subscriptiontype();
		$data = $data->findByKey( $id );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		if( $redirect ) {
			Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error ) );
		} else {
			return empty( $error );	
		}
	}
	
	public function export()
	{
		ini_set('max_execution_time', 300);
		$data = new Model_Yiama_Subscriptiontype();
		$data = $data->query()
			->combine( 'subscriptions' )
			->find();
		$csv_rows = array();
		foreach( $data as $d ) {
			if( empty( $d->subscriptions[0] ) ) {
				continue;
			}
			$csv_rows[] = array( '--' . $d->title . '--' );
			$info = ( array ) json_decode( $d->subscriptions[0]->info, true );
			$csv_rows[] = array_merge( array( 'email' ), array_keys( $info ) );
			foreach( $d->subscriptions as $sub ) {
				$sub_info = array();
				$sub_info[] = $sub->subscriber->email;
				$info = ( array ) json_decode( $sub->info, true );
				foreach( $info as $i ) {
					$sub_info[] = $i;
				}
				$csv_rows[] = $sub_info;
			}
		}
		$export = new Controller_Export();
		$export->csv( 'Εγγραφές χρηστών', null, $csv_rows );
	}
}

?>