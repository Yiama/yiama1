<?php
 
class Controller_Yiama_Usergroups extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkUser( 'superadmin' );
	}
	
	public function index()
	{
		$tb_name = 'ym_usergroups';
		$data = new Model_Yiama_Usergroup();
		$query = $data->query()
			->select( 'ym_usergroups.*, IF( COUNT( u.ym_usergroups_id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_users AS u', 'u.ym_usergroups_id = ym_usergroups.id', 'LEFT' )
			->group( 'ym_usergroups.id' )
			->search( Model_Helper_Query::search() );
        $cloned_query = clone $query;
		$groups = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
		Helper_Catalogue::setBulkActions( $tb_name, 'delete' );
		Helper_Catalogue::setHeaders( array(
			'Τίτλος' => 'title',
			'Alias' => 'alias',
			'Ενεργό' => 'is_published'
		) );
		Helper_Catalogue::setSearch( array(
			array( 'select' => Helper_Form::select( 'search[ym_usergroups.id]', $data->findAll(), 'id', 'title', 'id', Core_Request::getInstance()->getParams( 'search', '[ym_usergroups.id]' ) ) ),
			array( 'like' => 'alias' ),
			array( 'publish' => 'is_published' )
		) );
		Helper_Catalogue::setContent( 'ym_usergroups', $groups, array(
			array( 'text' => 'title' ),
			array( 'text' => 'alias' ),
			array( 'publish' => null )
		) );
		Helper_Catalogue::setContentActions( $groups, array( 'update', 'delete' ) );
		$catalogue = Helper_Catalogue::get();
		$header = Helper_Header::get( null, array( 'add' => Helper_Link::insertform() ) );
		/* View */
		$this->response->setBody( Helper_View::catalogue( $this, array( 'header' => $header, 'catalogue' => $catalogue, 'pagination_vars' => array( 'total_content' => count( $all_search_data ) ) ) )->render() );
	}
	
	public function insertform()
	{
		$form = Helper_Header::get( null, array( 'save' => null ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule( 'Βασικά' );
		$form .= Helper_Form::title( null, true );
		$form .= Helper_Form::alias();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form, 'header' => 'Εισαγωγή νέας Ομάδας' ) )->render() );
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_usergroups';
		$data = new Model_Yiama_Usergroup();
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		}
		Helper_Redirect::send( Helper_Link::updateform( $data ), null, array( 'insert', $error ) );
	}
	
	public function updateform( $id )
	{
		$tb_name = 'ym_usergroups';
		$data = new Model_Yiama_Usergroup();
		$data = $data->findByKey( $id );
		$form = Helper_Header::get( $data->title, array( 'save' => null, 'add' => Helper_Link::insertform() ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule( 'Βασικά' );
		$data = new Model_Yiama_Usergroup();
		$data = $data->findByKey( $id );
		$form .= Helper_Form::title( $data->title, true );
		$form .= Helper_Form::alias( $data->alias );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form ) )->render() );
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_usergroups';
		$data = new Model_Yiama_Usergroup();
		$data = $data->findByKey( $id );
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->update() ) {
			$error[] = 'update_row';
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'update', $error ) );
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$error = null;
		$tb_name = 'ym_usergroups';
		$data = new Model_Yiama_Usergroup();
		$data = $data->findByKey( $id );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		if( $redirect ) {
			Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error ) );
		} else {
			return empty( $error );	
		}
	}
}

?>