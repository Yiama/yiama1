<?php
 
class Controller_Yiama_Users extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
        $this->has_privileges = true;
        $action = $this->request->getAction();
        $user = new Model_Yiama_User();
        $logged_user = $user->getLogged();
        switch ($action) {
            case 'insert':
                $permissions = array(
                    'users_create',
                    'roles_create'
                );
                break;
            case 'update':
                $permissions = array(
                    'users_update',
                    'roles_update'
                );
                break;
            case 'delete':
                $permissions = array(
                    'users_delete',
                    'roles_delete'
                );
                break;
            default:
                $permissions = array(
                    'users_create',
                    'roles_create',
                    'users_update',
                    'roles_update',
                    'users_delete',
                    'roles_delete'
                );
        }
        $this->has_privileges = $logged_user->hasPermissions($permissions) ? true : false;
        if ($action != 'index' && !$this->has_privileges) {
            Helper_Redirect::send(Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'dashboard',
                        'action' => 'index' 
                    )
                ), 
                null, 
                array('error', 'no_privileges')
            );
        }
	}
	
	public function index() {
        
        // Query
		$model = new Model_Yiama_User();
		$query = $model->query()
            ->select( 'ym_users.*, GROUP_CONCAT( DISTINCT r.title ) AS roles, 1 as allow_deletion' )
			->join( 'ym_users_roles AS ur', 'ur.ym_users_id = ym_users.id', 'LEFT' )
			->join( 'ym_roles AS r', 'r.id = ur.ym_roles_id', 'LEFT' );
		$cloned_query = clone $query
			->group( 'ym_users.id' )
			->search( Model_Helper_Query::search(null, 'yiama_users') );
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Users(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator,
            $this->has_privileges
        );
        
        // Buttons
        $buttons = '';
        if ($this->has_privileges) {
            $buttons = Helper_Buttons::get(
                $this->translator,
                array(
                    'add' => Helper_Link::insertform(),
                    $this->translator->_('button.export_to_csv') => $this->request->url(
                        'cont_act_id',
                        array( 
                            'controller' => 'yiama_users',
                            'action' => 'export'
                        )
                    )
                )
            );
        }
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform(){
        // Form
		$form = new Form_Yiama_UserInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert() {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_users';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_User();
		$existed_data = $data->query()	
			->search( "email = '{$params['email']}'" )
			->find( ':first' );
		if( $existed_data ) { /* Αν υπάρχει ήδη ο χρήστης */
			$error[] = 'row_exists';
		} else {
			$data->username = $params['username'];
			$data->password = $data->hash( $params['password'] );
			$data->email = $params['email'];
            $data->image = isset($params['images']) ? $params['images'][0] : '@null:';
			$data->time_created = '@func:NOW()';
			$data->time_updated = '@func:NOW()';
			if( ! $data->insert() ) { /* Main entity */
				$error[] = 'insert_row';
			} elseif ( isset( $params['roles'] ) ) { /* Roles */
				foreach( array_filter( $params['roles'] ) as $val ) {
					$userrole = new Model_Yiama_Userrole();
					if( $userrole->findByKey( array( $data->id, $val ) ) ) { /* Αν υπάρχει ήδη ο role */
						continue;
					}
					$userrole->ym_users_id = $data->id;
					$userrole->ym_roles_id = $val;
					if( ! $userrole->insert() ){
						$error[] = 'insert_row';
						break;
					}
				}
			}
		}
        // Image
		if( empty( $error ) ) {
			$task = new Controller_Task();
			$warning[] = $task->insertImage( $data, $tb_name );
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_users/updateform/'.$data->id
        ));
    }
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_User();
		$form = new Form_Yiama_UserUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id ) {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_users';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_User();
		$data = $data->findByKey( $id );
		$data->username = $params['username'];
		if( ! empty( $params['password'] ) ) {
			$data->password = $data->hash( $params['password'] );
		}
		$data->email = $params['email'];
		$data->time_updated = '@func:NOW()';
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} elseif ( isset( $params['roles'] ) ) { /* Roles */
			foreach( array_filter( $params['roles'] ) as $val ) {
				$userrole = new Model_Yiama_Userrole();
				if( $userrole->findByKey( array( $data->id, $val ) ) ) { /* Αν υπάρχει ήδη ο role */
					continue;
				}
				$userrole->ym_users_id = $data->id;
				$userrole->ym_roles_id = $val;
				if( ! $userrole->insert() ){
					$error[] = 'update_row';
					break;
				}
			}
		}
        // Image
		if( empty( $error ) ) {
			$task = new Controller_Task();
			$warning[] = $task->deleteImage( $data, $tb_name );
			if( empty( $warning[0] ) ) {
				$warning[] = $task->insertImage( $data, $tb_name );
			}
		} 	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_users/updateform/'.$data->id
        ));
    }
    
	public function delete( $id, $redirect = true ) {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_users';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_User();
		$data = $data->findByKey( $id );
		$del_roles = DB_ActiveRecord_Model::$db_model
			->table( 'ym_users_roles' )
			->delete()
			->search( "ym_users_id = {$id}" )
			->execute();
		if( ! $del_roles 
		|| ! $data->delete() ) { /* Delete main, delete roles */
			$error[] = 'delete_row';
		}
        // Image
		if( empty( $error ) 
		&& ! empty( $data->image )
		&& ! Helper_Image::unlink( $tb_name, $data->image ) ) {
			$warning[] = 'unlink_image';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
	
	public function export()
	{
		ini_set('max_execution_time', 300);
		$data = new Model_Yiama_User();
		$users = $data->query()
			->select( 'ym_users.*, GROUP_CONCAT( r.title ) AS roles' )
			->join( 'ym_users_roles AS u_r', 'u_r.ym_users_id = ym_users.id' )
			->join( 'ym_roles AS r', 'r.id = u_r.ym_roles_id' )
			->group( 'ym_users.id' )
			->find();
		$csv_rows[] = array(     'Αναγνωριστικό', 'Email',      'Ημερομηνία',                                    'Ρόλοι' );
		foreach( $users as $user ) {
			$csv_rows[] = array( $user->username, $user->email, date( 'd/m/Y', strtotime( $user->time_created ) ), $user->roles );
		}
		$export = new Controller_Export();
		$export->csv( 'Χρήστες', null, $csv_rows );
	}
}