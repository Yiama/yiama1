<?php
 
class Controller_Yiama_Pages extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Data
		$model = new Model_Yiama_Page();
        $query = $model->query()
			->select( 'ym_pages.*, local.*, t.title AS type, p2l.title parent_name,'
                . 'IF( COUNT( p2.id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_pages AS p2', 'p2.parent_id = ym_pages.id', 'LEFT' )
			->join( 'ym_pages AS parent', 'parent.id = ym_pages.parent_id' )
            ->join('ym_pages_local p2l', 'p2l.ym_pages_id = parent.id'
                    . ' AND p2l.ym_languages_id = ' . $this->language->getCurrent()->id)
			->join( 'ym_pagetypes AS t', 't.id = ym_pages.ym_pagetypes_id '
                . 'AND (ym_pages.is_admin IS NULL OR ym_pages.is_admin <> 1)' 
                    )
            ->group( 'ym_pages.id' )
			->search( Model_Helper_Query::search(array('ym_pages.is_admin IS NULL '
                    . 'OR ym_pages.is_admin <> 1'), 'yiama_pages'));
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Pages(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array(
                'add' => Helper_Link::insertform(), 
                $this->translator->_('header.page_types') => Helper_Link::catalogue('yiama_pagetypes')
        ));
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_PageInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pages';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Page();
		$data->parent_id = $params['parent_id'];
		$data->ym_pagetypes_id = $params['type_id'];
		$data->url = ! isset( $params['url'] ) ? '@func:NULL' : $params['url'];
		$data->module = ! isset( $params['module'] ) ? '@func:NULL' : $params['module'];
		$data->is_published = $params['is_published'];
		$data->ordered = Model_Helper_Value::createOrdered( $data );
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['local'][ key( $params['local'] ) ]['title'] );
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Pagelocal();
				$data_local->ym_pages_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_pages/updateform/'.$data->id
        ));
	}
	
	public function updateform( $id ) {
        // Form
		$data = new Model_Yiama_Page();
		$form = new Form_Yiama_PageUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id ) {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pages';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Page();
		$data = $data->findByKey( $id );
		$data->parent_id = $params['parent_id'];
		$data->ym_pagetypes_id = $params['type_id'];
		$data->is_published = $params['is_published'];
		$data->url = ! isset( $params['url'] ) ? '@func:NULL' : $params['url'];
		$data->module = ! isset( $params['module'] ) ? '@func:NULL' : $params['module'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['local'][ key( $params['local'] ) ]['title'] );
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Pagelocal();
				$data_local->ym_pages_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_pages/updateform/'.$data->id
        ));
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pages';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Page();
		$data = $data->findByKey( $id );
		if( ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false 
		|| ! $data->delete() ) { /* Delete main */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>