<?php
class Controller_Yiama_Articles extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Data
		$model = new Model_Yiama_Article();
		$query = $model->query()
			->select( 'ym_articles.*, local.*, cl.title AS category_title, '
                    . 'u.username' )
			->join( 'ym_categories_local AS cl', 
                    'cl.ym_categories_id = ym_articles.ym_categories_id '
                    . "AND cl.ym_languages_id = {$this->language->id}" )
			->join('ym_users u', 'u.id = ym_articles.ym_users_id_created')
            ->group( 'ym_articles.id' )
			->search( Model_Helper_Query::search(null, 'yiama_articles') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Articles(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_ArticleInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert($redirect = true)
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_articles';	
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Article();
		$data->ym_categories_id = $params['cat_id'];
        $user = new Model_Yiama_User();
        $data->ym_users_id_created = $user->getLogged()->id;
        $data->ym_users_id_updated = $data->ym_users_id_created;
		$data->is_published = $params['is_published'];
        $data->is_favorite = $params['is_favorite'];
		$data->ordered = Model_Helper_Value::createOrdered( $data );
		$data->time_created = '@func:NOW()';
		$data->time_updated = '@func:NOW()';
		$data->visits = 0;
		$data->url = Model_Helper_Value::createURL( $data );
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Articlelocal();
				$data_local->ym_articles_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] ) 
                            ? $l['title'] 
                            : 'article_' . $key),
                    function($alias_value) use ($data_local, $data, $key) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->short_description = @$l['short_description'];
				$data_local->text = @$l['text'];
				$data_local->meta_title = @$l['meta_title'];
				$data_local->meta_description = @$l['meta_description'];
				$data_local->meta_keywords = @$l['meta_keywords'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
            // Attributes
			if( empty( $error ) ) {
                // Non multivalue attributes
                if( isset( $params['attributes'] ) ) {
                    foreach( array_filter( $params['attributes'], function( $var ) { return $var !== NULL && $var !== ''; } ) as $key => $val ) {
                        $attribute = new Model_Yiama_Attribute();
                        $attribute = $attribute->findByKey( $key );
                        $article_attribute = new Model_Yiama_Articleattribute();
                        $article_attribute->ym_articles_id = $data->id;
                        $article_attribute->ym_attributes_id = $key;
                        $article_attribute->val = $attribute->validateValue( $val );
                        if( ! $article_attribute->insert() ){
                            $error[] = 'insert_row';
                            break;
                        }
                    }
                }
                // Multivalue attributes
                if( isset( $params['multi_attributes'] ) ) {
                    foreach( array_filter( $params['multi_attributes'], function( $var ) { return $var = array_filter( $var ); return ! empty( $var ); } ) as $val ) {
                        $attributevalue = new Model_Yiama_Attributevalue();
                        $article_attributevalue = new Model_Yiama_Articleattributevalue();
                        foreach( $val as $v ) {
                            $article_attributevalue->ym_articles_id = $data->id;
                            $article_attributevalue->ym_attributes_values_id = $v;
                            if( ! $article_attributevalue->insert() ){
                                $error[] = 'insert_row';
                                break;
                            }
                        }
                    }
                }
			}
		}	
        // Tags
        if( empty( $error ) && !empty($params['tags'])) {
            $language = new Model_Yiama_Language();
            $default_language = $language->getDefault();
            foreach ($params['tags'] as $tag_title) {
                // If tag title does not exist insert it for default language 
                // only
                $tag_local = new Model_Yiama_Taglocal();
                $existed_tag = $tag_local
                    ->query()
                    ->search("title = '" . $tag_title . "'")
                    ->find(':first');
                if (empty($existed_tag)) {
                    $tag = new Model_Yiama_Tag();
                    $tag->is_published = 1;
                    if( !$tag->insert() ) {
                        $error[] = 'insert_row';
                    } else {
                        $tag_id = $tag->id;
                        $tag_local->ym_tags_id = $tag->id;
                        $tag_local->ym_languages_id = $default_language->id;
                        $tag_local->title = $tag_title;
                        $tag_local->alias = Model_Helper_Value::createAlias(
                            $tag_title,
                            function($alias_value) use ($tag_local) {
                                return $tag_local
                                    ->query()
                                    ->search(array(
                                        "alias = '" . $alias_value . "'"
                                    ))
                                    ->find();
                            },
                            $tag->id
                        );
                        if( !$tag_local->insert() ){
                            $error[] = 'insert_row';
                            break;
                        }
                    }
                } else {
                    $tag_id = $existed_tag->ym_tags_id;
                }
                // Connect article with tag
                if (empty($error)) {
                    $article_tag = new Model_Yiama_Articletag();
                    $article_tag->ym_articles_id = $data->id;
                    $article_tag->ym_tags_id = $tag_id;
                    if( !$article_tag->insert() ) {
                        $error[] = 'insert_row';
                    }
                }
            }
        }
        // Images
		if( empty( $error ) ) {
			$task = new Controller_Task();
			$warning[] = $task->insertImages( new Model_Yiama_Articleimage(), $tb_name, $data->getLastInsertedId() );
		} 	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'insert',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect($this->request->urlFromPath(
                'yiama_articles/updateform/'.$data->id
            ));
        } else {
			return empty($error);	
		}
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Article();
		$form = new Form_Yiama_ArticleUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function update( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_articles'; 
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Article();
		$data = $data->findByKey( $id );
		$data->ym_categories_id = $params['cat_id'];
        $user = new Model_Yiama_User();
        $data->ym_users_id_updated = $user->getLogged()->id;
		$data->time_updated = '@func:NOW()';
		$data->is_published = $params['is_published'];
        $data->is_favorite = $params['is_favorite'];
		$data->url = ! isset( $params['url'] ) ? Model_Helper_Value::createURL( $data ) : $params['url'];
        $user = new Model_Yiama_User();
        if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Articlelocal();
				$data_local->ym_articles_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] ) 
                            ? $l['title'] 
                            : 'article_' . $key),
                    function($alias_value) use ($data_local, $data, $key) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "NOT (ym_articles_id =" . $data->id
                                    ." AND ym_languages_id =". $key . ")",
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->short_description = @$l['short_description'];
				$data_local->text = @$l['text'];
				$data_local->meta_title = @$l['meta_title'];
				$data_local->meta_description = @$l['meta_description'];
				$data_local->meta_keywords = @$l['meta_keywords'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
			if( empty( $error ) ) { 
                // Non multivalue attributes
                if( isset( $params['attributes'] ) ) {
                    foreach( array_filter( $params['attributes'], function( $var ) { return $var !== NULL && $var !== ''; } ) as $key => $val ) {
                        $attribute = new Model_Yiama_Attribute();
                        $attribute = $attribute->findByKey( $key );
                        $article_attribute = new Model_Yiama_Articleattribute();
                        if( $existed_article_attribute = $article_attribute->findByKey( array( $data->id, $key ) ) ) { /* Αν υπάρχει ήδη τιμή για το attribute γίνεται update */ 
                            $existed_article_attribute->val = $attribute->validateValue( $val );
                            if( ! $existed_article_attribute->update() ) {
                                $warning[] = 'update_row';
                                break;
                            }
                        }
                        else{
                            $article_attribute->ym_articles_id = $data->id;
                            $article_attribute->ym_attributes_id = $key;
                            $article_attribute->val = $attribute->validateValue( $val );
                            if( ! $article_attribute->insert() ){
                                $error[] = 'insert_row';
                                break;
                            }
                        }
                    }
                }
                // Multivalue attributes
                if( isset( $params['multi_attributes'] ) ) {
                    $multi_attributes = array_map( function( $value ) { $val = array_filter( (array) $value ); return empty( $val ) ? NULL : $val; }, $params['multi_attributes'] );
                    foreach( array_filter( $multi_attributes, function( $var ) { $var = array_filter( (array) $var ); return ! empty( $var ); } ) as $key => $values ) {
                         $article_attributevalue = new Model_Yiama_Articleattributevalue();
                        $existed_article_attributevalues = $article_attributevalue
                            ->query()
                            ->select( 'ym_articles_attributes_values.*' )
                            ->join( 'ym_attributes_values AS attr_val', 'attr_val.id = ym_articles_attributes_values.ym_attributes_values_id AND attr_val.ym_attributes_id = ' . $key )
                            ->search( 'ym_articles_attributes_values.ym_articles_id = ' . $data->id )
                            ->find();
                        foreach( $existed_article_attributevalues as $existed ) {
                            if( ! $existed->delete() ) {
                                $error[] = 'delete_row';
                            }
                        }
                        if( ! $error ) {
                            foreach( $values as $v ) {
                                $article_attributevalue->ym_articles_id = $data->id;
                                $article_attributevalue->ym_attributes_values_id = $v;
                                if( ! $article_attributevalue->insert() ){
                                    $error[] = 'insert_row';
                                }
                            }
                        }
                    }
                }
			}
            // Tags
			if( empty( $error )) {
			    // First remove all article's tags
			    $article_tag = new Model_Yiama_Articletag();
			    $result = DB_ActiveRecord_Model::$db_model
    			    ->table('ym_articles_tags')
    			    ->delete()
    			    ->search("ym_articles_id =" . $id)
    			    ->execute();
			    if (!empty($params['tags'])) {
                    $language = new Model_Yiama_Language();
                    $default_language = $language->getDefault();
                    if ($result) {
                        foreach ($params['tags'] as $tag_title) {
                            // If tag title does not exist insert it for default language 
                            // only
                            $tag_local = new Model_Yiama_Taglocal();
                            $existed_tag = $tag_local
                                ->query()
                                ->search("title = '" . $tag_title . "'")
                                ->find(':first');
                            if (empty($existed_tag)) {
                                $tag = new Model_Yiama_Tag();
                                $tag->is_published = 1;
                                if( !$tag->insert() ) {
                                    $error[] = 'insert_row';
                                } else {
                                    $tag_id = $tag->id;
                                    $tag_local->ym_tags_id = $tag->id;
                                    $tag_local->ym_languages_id = $default_language->id;
                                    $tag_local->title = $tag_title;
                                    $tag_local->alias = Model_Helper_Value::createAlias(
                                        $tag_title,
                                        function($alias_value) use ($tag_local) {
                                            return $tag_local
                                                ->query()
                                                ->search(array(
                                                    "alias = '" . $alias_value . "'"
                                                ))
                                                ->find();
                                        },
                                        $tag->id
                                    );
                                    if( !$tag_local->insert() ){
                                        $error[] = 'insert_row';
                                        break;
                                    }
                                }
                            } else {
                                $tag_id = $existed_tag->ym_tags_id;
                            }
                            // Connect article with tag
                            if (empty($error)) {
                                $article_tag->ym_articles_id = $data->id;
                                $article_tag->ym_tags_id = $tag_id;
                                if( !$article_tag->insert() ) {
                                    $error[] = 'insert_row';
                                }
                            }
                        }
                    }
                }
            }
		}
		if( empty( $error )
		&& isset( $params['default_image'] ) ) { /* Set default image */
			if( ! Model_Helper_Action::setDefault( 'ym_articles_images', $params['default_image'], 'ym_articles_id = ' . $id ) ) {
				$warning[] = 'set_default_image';
			}
		}	
		if( empty( $error ) ) { /* Publish images */
			if( ! isset( $params['publish_images'] ) ) {
				$params['publish_images'] = array();
			}
			if( ! Model_Helper_Action::groupPublish( 'ym_articles_images', implode( ',', ( array ) $params['publish_images'] ), 'ym_articles_id = ' . $id ) ) {
				$warning[] = 'publish_image';
			}
		}
		if( empty( $error ) ) { /* Delete images */
			$task = new Controller_Task();
			$warning[] = $task->deleteImages( new Model_Yiama_Articleimage(), $tb_name );
		}
		if( empty( $error ) ) { /* Insert-update images */
			$warning[] = $task->insertImages( new Model_Yiama_Articleimage(), $tb_name, $id );
			$warning[] = $task->updateImages( $tb_name );
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'update',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect($this->request->urlFromPath(
                'yiama_articles/updateform/'.$data->id
            ));
        } else {
			return empty($error);	
		}
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$warning = null;
		$tb_name = 'ym_articles';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$image = new Model_Yiama_Articleimage();
		$images = $image->query()
			->search( "ym_articles_id = {$id}" )
			->find();
		$delete_attributes = DB_ActiveRecord_Model::$db_model
			->table( 'ym_articles_attributes' )
			->delete()
			->search( "ym_articles_id = {$id}" )
			->execute();
		$delete_attributes_values = DB_ActiveRecord_Model::$db_model
			->table( 'ym_articles_attributes_values' )
			->delete()
			->search( "ym_articles_id = {$id}" )
			->execute();
		$delete_tags = DB_ActiveRecord_Model::$db_model
			->table( 'ym_articles_tags' )
			->delete()
			->search( "ym_articles_id = {$id}" )
			->execute();
		if( ! $delete_attributes
		|| ! $delete_attributes_values
		|| ! $delete_tags
		|| ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false /* Delete local */
		|| ! Model_Helper_Action::deleteImages( $tb_name, $id ) /* Delete images */
		|| ! Model_Helper_Action::deleteById( $tb_name, $id ) ) { /* Delete main */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) { /* Unlink images */
			foreach( $images as $img ) {
				if( ! Helper_Image::unlink( $tb_name, $img->name ) ) {
					$warning[] = 'unlink_image';
				}
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
	
    /*
	public function importform()
	{
		$form = Helper_Form::openForm( $this->request->url(
				'cont_act_id',
				array( 
					'controller' => $this->request->getController(),
					'action' => 'import'
				),
				array( 'return_url' => $this->request->getParams( 'return_url' ) ) 
			) 
        );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::input( "csv", '', false, array( 'type' => 'file' ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		
		$buttons = Helper_Buttons::get(array( 'upload' => null, 'Επιστροφή' => Helper_Link::catalogue() ) );
		$this->response->setBody( Helper_View::form( $this, array('buttons' => $buttons, 'form' => $form ) )->render() );				
    }
        
    public function import()
    {
        $params = $this->request->getParams();
		$error = null;
		$warning = null;
		DB_ActiveRecord_Model::$db_model->startTransaction();
        if( ! empty( $params['files']['csv']['name'] ) ) {
            $extension = pathinfo($params['files']['csv']['name'], PATHINFO_EXTENSION);
            if($extension != 'csv') {
                $error = 'file_type';
            } else {
                $file = $params['files']['csv']['tmp_name'];
                $handle = fopen( $file, "r" );
                $rows = array();
                if( ! empty( $handle ) ) {
                    while( $rows[] = fgetcsv( $handle, 1000, "," ) ){}
                    fclose( $handle );
                }
                $rows = array_filter( $rows );
                // Remove headers
                array_shift( $rows );
                if(empty($rows)) {
                    $error = 'empty';
                } else {
                    foreach( $rows as $row ) {
                        // Check that required fields are not empty
                        if( empty( $row[0] ) 
                            || empty( $row[1] ) 
                        ) {
                            continue;
                        }
                        // Table properties
                        $category = new Model_Yiama_Category();
                        $cat = $category->query()
                            ->search( array( 'title="' . trim($row[0]) . '"' ) )
                            ->find( ':first' );
                        if( ! $cat ) {
                            continue;
                        }
                        $_POST['cat_id'] = $cat->id;
                        $_POST['is_published'] = 1;
                        $_POST['local'][1]['title'] = $row[1];
                        $_POST['local'][1]['description'] = ! empty($row[2]) ? $row[2] : '';
                        $_POST['local'][1]['short_description'] = ! empty($row[3]) ? $row[3] : '';
                        $_POST['local'][1]['text'] = ! empty($row[4]) ? $row[4] : '';
                        $_POST['local'][1]['meta_title'] = ! empty($row[5]) ? $row[5] : '';
                        $_POST['local'][1]['meta_description'] = ! empty($row[6]) ? $row[6] : '';
                        $_POST['local'][1]['meta_keywords'] = ! empty($row[7]) ? $row[7] : '';
                        $this->request->init();
                        if( ! $this->insert(false) ) {
                            $error = 'insert';
                            break;
                        }
                    }
                }
            }
            if( empty( $error ) ) {
                DB_ActiveRecord_Model::$db_model->commit();		
            } else {
                DB_ActiveRecord_Model::$db_model->rollback();	
            }
        } else {
            $error = 'insert';
        }
        Helper_Redirect::send( Helper_Link::catalogue(), null, array( 'insert', $error, array_filter( $warning ) ) );
    }
	
	public function exportform()
	{
		$form = Helper_Form::openForm( $this->request->url(
				'cont_act_id',
				array( 
					'controller' => $this->request->getController(),
					'action' => 'export'
				),
				array( 'return_url' => $this->request->getParams( 'return_url' ) ) 
			) 
        );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
        $language = new Model_Yiama_Language();
        $languages = $language->query()
            ->search( 'is_published = 1' )
            ->find();
        $form .= Helper_Form::select( "langcode", $languages, 'code', 'title', null, null, true );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		
		$buttons = Helper_Buttons::get(array( 'download' => null, 'Επιστροφή' => Helper_Link::catalogue() ) );
		$this->response->setBody( Helper_View::form( $this, array('buttons' => $buttons, 'form' => $form ) )->render() );				
    }

    public function export()
    {
        $params = $this->request->getParams();
        $language = new Model_Yiama_Language();
        $lang = $language->getCurrent();
        $model = new Model_Yiama_Article();
        $content = DB_ActiveRecord_Model::$db_model
			->table( 'ym_articles' )
            ->select( "ym_articles.*, ym_articles.ym_categories_id AS cat_id, {$lang->id} AS lang_id, local.*, c.title AS category" )		
            ->join( 'ym_articles_local AS local', 'local.ym_articles_id = ym_articles.id AND local.ym_languages_id = ' . $lang->id, 'LEFT' )
            ->join( 'ym_categories_local AS c', 'c.ym_categories_id = ym_articles.ym_categories_id AND c.ym_languages_id = ' . $lang->id, 'LEFT' )
			->execute();
        
        Helper_Export_Csv::export( array(
            'filename' => 'Άρθρα_' . $lang->code,
            'fields' => array(
                'id' => 'Id',
                'cat_id' => 'cat_id',
                'category' => 'Κατηγορία',
                'is_published' => 'Ενεργό',
                'time_created' => 'Εισήχθηκε',
                'time_updated' => 'Ενημερώθηκε',
                'visits' => 'Επισκέψεις',
                'alias' => 'Alias',
                'ordered' => 'Ταξινόμηση',
                'url' => 'Url',
                'lang_id' => 'lang_id',
                'title' => 'Τίτλος',
                'description' => 'Περιγραφή',
                'short_description' => 'Σύντομη περιγραφή',
                'text' => 'Κέιμενο',
                'meta_title' => 'Meta τίτλος',
                'meta_description' => 'Meta περιγραφή',
                'meta_keywords' => 'Meta keywords'
            ),
            'content' => $content
        ) );
    }
	
	public function mergeform()
	{
		$form  = Helper_Form::openForm( $this->request->url(
				'cont_act_id',
				array( 
					'controller' => $this->request->getController(),
					'action' => 'merge'
				),
				array( 'return_url' => $this->request->getParams( 'return_url' ) ) 
			) 
        );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::input( "csv", '', false, array( 'type' => 'file' ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		
		$buttons = Helper_Buttons::get(array( 'upload' => null, 'Επιστροφή' => Helper_Link::catalogue() ) );
		$this->response->setBody( Helper_View::form( $this, array('buttons' => $buttons, 'form' => $form ) )->render() );				
    }
        
    public function merge()
    {
        $params = $this->request->getParams();
		$error = null;
		$warning = null;
		DB_ActiveRecord_Model::$db_model->startTransaction();
        if( ! empty( $params['files']['csv']['name'] ) ) {
            $extension = pathinfo($params['files']['csv']['name'], PATHINFO_EXTENSION);
            if($extension != 'csv') {
                $error = 'file_type';
            } else {
                $file = $params['files']['csv']['tmp_name'];
                $handle = fopen( $file, "r" );
                $rows = array();
                if( ! empty( $handle ) ) {
                    while( $rows[] = fgetcsv( $handle, 1000, "," ) ){}
                    fclose( $handle );
                }
                $rows = array_filter( $rows );
                // Remove headers
                array_shift( $rows );
                if(empty($rows)) {
                    $error = 'empty';
                } else {
                    foreach( $rows as $row ) {
                        // Check that required fields are not empty
                        if( empty( $row[0] ) // Id
                            || empty( $row[1] ) // Category id
                            || empty( $row[10] ) // Language id
                        ) {
                            continue;
                        }
                        $_POST['cat_id'] = $row[1];
                        $_POST['is_published'] = $row[3];
                        $_POST['alias'] = $row[7];
                        $_POST['url'] = $row[9];
                        $_POST['local'][ $row[10] ]['title'] = $row[11];
                        $_POST['local'][ $row[10] ]['description'] = ! empty($row[12]) ? $row[12] : '';
                        $_POST['local'][ $row[10] ]['short_description'] = ! empty($row[13]) ? $row[13] : '';
                        $_POST['local'][ $row[10] ]['text'] = ! empty($row[14]) ? $row[14] : '';
                        $_POST['local'][ $row[10] ]['meta_title'] = ! empty($row[15]) ? $row[15] : '';
                        $_POST['local'][ $row[10] ]['meta_description'] = ! empty($row[16]) ? $row[16] : '';
                        $_POST['local'][ $row[10] ]['meta_keywords'] = ! empty($row[17]) ? $row[17] : '';
                        $this->request->init();
                        if( ! $this->update( $row[0], false) ) {
                            $error = 'update';
                            break;
                        }
                    }
                }
            }
            if( empty( $error ) ) {
                DB_ActiveRecord_Model::$db_model->commit();		
            } else {
                DB_ActiveRecord_Model::$db_model->rollback();	
            }
        } else {
            $error = 'update';
        }
        Helper_Redirect::send( Helper_Link::catalogue(), null, array( 'update', $error, array_filter( $warning ) ) );
    }
     */
}