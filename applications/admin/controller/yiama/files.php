<?php
class Controller_Yiama_Files extends Controller_Admin {

    private static $baseDir = PATH_ASSETS . "files";

    public static function directories() {
        $paths = Helper_File::subDirectoriesOf(self::$baseDir);
        array_unshift($paths, self::$baseDir);
        $directories = [];
        foreach ($paths as $path) {
            $directory = new StdClass();
            $directory->value = $path;
            $directory->text = $path;
            $directories[] = $directory;
        }
        return $directories;
    }

	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        $search = Model_Helper_Query::search(null, 'yiama_files');
        // Data
        $baseDir = self::$baseDir;
        $parentDir = null;

        // Get parent_id from search params
        if (!empty($search)) {
            foreach ($search as $s) {
                $sDecoded = urldecode($s);
                if (strpos($sDecoded, "parent_id", 0) === 0) {
                    $parent_id = str_replace("'", "", substr($sDecoded, strpos($sDecoded, "'")));
                    if (strpos($parent_id, "..") !== false) {
                        $parent_id = dirname(str_replace("..", "", $parent_id));
                    }
                    $parentDir = new \Yiama\File\File($parent_id);
                    break;
                }
            }
        }

        // Allow only if is child of baseDir
        if (!empty($parentDir) && $parentDir->isDirectory() && strlen($parentDir->getPath()) > strlen(self::$baseDir)) {
            $baseDir = $parentDir->getPath();
        } elseif (!empty($parentDir) && $parentDir->isFile() && strlen($parentDir->getPath()) > strlen(self::$baseDir)) {
            $baseDir = dirname($parentDir->getPath());
        }

        $filenames = scandir($baseDir);
        $data = [];
        foreach ($filenames as $filename) {
            if ($filename == ".") {
                continue;
            }
            $filePath = $baseDir . DS . $filename;
            $file = new class() {
                public function getPrimaryKey() {
                    return "path";
                }
            };
            $f = new \Yiama\File\File($filePath);
            $file->title = $filename;
            $file->size = $f->getHumanReadableSize();
            $file->mimeType = $f->isDirectory() ? "DIRECTORY" : $f->getMimeType();
            $file->path = $f->getPath();
            $file->id = $f->getPath();
            $data[] = $file;
        }

        // Catalogue
		$catalogue = new Catalogue_Yiama_Files(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(),
            'pagination_vars' => array('total_content' => 0)
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_FileInsert(
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert($redirect = true)
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];

		$fileParam = $params['files']['file'];
		if ((empty($fileParam['tmp_name']) && empty($params['dirname'])) ||
		    (!empty($fileParam['tmp_name']) && empty($params['file_directory'])) ||
		    (!empty($params['dirname']) && empty($params['parent_directory'])) ||
            (!empty($fileParam['tmp_name']) && !empty($params['file_directory']) && strlen($params['file_directory']) < self::$baseDir) ||
            (!empty($params['dirname']) && !empty($params['parent_directory']) && strlen($params['parent_directory']) < self::$baseDir)) {
            $error[] = 'insert_row';
        }

		$file = null;
		if (empty($error) && !empty($fileParam['tmp_name'])) {
		    $file = new \Yiama\File\File($params['file_directory'] . DS . $fileParam['name']);
		    $savePath = $file->getPath();
            if ($file->exists()) {
                $extra = "_" . date("YmdHis");
                if (strrpos($file->getPath(), ".") !== false) {
                    $savePath = substr($file->getPath(), 0, strrpos($file->getPath(), ".")) .
                        $extra . "." . $file->getExtension();
                } else {
                    $savePath = $file->getPath() . $extra;
                }
            }
            if (!move_uploaded_file($fileParam['tmp_name'], $savePath)) {
                $error[] = 'upload_file';
            }
        } elseif (empty($error) && !empty($params['dirname'])) {
            $newDir = $params['parent_directory'] . DS . $params['dirname'];
            $file = new \Yiama\File\File($newDir);
            if ($file->exists()) {
                $error[] = 'directory_exists';
            } else {
                if (!mkdir($newDir, 0777)) {
                    $error[] = 'create_directory';
                }
            }
        }
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'insert',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $search = [];
            if ($file != null) {
                $search["search[parent_id]"] = $file->isDirectory() ? $file->getPath() : dirname($file->getPath());
            }
            $this->response->redirect($this->request->url(
                'cont_act_id',
                array(
                    'controller' => 'yiama_files'
                ),
                $search
            ));
        } else {
			return empty($error);	
		}
	}
	
	public function updateform() {
        $data = new class() {
            public function getPrimaryKey() {
                return 'path';
            }
        };
        $file = new \Yiama\File\File($_GET['path']);
        $data->isFile = $file->isFile();
        $data->size = $file->getHumanReadableSize();
        $data->mimeType = $file->isDirectory() ? "DIRECTORY" : $file->getMimeType();
        $data->path = $file->getPath();
        $data->urlPath = "/" . str_replace(PATH_ROOT, "", $file->getPath());

        // Form
		$form = new Form_Yiama_FileUpdate(
            $data,
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function update($redirect = true)
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];

        if (empty($params["directory"]) || empty($params["name"])) {
            $error[] = "invalid_params";
        }

        $file = new \Yiama\File\File($params["current_path"]);
        if (empty($error)) {
            $currentDir = dirname($file->getPath());
            $currentName = basename($file->getPath());
            if ($params["directory"] != $currentDir || $params["name"] != $currentName) {
                if (!@rename($file->getPath(), $params["directory"] . DS . $params["name"])) {
                    $error[] = $file->isFile() ? "file_update" : "folder_update";
                }
            }
        }
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'update',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $search["search[parent_id]"] = $file->isDirectory() ? $file->getPath() : dirname($file->getPath());
            $this->response->redirect($this->request->url(
                'cont_act_id',
                array(
                    'controller' => 'yiama_files'
                ),
                $search
            ));
        } else {
			return empty($error);	
		}
	}

	public function delete($id = null, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
        // $id is used from bulkDelete
        $path = !empty($_GET['path']) ? $_GET['path'] : $id;

        $file = new \Yiama\File\File($path);
        if (($file->isFile() && !@unlink($file->getPath())) ||
            ($file->isDirectory() && !@rmdir($file->getPath()))) {
            $error[] = $file->isFile() ? "file_delete" : "folder_delete";
        }
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $search["search[parent_id]"] = $file->isDirectory() ? $file->getPath() : dirname($file->getPath());
            $this->response->redirect($this->request->url(
                'cont_act_id',
                array(
                    'controller' => 'yiama_files'
                ),
                $search
            ));
        } else {
			return empty($error);	
		}
	}
}