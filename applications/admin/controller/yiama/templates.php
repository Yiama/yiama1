<?php
 
class Controller_Yiama_Templates extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Template();
		$query = $model->query()
			->select( 'ym_templates.*, IF( COUNT( n.id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_notifications AS n', 'n.ym_templates_id = ym_templates.id', 'LEFT' ) 
			->group( 'ym_templates.id' )
			->search( Model_Helper_Query::search(null, 'yiama_templates') );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Templates(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
    }
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_TemplateInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_templates';	
		$data = new Model_Yiama_Template();
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		$data->text = $params['text'];
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		} else {
			file_put_contents( PATH_TEMPLATES . $data->file . '.php', $params['text'] );
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_templates/updateform/'.$data->id
        ));
    }
    
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Template();
		$form = new Form_Yiama_TemplateUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_templates';
		$data = new Model_Yiama_Template();
		$data = $data->findByKey( $id );
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		$data->text = $params['text'];
		if( ! $data->update() ) {
			$error[] = 'update_row';
		} else {
			file_put_contents( PATH_TEMPLATES . $data->file . '.php', $params['text'] );
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_templates/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_templates';
		$data = new Model_Yiama_Template();
		$file = $data->findByKey( $id )->file;
		if( ! Model_Helper_Action::deleteById( $tb_name, $id ) ) {
			$error[] = 'delete_row';
		} else {
			@unlink( PATH_TEMPLATES . $file . '.php' );
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>