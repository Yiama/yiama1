<?php
class Controller_Yiama_Attributes extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Data
		$model = new Model_Yiama_Attribute();
		$query = $model->query()
			->select( 'ym_attributes.*, local.*, IF( COUNT( af.ym_attributes_id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_articles_attributes AS af', 'af.ym_attributes_id = ym_attributes.id', 'LEFT' )
			->search( Model_Helper_Query::search(null, 'yiama_attributes') )
			->group( 'ym_attributes.id' );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Attributes(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_AttributeInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_attributes';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Attribute();
		$data->is_multivalue = isset( $params['values_titles'] ) ? 1 : 0;
		$data->attr_type = $params['type'];
		$data->description = $params['description'];
		$data->is_multiselect = isset( $params['is_multiselect'] ) ? 1 : 0;
		$data->is_published = $params['is_published'];
		$data->ordered = Model_Helper_Value::createOrdered( $data );
		$data->alias = Model_Helper_Value::createAlias( 
            !empty( $params['alias'] ) 
                ? $params['alias'] 
                : $params['local'][$this->language->id]['title'] 
        );
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Attributelocal();
				$data_local->ym_attributes_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
			if( empty( $error ) && isset( $params['values_titles'] ) ) { /* Values */
				foreach( array_filter( $params['values_titles'] ) as $val ) {
					$value = new Model_Yiama_Attributevalue();
					$search = $value->query()
						->search( array( "ym_attributes_id = {$data->id}", "title = '{$val}'" ) )
						->find();
					if( $search ) { /* Αν υπάρχει ήδη το value */
						continue;
					}
					$value->ym_attributes_id = $data->id;
					$value->ordered = intval( $value->getLastInsertedId()+1 )*100;
					if( ! $value->insert() ){ /* Insert value */
						$error[] = 'insert_row';
						break;
					} else { /* Insert local value !!! ΜΌΝΟ ΣΤΗΝ DEFAULT LANGUAGE */
						$value_local = new Model_Yiama_Attributevaluelocal();
						$lang = new Model_Yiama_Language();
						$value_local->ym_attributes_values_id = $value->getLastInsertedId();
						$value_local->ym_languages_id = $lang->getDefault()->id;
						$value_local->title = $val;
						if( ! $value_local->insert() ){
							$error[] = 'insert_row';
							break;
						}
					}
				}
			}
            // Categories
            if( empty( $error ) && isset( $params['cat_ids'] ) ) {
                $attribute_category = new Model_Yiama_Attributecategory();
                foreach ($params['cat_ids'] as $cat_id) {
                    $attribute_category->ym_attributes_id = $data->id;
                    $attribute_category->ym_categories_id = $cat_id;
                    if( !$attribute_category->insert() ){
                        $error[] = 'insert_row';
                        break;
                    }
                }
            }	
		}	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_attributes/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Attribute();
		$form = new Form_Yiama_AttributeUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_attributes';
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Attribute();
		$data = $data->findByKey( $id );
		$data->attr_type = $params['type'];
		$data->description = $params['description'];
		$data->is_multiselect = isset( $params['is_multiselect'] ) ? 1 : 0;
		$data->is_published = $params['is_published'];
		$data->alias = Model_Helper_Value::createAlias( 
            !empty( $params['alias'] ) 
                ? $params['alias'] 
                : $params['local'][$this->language->id]['title'] 
        );
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Attributelocal();
				$data_local->ym_attributes_id = $data->id;
				$data_local->ym_languages_id = $key;
				$data_local->title = @$l['title'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
			if( empty( $error ) && isset( $params['values_titles'] ) ) { /* Values */
				foreach( array_filter( $params['values_titles'] ) as $val ) {
					$value = new Model_Yiama_Attributevalue();
					$search = $value->query()
						->search( array( "ym_attributes_id = {$data->id}", "title = '{$val}'" ) )
						->find();
					if( $search ) { /* Αν υπάρχει ήδη το value */
						continue;
					}
					$value->ym_attributes_id = $data->id;
					$value->ordered = intval( $value->getLastInsertedId()+1 )*100;
					if( ! $value->insert() ){ /* Insert value */
						$error[] = 'insert_row';
						break;
					} else { /* Insert local value !!! ΜΌΝΟ ΣΤΗΝ DEFAULT LANGUAGE */
						$value_local = new Model_Yiama_Attributevaluelocal();
						$lang = new Model_Yiama_Language();
						$value_local->ym_attributes_values_id = $value->getLastInsertedId();
						$value_local->ym_languages_id = $lang->getDefault()->id;
						$value_local->title = $val;
						if( ! $value_local->insert() ){
							$error[] = 'insert_row';
							break;
						}
					}
				}
			}
            // Categories
            if( empty( $error )) {
                $attribute_category = new Model_Yiama_Attributecategory();
                // First delete all connected categories
                $result = DB_ActiveRecord_Model::$db_model
                    ->table('ym_attributes_categories')
                    ->search('ym_attributes_id = ' . $id)
                    ->delete()
                    ->execute();
                if (!$result) {
                    $error[] = 'delete_row';
                } // Insert categories
                elseif( isset( $params['cat_ids'] ) ) {
                    foreach ($params['cat_ids'] as $cat_id) {
                        $attribute_category->ym_attributes_id = $data->id;
                        $attribute_category->ym_categories_id = $cat_id;
                        if( !$attribute_category->insert() ){
                            $error[] = 'insert_row';
                            break;
                        }
                    }
                }
            }	
		}	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_attributes/updateform/'.$data->id
        ));
	}
	
	public function delete( $id, $redirect = true )
	{
        $params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_attributes';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Attribute();
		$data = $data->findByKey( $id );
		$cont = new Controller_Task();
		$result1 = DB_ActiveRecord_Model::$db_model
			->table( 'ym_attributes_categories' )
			->search( "ym_attributes_id = {$id}" )
            ->delete()
			->execute();
        $values = DB_ActiveRecord_Model::$db_model
            ->table( 'ym_attributes_values' )
            ->select( 'GROUP_CONCAT( id ) AS ids' )
            ->search( "ym_attributes_id = {$id}" )
            ->execute( 'one' );
		if(!$result1
        || ! $cont->bulkDelete( array( 'ids' => $values['ids'], 'controller' => 'yiama_attributevalues' ), false )
		|| ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false 
		|| ! $data->delete() ) { /* Delete main, delete local, delete values */
			$error[] = 'delete_row';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}