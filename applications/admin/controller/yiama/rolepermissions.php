<?php
 
class Controller_Yiama_Rolepermissions extends Controller_Admin {

    public function __construct() {
        parent::__construct();

        $this->checkRequestMethod(array(
            'update' => 'post',
            'delete' => 'post'
        ));
    }
	
	public function updateform() {
        // Form
        $data = new Model_Yiama_Rolepermission();
		$params = $this->request->getParams();
        $form = new Form_Yiama_RolePermissionUpdate(
            $data->findByKey(array($params['ym_roles_id'], $params['ym_permissions_id'])),
            $this->request,
            $this->translator
        );

        // Buttons
        $return_url = $this->request->getParams("return_url");
        $buttons = Helper_Buttons::get(
            $this->translator,
            array(
                'save' => null,
                'add' => 'asdasdasd'//Helper_Link::insertform()
            ),
            array('return' => !empty($return_url) ? urldecode($return_url) : "")
        );

        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons,
            'form' => $form->render()
        ));
        $this->response->setBody($view->render());
	}
	
	public function update()
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_roles_permissions';
		$data = new Model_Yiama_Rolepermission();
		$data = $data->findByKey(array($params['ym_roles_id'], $params['ym_permissions_id']));
        $data->state = $params['state'];
        if( ! $data->update() ) {
            $error[] = 'update_row';	
        }
		Helper_Redirect::send( $params['return_url'], null, array( 'update', $error, array_filter( $warning ) ) );
	}
	
	public function delete()
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
		$tb_name = 'ym_roles_permissions';
		$data = new Model_Yiama_Rolepermission();
		$data = $data->findByKey( array( $params['ym_roles_id'], $params['ym_permissions_id'] ) );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error ) );
	}
}

?>