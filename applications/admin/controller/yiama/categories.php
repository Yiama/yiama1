<?php
class Controller_Yiama_Categories extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Category();
		$query = $model->query()
			->select( 'ym_categories.*, local.*, '
                . 'IF( COUNT( a.id ) OR COUNT( c2.id ), 0, 1 ) as allow_deletion,'
                . 'cpl.title parenttitle, u.username' )
			->join( 'ym_categories AS c2', 'c2.parent_id = ym_categories.id', 'LEFT' )
			->join( 
                    'ym_categories_local AS cpl', 
                    'cpl.ym_categories_id = ym_categories.parent_id AND '
                    . "cpl.ym_languages_id = {$this->language->id}")
			->join( 'ym_articles AS a', 'a.ym_categories_id = ym_categories.id', 'LEFT' ) 
			->join('ym_users u', 'u.id = ym_categories.ym_users_id')
			->group( 'ym_categories.id' )
			->search( 
                array_merge( array( 'ym_categories.id > 1' ), 
                Model_Helper_Query::search(null, 'yiama_categories') ) 
            );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Categories(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {        
        // Form
		$form = new Form_Yiama_CategoryInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert() {
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$warning = null;
		$tb_name = 'ym_categories';
		$user = new Model_Yiama_User();
		DB_ActiveRecord_Model::$db_model->startTransaction();	
		$data = new Model_Yiama_Category();
		$data->parent_id = $params['parent_id'];
		$data->is_published = $params['is_published'];
		$data->ordered = Model_Helper_Value::createOrdered( $data );
		$data->url = Model_Helper_Value::createURL( $data );
        $user = new Model_Yiama_User();
        $data->ym_users_id = $user->getLogged()->id;
		if( ! $data->insert() ) { /* Main entity */
			$error[] = 'insert_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Categorylocal();
				$data_local->ym_categories_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] ) 
                            ? $l['title'] 
                            : 'category_' . $key),
                    function($alias_value) use ($data_local) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->text = @$l['text'];
				$data_local->meta_title = @$l['meta_title'];
				$data_local->meta_description = @$l['meta_description'];
				$data_local->meta_keywords = @$l['meta_keywords'];
				if( ! $data_local->insert() ){
					$error[] = 'insert_row';
					break;
				}
			}
		}
		if( empty( $error ) ) { /* Insert image */
			$task = new Controller_Task();
			$warning[] = $task->insertImage( $data, $tb_name );
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_categories/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Category();
		$form = new Form_Yiama_CategoryUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$warning = null;
		$tb_name = 'ym_categories';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Category();
		$data = $data->findByKey( $id );
		$data->parent_id = $params['parent_id'];
		$data->is_published = $params['is_published'];
		$data->url = ! isset( $params['url'] ) ? Model_Helper_Value::createURL( $data ) : $params['url'];
		if( ! $data->update() ) { /* Main entity */
			$error[] = 'update_row';
		} else { /* Local */
			foreach( $params['local'] as $key => $l ) {
				$data_local = new Model_Yiama_Categorylocal();
				$data_local->ym_categories_id = $data->id;
				$data_local->ym_languages_id = $key;
                $data_local->alias = Model_Helper_Value::createAlias(
                    !empty( $l['alias'] ) 
                        ? $l['alias'] 
                        : ( !empty( $l['title'] )
                            ? $l['title'] 
                            : 'category_' . $key),
                    function($alias_value) use ($data_local, $data, $key) {
                        return $data_local
                            ->query()
                            ->search(array(
                                "NOT (ym_categories_id =" . $data->id
                                    ." AND ym_languages_id =". $key . ")",
                                "alias = '" . $alias_value . "'"
                            ))
                            ->find();
                    },
                    $data->id
                );
				$data_local->title = @$l['title'];
				$data_local->description = @$l['description'];
				$data_local->text = @$l['text'];
				$data_local->meta_title = @$l['meta_title'];
				$data_local->meta_description = @$l['meta_description'];
				$data_local->meta_keywords = @$l['meta_keywords'];
				if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
					if( ! $data_local->update() ) {
						$error[] = 'update_row';	
						break;
					}
				} elseif ( ! $data_local->insert() ) { /* Insert */
					$error[] = 'insert_row';	
					break;	
				}
			}
		}
		if( empty( $error ) ) {
			$task = new Controller_Task();
			$warning[] = $task->deleteImage( $data, $tb_name );
			if( empty( $warning[0] ) ) { /* Insert image */
				$warning[] = $task->insertImage( $data, $tb_name );
			}
		} 	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_categories/updateform/'.$data->id
        ));
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
		$tb_name = 'ym_categories';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Category();
		$data = $data->findByKey( $id );
		if( ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false 
		|| ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		if( empty( $error ) 
		&& ! empty( $data->image )
		&& ! Helper_Image::unlink( $tb_name, $data->image ) ) { /* Delete image */
			$warning[] = 'unlink_image';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
	
    /*
	public function importform()
	{
		$form = Helper_Buttons::get(array( 'save' => null ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( $this->request->url(
				'cont_act_id',
				array( 
					'controller' => $this->request->getController(),
					'action' => 'import'
				),
				array( 'return_url' => $this->request->getParams( 'return_url' ) ) 
			) 
        );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
        $form .= Helper_Form::input( "csv", '', false, array( 'type' => 'file' ) );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
	
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form ) )->render() );				
    }
        
    public function import()
    {
        $params = $this->request->getParams();
		$error = null;
		$warning = null;
		DB_ActiveRecord_Model::$db_model->startTransaction();
        if( ! empty( $params['files']['csv']['name'] ) ) {
            $extension = pathinfo($params['files']['csv']['name'], PATHINFO_EXTENSION);
            if($extension != 'csv') {
                $error = 'file_type';
            } else {
                $file = $params['files']['csv']['tmp_name'];
                $handle = fopen( $file, "r" );
                $rows = array();
                if( ! empty( $handle ) ) {
                    while( $rows[] = fgetcsv( $handle, 1000, ";" ) ){}
                    fclose( $handle );
                }
                $rows = array_filter( $rows );
                // Remove headers
                array_shift( $rows );
                if(empty($rows)) {
                    $error = 'empty';
                } else {
                    foreach( $rows as $row ) {
                        // Check that required fields are not empty
                        if( empty( $row[0] ) 
                            || empty( $row[1] ) 
                        ) {
                            continue;
                        }
                        // Table properties
                        $category = new Model_Yiama_Category();
                        $cat = $category->query()
                            ->search( array( 'title="' . trim($row[0]) . '"' ) )
                            ->find( ':first' );
                        if( ! $cat ) {
                            continue;
                        }
                        $_POST['parent_id'] = $cat->id;
                        $_POST['is_published'] = 1;
                        $_POST['local'][1]['title'] = $row[1];
                        $_POST['local'][1]['description'] = ! empty($row[2]) ? $row[2] : '';
                        $_POST['local'][1]['short_description'] = ! empty($row[3]) ? $row[3] : '';
                        $_POST['local'][1]['text'] = ! empty($row[4]) ? $row[4] : '';
                        $_POST['local'][1]['meta_title'] = ! empty($row[5]) ? $row[5] : '';
                        $_POST['local'][1]['meta_description'] = ! empty($row[6]) ? $row[6] : '';
                        $_POST['local'][1]['meta_keywords'] = ! empty($row[7]) ? $row[7] : '';
                        $this->request->init();
                        if( ! $this->insert(false) ) {
                            $error = 'insert_row';
                            break;
                        }
                    }
                }
            }
            if( empty( $error ) ) {
                DB_ActiveRecord_Model::$db_model->commit();		
            } else {
                DB_ActiveRecord_Model::$db_model->rollback();	
            }
        } else {
            $error = 'insert_row';
        }
        Helper_Redirect::send( Helper_Link::catalogue(), null, array( 'insert', $error, array_filter( $warning ) ) );
    }
    */
}