<?php
 
class Controller_Yiama_Attributevalues extends Controller_Admin {

    public function __construct() {
        parent::__construct();

        $this->checkRequestMethod(array(
            'update' => 'post'
        ));
    }

    public function updateform($id) {
        // Form
        $data = new Model_Yiama_Attributevalue();
        $form = new Form_Yiama_AttributeValueUpdate(
            $data->findByKey($id),
            $this->request,
            $this->translator
        );

        // Buttons
        $return_url = $this->request->getParams("return_url");
        $buttons = Helper_Buttons::get(
            $this->translator,
            array(
                'save' => null,
                'add' => Helper_Link::insertform()
            ),
            array('return' => !empty($return_url) ? urldecode($return_url) : "")
        );

        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons,
            'form' => $form->render()
        ));
        $this->response->setBody($view->render());
    }
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_attributes_values';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Attributevalue();
		$data = $data->findByKey( $id );
		foreach( $params['local'] as $key => $l ) {
			$data_local = new Model_Yiama_Attributevaluelocal();
			$data_local->ym_attributes_values_id = $data->id;
			$data_local->ym_languages_id = $key;
			$data_local->title = @$l['title'];
			if( $data_local->findByKey( array( $data->id, $key ) ) ) { /* Update if exists */
				if( ! $data_local->update() ) {
					$error[] = 'update_row';	
					break;
				}
			} elseif ( ! $data_local->insert() ) { /* Insert */
				$error[] = 'insert_row';	
				break;	
			}
		}
		if( empty( $error ) ) {
			$task = new Controller_Task();
			$warning[] = $task->deleteImage( $data, $tb_name );
			if( empty( $warning[0] ) ) { /* Insert image */
				$warning[] = $task->insertImage( $data, $tb_name );
			}
		} 	
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'update', $error, array_filter( $warning ) ) );
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$error = null;
        $warning = [];
		$tb_name = 'ym_attributes_values';
		DB_ActiveRecord_Model::$db_model->startTransaction();
		$data = new Model_Yiama_Attributevalue();
		$data = $data->findByKey( $id );
		$image = $data->image;
		if( ! Model_Helper_Action::deleteLocal( $tb_name, $id ) !== false
		|| ! $data->delete() ) { /* Delete main, delete local */
			$error[] = 'delete_row';
		}
		if( empty( $error ) 
		&& ! empty( $image )
		&& ! Helper_Image::unlink( $tb_name, $image ) ) { /* Delete image */
			$warning[] = 'unlink_image';
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
		if( $redirect ) {
			Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error, $warning ) );
		} else {
			return empty( $error );	
		}
	}
}

?>