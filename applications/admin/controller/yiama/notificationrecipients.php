<?php
 
class Controller_Yiama_Notificationrecipients extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
	}
	
	public function delete()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_notificationrecipients';
		$data = new Model_Yiama_Notificationrecipient();
		$data = $data->findByKey( $params['id'] );
		if( ! $data->delete() ) {
			$error[] = 'delete_row';
		}
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'delete',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
	}
}

?>