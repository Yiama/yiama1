<?php
 
class Controller_Yiama_Pagetypes extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
        
        $this->checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post'
        ));
	}
	
	public function index() {
        // Query
		$model = new Model_Yiama_Pagetype();
		$query = $model->query()
			->select( 'ym_pagetypes.*, '
                . 'IF( COUNT( p.id ), 0, 1 ) as allow_deletion' )
			->join( 'ym_pages AS p', 'p.ym_pagetypes_id = ym_pagetypes.id', 'LEFT' )
			->search( Model_Helper_Query::search(null, 'yiama_pagetypes') )
			->group( 'ym_pagetypes.id' );
        $cloned_query = clone $query;
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Yiama_Pagetypes(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array('add' => Helper_Link::insertform())
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
	
	public function insertform() {
        // Form
		$form = new Form_Yiama_PagetypeInsert(
            $this->request, 
            $this->translator
        );
		
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pagetypes';	
		$data = new Model_Yiama_Pagetype();
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		}
        
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'insert',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_pagetypes/updateform/'.$data->id
        ));
	}
	
	public function updateform($id) {
        // Form
		$data = new Model_Yiama_Pagetype();
		$form = new Form_Yiama_PagetypeUpdate(
            $data->findByKey($id), 
            $this->request, 
            $this->translator
        );
		
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array( 
                'save' => null, 
                'add' => Helper_Link::insertform() 
            ), 
            array('return' => Helper_Link::catalogue()) 
        );
        
        // Response
        $view = $this->getViewForm(array(
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'form' => $form->render() 
        ));
		$this->response->setBody($view->render());
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pagetypes';
		$data = new Model_Yiama_Pagetype();
		$data = $data->findByKey( $id );
		$data->title = $params['title'];
		$data->alias = Model_Helper_Value::createAlias( !empty( $params['alias'] ) ? $params['alias'] : $params['title'] );
		if( ! $data->update() ) {
			$error[] = 'update_row';
		}
	
        // Response
		$helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'update',
            'error' => $error,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect($this->request->urlFromPath(
            'yiama_pagetypes/updateform/'.$data->id
        ));
    }
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		$this->checkCsrf($params);
		$error = null;
        $warning = [];
		$tb_name = 'ym_pagetypes';
		if( ! Model_Helper_Action::deleteById( $tb_name, $id ) ) {
			$error[] = 'delete_row';
		}
        
        // Response
		if($redirect) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'delete',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
        } else {
			return empty($error);	
		}
	}
}

?>