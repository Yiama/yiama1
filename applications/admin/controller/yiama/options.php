<?php
 
class Controller_Yiama_Options extends Core_Controller
{
    public function before()
	{
		Helper_Before::checkLoggedUser();
        Helper_Before::checkRequestMethod(array(
            'insert' => 'post',
            'update' => 'post',
            'delete' => 'post'
        ));
		$user = new Model_Yiama_User();
		$logged_user = $user->getLogged();
        $has_permissions = $logged_user->hasPermissions(array(
            'options_crud'
        )) ? true : false;
        if (!$has_permissions) {
            Helper_Redirect::send(Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'dashboard',
                        'action' => 'index' 
                    )
                ), 
                null, 
                array('error', 'no_privileges')
            );
        }
	}
	
	public function index()
	{
		$data = new Model_Yiama_Option();
		$query = $data->query()
			->search( Model_Helper_Query::search(null, 'yiama_options') );
        $cloned_query = clone $query;
		$options = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_search_data = $cloned_query->find();
		Helper_Catalogue::setBulkActions( 'ym_options', 'delete' );
		Helper_Catalogue::setHeaders( array(
			'Ιδιότητα' => 'key',
			'Τιμή' => 'value'
		) );
		Helper_Catalogue::setSearch( array(
			array( 'like' => 'key' ),
			array( 'empty' => null )
		) );
		Helper_Catalogue::setContent( 'ym_options', $options, array(
			array( 'text' => 'key' ),
			array( 'text' => 'value' )
		) );
		Helper_Catalogue::setContentActions( $options, array( 'update', 'delete' ) );
		$catalogue = Helper_Catalogue::get();
		$buttons = Helper_Buttons::get(array( 'add' => Helper_Link::insertform() ) );
		/* View */
		$this->response->setBody( Helper_View::catalogue( $this, array( 'buttons' => $buttons, 'catalogue' => $catalogue, 'pagination_vars' => array( 'total_content' => count( $all_search_data ) ) ) )->render() );
	}
	
	public function insertform()
	{
		$form = Helper_Buttons::get(array( 'save' => null ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::insert() );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'key', null, true, null, 'Ιδιότητα' );
		$form .= Helper_Form::input( 'value', null, true, null, 'Τιμή' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form ) )->render() );
	}
	
	public function insert()
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
		$tb_name = 'ym_options';	
		$data = new Model_Yiama_Option();
		$data->key = $params['key'];
		$data->value = $params['value'];
		if( ! $data->insert() ) {
			$error[] = 'insert_row';
		}
		Helper_Redirect::send( Helper_Link::updateform( $data ), null, array( 'insert', $error ) );
	}
	
	public function updateform( $id )
	{
		$tb_name = 'ym_options';
		$data = new Model_Yiama_Option();
		$data = $data->findByKey( $id );
		$form = Helper_Header::get( $data->key, array( 'save' => null ), array( 'Επιστροφή' => Helper_Link::catalogue() ) );
		$form .= Helper_Form::openForm( Helper_Link::update( $data ) );
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::input( 'key', $data->key, true, null, 'Ιδιότητα' );
		$form .= Helper_Form::input( 'value', $data->value, true, null, 'Τιμή' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn( Helper_Link::updateform( $data ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form ) )->render() );
	}
	
	public function update( $id )
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
		$tb_name = 'ym_options';
		$data = new Model_Yiama_Option();
		$data = $data->findByKey( $id );
		$data->key = $params['key'];
		$data->value = $params['value'];
		if( ! $data->update() ) {
			$error[] = 'update_row';
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'update', $error ) );
	}
	
	public function delete( $id, $redirect = true )
	{
		$params = $this->request->getParams();
		Helper_Before::checkCsrf($params);
		$error = null;
		$tb_name = 'ym_options';
		if( ! Model_Helper_Action::deleteById( $tb_name, $id ) ) {
			$error[] = 'delete_row';
		}
		if( $redirect ) {
			Helper_Redirect::send( $params['return_url'], null, array( 'delete', $error ) );
		} else {
			return empty( $error );	
		}
	}
}

?>