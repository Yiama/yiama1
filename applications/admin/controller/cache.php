<?php
 
class Controller_Cache extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
	}
	
	public function flush()
	{
		$cache = new Core_Cache();
		$cache->flush();
				
		$this->response->redirect( $this->request->urlFromPath( Core_App::getConfig( 'applications.admin.defaultpage' ) ) );
	}
}

?>
