<?php
class Controller_Admin extends Core_Controller { 
    
    public function __construct() {
        parent::__construct();
        
        // Check user
        $user = new Model_Yiama_User();
        if( ! ( $logged_user = $user->getLogged() )) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'user',
                        'action' => 'loginform' 
                    )
                )
            );
        }
        
        // Locales
        $language = new Model_Yiama_Language();
        $this->language = $language->getDefault();
        $locale = $this->language->code;
        $this->translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $this->translator->setLocale($locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'default.php'), $locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'dashboard.php'), $locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'catalogue.php'), $locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'form.php'), $locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'messages.php'), $locale);
    }

    /**
     * @param array $action_methods ['action' => 'method']
     */
    protected function checkRequestMethod($action_methods) {
        $action = Core_Request::getInstance()->getAction();
        $method = strtolower(Core_Request::getInstance()->getMethod());
        foreach ($action_methods as $act => $meth) {
            if (    ($act == $action || $act == '*')
                &&  $meth != $method) {
                Core_Response::getInstance()->redirect(
                    Core_Request::getInstance()->url(
                        'cont_act_id',
                        array( 
                            'controller' => 'error',
                            'action' => 'notfound' 
                        )
                    )
                );
            }
        }
    }

    /**
     * @param array $params
     */
    protected function checkCsrf(array $params = array()) {
        $csrf = new Security_Csrf(Core_Session::getInstance());
        if (    empty($params[$csrf->getRequestTokenName()])
            ||  !$csrf->isValid($params[$csrf->getRequestTokenName()])) {
            Core_Response::getInstance()->redirect(
                Core_Request::getInstance()->url(
                    'cont_act_id',
                    array( 
                        'controller' => 'error',
                        'action' => 'notfound' 
                    )
                )
            );
        }
    }
    
    protected function getViewCatalogue(array $params = array()) {
		$controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS
            .'catalogue.php');
        $controller_view->addVars($params);
		$main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'
            .DS.'default.php' );
        $main_view->addVars(array_merge(
            $params, 
            array('controller_view' => $controller_view)
        ));
		return $main_view;
    }
    
    protected function getViewForm(array $params = array()) {
		$controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS
            .'form.php');
        $controller_view->addVars($params);
		$main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'
            .DS.'default.php' );
        $main_view->addVars(array_merge(
            $params, 
            array('controller_view' => $controller_view)
        ));
		return $main_view;
    }
}