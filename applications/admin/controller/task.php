<?php
class Controller_Task extends Controller_Admin
{
	public function __construct() {
        parent::__construct();
        $this->checkCsrf($this->request->getParams());
	}
	
	/* Publish - unpublish */
	public function publish( $id )
	{
		$params = $this->request->getParams();
        $warning = [];
		if( ! Model_Helper_Action::publish( $params['tb_name'], $id ) ){
			$warning[] = 'publish';
		}
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'publish',
            'error' => null,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
	}

    /* Favorite - unfavorite */
    public function favorite( $id )
    {
        $params = $this->request->getParams();
        $warning = [];
        if( ! Model_Helper_Action::favorite( $params['tb_name'], $id ) ){
            $warning[] = 'favorite';
        }
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'action',
            'error' => null,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
    }
	
	/* Order up - down */
	public function order( $id )
	{
		$params = $this->request->getParams();
        $warning = [];
		$order = $params['order_dir'];
		if( ( $order == 'up' && ! Model_Helper_Action::orderUp( $params['tb_name'], $id, @$params['group_field'] ) )
		|| ( $order == 'down' && ! Model_Helper_Action::orderDown( $params['tb_name'], $id, @$params['group_field'] ) ) ){
			$warning[] = 'order';
		}
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'order',
            'error' => null,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
	}
	
	/* Bulk publish - unpublish */
	public function bulkpublish()
	{
		$params = $this->request->getParams();
        $warning = [];
		$ids = array_filter( ( array ) explode( ',', $params['ids'] ) );
		if( empty( $params['ids'] ) ) {
			$warning[] = 'no_rows_selected';
		} elseif ( ! Model_Helper_Action::bulkPublish( $params['tb_name'], $ids ) ){
			$warning[] = 'bulk_publish';
		}
        $helper_message = new Helper_Message(Core_Session::getInstance());
        $helper_message->store(array(
            'bulk_publish',
            'error' => null,
            'warning' => array_filter($warning)
        ));
        $this->response->redirect(urldecode($params['return_url']));
	}
	

	public function bulkdelete( $params = null, $redirect = true )
	{
		$params = is_null( $params ) ? $this->request->getParams() : $params;
		$error = null;
        $warning = [];
		$ids = array_filter( ( array ) explode( ',', $params['ids'] ) );
		if( empty( $ids ) ) {
			$warning[] = 'no_rows_selected';
		} else {
			$cont = 'Controller_' . $params['controller'];
			$controller = new $cont();
			foreach( $ids as $id ) {
				if( ! $controller->delete( $id, false ) ) {
					$error[] = 'bulk_deletion'; 
					break;
				}
			}
		}
		if( $redirect ) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                'bulk_deletion',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
            $this->response->redirect(urldecode($params['return_url']));
		} else {
			return empty($error);	
		}
	}
	
	public function insertImage( $model_instance, $tb_name )
	{
		$params = $this->request->getParams();
		$error = null;
		if( ! empty( $params['files']['images']['name'][0] ) ) {
			if( ( $new_name = Helper_Image::upload( 
				$tb_name, 
				$model_instance->findAttr( 'image' ), 
				$params['files']['images']['name'][0], 
				$params['files']['images']['tmp_name'][0] ) ) === false ) {
					$error = 'upload_image';
			} else {
				$model_instance->image = $new_name;
				if( ! $model_instance->update() ) {
					Helper_Image::unlink( $tb_name, $new_name );
					$error = 'insert_image';
				}	
			}
		} 
		return $error;	
	}
	
	public function deleteImage( $model_instance, $tb_name )
	{
		$params = $this->request->getParams();
		$error = null;
		if( ! empty( $params['delete_image'] )
			|| ( ! empty( $params['files']['images']['name'][0] )
				&& ! empty( $model_instance->image ) ) ) { /* Delete image */
			$old_image = $model_instance->image;
			$model_instance->image = '@func:NULL';
			if( ! $model_instance->update() ) {
				$error = 'update_image';	
			} elseif ( ! Helper_Image::unlink( $tb_name, $old_image ) ) {
				$error = 'unlink_image';
			}
		}
		return $error;	
	}
	
	public function insertImages( $model_instance, $tb_name, $foreign_id )
	{
		$params = $this->request->getParams();
		$error = null;
		$foreign_key = $tb_name . '_id';
		if( ! empty( $params['files']['images']['name'][0] ) ) {
			foreach( array_filter( $params['files']['images']['name'] ) as $key => $name ) {
				$db_names = $model_instance->findAttr( 'name' );
				if( ( $new_name = Helper_Image::upload( $tb_name, $db_names, $name, $params['files']['images']['tmp_name'][ $key ] ) ) === false ) {
					$error = 'upload_image';
					break;
				} else {
					$model_instance->name = $new_name;
					$model_instance->$foreign_key = $foreign_id;
					$model_instance->is_published = 1;
					$model_instance->is_default = 0;
					if( ! $model_instance->insert() ) {
						Helper_Image::unlink( $tb_name, $new_name );
						$error = 'insert_image';
						break;
					}	
				}
			}
		}
		return $error;	 	
	}
	
	public function deleteImages( $model_instance, $tb_name )
	{
		$params = $this->request->getParams();
		$error = null;
		if( ! empty( $params['delete_images'] ) ) {
			$names = $model_instance->query()
				->search( 'id IN( ' . implode( ',', $params['delete_images'] ) . ' )' )
				->find( 'name' );
			$result = DB_ActiveRecord_Model::$db_model
				->table( $tb_name . '_images' )
				->delete()
				->search( 'id IN( ' . implode( ',', $params['delete_images'] ) . ' )' )
				->execute();
			if( $result ) {
				foreach( $names as $n ) {
					if( ! Helper_Image::unlink( $tb_name, $n ) ) {
						$error = 'unlink_image';
					}
				}
			} else {
				$error = 'delete_image';
			}
		}
		return $error;	
	}
	
	public function updateImages( $tb_name )
	{
		$params = $this->request->getParams();
		$error = null;
		if( ! empty( $params['alt_images'] ) ) {
			foreach( $params['alt_images'] as $key => $value ) {
				$result = DB_ActiveRecord_Model::$db_model
					->table( $tb_name . '_images' )
					->update( array( 'alt' => $value ) )
					->search( array( 'id = ' . $key ) )
					->execute();
				if( ! $result ) {
					$error = 'update_image';
				}
			}
		}
		return $error;	
	}
}

?>