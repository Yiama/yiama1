<?php
 
class Controller_Backup extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
	}
	
	/*
	 * Αν ο server επιτρέπει να ορίσουμε δικαίωμα FILE στον χρήστη ( απαιτείται για να 
	 * γράψουμε σε αρχείο σε κάποιο directory του server μέσω της mysql )
	 * χρησιμοποιούμε για πιο γρήγορη εκτέλεση αυτήν την db().
	 * 
	 * " GRANT FILE ON *.* TO '<mysql_user>'@'localhost' "
	 */
	/*
	public function db()
	{
		$backupFolder = Core_App::getConfig( 'applications.admin.backupFolder' ) . DS . 'db';
		
		// Διαγραφή των υπαρχόντων αρχείων 
		$files = glob( $backupFolder . DS . '*' );
		foreach( $files as $f )
		{
			if( is_file( $f ) )
			{
				@unlink( $f );
			}
		}
		
		// Query και export
		$this->dbModel = new DB_Model( Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.database' ) );
		$tables = $this->dbModel
			->query( "SHOW TABLES" )
			->execute();
		
		foreach( $tables as $key => $t )
		{
			$tableName = current( $t );
			$file = $backupFolder . DS . $tableName . ".sql";
			
			$this->dbModel
				->query( "SELECT *
						INTO OUTFILE '" . str_replace( DS, '/', $file ) . "'
						FIELDS TERMINATED BY ', '
						OPTIONALLY ENCLOSED BY '\"'
						LINES TERMINATED BY '),\n('
						FROM {$tableName}"
				)
				->execute();
			
			// Πρόσθεση MySQL εντολών στο αρχείο
			$fileContent = "INSERT INTO {$tableName} VALUES \n(" . substr( file_get_contents( $file ), 0, -3 ) . ";";
			file_put_contents( $file, $fileContent );
		}
				
		$this->redirect->go( 1, null, array( 'controller' => $this->params['cont'] ) );
	}
	*/
	
	public function db()
	{
		$params = $this->request->getParams();
		$error = null;	
		$backupFolder = Core_App::getConfig( 'applications.admin.backupFolder' ) . DS . 'db';
		
		//Create dir if not exists
		if( ! file_exists( $backupFolder ) ) { 
		   mkdir( $backupFolder, 0777, true);
		} 
		
		// Διαγραφή των υπαρχόντων αρχείων 
		$files = glob( $backupFolder . DS . '*' );
		foreach( $files as $f )
		{
			if( is_file( $f ) )
			{
				@unlink( $f );
			}
		}
		
		$this->dbModel = new DB_Model( Core_App::getConfig( 'applications.' . Core_App::getAppName() . '.database' ) );
		$tables = $this->dbModel
			->query( "SHOW TABLES" )
			->execute();

		foreach( $tables as $key => $t )
		{
			$tableName = current( $t );
			$file = $backupFolder . DS . $tableName . ".sql";
			
			$result = $this->dbModel
				->table( $tableName )
				->select( "*" )
				->execute();
			
			$query = "";
			foreach( $result as $r )
			{
				$values = array();
				foreach( $r as $value )
				{
					$values[] = is_null( $value ) ? "\N" : "'" . $this->dbModel->escape( $value ) . "'";
				}
				$query .= "( " . implode( ", ", $values ) . " ),\n";
			}	
				
			$fileContent = "INSERT INTO {$tableName} VALUES \n" . substr( $query, 0, -2 ) . ";\n";
			$error = file_put_contents( $file, $fileContent ) === false ? 'backup' : null;
		}
				
		Helper_Redirect::send( null, null, array( 'backup' ) );
	}
}

?>
