<?php
 
class Controller_User extends Core_Controller
{
    public function __construct() {
        parent::__construct();

        // Locales
        $language = new Model_Yiama_Language();
        $this->language = $language->getDefault();
        $locale = $this->language->code;
        $this->translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $this->translator->setLocale($locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'default.php'), $locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'messages.php'), $locale);
    }
    
	public function loginform()
	{
		$form = Helper_Form::openForm($this->request->url(
			'cont_act_id',
			array(  
				'controller' => 'user',
				'action' => 'login'
		)));
		$form .= Helper_Form::input( 'username', null, true, array( 'id' => 'inputname', 'validValues' => 'valLatin,_', 'class' => 'login' ), 'Χρήστης' );
		$form .= Helper_Form::input( 'password', null, true, array( 'type' => 'password', 'validValues' => 'valLatin,valInteger', 'valueLength' => 'max:15', 'class' => 'login' ), array( 'Κωδικός', 'επιτρέπονται μόνο Λατινικά γράμματα και αριθμοί' ) );
		$form .= Helper_Form::submit();
		$form .= Helper_Form::element(HTML_Tag::closed( 'a', array( 
				'class' => 'button', 
				'id' => 'main_form_right_top_button', 
				'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
			), '<span>Σύνδεση</span>' 
		));
		$form .= HTML_Tag::closed( 'div', array( 'class' => 'contact' ), 'Επικοινωνία: info@customweb.gr' );
		$form .= Helper_Form::closeForm();
		
		/* Headers */
		$this->response->setHeaders( array( 
			'Expires: Tue, 01 Jan 2000 00:00:00 GMT', 
			'Last-Modified: ' . gmdate( "D, d M Y H:i:s" ) . ' GMT',
			'Cache-Control: no-store, no-cache, must-revalidate, max-age=0',
			'Cache-Control: post-check=0, pre-check=0',
			'Pragma: no-cache'
		) );
		
		// Response
		$controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'user'
			.DS.'login.php');
		$controller_view->addVars(array(
            'form' => $form 
        ));
		$main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'
			.DS.'minimal.php' );
		$main_view->addVars(array(
            'controller_view' => $controller_view,
            'translator' => $this->translator
        ));
		$this->response->setBody($main_view->render());
	}

    public function login()
    {
        $params = $this->request->getParams();
        $error = null;
        $warning = [];
        $user = new Model_Yiama_User();
        $request_user = $user->query()
            ->search( array(
                "username = '{$params['username']}'",
                "password = '{$user->hash( $params['password'] )}'"
            ) )
            ->combine( 'roles' )
            ->find( ':first' );
        if(empty($request_user)) {
            $error[] = 'login_wrong_credentials';
        } elseif (!$request_user->hasRoles(array('superadmin', 'admin'))) {
            $error[] = 'login_wrong_privilege';
        } elseif (!$request_user->login(time() + Core_App::getConfig( "applications." . Core_App::getAppName() . ".user.login.interval" ))) {
            $error[] = 'login_failed';
        }
        
        // Response
		if($error) {
            $helper_message = new Helper_Message(Core_Session::getInstance());
            $helper_message->store(array(
                '',
                'error' => $error,
                'warning' => array_filter($warning)
            ));
			$this->response->redirect($this->request->urlFromPath(
                'user/loginform'
            ));
        } else {
			$this->response->redirect($this->request->urlFromPath(
                Core_App::getConfig('applications.admin.defaultpage')
            ));
        }
	}
	
	public function logout()
	{
		$user = new Model_Yiama_User();
		if( ( $logged_user = $user->getLogged() ) && $logged_user->logout() );
		$this->response->redirect(
			$this->request->url(
				'default',
				array()
			)
		);
	}
}

?>