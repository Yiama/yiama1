<?php
class Controller_Error extends Core_Controller {
    
	public function __construct() {
        parent::__construct();
        
        $language = new Model_Yiama_Language();
        $this->language = $language->getDefault();
        $locale = $this->language->code;
        $this->translator = new \Yiama\I18n\Translator(new \Yiama\I18n\Loader\ArrayLoader());
        $this->translator->setLocale($locale);
        $this->translator->addResource(new \Yiama\File\File(PATH_APPS.'admin'.DS.'translations'.DS.$locale.DS.'error.php'), $locale);
	}
	
	public function user() {		
		$this->response->setHeaders( 'HTTP/1.0 404 Not Found' );
        $controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'error'
            .DS.'user');
        $controller_view->addVars(array('t' => $this->translator));
        $main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'.DS
            .'minimal.php');
        $main_view->addVars(array(
            'controller_view' => $controller_view, 
            'translator' => $this->translator
        ));
		$this->response->setBody($main_view->render());
	}	
	
	public function notfound() {	
		$this->response->setHeaders( 'HTTP/1.0 404 Not Found' );
        $controller_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'error'
            .DS.'notfound');
        $controller_view->addVars(array('t' => $this->translator));
        $main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'.DS
            .'minimal.php');
        $main_view->addVars(array(
            'controller_view' => $controller_view, 
            'translator' => $this->translator
        ));
		$this->response->setBody($main_view->render());	
	}
}