<?php 
 
class Controller_Tools_Parse extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
	}
	
	public function htmlform()
	{
		$form = Helper_Form::openForm( $this->request->url( 'cont_act_id', array(  
			'controller' => 'tools_parse',
			'action' => 'parsehtml'
		) ) );	
		$form .= Helper_Form::openWrapperColumnLeft();
		$form .= Helper_Form::openWrapperModule( 'Βασικά' );
		$form .= Helper_Form::input( 'html', null, true, null, 'Εισαγωγή κειμένου <br/><small>Το κείμενο πρέπει να είναι σε μορφή HTML</small>' ); 
		$form .= Helper_Form::input( 'tags', null, false, null, 'Ετικέτες που αναζητούνται <br/><small>ο διαχωρισμός γίνεται με το κόμμα, π.χ. "div,a"</small>' );
		$form .= Helper_Form::input( 'attributes', null, false, null, 'Ιδιότητες που αναζητούνται <br/><small>ο διαχωρισμός γίνεται με το κόμμα, π.χ. "href,src"</small>' );
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::closeWrapperColumnLeft();
		$form .= Helper_Form::hiddenReturn();
		$form .= Helper_Form::submit( 'Εισαγωγή' );
		$form .= Helper_Form::closeForm();
		/* View */
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $form, 'header' => 'Αποδόμηση HTML κώδικα' ) )->render() );
	}
		
	public function parsehtml( $options = null )
	{
		$dom = new DOMDocument;
		$params = $this->request->getParams();
		
		//Parse the HTML. The @ is used to suppress any parsing errors
		//that will be thrown if the $html string isn't valid XHTML.
		@$dom->loadHTML( $params['html'] );
		$find_tags = isset( $params['tags'] ) ? explode( ',', $params['tags'] ) : null;
		$tags = $this->getHTMLTags( $dom, $find_tags );
		$s = '';
		if( ! empty( $tags ) ) {
			$s .= "<table cellpadding='10' cellspacing='1' border='1'>";
 			foreach( $tags as $t ){
				$find_attrs = isset( $params['attributes'] ) ? explode( ',', $params['attributes'] ) : null;
				$attrs = $this->getHTMLAttributes( $t, $find_attrs );
				if( ! empty( $attrs ) ) {
					$s .= "<th colspan='2'>{$t->nodeName}</th>";
					foreach( $attrs as $at ) {
						$s .= "<tr><td>{$at->nodeName}</td><td>{$at->nodeValue}</td></tr>";
					}
				}
			}
			$s .= '</table>';
		}
		/* View */
		Core_HTML::addFiles( 'admin\catalogue.css' );
		$this->response->setBody( Helper_View::form( $this, array( 'form' => $s, 'header' => 'Αποδομημένος HTML κώδικας' ) )->render() );
	}
	
	private function getHTMLTags( $dom, $find_tags = null )
	{
		$tags = array();
		if( ! empty( $find_tags ) ) {
			foreach( $find_tags as $tag ) {
				foreach( $dom->getElementsByTagName( $tag ) as $t ) {
					$tags[] = $t;
				}
			}
		} else {
			$tags = $dom->getElementsByTagName( '*' );
		}
		return $tags;
	}
	
	private function getHTMLAttributes( $tag, $find_attrs = null )
	{
		$attrs = array();
		if( ! empty( $find_attrs ) ) {
			foreach( $find_attrs as $at ) {
				if( $tag->hasAttribute( $at ) ) {
					$attrs[] = $tag->getAttributeNode( $at );
				}
			}
		} elseif ( $tag->hasAttributes() ) {
			$attrs = $tag->attributes;
		}
		return $attrs;
	}
}
?>