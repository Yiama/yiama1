<?php
class Controller_AdminModule extends Controller_Admin {
    
    private $module;
    private $module_name;
    
    public function __construct() {
        parent::__construct();
        
        $params = $this->request->getParams();
        $this->module_name = $params['modname'];
        $modname = 'Module_'.ucwords($params['modname'], '_').'_Admin';
        $this->module = new $modname();
    }
    
    public function index($modaction = null) {
        
        // Redirect to module's action
        if (!empty($modaction)) {
            $this->module->$modaction();
            return;
        }
        
        // Buttons
        $buttons = Helper_Buttons::get(
            $this->translator,
            array('save' => null)
        );
        
        // View
        $controller_view = new Core_View();
        $controller_view->setContent($this->module->render());
		$main_view = new Core_View(PATH_APPS.'admin'.DS.'views'.DS.'main'
            .DS.'default.php' );
        $main_view->addVars(array(
            'translator' => $this->translator,
            'buttons' => $buttons,
            'controller_view' => $controller_view
        ));
		$this->response->setBody($main_view->render());
    }
}