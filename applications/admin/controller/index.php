<?php
class Controller_Index extends Controller_Admin {
	
	public function __construct() {
		parent::__construct();
	}
    
	public  function index()
	{
		$url = $this->request->url(
			'cont_act_id',
			array( 
				'controller' => 'dashboard'
			)
		);
		$this->response->redirect( $url );
	}
}