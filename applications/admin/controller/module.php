<?php
class Controller_Module extends Core_Controller {
    
    public function index($modaction = null) {
        $params = $this->request->getParams();
        $modname = 'Module_'.ucwords($params['modname'], '_');
        $module = new $modname();
        if (!empty($modaction)) {
            $module->$modaction();
        } else {
            $module->render($params);
        }
    }
}