<?php
class Controller_Export extends Core_Controller
{
	/*
	 * @param $filename string -Χωρίς το extension
	 * @param $headers array
	 * @param $data array( 2D ) 
	 */
	public function csv( $filename, $headers = null, $data )
	{
		$tab = "\t";
		$eol = "\n";
		$content  =  ! empty( $headers ) ? '"' . implode( '"' . $tab . '"', $headers ) . '"' . $eol : '';
	    foreach( $data as $row ) {
	      $content .= '"' . implode( '"' . $tab . '"', $row ) . '"' . $eol;
	    }
		$this->response->setHeaders( array(
		   'Content-Description: File Transfer',
		   'Content-Type: application/octet-stream',
		   'Content-Disposition: attachment; filename="'.$filename.'.csv"',
		   'Content-Transfer-Encoding: binary',
		   'Expires: 0',
		   'Cache-Control: must-revalidate, post-check=0, pre-check=0',
		   'Pragma: public',
		   'Content-Length: '. strlen( $content )
	    ) );
	    $content = chr(255) . chr(254) . $content; // πρόσθεση UTF-8 BOM
	    $this->response->setBody( $content );
	}
}
?>