<?php 
class Controller_Import extends Core_Controller
{
	public function index()
	{
		$cat_name = 'Τσάντα';
		$cat_id = 10;
		$tb_name = 'ym_articles';
		$dirname = "c:" . DS . "xampp" . DS . "htdocs" . DS . "yiama" . DS . "files";
		$dir = new DirectoryIterator( $dirname );
		$data = new Model_Yiama_Article();
		foreach( $dir as $fileinfo ) {
	    	if( ! $fileinfo->isDot() ) {
				$error = null; 
				$filename = $fileinfo->getFilename();
				$file = pathinfo( $filename, PATHINFO_FILENAME  );
				$title = $cat_name . ' ' . $file;
				DB_ActiveRecord_Model::$db_model->startTransaction();	
				$data->ym_categories_id = $cat_id;
				$data->is_published = 1;
				$data->ordered = Model_Helper_Value::createOrdered( $data );
				$data->time_created = '@func:NOW()';
				$data->time_updated = '@func:NOW()';
				$data->visits = 0;
				$data->alias = Model_Helper_Value::createAlias( $title );
				$new_id = $data->getLastInsertedId()+1;
				$data->url = 'article/' . $new_id;
				if( ! $data->insert() ) { /* Main entity */
					$error[] = 'insert_row';
					break;
				} else { /* Local */
					$data_local = new Model_Yiama_Articlelocal();
					$data_local->ym_articles_id = $data->id;
					$data_local->ym_languages_id = 1; // Default language
					$data_local->title = $title;
					if( ! $data_local->insert() ){
						$error[] = 'insert_row';
						break;
					} else {
						$data_local->ym_articles_id = $new_id;
						$data_local->ym_languages_id = 2;
						$data_local->title = '';
						if( ! $data_local->insert() ){
							$error[] = 'insert_row';
							break;
						}
					}
				}
				if( empty( $error ) ) {
					$model_image = new Model_Yiama_Articleimage();
					$db_names = $model_image->findAttr( 'name' );
					if( ( $new_name = Helper_Image::upload( $tb_name, $db_names, $filename, $dirname . DS . $filename ) ) === false ) {
						$error = 'upload_image';
						break;
					} else {
						$model_image = new Model_Yiama_Articleimage();
						$model_image->name = $new_name;
						$model_image->ym_articles_id = $new_id;
						$model_image->is_published = 1;
						$model_image->is_default = 0;
						if( ! $model_image->insert() ) {
							Helper_Image::unlink( $tb_name, $new_name );
							$error = 'insert_image';
							break;
						}	
					}
				} 
			}   
	    }
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
	}
}
?>