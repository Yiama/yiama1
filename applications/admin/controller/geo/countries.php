<?php
 
class Controller_Geo_Countries extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
	}
	
	public function index() {
        // Query
		$model = new Model_Geo_Country();
		$cloned_query = clone $query = $model->query()
			->search( Model_Helper_Query::search(null, 'geo_countries') );
		$data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
        $all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Geo_Countries(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array(
                $this->translator->_('button.regions') => Helper_Link::catalogue('geo_regions')
        ));
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	} 
}

?>