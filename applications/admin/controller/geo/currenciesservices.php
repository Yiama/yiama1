<?php 
class Controller_Geo_Currenciesservices extends Core_Controller
{
	public function update()
	{
		set_time_limit( 600 );
		$currency = new Model_Geo_Currency();
		$default = $currency->getDefault()->code;
		/*  http://www.webservicex.net/ [XML] */
		$url1 = "http://www.webservicex.net/currencyconvertor.asmx/ConversionRate?FromCurrency={$default}&ToCurrency=";
		/*  http://free.currencyconverterapi.com/ [JSON] */
		$url2 = "http://free.currencyconverterapi.com/api/v3/convert?q={$default}_";
		$error = 0;
		$error_message = 'Geo currency rates update failed on "' . date( 'd/m/Y' ) . '" for currency codes: ';
		$error_codes = array();
		foreach( $currency->findAll() as $key => $c ){
			if( ( $XML = @simplexml_load_file( $url1 . $c->code ) ) && is_float( $XML ) ){
				$value = $XML;
			} elseif ( $JSON = $this->response->request( $url2 . $c->code ) ) {
				$code = $default . '_' . $c->code;
				$code = json_decode( $JSON )->results->$code;
				$value = isset( $code->val ) ? $code->val : null;
			} else{
				$error = 1;
				$error_codes[] = $c->code;
				continue;
			}
			if( ! empty( $value ) ) {
				DB_ActiveRecord_Model::$db_model
					->table( 'geo_currencies' )
					->insert_update( array( 'id', 'rate', 'rate_time_updated' ), array( 'rate' => $value, 'rate_time_updated' => '@func:NOW()' )  )
					->values( array( $c->id, $value, '@func:NOW()' ) )
					->execute();
			}
		}
		if( $error ) {
			$error_message .= implode( ',', $error_codes ); 
			$notification = new Model_Yiama_Notification();
			$notify = $notification->query()
				->search( "alias = 'site-error'" )
				->combine( array( 'recipients', 'template' ) )
				->find( ':first' );
			$email_message = $notify->template->render( array( 'message' => $error_message ) );
			$email = new Email();
			$email->chunk = 200;
			$email->from( Core_App::getConfig( 'domain' ) );
			$email->headers( 'content-type: text/html; charset="utf-8"' );
			$email->subject( "Πρόβλημα ενημέρωσης νομισματικών αξιών " . Core_App::getConfig( 'domain' ) );
			$email->message( $email_message );
			foreach( $notify->recipients as $v ) {
				$email->to( $v->email );
				$send = $email->send();
			}
			error_log( $error_message, 3, PATH_ROOT . 'log' . DS . 'admin' . DS . 'cron_jobs.txt' );  
		}
	}
}
?>