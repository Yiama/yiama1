<?php
 
class Controller_Geo_Regions extends Controller_Admin {
    
	public function __construct() {
        parent::__construct();
	}
	
	public function index() {
        // Query
		$model = new Model_Geo_Region();
        $cloned_query = clone $query = $model->query()
			->select( 'geo_regions.*, c.title AS country_title' )
			->join( 'geo_countries AS c', 'c.id = geo_regions.geo_countries_id' )
			->group( 'geo_regions.id' )
			->search( Model_Helper_Query::search(null, 'geo_regions') );
        $data = $query
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
        $all_search_data = $cloned_query->find();
        
        // Catalogue
		$catalogue = new Catalogue_Geo_Regions(
            new Helper_Catalogue(),
            $data, 
            $this->request, 
            $this->translator
        );
        
        // Buttons
		$buttons = Helper_Buttons::get(
            $this->translator,
            array($this->translator->_('button.countries') => Helper_Link::catalogue('geo_countries'))
        );
		
        // Response
        $view = $this->getViewCatalogue(array( 
            'translator' => $this->translator,
            'buttons' => $buttons, 
            'catalogue' => $catalogue->render(), 
            'pagination_vars' => array('total_content' => count($all_search_data))
        ));
		$this->response->setBody($view->render());
	}
}

?>