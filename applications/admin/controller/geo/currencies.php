<?php
 
class Controller_Geo_Currencies extends Core_Controller
{
	public function before()
	{
		Helper_Before::checkLoggedUser();
	}
	
	public function index()
	{
		$data = new Model_Geo_Currency();
		$currencies = $data->query()
			->search( Model_Helper_Query::search(null, 'geo_currencies') )
			->limit( Model_Helper_Query::limit() )
			->order( Model_Helper_Query::order() )
			->find();
		$all_data = $data->findAll();
		Helper_Catalogue::setBulkActions( 'geo_currencies', 'publish' );
		Helper_Catalogue::setHeaders( array(
			'Κωδικός' => 'code',
			'Σύμβολο' => 'symbol',
			'Προεπιλεγμένο' => 'is_default',
			'Ενεργό' => 'is_published',
			'Ισοτιμία' => 'rate',
			'Ενημερώθηκε' => 'rate_time_updated'
		) );
		Helper_Catalogue::setSearch( array(
			array( 'like' => 'code' ),
			array( 'empty' => null ),
			array( 'boolean' => 'is_default' ),
			array( 'publish' => 'is_published' ),
			array( 'empty' => null ),
			array( 'date' => 'rate_time_updated' )
		) );
		Helper_Catalogue::setContent( 'geo_countries', $currencies, array(
			array( 'text' => 'code' ),
			array( 'text' => 'symbol' ),
			array( 'boolean' => 'is_default' ),
			array( 'publish' => null ),
			array( 'value' => 'rate' ),
			array( 'date' => 'rate_time_updated' )
		) );
		Helper_Catalogue::setContentActions( $currencies, array() );
		$catalogue = Helper_Catalogue::get();
		$header = Helper_Header::get();
		/* View */
		$this->response->setBody( Helper_View::catalogue( $this, array( 'buttons' => $buttons, 'catalogue' => $catalogue, 'pagination_vars' => array( 'total_content' => count( $all_data ) ) ) )->render() );
	} 
}

?>