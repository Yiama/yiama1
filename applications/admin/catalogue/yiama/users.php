<?php
class Catalogue_Yiama_Users implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t, $has_privileges) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_users';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			'Username' => 'username',
			'Email' => 'email',
			$t->_('roles') => 'roles',
			$t->_('creation') => 'time_created',
			$t->_('status') => 'is_published'
		));
        
        // Search
		$role = new Model_Yiama_Role();
        $roles = $role->query()
            ->search("id > 1")
            ->find();
		$this->catalogue->setSearch( array(
            array('main' => array(
                'username',
                $request->getParams( 
                    'search', 
                    'username.like' 
                ),
                $t->_('search.search')
            )),
			array('select' => array( 
                'search[r.id]', 
                $roles, 
                'id', 
                'title', 
                'id', 
                $request->getParams( 
                    'search', 
                    '[r.id]' 
                ),
                $t->_('search.role')    
            )),
			array('publish' => 'ym_users.is_published')
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'username' ),
			array( 'text' => 'email' ),
			array('text' => 'roles'),
			array( 'date' => 'time_created' ),
			array( 'publish' => null )
		));
		$this->catalogue->setContentActions(
            $data, 
            $has_privileges ? array('update', 'delete') : null
        );
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}