<?php
class Catalogue_Yiama_Attributes implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_attributes';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
		$this->catalogue->setHeaders( array(
			$t->_('header.title') => 'title',
			'Alias' => 'alias',
			$t->_('header.description') => 'description',
			$t->_('header.active') => 'attr_type',
			$t->_('header.description') => 'is_published',
			$t->_('header.order') => 'ordered'
		) );
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch( array(
            array('main' => array(
                'local.title',
                isset($query_search['local.title']['like'])
                    ? $query_search['local.title']['like']
                    : (isset($session_search['yiama_attributes']['local.title']['like'])
                        ? $session_search['yiama_attributes']['local.title']['like']
                        : null),
                $t->_('search.search')
            )),
            array( 'publish' => array(
                'ym_attributes.is_published',
                isset($query_search['ym_attributes.is_published'])
                    ? $query_search['ym_attributes.is_published']
                    : (isset($session_search['yiama_attributes']['ym_attributes.is_published'])
                        ? $session_search['yiama_attributes']['ym_attributes.is_published']
                        : null),
                $t->_('search.publish')
            ))
		) );
        
        // Content
		$this->catalogue->setContent( $tb_name, $data, array(
			array( 'text' => 'title' ),
			array( 'text' => 'alias' ),
			array( 'text' => 'description' ),
			array( 'text' => 'attr_type' ),
			array( 'publish' => null ),
			array( 'order' => null )
		) );
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}