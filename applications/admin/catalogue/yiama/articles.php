<?php
class Catalogue_Yiama_Articles implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_articles';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
			$t->_('header.category') => 'ym_categories_id',
			$t->_('header.visits') => 'visits',
			$t->_('header.creation') => 'time_created',
			$t->_('header.user') => 'username',
			$t->_('header.active') => 'is_published',
            $t->_('header.favorite') => 'is_favorite',
			$t->_('header.order') => 'ordered'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$category = new Model_Yiama_Category();
		$categories = DB_ActiveRecord_Array::flatten(
            DB_ActiveRecord_Array::tree(
                $category->findAll(), 
                1, 
                'id', 
                'parent_id'
            ), 
            'children' 
        );
		foreach( $categories as $c ) {
			$c->tree_disabled = ( $c->total_children ) ? true : false;
		}
        $this->catalogue->setSearch(array(
            array('main' => array(
                'local.title',
                isset($query_search['local.title']['like'])
                    ? $query_search['local.title']['like']
                    : (isset($session_search['yiama_articles']['local.title']['like'])
                        ? $session_search['yiama_articles']['local.title']['like']
                        : null),
                $t->_('search.search')
            )),
			array('select-tree' => array(
                $categories, 
                'search[ym_articles.ym_categories_id]', 
                isset($query_search['ym_articles.ym_categories_id'])
                    ? $query_search['ym_articles.ym_categories_id']
                    : (isset($session_search['yiama_articles']['ym_articles.ym_categories_id'])
                        ? $session_search['yiama_articles']['ym_articles.ym_categories_id']
                        : null),
                $t->_('search.category')
            )), 
            array( 'publish' => array(
                'ym_articles.is_published',
                isset($query_search['ym_articles.is_published'])
                    ? $query_search['ym_articles.is_published']
                    : (isset($session_search['yiama_articles']['ym_articles.is_published'])
                        ? $session_search['yiama_articles']['ym_articles.is_published']
                        : null),
                $t->_('search.publish')
            )),
            array( 'favorite' => array(
                'ym_articles.is_favorite',
                isset($query_search['ym_articles.is_favorite'])
                    ? $query_search['ym_articles.is_favorite']
                    : (isset($session_search['yiama_articles']['ym_articles.is_favorite'])
                    ? $session_search['yiama_articles']['ym_articles.is_favorite']
                    : null),
                $t->_('search.favorite')
            ))
		));
        
        // Content
		$category_url_path = $request->url(
            'cont_act_id', 
            array('controller' => 'yiama_categories') 
        );
		$this->catalogue->setContent($tb_name, $data, array(
			array('text' => 'title'),
			array('anchor' => array( 
                'category_title', 
                $category_url_path, 
                'search[ym_categories.id]', 
                'ym_categories_id' 
            )),
			array('value' => 'visits'),
			array('date' => 'time_updated'),
			array('value' => 'username'),
			array('publish' => null),
            array('favorite' => null),
			array('order' => 'ym_categories_id')
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}