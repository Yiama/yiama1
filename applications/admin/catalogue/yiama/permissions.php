<?php
class Catalogue_Yiama_Permissions implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t, $has_privileges = null) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_permissions';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'title'
		));
        
        // Search
		$this->catalogue->setSearch(array(
            array('main' => array(
                'title',
                isset($query_search['title']['like'])
                    ? $query_search['title']['like']
                    : (isset($session_search['yiama_permissions']['title']['like'])
                        ? $session_search['yiama_permissions']['title']['like']
                        : null),
                $t->_('search.search')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' )
		));
		$this->catalogue->setContentActions(
            $data, 
            $has_privileges ? array('update', 'delete') : null
        );
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}