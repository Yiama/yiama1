<?php
class Catalogue_Yiama_Files implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = '';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'title',
            $t->_('header.size') => 'size',
            $t->_('header.type') => 'mimeType'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
        $this->catalogue->setSearch(array());
        
        // Content
		$directory_url_path = $request->url(
            'cont_act_id', 
            array('controller' => 'yiama_files')
        );

		$this->catalogue->setContent("", $data, array(
            array('anchor' => array(
                'title',
                $directory_url_path,
                'search[parent_id]',
                'path'
            )),
            array('text' => 'size'),
            array('text' => 'mimeType')
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}