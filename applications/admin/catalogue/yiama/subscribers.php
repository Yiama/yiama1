<?php
class Catalogue_Yiama_Subscribers implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_subscribers';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
		Helper_Catalogue::setHeaders(array(
            'Email' => 'email',
            $t->_('header.subscription_type') => 'subscription_title'
        ));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$type = new Model_Yiama_Subscriptiontype();
        $types = $type
            ->query()
            ->find();
		$this->catalogue->setSearch(array(
			array('main' => array(
                'email',
                isset($query_search['email']['like'])
                    ? $query_search['email']['like']
                    : (isset($session_search['yiama_subscribers']['email']['like'])
                        ? $session_search['yiama_subscribers']['email']['like']
                        : null),
                $t->_('search.search')
            )),
			array('select' => array( 
                'search[ss.ym_subscriptiontypes_id]', 
                $types, 
                'id', 
                'title', 
                'id', 
                isset($query_search['ss.ym_subscriptiontypes_id'])
                    ? $query_search['ss.ym_subscriptiontypes_id']
                    : (isset($session_search['yiama_subscribers']['ss.ym_subscriptiontypes_id'])
                        ? $session_search['yiama_subscribers']['ss.ym_subscriptiontypes_id']
                        : null),
                $t->_('search.subscription_type')  
            ))
		));
        
        // Content
		$item_url_path = $request->url( 
            'cont_act_id', 
            array( 
                'controller' => 'yiama_subscribers',
                'action' => 'item'
            )
        );
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'anchor' => array( 
                'email', 
                $item_url_path, 
                'subscriberid_subscriptionid', 
                'subscriberid_subscriptionid' 
            )),
            array('text' => 'subscription_title')
		));
		$this->catalogue->setContentActions(
            $data, 
            array('item', 'delete'), 
            null, 
            array('search[ym_subscribers_subscriptiontypes.id]' => $request
                ->getParams( 
                    'search', 
                    '[ym_subscribers_subscriptiontypes.id]' 
                ) 
            ) 
        );
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}