<?php
class Catalogue_Yiama_Categories implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_categories';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
			$t->_('header.supercategory') => 'ym_categories.parent_id',
			$t->_('header.user') => 'username',
			$t->_('header.image') => 'image',
			$t->_('header.active') => 'is_published',
			$t->_('header.order') => 'ordered'
		));
        
        // Search
		$category = new Model_Yiama_Category();
		$supercategories = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree( 
                $category->findAll(), 
                1, 
                'id', 
                'parent_id' 
            ), 
            'children' 
        );
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch(array(
            array('main' => array(
                'local.title',
                isset($query_search['local.title']['like'])
                    ? $query_search['local.title']['like']
                    : (isset($session_search['yiama_categories']['local.title']['like'])
                        ? $session_search['yiama_categories']['local.title']['like']
                        : null),
                $t->_('search.search')
            )),
			array('select-tree' => array(
                $supercategories, 
                'search[ym_categories.parent_id]', 
                isset($query_search['ym_categories.parent_id'])
                    ? $query_search['ym_categories.parent_id']
                    : (isset($session_search['yiama_categories']['ym_categories.parent_id'])
                        ? $session_search['yiama_categories']['ym_categories.parent_id']
                        : null),
                $t->_('search.supercategory')
            )), 
            array( 'publish' => array(
                'ym_categories.is_published',
                isset($query_search['ym_categories.is_published'])
                    ? $query_search['ym_categories.is_published']
                    : (isset($session_search['yiama_categories']['ym_categories.is_published'])
                        ? $session_search['yiama_categories']['ym_categories.is_published']
                        : null),
                $t->_('search.publish')
            ))
		));
        
        // Content
		$category_url_path = $request->url( 
            'cont_act_id', 
            array( 'controller' => 'yiama_categories' ) 
        );
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'anchor' => array( 
                'title', 
                $category_url_path, 
                'search[ym_categories.parent_id]', 'id' 
            )),
			array( 'anchor' => array( 
                'parenttitle', 
                $category_url_path, 
                'search[ym_categories.parent_id]', 
                'parent_id' 
            )),
			array( 'text' => 'username' ),
			array( 'image' => 'image' ),
			array( 'publish' => null ),
			array( 'order' => 'parent_id' )
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}