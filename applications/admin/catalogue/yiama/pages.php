<?php
class Catalogue_Yiama_Pages implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_pages';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
		Helper_Catalogue::setHeaders( array(
			$t->_('header.title') => 'local.title',
			$t->_('header.superpage') => 'parent_name',
			$t->_('header.type')  => 'type',
			'URL' => 'url',
			$t->_('header.active') => 'is_published',
			$t->_('header.order') => 'ordered'
		));
        
        // Search
        $page = new Model_Yiama_Page();
        $superpages = $page
            ->query()
			->search(Model_Helper_Query::search(array('ym_pages.is_admin IS NULL '
                . 'OR ym_pages.is_admin <> 1')))
            ->find();
		$superpages = DB_ActiveRecord_Array::flatten( 
            DB_ActiveRecord_Array::tree($superpages, 1, 'id', 'parent_id'), 
            'children' 
        );
		$type = new Model_Yiama_Pagetype();
        $types = $type
            ->query()
            ->search('is_admin IS NULL OR is_admin <> 1')
            ->find();
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
        Helper_Catalogue::setSearch( array(
			array('main' => array(
                'local.title',
                isset($query_search['local.title']['like'])
                    ? $query_search['local.title']['like']
                    : (isset($session_search['yiama_pages']['local.title']['like'])
                        ? $session_search['yiama_pages']['local.title']['like']
                        : null),
                $t->_('search.search')
            )),
			array('select-tree' => array(
                $superpages, 
                'search[ym_pages.parent_id]', 
                isset($query_search['ym_pages.parent_id'])
                    ? $query_search['ym_pages.parent_id']
                    : (isset($session_search['yiama_pages']['ym_pages.parent_id'])
                        ? $session_search['yiama_pages']['ym_pages.parent_id']
                        : null),
                $t->_('search.superpage')
            )),
			array('select' => array( 
                'search[ym_pages.ym_pagetypes_id]', 
                $types, 
                'id', 
                'title', 
                'id', 
                isset($query_search['ym_pages.ym_pagetypes_id'])
                    ? $query_search['ym_pages.ym_pagetypes_id']
                    : (isset($session_search['yiama_pages']['ym_pages.ym_pagetypes_id'])
                        ? $session_search['yiama_pages']['ym_pages.ym_pagetypes_id']
                        : null),
                $t->_('search.page_type')   
            )),
            array( 'publish' => array(
                'ym_pages.is_published',
                isset($query_search['ym_pages.is_published'])
                    ? $query_search['ym_pages.is_published']
                    : (isset($session_search['yiama_pages']['ym_pages.is_published'])
                        ? $session_search['yiama_pages']['ym_pages.is_published']
                        : null),
                $t->_('search.publish')
            ))
		) );
        
        // Content
		$page_url_path = $request->url( 
            'cont_act_id', 
            array('controller' => 'yiama_pages') 
        );
		Helper_Catalogue::setContent( 'ym_pages', $data, array(
			array( 'anchor' => array( 
                'title', 
                $page_url_path, 
                'search[ym_pages.parent_id]', 
                'id', 
                'Υποσελίδες' 
            )),
			array( 'text' => 'parent_name' ),
			array( 'text' => 'type' ),
			array( 'text' => 'url' ),
			array( 'publish' => null ),
			array( 'order' => 'parent_id' )
		) );
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}