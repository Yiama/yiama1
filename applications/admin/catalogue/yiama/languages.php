<?php
class Catalogue_Yiama_Languages implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_languages';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
            $t->_('header.shortcode') => 'code',
			$t->_('header.default') => 'is_sites_default',
			$t->_('header.active') => 'is_published',
			$t->_('header.order') => 'ordered'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch(array(
            array('main' => array(
                'title',
                isset($query_search['title']['like'])
                    ? $query_search['title']['like']
                    : (isset($session_search['yiama_languages']['title']['like'])
                        ? $session_search['yiama_languages']['title']['like']
                        : null),
                $t->_('search.search')
            )),
            array( 'publish' => array(
                'ym_languages.is_published',
                isset($query_search['ym_languages.is_published'])
                    ? $query_search['ym_languages.is_published']
                    : (isset($session_search['yiama_languages']['ym_languages.is_published'])
                        ? $session_search['yiama_languages']['ym_languages.is_published']
                        : null),
                $t->_('search.publish')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' ),
			array( 'text' => 'code' ),
			array( 'action' => array('setsitesdefault', 'is_sites_default') ),
			array( 'publish' => null ),
			array( 'order' => null )
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}