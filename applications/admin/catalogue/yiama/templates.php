<?php
class Catalogue_Yiama_Templates implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_templates';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'title'
		));
        
        // Search
		$this->catalogue->setSearch(array(
            array('main' => array(
                'title',
                $request->getParams( 
                    'search', 
                    '[title].like' 
                ),
                $t->_('search.search')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' )
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}