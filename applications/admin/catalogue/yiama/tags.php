<?php
class Catalogue_Yiama_Tags implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_tags';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
			$t->_('header.active') => 'is_published'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch(array(
            array('main' => array(
                'local.title',
                isset($query_search['local.title']['like'])
                    ? $query_search['local.title']['like']
                    : isset($session_search['yiama_tags']['local.title']['like'])
                        ? $session_search['yiama_tags']['local.title']['like']
                        : null,
                $t->_('search.search')
            )),
            array( 'publish' => array(
                'ym_tags.is_published',
                isset($query_search['ym_tags.is_published'])
                    ? $query_search['ym_tags.is_published']
                    : (isset($session_search['yiama_tags']['ym_tags.is_published'])
                        ? $session_search['yiama_tags']['ym_tags.is_published']
                        : null),
                $t->_('search.publish')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' ),
			array( 'publish' => null )
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}