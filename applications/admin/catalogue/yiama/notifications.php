<?php
class Catalogue_Yiama_Notifications implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_notifications';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'title'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch(array(
            array('main' => array(
                'title',
                isset($query_search['title']['like'])
                    ? $query_search['title']['like']
                    : (isset($session_search['yiama_notifications']['title']['like'])
                        ? $session_search['yiama_notifications']['title']['like']
                        : null),
                $t->_('search.search')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' )
		));
		$this->catalogue->setContentActions($data, array('update', 'delete'));
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}