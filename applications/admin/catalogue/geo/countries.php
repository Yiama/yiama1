<?php
class Catalogue_Geo_Countries implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_countries';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
			$t->_('header.shortcode') => 'code',
			$t->_('header.currency') => 'currency',
			'Iso' => 'iso',
			'IsoAlpha3' => 'isoAlpha3',
			'GeonameId' => 'geonameId',
			$t->_('header.continent') => 'continent',
			$t->_('header.continent_shortcode') => 'continentCode',
			$t->_('header.active') => 'is_published'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$this->catalogue->setSearch(array(
            array('main' => array(
                'title',
                isset($query_search['title']['like'])
                    ? $query_search['title']['like']
                    : (isset($session_search['geo_countries']['title']['like'])
                        ? $session_search['geo_countries']['title']['like']
                        : null),
                $t->_('search.search')
            )),
            array( 'publish' => array(
                'geo_countries.is_published',
                isset($query_search['geo_countries.is_published'])
                    ? $query_search['geo_countries.is_published']
                    : (isset($session_search['geo_countries']['geo_countries.is_published'])
                        ? $session_search['geo_countries']['geo_countries.is_published']
                        : null),
                $t->_('search.publish')
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' ),
			array( 'text' => 'code' ),
			array( 'value' => 'currency' ),
			array( 'value' => 'iso' ),
			array( 'value' => 'isoAlpha3' ),
			array( 'value' => 'geonameId' ),
			array( 'text' => 'continent' ),
			array( 'value' => 'continentCode' ),
			array( 'publish' => null )
		));
		$this->catalogue->setContentActions($data);
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}