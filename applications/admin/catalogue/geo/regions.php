<?php
class Catalogue_Geo_Regions implements Catalogue_Interface{
    
    private $catalogue;
    
    public function __construct($catalogue, $data, $request, $t) {
        $this->catalogue = $catalogue;
        $tb_name = 'ym_regions';
        
        // Headers
		$this->catalogue->setBulkActions($tb_name, array('publish', 'delete'));
        $this->catalogue->setHeaders(array(
			$t->_('header.title') => 'local.title',
			$t->_('header.country') => 'geo_countries_id'
		));
        
        // Search
        $query_search = $request->getParams('search');
        $session_search = Core_Session::getInstance()->get('search');
		$country = new Model_Geo_Country();
		$countries = $country->findAll();
        $country_url_path = $request->url('cont_act_id', array(
            'controller' => 'geo_countries'
        ));
		$this->catalogue->setSearch(array(
            array('main' => array(
                'geo_regions.title',
                isset($query_search['geo_regions.title']['like'])
                    ? $query_search['geo_regions.title']['like']
                    : (isset($session_search['geo_regions']['geo_regions.title']['like'])
                        ? $session_search['geo_regions']['geo_regions.title']['like']
                        : null),
                $t->_('search.search')
            )),
			array('select' => array(
                'search[geo_regions.geo_countries_id]', 
                $countries, 
                'id', 
                'title',
                'id',
                isset($query_search['geo_regions.geo_countries_id'])
                    ? $query_search['geo_regions.geo_countries_id']
                    : (isset($session_search['geo_regions']['geo_regions.geo_countries_id'])
                        ? $session_search['geo_regions']['geo_regions.geo_countries_id']
                        : null),
                $t->_('search.country') 
            ))
		));
        
        // Content
		$this->catalogue->setContent($tb_name, $data, array(
			array( 'text' => 'title' ),
			array( 'anchor' => array( 
                'country_title', 
                $country_url_path, 
                'search[id]',
                'geo_countries_id', 
                $t->_('header.country')
            )),
		));
		$this->catalogue->setContentActions($data);
	}
    
    public function render() {
        return $this->catalogue->get();
    }
}