<?php 
	Core_HTML::addFiles( array(
		'admin\statistics.css',
		'library\chart\Chart.js',
		'library\chart\yiama_chart.js'
	) ); 
?>
<!-- admin statistics ( -->
<div class='topHeader'>Στατιστικά Πελάτη</div>
<?php echo $this->form; ?>
<div class='chart'>
	<div class='header'>Παραγγελίες ανά ημέρα</div>
	<canvas id="chart_count" height="200"></canvas>
</div>
<div class='chart'>
	<div class='header'>Προϊόντα ανά ημέρα ( τεμ. )</div>
	<canvas id="chart_quantity"></canvas>
</div>
<div class='chart'>
	<div class='header'>Κέρδος ανά ημέρα</div>
	<canvas id="chart_price"></canvas>
</div>
<script type='text/javascript'>

	/* Count */
	var data = {};
	<?php foreach( $this->ordersByDay as &$o ) $o['monthday'] = $o['day'] . ' ' . $this->months[ intval( $o['month'] ) ]; ?>
	data.labels = [<?php echo "'" . implode( "','", Core_Array::array_column( $this->ordersByDay, 'monthday' ) ) . "'"; ?>];
	
	data.datasets = [
		{
			fillColor: "#8899aa",
	        strokeColor: "#334455",
	        pointColor: "#445566",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "#223344",
	        data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'count' ) ); ?>]
		}
	];
	
	var options = {
		labelFontSize: 16,
		scaleBeginAtZero : true
	}
	
	line_chart( "chart_count", data, options );
	
	/* Quantity */
	data.datasets = [
	    {
	     	fillColor: "#8899cc",
	        strokeColor: "#334477",
	        pointColor: "#445588",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "#223366",
	        data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'quantity' ) ); ?>]
	    }
	 ];
	
	bar_chart( "chart_quantity", data );
	
	/* Profit */
	data.datasets = [
	    {
	     	fillColor: "#8899ee",
	        strokeColor: "#334499",
	        pointColor: "#4455aa",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "#223388",
	        data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'price' ) ); ?>]
	    }
	 ];
	
	var options = {
		bezierCurve : false,
		datasetFill : false
	}
	
	line_chart( "chart_price", data, options );
</script>
<!-- ) admin statistics -->