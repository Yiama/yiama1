<?php 
	Core_HTML::addFiles( array(
		'admin\statistics.css',
		'library\chart\Chart.js',
		'library\chart\yiama_chart.js'
	) ); 
?>
<!-- admin statistics ( -->
<div class='topHeader'>Στατιστικά Παραγγελιών</div>
<?php echo $this->form; ?>
<div class='chart'>
	<div class='header'>Παραγγελίες ανά ημέρα</div>
	<canvas id="chart_count" height="200"></canvas>
</div>
<div class='chart'>
	<div class='header'>Προϊόντα ανά ημέρα ( τεμ. )</div>
	<canvas id="chart_quantity"></canvas>
</div>
<div class='chart'>
	<div class='header'>Κέρδος ανά ημέρα</div>
	<canvas id="chart_price"></canvas>
</div>
<script type='text/javascript'>

	/* Count */
	var data = {};
	data.labels = [<?php echo "'" . implode( "','", Core_Array::array_column( $this->ordersByDay, 'day' ) ) . "'"; ?>];

	data.datasets = [
		{
			fillColor: "#8899aa",
            strokeColor: "#334455",
            pointColor: "#445566",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#223344",
            data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'count' ) ); ?>]
		}
	];

	var options = {
		labelFontSize: 16,
		scaleBeginAtZero : true
	}
	
	line_chart( "chart_count", data, options );

	/* Quantity */
	data.datasets = [
        {
         	fillColor: "#8899cc",
            strokeColor: "#334477",
            pointColor: "#445588",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#223366",
            data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'quantity' ) ); ?>]
        }
     ];
	
	bar_chart( "chart_quantity", data );

	/* Profit */
	data.datasets = [
        {
         	fillColor: "#8899ee",
            strokeColor: "#334499",
            pointColor: "#4455aa",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#223388",
            data: [<?php echo implode( ',', Core_Array::array_column( $this->ordersByDay, 'price' ) ); ?>]
        }
     ];

	var options = {
		bezierCurve : false,
		datasetFill : false
	}
	
	line_chart( "chart_price", data, options );
</script>

<script type='text/javascript'>

makeColors( <?php echo count( $this->statusesActiveMonths ); ?> );

<?php 
	$legendTemp = '';
	foreach( $this->orderstatuses as $key => $os )
	{
		$legendTemp .= '<div class="colorIndex"><div class="color" style="background-color:\' + getColor( ' . $key . ' ) + \'"></div><div class="text">' . $os['title'] . '</div></div>';
	}
?>
var legendTemplate = '<?php echo $legendTemp; ?>';

</script>
<div class='chart'>
	<div class='header'>Σύνολο Παραγγελιών ανά Μήνα</div>
	<canvas id="chart_count_all" height="200"></canvas>
	<div id='legendTemplate'></div>
</div>
<script type='text/javascript'>

	/* Count */
	var data = {};
	data.labels = [
		<?php echo "'" . implode( "','", $this->statusesActiveMonths ) . "'"; ?>
	];
	data.datasets = [
		<?php foreach( $this->orderstatuses as $key => $x ): ?>
		<?php $data = ( isset( $this->allStatusesByMonth[ $key ][0] ) ) ? Core_Array::array_column( $this->allStatusesByMonth[ $key ], 'count' ) : array(); ?>
		{
			label: '<?php if( isset( $this->allStatusesByMonth[ $key ][0] ) ) echo $this->allStatusesByMonth[ $key ][0]['status']; ?>',
			fillColor: getColor( <?php echo $key; ?> ),
            strokeColor: getColor( <?php echo $key; ?> ),
            pointColor: getColor( <?php echo $key; ?> ),
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#223300",
            data: [<?php echo implode( ',', $data ); ?>]
		}
		<?php if( $key < count( $this->orderstatuses )-1 ) echo ','; ?>
		<?php endforeach; ?>
	];

	var options = {
		labelFontSize: 16,
		scaleBeginAtZero : true,
		legendTemplate: legendTemplate,
		legendTemplateHTMLElementId: 'legendTemplate'
	}
	
	line_chart( "chart_count_all", data, options );
</script>
<!-- ) admin statistics -->