<?php 
Core_HTML::addFiles( array(
        'admin\form.css',
		'admin\login_index.css',
		'library\preload.js'
	) ); 
?>
<script type='text/javascript'>
	_addOnLoad( "_get( 'inputname' ).focus()" );
</script>
<div class='form admin login'>
    <a name='anc'></a>
    <div class='logo'></div>
    <div class='version'>v1.5</div>
    <div><?php echo $this->form; ?></div>
</div>