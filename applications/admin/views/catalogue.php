<?php 
Core_HTML::addFiles( 'admin\mod_pagination.css' );
$pagination = new Module_Pagination();
?>
<div class='catalogue'>
  <?php echo $this->catalogue; ?>
  <?php echo $pagination->render($this->pagination_vars); ?>
</div>