<?php 
Core_HTML::addFiles(array(
    'admin\fonts.css',
    'admin\dashboard.css'
));
?>
<div class="dashboard">
    <div class="section s1">
        <div class="item">
        <?php echo $this->articles['total'] . ' ' . $this->translator->_('section1.total'); ?>
        </div>
        <div class="item">
        <?php echo $this->articles['visits'] . ' ' . $this->translator->_('section1.visits'); ?>
        </div>
        <div class="item">
        <?php echo $this->articles['active'] . ' ' . $this->translator->_('section1.active'); ?>
        </div>
        <div class="item">
        <?php echo $this->articles['users'] . ' ' 
                . $this->translator->_('section1.from') . ' ' . $this->user->username; ?>
        </div>
    </div>
    <div class="section s2">
    </div>
    <div class="section s3">
        <div class="item">
            <div class="header"><?php echo $this->translator->_('section3.latest'); ?></div>
            <?php foreach($this->articles['latest'] as $article): ?>
            <div class="row">
                <div class="image cell">
                    <img src="<?php echo $this->user->getImage(); ?>" />
                    <a 
                        <?php if ($this->user->is_superadmin): ?>
                        href="<?php echo $this->user->link; ?>"
                        <?php endif;?>
                    ><?php echo $this->user->username; ?></a>
                </div>
                <div class="text cell">
                    <div class="subtext date">
                    <?php echo date("d/m/Y H:i", strtotime($article->time_created)); ?>
                    </div>
                    <?php echo Core_Strings::short($article->title, 50); ?>
                </div>
                <a  class="button-edit cell"
                    href="<?php echo $article->link; ?>"></a>
            </div>                
            <?php endforeach; ?>
        </div>
        <div class="item">
            <div class="header"><?php echo $this->translator->_('section3.popular'); ?></div>
            <?php foreach($this->articles['popular'] as $article): ?>
            <div class="row">
                <div class="image cell">
                    <img src="<?php echo $this->user->getImage(); ?>" />
                    <?php echo $this->user->username; ?>
                </div>
                <div class="text cell">
                    <div class="subtext visits">
                    <?php echo $article->visits; ?>
                    </div>
                    <?php echo Core_Strings::short($article->title, 50); ?>
                </div>
                <a  class="button-edit cell"
                    href="<?php echo $article->link; ?>"></a>
            </div>                
            <?php endforeach; ?>
        </div>
    </div>
    <div class="section s4">
    </div>
</div>