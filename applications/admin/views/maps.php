<?php 
	Core_HTML::addFiles( array(
		'admin\maps.css',
		'library\popups.js',
		'library\basic.js'
	) ); 
?>
<!-- admin index ( -->
<?php
	echo"<div class='header'>{$tb_item['title']}</div>\n";
	
	if( ! empty( $this->maps ) )
	{
		echo HTML_Form_Map::initialize( array( 
				'id' => 'map-canvas',
				'inputLat_id' => 'latitude',
				'inputLng_id' => 'longitude',
				'start' => array(
					'latitude' => $this->maps[0]['latitude'],
					'longitude' => $this->maps[0]['longitude']
				),
				'markers' => $this->maps
		) );
		echo HTML_Form_Map::map( array( 'id' => 'map-canvas' ) );
	}
	
	/* Add map */
	echo"  <div class='item rc1'>\n";  
	echo"    <div class='title'></div>\n";
	echo"    <a href='{$this->addLink}' class='add'><img src='" . PATH_REL_IMAGES . "admin/maps/addmap.png' title='Νέο σημείο'/></a>\n";
	echo"  </div>\n";
	
	echo"</div>\n";
?>
<!-- ) admin index -->