<?php 
	Core_HTML::addFiles('admin\error\error.css');
	Core_HTML::addToTitle($this->t->_('html_headers.not_found.title'));
?>
<!-- error ( -->
<div class='error'>
	<img src='<?php echo PATH_REL_IMAGES . "admin/error/notfound.png'/"; ?>'><br/>
	<?php echo $this->t->_('notfound.text'); ?><br/>
	<a href='<?php echo $this->controller->request->urlFromPath( Core_App::getConfig( 'applications.admin.defaultpage' ) ); ?>'>
        <?php echo $this->t->_('notfound.link'); ?>
    </a>	
</div>
<!-- ) error -->