<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<?php 
Core_HTML::addFiles( array(
    'library\basic.js',
    'library\cookies.js',
    'admin\global.css',
    'admin\fonts.css',
    'admin\templates\default.css'
) ); 
// Απενεργοποίηση των Links ώστε να μην μπορεί να γίνει right-click->open in new tab
Core_HTML::addJs( " 
    function handleAnchors()
    {
        var anchors = document.getElementsByTagName( 'a' );
        for( var i = 0; i < anchors.length; i++ ){
            if( attr = anchors[i].getAttribute( 'href' ) ) {
                anchors[i].setAttribute( 'data-href', attr );
                anchors[i].removeAttribute( 'href' );
                anchors[i].onclick = function(){
                    window.location = this.getAttribute( 'data-href' );
                }
            }
        }
    }
    _addOnLoad( 'handleAnchors()' );
" );
?>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='el' lang='el'>
<head>
<?php 
$controller_view = $this->controller_view->render();
$helper_message = new Helper_Message(Core_Session::getInstance());
$system_messages = $helper_message->get($this->translator);
$menu_vertical = new Module_Admin_Menu_Vertical();
$menu_vertical_rendered = $menu_vertical->render();
$toggle_cookie = Core_Cookie::get('menu_toggle');
$mod_navigation = new Module_Admin_Navigation();
echo Core_HTML::getHeaders();
echo "<link rel='shortcut icon' href='".PATH_REL_ASSETS.DS."images/admin/default/admin_favicon.png'/>";
echo Core_HTML::getFiles();
echo Core_HTML::getScripts();
echo Core_HTML::getStyles();
?>
</head>
<body onload='_onLoad()'>
    <div class='header <?php echo $toggle_cookie ? 'toggle' :''; ?>'>
        <div class="left">
            <div    class="menu-toggle <?php echo $toggle_cookie ? 'toggle' :''; ?>"
                    id="menuToggle"></div>
        </div>
        <div class="right">
            <div class="admin-menu"></div>
            <div class="messages-notifications">
                <div class="messages"></div>
                <div class="notifications"></div>                
            </div>
        </div>
    </div>
    <div class='body <?php echo $toggle_cookie ? 'toggle' :''; ?>'>
        <div class='left'>
            <?php echo $menu_vertical_rendered; ?>
        </div>
        <div class='right'>
            <?php echo $mod_navigation->render(); ?>
            <?php echo $system_messages; ?>
            <?php echo isset($this->buttons) ? $this->buttons : ''; ?>
            <?php echo $controller_view; ?>
        </div>
    </div>
    <div class='footer'>
    </div>
    <script type="text/javascript">
    /*========================================================================
        MENU TOGGLE
      ------------------------------------------------------------------------*/
    // Function
    function templateDefaultToggleButton(className, toggle) {
        document.body.getElementsByClassName(className)[0]
                .className = document.body.getElementsByClassName(className)[0]
                    .className.replace(' toggle', '');
        if (toggle) {
            document.body.getElementsByClassName(className)[0]
                .className += ' toggle';
        }
    }
    // Onclick
    document.getElementById('menuToggle').addEventListener(
        'click', 
        function(event) {
            // Is toggled
            if (event.target.className.indexOf(' toggle') > -1) {
                _setCookie('menu_toggle', '0', 30*24*60*60*1000, '/');
                event.target.className = event.target.className
                    .replace(' toggle', '');
                templateDefaultToggleButton('header', false);
                templateDefaultToggleButton('body', false);
            } // Is not toggled
            else {
                _setCookie('menu_toggle', '1', 30*24*60*60*1000, '/');
                event.target.className += ' toggle';
                templateDefaultToggleButton('header', true);
                templateDefaultToggleButton('body', true);
            }
        }
    );
    </script>
</body>
</html>