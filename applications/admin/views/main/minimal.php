<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<?php
Core_HTML::addFiles( array(
    'admin\fonts.css',
	'admin\global.css',
	'admin\templates\default.css'
) ); 
?>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='el' lang='el'>
<head>
<?php 
    $controller_view = $this->controller_view->render();
    $helper_message = new Helper_Message(Core_Session::getInstance());
    $system_messages = $helper_message->get($this->translator);
    echo Core_HTML::getHeaders();
    echo "<link rel='shortcut icon' href='".PATH_REL_ASSETS.DS."images/admin/default/admin_favicon.png'/>";
	echo Core_HTML::getFiles();
	echo Core_HTML::getScripts(); 
	echo Core_HTML::getStyles();
?>
</head>
<body onload='_onLoad()'>
  <div class='body_wrapper'>
    <?php echo $system_messages; ?>
    <?php echo $controller_view; ?>
  </div>
</body>
</html>