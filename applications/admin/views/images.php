<?php 
	Core_HTML::addFiles( array(
		'admin\media.css',
		'js\library\popupImage.css',
		'js\library\popupImage.js',
		'library\popups.js',
		'library\basic.js'	
	) ); 
?>
<!-- admin index ( -->
<?php
	echo"<div class='header'>{$this->tb_item['title']}<br/>
	<small><small>Αν η εικόνα ξεπερνάει σε μήκος τα {$this->normal_width} pixel ή σε πλάτος τα {$this->normal_height} pixel θα γίνεται σμίκρυνση</small></small></div>\n";

/*	
	echo"<script type='text/javascript'>
		function mediaMenu( index )
		{ 
			var li = document.getElementById( 'mediaMenuLi' ).getElementsByTagName( 'li' ); 
			for( var i = 0; i <= 2; i++ ) 
			{
				if( i != index )
				{ 
					li[i].setAttribute( 'class', 'rc2Top' ); 
					li[i].childNodes[0].setAttribute( 'class', 'rc2Top' );
				} 
				else
				{ 
					li[i].setAttribute( 'class', 'selected rc2Top' ); 
					li[i].childNodes[0].setAttribute( 'class', 'selected rc2Top' );
				}
			}
		}
	</script>\n";
	echo"<div class='mediaMenu'><ul id='mediaMenuLi'>\n";
	echo"    <li class='selected rc2Top' onclick=\"mediaMenu( 0 );\"><span class='selected rc2Top'>Εικόνες</span></li>\n";
	echo"    <li class='rc2Top' onclick=\"mediaMenu( 1 );\"><span class='rc2Top'>Έγγραφα</span></li>\n";
	echo"    <li class='rc2Top' onclick=\"mediaMenu( 2 );\"><span class='rc2Top'>Αρχεία ήχου</span></li>\n";
	echo"</ul></div>\n";
*/
	/* Images */
	echo"<div class='wrapper' id='images'>\n";
	foreach( $this->images as $m )
	{
		echo"  <div class='item rc1" . ( ( $m['is_published'] ) ? " published" : "" ) . ( ( $m['is_default'] ) ? " default" : "" ) . "'>\n";  
		echo"    <div class='title'>\n";
		echo"      <div class='actionsButton' onclick=\"var a = document.getElementById( 'action_{$m['id']}' ); if( a.style.display == 'block' ) a.style.display = 'none'; else a.style.display = 'block';\">&darr;</div>\n";
		echo"    </div>\n";
		echo"    <img src='{$this->imageDir}/thumbnails/thumb_{$m['name']}' onclick=\"_popupImage( '{$this->imageDir}/{$m['name']}' )\"/>\n";
		echo"    <div class='actions' id='action_{$m['id']}'>\n";
		echo"      " . ( ( ! $m['is_default'] ) ? "<a href='{$m['setdefaultLink']}'>Προεπιλεγμένο</a>" : "" ) . "\n";
		echo"      <a href='{$m['publishLink']}'>" . ( ( $m['is_published'] ) ? "Ανενεργό" : "Ενεργό" ) . "</a>\n";
		echo"      <a href='{$m['deleteLink']}'>Διαγραφή</a>\n";
		echo"    </div>\n";
		echo"  </div>\n";
	}
	
	/* Add image */
	echo"  <div class='item rc1'>\n";  
	echo"    <div class='title'></div>\n";
	echo"    <a href='{$this->addLink}" . ( ( strpos( $this->addLink, '?' ) === false ) ? '?' : '&' ) . "media_type=image' class='add'><img src='" . PATH_REL_IMAGES . "admin/media/thumbnails/thumb_addimage.png' title='Νέα εικόνα'/></a>\n";
	echo"  </div>\n";
	
	echo"</div>\n";
?>
<!-- ) admin index -->