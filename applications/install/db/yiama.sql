 SET foreign_key_checks = 0;-- END_OF_COMMAND
--
-- YIAMA 
-- 
CREATE TABLE IF NOT EXISTS `ym_users` (
  `id` bigint unsigned,
  `is_published` tinyint unsigned,
  `username` varchar(255),
  `password` varchar(255),
  `email` varchar(200),
  `image` varchar(255),
  `time_created` datetime,
  `time_updated` datetime,
  `time_last_login` datetime,
  `time_last_logout` datetime,
  `login_token` varchar(255),
  `login_token_expiration` int,
  `reset_password_token` varchar(255),
  `reset_password_time` datetime,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ym_users_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_roles` (
  `id` bigint unsigned,
  `parent_id` bigint unsigned,
  `title` varchar(255),
  `description` varchar(255),
  UNIQUE(`title`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_roles` (`id`, `parent_id`, `title`, `description`) VALUES
(1, 0, 'root', 'Root'),
(2, 1, 'superadmin', 'Πλήρης πρόσβαση διαχείρισης'),
(3, 1, 'admin', 'Βασική πρόσβαση διαχείρισης'),
(4, 1, 'guest', 'Απλός επισκέπτης');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_users_roles` (
	`ym_users_id` bigint unsigned,
	`ym_roles_id` bigint unsigned,
	PRIMARY KEY (`ym_users_id`, `ym_roles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_permissions` (
  `id` bigint unsigned,
  `title` varchar(255),
  `description` varchar(255),
  UNIQUE(`title`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_permissions` (`id`, `title`, `description`) VALUES
(1, 'users_create', ''),	
(2, 'users_update', ''),
(3, 'users_delete', ''),
(4, 'roles_create', ''),
(5, 'roles_update', ''),
(6, 'roles_delete', ''),
(7, 'permissions_create', ''),
(8, 'permissions_update', ''),
(9, 'permissions_delete', ''),
(10, 'pages_create', ''),
(11, 'pages_update', ''),
(12, 'pages_delete', ''),
(13, 'options_crud', '');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_roles_permissions` (
	`ym_roles_id` bigint unsigned,
	`ym_permissions_id` bigint unsigned,
    `state` tinyint,
    UNIQUE(`ym_roles_id`, `ym_permissions_id`),
	PRIMARY KEY (`ym_roles_id`, `ym_permissions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_roles_permissions` (`ym_roles_id`, `ym_permissions_id`, `state`) VALUES
(2, 1, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(2, 5, 1),
(2, 6, 1),
(2, 7, 1),
(2, 8, 1),
(2, 9, 1),
(2, 10, 1),
(2, 11, 1),
(2, 12, 1),
(3, 10, 1),
(3, 11, 1),
(3, 12, 1),
(2, 13, 1);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_languages` (
  `id` bigint unsigned,
  `code` varchar(255),
  `title` varchar(255),
  `is_default` tinyint unsigned,
  `is_sites_default` tinyint unsigned,
  `is_published` tinyint unsigned,
  `ordered` bigint unsigned,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_languages` (`id`, `code`, `title`, `is_default`, `is_sites_default`, `is_published`, `ordered`) VALUES
(1, 'el', 'Ελληνικά', 1, 1, 1, 100),
(2, 'en', 'English', 0, 0, 1, 200);-- END_OF_COMMAND
  
CREATE TABLE IF NOT EXISTS `ym_categories` (
  `id` bigint unsigned,
  `ym_users_id` bigint unsigned,
  `parent_id` bigint unsigned,
  `is_published` tinyint unsigned,
  `url` varchar( 300),
  `ordered` bigint unsigned,
  `image` varchar( 300 ),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_categories` (`id`, `ym_users_id`, `parent_id`, `is_published`) VALUES
(1, 1, 0, 1),
(2, 1, 1, 1);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_categories_local` (
  `ym_categories_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `alias` varchar(255),
  `title` varchar(255),
  `description` text,
  `text` text,
  `meta_title` varchar(255),
  `meta_description` varchar(255),
  `meta_keywords` varchar(255),
  PRIMARY KEY (`ym_categories_id`,`ym_languages_id` ),
  UNIQUE(`alias`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_categories_local` (`ym_categories_id`, `ym_languages_id`, `alias`, `title`) VALUES
(1, 1, 'root', 'Root'),
(2, 1, 'genikh', 'Γενική');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles` (
  `id` bigint unsigned,
  `ym_categories_id` bigint unsigned,
  `ym_users_id_created` bigint unsigned,
  `ym_users_id_updated` bigint unsigned,
  `is_published` tinyint  unsigned,
  `is_favorite` tinyint  unsigned,
  `time_created` datetime,
  `time_updated` datetime,
  `visits` int,
  `ordered` bigint unsigned,
  url varchar( 300),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles_local` (
  `ym_articles_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `alias` varchar(255),
  `title` text,
  `description` text,
  `short_description` varchar(255),
  `text` text,
  `meta_title` varchar(255),
  `meta_description` varchar(255),
  `meta_keywords` varchar(255),
  PRIMARY KEY (`ym_articles_id`,`ym_languages_id`),
  UNIQUE(`alias`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles_images` (
	`id` bigint unsigned,
	`ym_articles_id` bigint unsigned,
	`name` varchar(255),
	`alt` varchar(255),
	`is_published` tinyint unsigned,
	`is_default` tinyint unsigned,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_attributes` (
  `id` bigint unsigned,
  `description` varchar(255),
  `is_multivalue` tinyint,
  `is_multiselect` tinyint,
  `attr_type` varchar(255) COMMENT 'string,float,date,boolean',
  `ordered` bigint unsigned,
  `is_published` tinyint unsigned,
  `alias` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_attributes_local` (
  `ym_attributes_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `title` varchar(255),
  PRIMARY KEY (`ym_attributes_id`,`ym_languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_attributes_values` (
  `id` bigint unsigned,
  `ym_attributes_id` bigint unsigned,
  `ordered` bigint unsigned,
  `image` varchar( 300 ),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_attributes_values_local` (
  `ym_attributes_values_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `title` varchar(255),
  `alias` varchar(255),
  PRIMARY KEY (`ym_attributes_values_id`,`ym_languages_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_attributes_categories` (
  `ym_categories_id` bigint unsigned,
  `ym_attributes_id` bigint unsigned,
  PRIMARY KEY (`ym_categories_id`, `ym_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles_attributes` (
  `ym_articles_id` bigint unsigned,
  `ym_attributes_id` bigint unsigned,
  `val` varchar(255),
  PRIMARY KEY (`ym_articles_id`, `ym_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles_attributes_values` (
  `ym_articles_id` bigint unsigned,
  `ym_attributes_values_id` bigint unsigned,
  PRIMARY KEY (`ym_articles_id`, `ym_attributes_values_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_tags` (
  `id` bigint unsigned,
  `ym_users_id` bigint unsigned,
  `is_published` tinyint unsigned,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_tags_local` (
  `ym_tags_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `alias` varchar(255),
  `title` varchar(255),
  PRIMARY KEY (`ym_tags_id`,`ym_languages_id`),
  UNIQUE(`alias`),
  UNIQUE(`title`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_articles_tags` (
  `ym_articles_id` bigint unsigned,
  `ym_tags_id` bigint unsigned,
  PRIMARY KEY (`ym_articles_id`, `ym_tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_notifications` (
  `id` bigint unsigned,
  `ym_templates_id` bigint unsigned,
  `title` varchar(255),
  `alias` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_notifications` (`id`, `ym_templates_id`, `title`, `alias`) VALUES
(1, 1, 'Newsletter', 'newsletter');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_notificationrecipients` (
  `id` bigint unsigned,
  `ym_notifications_id` bigint unsigned,
  `email` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_options` (
  `id` bigint unsigned AUTO_INCREMENT,
  `key` varchar(255),
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_options` (`id`, `key`, `value`) VALUES
(1, 'image', '{ "size": { "normal": [ 1280, 728 ], "thumbnail": [ 300, 300 ] }, "dir": "db" }'),
(2, 'contact_email', ''),
(3, 'slider', '{"image": [ 1280, 430]}'),
(4, 'url_types', '{ "ym_articles": "article/#id", "ym_categories": "articles/#id" }' ),
(5, 'notifications_language_id', '1' );-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_pagetypes` (
  `id` bigint unsigned,
  `title` varchar(255),
  `alias` varchar(255),
  `is_admin` tinyint,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_pagetypes` (`id`, `title`, `alias`, `is_admin`) VALUES
(1, 'Admin', 'admin', 1),
(2, 'Admin Horizontal', 'admin-horizontal', 1),
(1000, 'Separator', 'separator', 1);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_pages` (
  `id` bigint unsigned,
  `ym_pagetypes_id` bigint unsigned,
  `parent_id` bigint unsigned,
  `url` varchar( 300 ),
  `module` text,
  `alias` varchar(255),
  `is_published` tinyint unsigned,
  `is_admin` tinyint unsigned,
  `ordered` bigint unsigned,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND
 
INSERT IGNORE INTO `ym_pages` (`id`, `ym_pagetypes_id`, `parent_id`, `url`, `module`, `alias`, `is_published`, `is_admin`, `ordered`) VALUES
(1,1,0,NULL,NULL,'root',1,0,0),
(3,1,10,'yiama_articles',NULL,'periexomeno',1,1,21),
(5,1,10,'yiama_categories',NULL,'kathgories',1,1,20),
(7,1,1,'yiama_pages',NULL,'selides',1,1,26),
(9,1,23,'yiama_languages',NULL,'glwsses',1,1,27),
(10,1,1,NULL,NULL,'cms',1,1,22),
(23,1,1,NULL,NULL,'topika',1,1,28),
(52,1,1,NULL,NULL,'ergaleia',0,1,5220),
(53,1,52,NULL,NULL,'apodomhsh-kwdika-keimenoy',1,1,5320),
(54,1,1,'yiama_options',NULL,'rytmiseis',1,1,5420),
(55,1,230,'yiama_templates',NULL,'protypa',1,1,5520),
(60,1,230,'yiama_notifications',NULL,'eidopoihseis',1,1,6020),
(73,1,53,'tools_parse/htmlform',NULL,'html',1,1,7318),
(74,1,23,'geo_countries',NULL,'xwres',1,1,7418),
(77,2,1,NULL,NULL,'xrhsths',1,1,7720),
(78,2,77,'user/logout',NULL,'aposyndesh',1,1,7820),
(79,2,1,NULL,NULL,'ergaleia',1,1,7920),
(80,2,79,'backup/db',NULL,'backup-bashs',1,1,8020),
(81,2,79,'cache/flush',NULL,'diagrafh-cache',1,1,8120),
(82,1,10,'yiama_attributes',NULL,'idiothtes',1,1,8220),
(206,1,1,NULL,NULL,'xrhstes',1,1,7917),
(208,1,7,'yiama_pagetypes',NULL,'typoi-selidwn',0,1,20920),
(218,1,74,'geo_regions',NULL,'perioxes',0,1,21820),
(220,1,206,'yiama_subscribers',NULL,'eggegramenoi-xrhstes',1,1,23200),
(225,1,10,'yiama_attributevalues',NULL,'times-xarakthristikwn',0,1,22520),
(227,1,10,'yiama_tags',NULL,'tags',1,1,22700),
(228,1,1,'dashboard',NULL,'dashboard',1,1,10),
(229,1,230,'yiama_forms',NULL,'forms',1,1,10),
(230,1,1,'contact',NULL,'contact',1,1,7000),
(231,1,206,'yiama_users',NULL,'lista',1,1,23100),
(233,1,206,'yiama_roles',NULL,'roloi',1,1,23300),
(234,1,206,'yiama_permissions',NULL,'dikaiwmata',1,1,23400),
(235,1,206,'yiama_rolepermissions',NULL,'katastasi-dikaiwmatos-rolou',0,1,23500),
(236,1,1,NULL,NULL,'epektaseis',1,1,23600),
(237,1,236,'adminmodule/google_recaptcha',NULL,'google-recaptcha',1,1,23700),
(238,1,236,'adminmodule/slider',NULL,'slider',1,1,23800),
(239,1,236,'adminmodule/social_links',NULL,'social-links',1,1,23900),
(240,1,1,'yiama_files',NULL,'arxeia',1,1,24000),
(241,1,236,'adminmodule/text',NULL,'text',1,1,24100),
(10000,1,1,NULL,NULL,'separator',0,1,100020);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_pages_local` (
  `ym_pages_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `title` varchar(255),
  PRIMARY KEY (`ym_pages_id`,`ym_languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_pages_local` (`ym_pages_id`, `ym_languages_id`, `title`) VALUES
(1,1,'Root'),
(3,1,'Άρθρα'),
(3,2,''),
(5,1,'Κατηγορίες'),
(5,2,''),
(7,1,'Σελίδες'),
(7,2,''),
(9,1,'Γλώσσες'),
(9,2,''),
(10,1,'Περιεχόμενο'),
(10,2,''),
(23,1,'Τοπικά'),
(23,2,''),
(52,1,'Εργαλεία'),
(52,2,''),
(53,1,'Αποδόμηση κώδικα κειμένου'),
(53,2,''),
(54,2,''),
(55,1,'Πρότυπα'),
(55,2,''),
(60,1,'Ειδοποιήσεις'),
(60,2,''),
(73,1,'HTML'),
(73,2,''),
(74,1,'Χώρες'),
(74,2,''),
(77,1,'Χρήστης'),
(77,2,''),
(78,1,'Αποσύνδεση'),
(78,2,''),
(79,1,'Εργαλεία'),
(79,2,''),
(80,1,'Backup Βάσης'),
(80,2,''),
(81,1,'Διαγραφή Cache'),
(81,2,''),
(82,1,'Ιδιότητες'),
(82,2,''),
(206,1,'Χρήστες'),
(206,2,''),
(208,1,'Τύποι σελίδων'),
(208,2,''),
(218,1,'Περιοχές'),
(218,2,''),
(220,1,'Εγγεγραμένοι'),
(220,2,''),
(225,1,'Τιμές χαρακτηριστικών'),
(225,2,''),
(227,1,'Tags'),
(227,2,''),
(228,1,'Dashboard'),
(228,2,''),
(229,1,'Φόρμες'),
(229,2,''),
(230,1,'Επικοινωνία'),
(230,2,''),
(231,1,'Λίστα'),
(231,2,''),
(233,1,'Ρόλοι'),
(233,2,''),
(234,1,'Δικαιώματα'),
(234,2,''),
(235,1,'Κατάσταση δικαιώματος ρόλου'),
(235,2,''),
(236,1,'Επεκτάσεις'),
(236,2,''),
(237,1,'Google Recaptcha'),
(237,2,''),
(238,1,'Slider'),
(238,2,''),
(239,1,'Social Links'),
(239,2,''),
(240,1,'Αρχεία'),
(240,2,''),
(241,1,'Text'),
(241,2,''),
(10000,1,'Separator'),
(10000,2,'Separator');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_subscribers` (
  `id` bigint unsigned,
  `email` varchar(254),
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ym_subscribers_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_subscriptiontypes` (
  `id` bigint unsigned,
  `title` varchar(255),
  `alias` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_subscriptiontypes` (`id`, `title`, `alias`) VALUES
(1, 'Newsletter', 'newsletter');-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_subscribers_subscriptiontypes` (
  `ym_subscribers_id` bigint unsigned,
  `ym_subscriptiontypes_id` bigint unsigned,
  `info` text,
  PRIMARY KEY (`ym_subscribers_id`,`ym_subscriptiontypes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_templates` (
  `id` bigint unsigned,
  `title` varchar(255),
  `alias` varchar(255),
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `ym_templates` (`id`, `title`, `alias`, `text`) VALUES
(1, 'Επικοινωνία χρήστη', 'user-contact', '<table width="100%" cellpading="10" style="width: 100%; padding: 10px;">
    <tbody>
    <tr>
    <th colspan="2" bgcolor="#dddddd" align="center" style="padding: 10px; text-align: center; font-size: 14pt; background-color: #ddd;">Επικοινωνία χρήστη από το {{domain}}</th>
    </tr>
    <tr>
    <td bgcolor="#f5f5f5" align="right" style="padding: 10px; text-align: right; font-size: 12pt; background-color: #f5f5f5;">Όνομα / Εταιρία</td>
    <td align="left" style="padding: 10px; text-align: left; font-size: 12pt;">{{name}}</td>
    </tr>
    <tr>
    <td bgcolor="#f5f5f5" align="right" style="padding: 10px; text-align: right; font-size: 12pt; background-color: #f5f5f5;">Email</td>
    <td align="left" style="padding: 10px; text-align: left; font-size: 12pt;">{{email}}</td>
    </tr>
    <tr>
    <td colspan="2" bgcolor="#f5f5f5" align="center" style="padding: 10px; text-align: left; font-size: 12pt; background-color: #f5f5f5;"><big>{{message}}</big></td>
    </tr>
    </tbody>
    </table>
    <!-- copyright -->
    <table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr>
    <td style="padding: 5px; border-top: 1px solid #aaa;">© {{copy}}</td>
    </tr>
    </tbody>
    </table>'
),
(2, 'Εγγραφή χρήστη', 'user-registration', '<table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr style="padding: 10px;">
    <td><img src="{{logo}}&lt;/td" /></td>
    </tr>
    <tr width="100%" style="width: 100%; padding: 10px;">
    <th colspan="2" width="100%" align="left" color="#666666" style="width: 100%; padding: 0px; text-align: left; font-size: 20px; color: #666;">{{sir}}</th>
    </tr>
    <tr width="100%" style="width: 100%;">
    <td colspan="2" width="100%" align="left" color="#666666" style="width: 100%; padding: 20px 0; text-align: left; font-size: 16px; color: #666;">{{text}}</td>
    </tr>
    </tbody>
    </table>
    <!-- credentials -->
    <table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr width="100%" style="width: 100%; padding: 10px;">
    <td width="50%" align="center" color="#666666" bgcolor="#eee" style="width: 50%; padding: 5px; text-align: center; font-size: 16px; color: #666; background-color: #eee;">{{label_username}} [{{username}}]</td>
    </tr>
    <tr></tr>
    <tr width="100%" style="width: 100%; padding: 10px;">
    <td width="50%" align="center" color="#666666" bgcolor="#eee" style="width: 50%; padding: 5px; text-align: center; font-size: 16px; color: #666; background-color: #eee;">Email [{{email}}]</td>
    </tr>
    <tr></tr>
    </tbody>
    </table>
    <!-- button --> <a href="{{link_login}}" style="display: inline-block; margin: 50px 100px; width: 300px; padding: 10px 50px; text-align: center; font-size: 18px; color: #fff; text-decoration: none; background-color: #ff0000; cursor: pointer;">{{label_login}}</a> <!-- copyright -->
    <table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr>
    <td style="padding: 5px; border-top: 1px solid #aaa;">© {{copy}}</td>
    </tr>
    </tbody>
    </table>'
),
(3, 'Αλλαγή κωδικού χρήστη', 'user-reset-password', '<table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr style="padding: 10px;">
    <td><img src="{{logo}}" /></td>
    </tr>
    <tr width="100%" style="width: 100%; padding: 10px;">
    <th colspan="2" width="100%" align="left" color="#666666" style="width: 100%; padding: 0px; text-align: left; font-size: 20px; color: #666;">{{sir}}</th>
    </tr>
    <tr width="100%" style="width: 100%;">
    <td colspan="2" width="100%" align="left" color="#666666" style="width: 100%; padding: 20px 0; text-align: left; font-size: 16px; color: #666;">{{text}}</td>
    </tr>
    </tbody>
    </table>
    <!-- button --> <a href="{{link_reset}}" style="display: inline-block; margin: 50px 100px; width: 300px; padding: 10px 50px; text-align: center; font-size: 18px; color: #fff; text-decoration: none; background-color: #ed9964; cursor: pointer;">{{label_reset}}</a> <!-- copyright -->
    <table frame="box" cellspacing="0" width="600" cellpadding="20" style="width: 600px; border-collapse: collapse; border: none;">
    <tbody>
    <tr>
    <td style="padding: 5px; border-top: 1px solid #aaa;">© {{copy}}</td>
    </tr>
    </tbody>
    </table>'
);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_forms` (
  `id` bigint unsigned,
  `alias` varchar(255),
  `json` text,
  PRIMARY KEY (`id`),
  UNIQUE(`alias`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_forms_local` (
  `ym_forms_id` bigint unsigned,
  `ym_languages_id` bigint unsigned,
  `title` text,
  `description` text,
  `text` text,
  PRIMARY KEY (`ym_forms_id`,`ym_languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `ym_modules` (
  `name` varchar(254),
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;-- END_OF_COMMAND

ALTER TABLE `ym_categories`
  ADD CONSTRAINT `fk_ym_categories_users` FOREIGN KEY (`ym_users_id`) REFERENCES `ym_users` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_categories_local`
  ADD CONSTRAINT `fk_ym_categories_local_categories` FOREIGN KEY (`ym_categories_id`) REFERENCES `ym_categories` (`id`),
  ADD CONSTRAINT `fk_ym_categories_local_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_articles`
  ADD CONSTRAINT `fk_ym_articles_categories` FOREIGN KEY (`ym_categories_id`) REFERENCES `ym_categories` (`id`),
  ADD CONSTRAINT `fk_ym_articles_users_created` FOREIGN KEY (`ym_users_id_created`) REFERENCES `ym_users` (`id`),
  ADD CONSTRAINT `fk_ym_articles_users_updated` FOREIGN KEY (`ym_users_id_updated`) REFERENCES `ym_users` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_articles_local`
  ADD CONSTRAINT `fk_ym_articles_local_articles` FOREIGN KEY (`ym_articles_id`) REFERENCES `ym_articles` (`id`),
  ADD CONSTRAINT `fk_ym_articles_local_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND
    
ALTER TABLE `ym_articles_images`
  ADD CONSTRAINT `fk_ym_articles_images_ym_articles` FOREIGN KEY (`ym_articles_id`) REFERENCES `ym_articles` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_attributes_local`
  ADD CONSTRAINT `fk_ym_attributes_local_ym_attributes` FOREIGN KEY (`ym_attributes_id`) REFERENCES `ym_attributes` (`id`),
  ADD CONSTRAINT `fk_ym_attributes_local_ym_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_attributes_values`
  ADD CONSTRAINT `fk_ym_attributes_values_ym_attributes` FOREIGN KEY (`ym_attributes_id`) REFERENCES `ym_attributes` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_attributes_values_local`
  ADD CONSTRAINT `fk_ym_attributes_values_local_ym_attributes` FOREIGN KEY (`ym_attributes_values_id`) REFERENCES `ym_attributes_values` (`id`),
  ADD CONSTRAINT `fk_ym_attributes_values_local_ym_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_attributes_categories`
  ADD CONSTRAINT `fk_ym_attributes_categories_ym_categories` FOREIGN KEY (`ym_categories_id`) REFERENCES `ym_categories` (`id`),
  ADD CONSTRAINT `fk_ym_attributes_categories_ym_attributes` FOREIGN KEY (`ym_attributes_id`) REFERENCES `ym_attributes` (`id`);-- END_OF_COMMAND
   
ALTER TABLE `ym_articles_attributes`
  ADD CONSTRAINT `fk_ym_articles_attributes_ym_articles` FOREIGN KEY (`ym_articles_id`) REFERENCES `ym_articles` (`id`),
  ADD CONSTRAINT `fk_ym_articles_attributes_ym_attributes` FOREIGN KEY (`ym_attributes_id`) REFERENCES `ym_attributes` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_articles_attributes_values`
  ADD CONSTRAINT `fk_ym_articles_attributes_values_ym_articles` FOREIGN KEY (`ym_articles_id`) REFERENCES `ym_articles` (`id`),
  ADD CONSTRAINT `fk_ym_articles_attributes_values_ym_attributes_values` FOREIGN KEY (`ym_attributes_values_id`) REFERENCES `ym_attributes_values` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_tags`  
 ADD CONSTRAINT `fk_ym_tags_users` FOREIGN KEY (`ym_users_id`) REFERENCES `ym_users` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_tags_local`
  ADD CONSTRAINT `fk_ym_tags_local_ym_tags` FOREIGN KEY (`ym_tags_id`) REFERENCES `ym_tags` (`id`),
  ADD CONSTRAINT `fk_ym_tags_local_ym_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_articles_tags`
  ADD CONSTRAINT `fk_ym_articles_tags_ym_articles` FOREIGN KEY (`ym_articles_id`) REFERENCES `ym_articles` (`id`),
  ADD CONSTRAINT `fk_ym_articles_tags_ym_tags` FOREIGN KEY (`ym_tags_id`) REFERENCES `ym_tags` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_notifications`
  ADD CONSTRAINT `fk_ym_notifications_ym_templates` FOREIGN KEY (`ym_templates_id`) REFERENCES `ym_templates` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_notificationrecipients`
  ADD CONSTRAINT `fk_ym_notificationrecipients_notifications` FOREIGN KEY (`ym_notifications_id`) REFERENCES `ym_notifications` (`id`);-- END_OF_COMMAND
  
ALTER TABLE `ym_pages`
  ADD CONSTRAINT `fk_ym_pages_pagetypes_id` FOREIGN KEY (`ym_pagetypes_id`) REFERENCES `ym_pagetypes` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_pages_local`
  ADD CONSTRAINT `fk_ym_pages_local_pages` FOREIGN KEY (`ym_pages_id`) REFERENCES `ym_pages` (`id`),
  ADD CONSTRAINT `fk_ym_pages_local_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND       
  
ALTER TABLE `ym_subscribers_subscriptiontypes`
  ADD CONSTRAINT `fk_ym_subscribers_subscriptiontypes_subscribers` FOREIGN KEY (`ym_subscribers_id`) REFERENCES `ym_subscribers` (`id`),
  ADD CONSTRAINT `fk_ym_subscribers_subscriptiontypes_subscriptiontypes` FOREIGN KEY (`ym_subscriptiontypes_id`) REFERENCES `ym_subscriptiontypes` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_users_roles`
  ADD CONSTRAINT `fk_ym_users_roles_ym_users` FOREIGN KEY (`ym_users_id`) REFERENCES `ym_users` (`id`),
  ADD CONSTRAINT `fk_ym_users_roles_ym_roles` FOREIGN KEY (`ym_roles_id`) REFERENCES `ym_roles` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_roles_permissions`
  ADD CONSTRAINT `fk_ym_roles_permissions_ym_permissions` FOREIGN KEY (`ym_permissions_id`) REFERENCES `ym_permissions` (`id`),
  ADD CONSTRAINT `fk_ym_roles_permissions_ym_roles` FOREIGN KEY (`ym_roles_id`) REFERENCES `ym_roles` (`id`);-- END_OF_COMMAND

ALTER TABLE `ym_forms_local`
  ADD CONSTRAINT `fk_ym_forms_local_forms` FOREIGN KEY (`ym_forms_id`) REFERENCES `ym_forms` (`id`),
  ADD CONSTRAINT `fk_ym_forms_local_languages` FOREIGN KEY (`ym_languages_id`) REFERENCES `ym_languages` (`id`);-- END_OF_COMMAND
-- 
-- GEO 
--
CREATE TABLE IF NOT EXISTS `geo_countries` (
  `id` bigint unsigned,
  `title` varchar(255),
  `code` varchar(255) DEFAULT '',
  `currency` varchar(255),
  `iso` varchar(255),
  `isoAlpha3` varchar(255),
  `geonameId` int,
  `continent` varchar(255),
  `continentCode` varchar(255),
  `decimals` int,
  `is_published` tinyint unsigned,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `geo_countries` (`id`, `title`, `code`, `currency`, `iso`, `isoAlpha3`, `geonameId`, `continent`, `continentCode`, `is_published`) VALUES
(1, 'Andorra', 'AD', 'EUR', '020', 'AND', 3041565, 'Europe', 'EU', 0),
(2, 'United Arab Emirates', 'AE', 'AED', '784', 'ARE', 290557, 'Asia', 'AS', 0),
(3, 'Afghanistan', 'AF', 'AFN', '004', 'AFG', 1149361, 'Asia', 'AS', 0),
(4, 'Antigua and Barbuda', 'AG', 'XCD', '028', 'ATG', 3576396, 'North America', 'NA', 0),
(5, 'Anguilla', 'AI', 'XCD', '660', 'AIA', 3573511, 'North America', 'NA', 0),
(6, 'Albania', 'AL', 'ALL', '008', 'ALB', 783754, 'Europe', 'EU', 0),
(7, 'Armenia', 'AM', 'AMD', '051', 'ARM', 174982, 'Asia', 'AS', 0),
(8, 'Angola', 'AO', 'AOA', '024', 'AGO', 3351879, 'Africa', 'AF', 0),
(9, 'Antarctica', 'AQ', '', '010', 'ATA', 6697173, 'Antarctica', 'AN', 0),
(10, 'Argentina', 'AR', 'ARS', '032', 'ARG', 3865483, 'South America', 'SA', 0),
(11, 'American Samoa', 'AS', 'USD', '016', 'ASM', 5880801, 'Oceania', 'OC', 0),
(12, 'Austria', 'AT', 'EUR', '040', 'AUT', 2782113, 'Europe', 'EU', 0),
(13, 'Australia', 'AU', 'AUD', '036', 'AUS', 2077456, 'Oceania', 'OC', 0),
(14, 'Aruba', 'AW', 'AWG', '533', 'ABW', 3577279, 'North America', 'NA', 0),
(15, 'Åland', 'AX', 'EUR', '248', 'ALA', 661882, 'Europe', 'EU', 0),
(16, 'Azerbaijan', 'AZ', 'AZN', '031', 'AZE', 587116, 'Asia', 'AS', 0),
(17, 'Bosnia and Herzegovina', 'BA', 'BAM', '070', 'BIH', 3277605, 'Europe', 'EU', 0),
(18, 'Barbados', 'BB', 'BBD', '052', 'BRB', 3374084, 'North America', 'NA', 0),
(19, 'Bangladesh', 'BD', 'BDT', '050', 'BGD', 1210997, 'Asia', 'AS', 0),
(20, 'Belgium', 'BE', 'EUR', '056', 'BEL', 2802361, 'Europe', 'EU', 0),
(21, 'Burkina Faso', 'BF', 'XOF', '854', 'BFA', 2361809, 'Africa', 'AF', 0),
(22, 'Bulgaria', 'BG', 'BGN', '100', 'BGR', 732800, 'Europe', 'EU', 0),
(23, 'Bahrain', 'BH', 'BHD', '048', 'BHR', 290291, 'Asia', 'AS', 0),
(24, 'Burundi', 'BI', 'BIF', '108', 'BDI', 433561, 'Africa', 'AF', 0),
(25, 'Benin', 'BJ', 'XOF', '204', 'BEN', 2395170, 'Africa', 'AF', 0),
(26, 'Saint Barthélemy', 'BL', 'EUR', '652', 'BLM', 3578476, 'North America', 'NA', 0),
(27, 'Bermuda', 'BM', 'BMD', '060', 'BMU', 3573345, 'North America', 'NA', 0),
(28, 'Brunei', 'BN', 'BND', '096', 'BRN', 1820814, 'Asia', 'AS', 0),
(29, 'Bolivia', 'BO', 'BOB', '068', 'BOL', 3923057, 'South America', 'SA', 0),
(30, 'Bonaire', 'BQ', 'USD', '535', 'BES', 7626844, 'North America', 'NA', 0),
(31, 'Brazil', 'BR', 'BRL', '076', 'BRA', 3469034, 'South America', 'SA', 0),
(32, 'Bahamas', 'BS', 'BSD', '044', 'BHS', 3572887, 'North America', 'NA', 0),
(33, 'Bhutan', 'BT', 'BTN', '064', 'BTN', 1252634, 'Asia', 'AS', 0),
(34, 'Bouvet Island', 'BV', 'NOK', '074', 'BVT', 3371123, 'Antarctica', 'AN', 0),
(35, 'Botswana', 'BW', 'BWP', '072', 'BWA', 933860, 'Africa', 'AF', 0),
(36, 'Belarus', 'BY', 'BYR', '112', 'BLR', 630336, 'Europe', 'EU', 0),
(37, 'Belize', 'BZ', 'BZD', '084', 'BLZ', 3582678, 'North America', 'NA', 0),
(38, 'Canada', 'CA', 'CAD', '124', 'CAN', 6251999, 'North America', 'NA', 0),
(39, 'Cocos [Keeling] Islands', 'CC', 'AUD', '166', 'CCK', 1547376, 'Asia', 'AS', 0),
(40, 'Democratic Republic of the Congo', 'CD', 'CDF', '180', 'COD', 203312, 'Africa', 'AF', 0),
(41, 'Central African Republic', 'CF', 'XAF', '140', 'CAF', 239880, 'Africa', 'AF', 0),
(42, 'Republic of the Congo', 'CG', 'XAF', '178', 'COG', 2260494, 'Africa', 'AF', 0),
(43, 'Switzerland', 'CH', 'CHF', '756', 'CHE', 2658434, 'Europe', 'EU', 0),
(44, 'Ivory Coast', 'CI', 'XOF', '384', 'CIV', 2287781, 'Africa', 'AF', 0),
(45, 'Cook Islands', 'CK', 'NZD', '184', 'COK', 1899402, 'Oceania', 'OC', 0),
(46, 'Chile', 'CL', 'CLP', '152', 'CHL', 3895114, 'South America', 'SA', 0),
(47, 'Cameroon', 'CM', 'XAF', '120', 'CMR', 2233387, 'Africa', 'AF', 0),
(48, 'China', 'CN', 'CNY', '156', 'CHN', 1814991, 'Asia', 'AS', 0),
(49, 'Colombia', 'CO', 'COP', '170', 'COL', 3686110, 'South America', 'SA', 0),
(50, 'Costa Rica', 'CR', 'CRC', '188', 'CRI', 3624060, 'North America', 'NA', 0),
(51, 'Cuba', 'CU', 'CUP', '192', 'CUB', 3562981, 'North America', 'NA', 0),
(52, 'Cape Verde', 'CV', 'CVE', '132', 'CPV', 3374766, 'Africa', 'AF', 0),
(53, 'Curacao', 'CW', 'ANG', '531', 'CUW', 7626836, 'North America', 'NA', 0),
(54, 'Christmas Island', 'CX', 'AUD', '162', 'CXR', 2078138, 'Asia', 'AS', 0),
(55, 'Cyprus', 'CY', 'EUR', '196', 'CYP', 146669, 'Europe', 'EU', 0),
(56, 'Czech Republic', 'CZ', 'CZK', '203', 'CZE', 3077311, 'Europe', 'EU', 0),
(57, 'Germany', 'DE', 'EUR', '276', 'DEU', 2921044, 'Europe', 'EU', 0),
(58, 'Djibouti', 'DJ', 'DJF', '262', 'DJI', 223816, 'Africa', 'AF', 0),
(59, 'Denmark', 'DK', 'DKK', '208', 'DNK', 2623032, 'Europe', 'EU', 0),
(60, 'Dominica', 'DM', 'XCD', '212', 'DMA', 3575830, 'North America', 'NA', 0),
(61, 'Dominican Republic', 'DO', 'DOP', '214', 'DOM', 3508796, 'North America', 'NA', 0),
(62, 'Algeria', 'DZ', 'DZD', '012', 'DZA', 2589581, 'Africa', 'AF', 0),
(63, 'Ecuador', 'EC', 'USD', '218', 'ECU', 3658394, 'South America', 'SA', 0),
(64, 'Estonia', 'EE', 'EUR', '233', 'EST', 453733, 'Europe', 'EU', 0),
(65, 'Egypt', 'EG', 'EGP', '818', 'EGY', 357994, 'Africa', 'AF', 0),
(66, 'Western Sahara', 'EH', 'MAD', '732', 'ESH', 2461445, 'Africa', 'AF', 0),
(67, 'Eritrea', 'ER', 'ERN', '232', 'ERI', 338010, 'Africa', 'AF', 0),
(68, 'Spain', 'ES', 'EUR', '724', 'ESP', 2510769, 'Europe', 'EU', 0),
(69, 'Ethiopia', 'ET', 'ETB', '231', 'ETH', 337996, 'Africa', 'AF', 0),
(70, 'Finland', 'FI', 'EUR', '246', 'FIN', 660013, 'Europe', 'EU', 0),
(71, 'Fiji', 'FJ', 'FJD', '242', 'FJI', 2205218, 'Oceania', 'OC', 0),
(72, 'Falkland Islands', 'FK', 'FKP', '238', 'FLK', 3474414, 'South America', 'SA', 0),
(73, 'Micronesia', 'FM', 'USD', '583', 'FSM', 2081918, 'Oceania', 'OC', 0),
(74, 'Faroe Islands', 'FO', 'DKK', '234', 'FRO', 2622320, 'Europe', 'EU', 0),
(75, 'France', 'FR', 'EUR', '250', 'FRA', 3017382, 'Europe', 'EU', 0),
(76, 'Gabon', 'GA', 'XAF', '266', 'GAB', 2400553, 'Africa', 'AF', 0),
(77, 'United Kingdom', 'GB', 'GBP', '826', 'GBR', 2635167, 'Europe', 'EU', 0),
(78, 'Grenada', 'GD', 'XCD', '308', 'GRD', 3580239, 'North America', 'NA', 0),
(79, 'Georgia', 'GE', 'GEL', '268', 'GEO', 614540, 'Asia', 'AS', 0),
(80, 'French Guiana', 'GF', 'EUR', '254', 'GUF', 3381670, 'South America', 'SA', 0),
(81, 'Guernsey', 'GG', 'GBP', '831', 'GGY', 3042362, 'Europe', 'EU', 0),
(82, 'Ghana', 'GH', 'GHS', '288', 'GHA', 2300660, 'Africa', 'AF', 0),
(83, 'Gibraltar', 'GI', 'GIP', '292', 'GIB', 2411586, 'Europe', 'EU', 0),
(84, 'Greenland', 'GL', 'DKK', '304', 'GRL', 3425505, 'North America', 'NA', 0),
(85, 'Gambia', 'GM', 'GMD', '270', 'GMB', 2413451, 'Africa', 'AF', 0),
(86, 'Guinea', 'GN', 'GNF', '324', 'GIN', 2420477, 'Africa', 'AF', 0),
(87, 'Guadeloupe', 'GP', 'EUR', '312', 'GLP', 3579143, 'North America', 'NA', 0),
(88, 'Equatorial Guinea', 'GQ', 'XAF', '226', 'GNQ', 2309096, 'Africa', 'AF', 0),
(89, 'Greece', 'GR', 'EUR', '300', 'GRC', 390903, 'Europe', 'EU', 1),
(90, 'South Georgia and the South Sandwich Islands', 'GS', 'GBP', '239', 'SGS', 3474415, 'Antarctica', 'AN', 0),
(91, 'Guatemala', 'GT', 'GTQ', '320', 'GTM', 3595528, 'North America', 'NA', 0),
(92, 'Guam', 'GU', 'USD', '316', 'GUM', 4043988, 'Oceania', 'OC', 0),
(93, 'Guinea-Bissau', 'GW', 'XOF', '624', 'GNB', 2372248, 'Africa', 'AF', 0),
(94, 'Guyana', 'GY', 'GYD', '328', 'GUY', 3378535, 'South America', 'SA', 0),
(95, 'Hong Kong', 'HK', 'HKD', '344', 'HKG', 1819730, 'Asia', 'AS', 0),
(96, 'Heard Island and McDonald Islands', 'HM', 'AUD', '334', 'HMD', 1547314, 'Antarctica', 'AN', 0),
(97, 'Honduras', 'HN', 'HNL', '340', 'HND', 3608932, 'North America', 'NA', 0),
(98, 'Croatia', 'HR', 'HRK', '191', 'HRV', 3202326, 'Europe', 'EU', 0),
(99, 'Haiti', 'HT', 'HTG', '332', 'HTI', 3723988, 'North America', 'NA', 0),
(100, 'Hungary', 'HU', 'HUF', '348', 'HUN', 719819, 'Europe', 'EU', 0),
(101, 'Indonesia', 'ID', 'IDR', '360', 'IDN', 1643084, 'Asia', 'AS', 0),
(102, 'Ireland', 'IE', 'EUR', '372', 'IRL', 2963597, 'Europe', 'EU', 0),
(103, 'Israel', 'IL', 'ILS', '376', 'ISR', 294640, 'Asia', 'AS', 0),
(104, 'Isle of Man', 'IM', 'GBP', '833', 'IMN', 3042225, 'Europe', 'EU', 0),
(105, 'India', 'IN', 'INR', '356', 'IND', 1269750, 'Asia', 'AS', 0),
(106, 'British Indian Ocean Territory', 'IO', 'USD', '086', 'IOT', 1282588, 'Asia', 'AS', 0),
(107, 'Iraq', 'IQ', 'IQD', '368', 'IRQ', 99237, 'Asia', 'AS', 0),
(108, 'Iran', 'IR', 'IRR', '364', 'IRN', 130758, 'Asia', 'AS', 0),
(109, 'Iceland', 'IS', 'ISK', '352', 'ISL', 2629691, 'Europe', 'EU', 0),
(110, 'Italy', 'IT', 'EUR', '380', 'ITA', 3175395, 'Europe', 'EU', 0),
(111, 'Jersey', 'JE', 'GBP', '832', 'JEY', 3042142, 'Europe', 'EU', 0),
(112, 'Jamaica', 'JM', 'JMD', '388', 'JAM', 3489940, 'North America', 'NA', 0),
(113, 'Jordan', 'JO', 'JOD', '400', 'JOR', 248816, 'Asia', 'AS', 0),
(114, 'Japan', 'JP', 'JPY', '392', 'JPN', 1861060, 'Asia', 'AS', 0),
(115, 'Kenya', 'KE', 'KES', '404', 'KEN', 192950, 'Africa', 'AF', 0),
(116, 'Kyrgyzstan', 'KG', 'KGS', '417', 'KGZ', 1527747, 'Asia', 'AS', 0),
(117, 'Cambodia', 'KH', 'KHR', '116', 'KHM', 1831722, 'Asia', 'AS', 0),
(118, 'Kiribati', 'KI', 'AUD', '296', 'KIR', 4030945, 'Oceania', 'OC', 0),
(119, 'Comoros', 'KM', 'KMF', '174', 'COM', 921929, 'Africa', 'AF', 0),
(120, 'Saint Kitts and Nevis', 'KN', 'XCD', '659', 'KNA', 3575174, 'North America', 'NA', 0),
(121, 'North Korea', 'KP', 'KPW', '408', 'PRK', 1873107, 'Asia', 'AS', 0),
(122, 'South Korea', 'KR', 'KRW', '410', 'KOR', 1835841, 'Asia', 'AS', 0),
(123, 'Kuwait', 'KW', 'KWD', '414', 'KWT', 285570, 'Asia', 'AS', 0),
(124, 'Cayman Islands', 'KY', 'KYD', '136', 'CYM', 3580718, 'North America', 'NA', 0),
(125, 'Kazakhstan', 'KZ', 'KZT', '398', 'KAZ', 1522867, 'Asia', 'AS', 0),
(126, 'Laos', 'LA', 'LAK', '418', 'LAO', 1655842, 'Asia', 'AS', 0),
(127, 'Lebanon', 'LB', 'LBP', '422', 'LBN', 272103, 'Asia', 'AS', 0),
(128, 'Saint Lucia', 'LC', 'XCD', '662', 'LCA', 3576468, 'North America', 'NA', 0),
(129, 'Liechtenstein', 'LI', 'CHF', '438', 'LIE', 3042058, 'Europe', 'EU', 0),
(130, 'Sri Lanka', 'LK', 'LKR', '144', 'LKA', 1227603, 'Asia', 'AS', 0),
(131, 'Liberia', 'LR', 'LRD', '430', 'LBR', 2275384, 'Africa', 'AF', 0),
(132, 'Lesotho', 'LS', 'LSL', '426', 'LSO', 932692, 'Africa', 'AF', 0),
(133, 'Lithuania', 'LT', 'LTL', '440', 'LTU', 597427, 'Europe', 'EU', 0),
(134, 'Luxembourg', 'LU', 'EUR', '442', 'LUX', 2960313, 'Europe', 'EU', 0),
(135, 'Latvia', 'LV', 'EUR', '428', 'LVA', 458258, 'Europe', 'EU', 0),
(136, 'Libya', 'LY', 'LYD', '434', 'LBY', 2215636, 'Africa', 'AF', 0),
(137, 'Morocco', 'MA', 'MAD', '504', 'MAR', 2542007, 'Africa', 'AF', 0),
(138, 'Monaco', 'MC', 'EUR', '492', 'MCO', 2993457, 'Europe', 'EU', 0),
(139, 'Moldova', 'MD', 'MDL', '498', 'MDA', 617790, 'Europe', 'EU', 0),
(140, 'Montenegro', 'ME', 'EUR', '499', 'MNE', 3194884, 'Europe', 'EU', 0),
(141, 'Saint Martin', 'MF', 'EUR', '663', 'MAF', 3578421, 'North America', 'NA', 0),
(142, 'Madagascar', 'MG', 'MGA', '450', 'MDG', 1062947, 'Africa', 'AF', 0),
(143, 'Marshall Islands', 'MH', 'USD', '584', 'MHL', 2080185, 'Oceania', 'OC', 0),
(144, 'Macedonia', 'MK', 'MKD', '807', 'MKD', 718075, 'Europe', 'EU', 0),
(145, 'Mali', 'ML', 'XOF', '466', 'MLI', 2453866, 'Africa', 'AF', 0),
(146, 'Myanmar [Burma]', 'MM', 'MMK', '104', 'MMR', 1327865, 'Asia', 'AS', 0),
(147, 'Mongolia', 'MN', 'MNT', '496', 'MNG', 2029969, 'Asia', 'AS', 0),
(148, 'Macao', 'MO', 'MOP', '446', 'MAC', 1821275, 'Asia', 'AS', 0),
(149, 'Northern Mariana Islands', 'MP', 'USD', '580', 'MNP', 4041468, 'Oceania', 'OC', 0),
(150, 'Martinique', 'MQ', 'EUR', '474', 'MTQ', 3570311, 'North America', 'NA', 0),
(151, 'Mauritania', 'MR', 'MRO', '478', 'MRT', 2378080, 'Africa', 'AF', 0),
(152, 'Montserrat', 'MS', 'XCD', '500', 'MSR', 3578097, 'North America', 'NA', 0),
(153, 'Malta', 'MT', 'EUR', '470', 'MLT', 2562770, 'Europe', 'EU', 0),
(154, 'Mauritius', 'MU', 'MUR', '480', 'MUS', 934292, 'Africa', 'AF', 0),
(155, 'Maldives', 'MV', 'MVR', '462', 'MDV', 1282028, 'Asia', 'AS', 0),
(156, 'Malawi', 'MW', 'MWK', '454', 'MWI', 927384, 'Africa', 'AF', 0),
(157, 'Mexico', 'MX', 'MXN', '484', 'MEX', 3996063, 'North America', 'NA', 0),
(158, 'Malaysia', 'MY', 'MYR', '458', 'MYS', 1733045, 'Asia', 'AS', 0),
(159, 'Mozambique', 'MZ', 'MZN', '508', 'MOZ', 1036973, 'Africa', 'AF', 0),
(160, 'Namibia', 'NA', 'NAD', '516', 'NAM', 3355338, 'Africa', 'AF', 0),
(161, 'New Caledonia', 'NC', 'XPF', '540', 'NCL', 2139685, 'Oceania', 'OC', 0),
(162, 'Niger', 'NE', 'XOF', '562', 'NER', 2440476, 'Africa', 'AF', 0),
(163, 'Norfolk Island', 'NF', 'AUD', '574', 'NFK', 2155115, 'Oceania', 'OC', 0),
(164, 'Nigeria', 'NG', 'NGN', '566', 'NGA', 2328926, 'Africa', 'AF', 0),
(165, 'Nicaragua', 'NI', 'NIO', '558', 'NIC', 3617476, 'North America', 'NA', 0),
(166, 'Netherlands', 'NL', 'EUR', '528', 'NLD', 2750405, 'Europe', 'EU', 0),
(167, 'Norway', 'NO', 'NOK', '578', 'NOR', 3144096, 'Europe', 'EU', 0),
(168, 'Nepal', 'NP', 'NPR', '524', 'NPL', 1282988, 'Asia', 'AS', 0),
(169, 'Nauru', 'NR', 'AUD', '520', 'NRU', 2110425, 'Oceania', 'OC', 0),
(170, 'Niue', 'NU', 'NZD', '570', 'NIU', 4036232, 'Oceania', 'OC', 0),
(171, 'New Zealand', 'NZ', 'NZD', '554', 'NZL', 2186224, 'Oceania', 'OC', 0),
(172, 'Oman', 'OM', 'OMR', '512', 'OMN', 286963, 'Asia', 'AS', 0),
(173, 'Panama', 'PA', 'PAB', '591', 'PAN', 3703430, 'North America', 'NA', 0),
(174, 'Peru', 'PE', 'PEN', '604', 'PER', 3932488, 'South America', 'SA', 0),
(175, 'French Polynesia', 'PF', 'XPF', '258', 'PYF', 4030656, 'Oceania', 'OC', 0),
(176, 'Papua New Guinea', 'PG', 'PGK', '598', 'PNG', 2088628, 'Oceania', 'OC', 0),
(177, 'Philippines', 'PH', 'PHP', '608', 'PHL', 1694008, 'Asia', 'AS', 0),
(178, 'Pakistan', 'PK', 'PKR', '586', 'PAK', 1168579, 'Asia', 'AS', 0),
(179, 'Poland', 'PL', 'PLN', '616', 'POL', 798544, 'Europe', 'EU', 0),
(180, 'Saint Pierre and Miquelon', 'PM', 'EUR', '666', 'SPM', 3424932, 'North America', 'NA', 0),
(181, 'Pitcairn Islands', 'PN', 'NZD', '612', 'PCN', 4030699, 'Oceania', 'OC', 0),
(182, 'Puerto Rico', 'PR', 'USD', '630', 'PRI', 4566966, 'North America', 'NA', 0),
(183, 'Palestine', 'PS', 'ILS', '275', 'PSE', 6254930, 'Asia', 'AS', 0),
(184, 'Portugal', 'PT', 'EUR', '620', 'PRT', 2264397, 'Europe', 'EU', 0),
(185, 'Palau', 'PW', 'USD', '585', 'PLW', 1559582, 'Oceania', 'OC', 0),
(186, 'Paraguay', 'PY', 'PYG', '600', 'PRY', 3437598, 'South America', 'SA', 0),
(187, 'Qatar', 'QA', 'QAR', '634', 'QAT', 289688, 'Asia', 'AS', 0),
(188, 'Réunion', 'RE', 'EUR', '638', 'REU', 935317, 'Africa', 'AF', 0),
(189, 'Romania', 'RO', 'RON', '642', 'ROU', 798549, 'Europe', 'EU', 0),
(190, 'Serbia', 'RS', 'RSD', '688', 'SRB', 6290252, 'Europe', 'EU', 0),
(191, 'Russia', 'RU', 'RUB', '643', 'RUS', 2017370, 'Europe', 'EU', 0),
(192, 'Rwanda', 'RW', 'RWF', '646', 'RWA', 49518, 'Africa', 'AF', 0),
(193, 'Saudi Arabia', 'SA', 'SAR', '682', 'SAU', 102358, 'Asia', 'AS', 0),
(194, 'Solomon Islands', 'SB', 'SBD', '090', 'SLB', 2103350, 'Oceania', 'OC', 0),
(195, 'Seychelles', 'SC', 'SCR', '690', 'SYC', 241170, 'Africa', 'AF', 0),
(196, 'Sudan', 'SD', 'SDG', '729', 'SDN', 366755, 'Africa', 'AF', 0),
(197, 'Sweden', 'SE', 'SEK', '752', 'SWE', 2661886, 'Europe', 'EU', 0),
(198, 'Singapore', 'SG', 'SGD', '702', 'SGP', 1880251, 'Asia', 'AS', 0),
(199, 'Saint Helena', 'SH', 'SHP', '654', 'SHN', 3370751, 'Africa', 'AF', 0),
(200, 'Slovenia', 'SI', 'EUR', '705', 'SVN', 3190538, 'Europe', 'EU', 0),
(201, 'Svalbard and Jan Mayen', 'SJ', 'NOK', '744', 'SJM', 607072, 'Europe', 'EU', 0),
(202, 'Slovakia', 'SK', 'EUR', '703', 'SVK', 3057568, 'Europe', 'EU', 0),
(203, 'Sierra Leone', 'SL', 'SLL', '694', 'SLE', 2403846, 'Africa', 'AF', 0),
(204, 'San Marino', 'SM', 'EUR', '674', 'SMR', 3168068, 'Europe', 'EU', 0),
(205, 'Senegal', 'SN', 'XOF', '686', 'SEN', 2245662, 'Africa', 'AF', 0),
(206, 'Somalia', 'SO', 'SOS', '706', 'SOM', 51537, 'Africa', 'AF', 0),
(207, 'Suriname', 'SR', 'SRD', '740', 'SUR', 3382998, 'South America', 'SA', 0),
(208, 'South Sudan', 'SS', 'SSP', '728', 'SSD', 7909807, 'Africa', 'AF', 0),
(209, 'São Tomé and Príncipe', 'ST', 'STD', '678', 'STP', 2410758, 'Africa', 'AF', 0),
(210, 'El Salvador', 'SV', 'USD', '222', 'SLV', 3585968, 'North America', 'NA', 0),
(211, 'Sint Maarten', 'SX', 'ANG', '534', 'SXM', 7609695, 'North America', 'NA', 0),
(212, 'Syria', 'SY', 'SYP', '760', 'SYR', 163843, 'Asia', 'AS', 0),
(213, 'Swaziland', 'SZ', 'SZL', '748', 'SWZ', 934841, 'Africa', 'AF', 0),
(214, 'Turks and Caicos Islands', 'TC', 'USD', '796', 'TCA', 3576916, 'North America', 'NA', 0),
(215, 'Chad', 'TD', 'XAF', '148', 'TCD', 2434508, 'Africa', 'AF', 0),
(216, 'French Southern Territories', 'TF', 'EUR', '260', 'ATF', 1546748, 'Antarctica', 'AN', 0),
(217, 'Togo', 'TG', 'XOF', '768', 'TGO', 2363686, 'Africa', 'AF', 0),
(218, 'Thailand', 'TH', 'THB', '764', 'THA', 1605651, 'Asia', 'AS', 0),
(219, 'Tajikistan', 'TJ', 'TJS', '762', 'TJK', 1220409, 'Asia', 'AS', 0),
(220, 'Tokelau', 'TK', 'NZD', '772', 'TKL', 4031074, 'Oceania', 'OC', 0),
(221, 'East Timor', 'TL', 'USD', '626', 'TLS', 1966436, 'Oceania', 'OC', 0),
(222, 'Turkmenistan', 'TM', 'TMT', '795', 'TKM', 1218197, 'Asia', 'AS', 0),
(223, 'Tunisia', 'TN', 'TND', '788', 'TUN', 2464461, 'Africa', 'AF', 0),
(224, 'Tonga', 'TO', 'TOP', '776', 'TON', 4032283, 'Oceania', 'OC', 0),
(225, 'Turkey', 'TR', 'TRY', '792', 'TUR', 298795, 'Asia', 'AS', 0),
(226, 'Trinidad and Tobago', 'TT', 'TTD', '780', 'TTO', 3573591, 'North America', 'NA', 0),
(227, 'Tuvalu', 'TV', 'AUD', '798', 'TUV', 2110297, 'Oceania', 'OC', 0),
(228, 'Taiwan', 'TW', 'TWD', '158', 'TWN', 1668284, 'Asia', 'AS', 0),
(229, 'Tanzania', 'TZ', 'TZS', '834', 'TZA', 149590, 'Africa', 'AF', 0),
(230, 'Ukraine', 'UA', 'UAH', '804', 'UKR', 690791, 'Europe', 'EU', 0),
(231, 'Uganda', 'UG', 'UGX', '800', 'UGA', 226074, 'Africa', 'AF', 0),
(232, 'U.S. Minor Outlying Islands', 'UM', 'USD', '581', 'UMI', 5854968, 'Oceania', 'OC', 0),
(233, 'United States', 'US', 'USD', '840', 'USA', 6252001, 'North America', 'NA', 0),
(234, 'Uruguay', 'UY', 'UYU', '858', 'URY', 3439705, 'South America', 'SA', 0),
(235, 'Uzbekistan', 'UZ', 'UZS', '860', 'UZB', 1512440, 'Asia', 'AS', 0),
(236, 'Vatican City', 'VA', 'EUR', '336', 'VAT', 3164670, 'Europe', 'EU', 0),
(237, 'Saint Vincent and the Grenadines', 'VC', 'XCD', '670', 'VCT', 3577815, 'North America', 'NA', 0),
(238, 'Venezuela', 'VE', 'VEF', '862', 'VEN', 3625428, 'South America', 'SA', 0),
(239, 'British Virgin Islands', 'VG', 'USD', '092', 'VGB', 3577718, 'North America', 'NA', 0),
(240, 'U.S. Virgin Islands', 'VI', 'USD', '850', 'VIR', 4796775, 'North America', 'NA', 0),
(241, 'Vietnam', 'VN', 'VND', '704', 'VNM', 1562822, 'Asia', 'AS', 0),
(242, 'Vanuatu', 'VU', 'VUV', '548', 'VUT', 2134431, 'Oceania', 'OC', 0),
(243, 'Wallis and Futuna', 'WF', 'XPF', '876', 'WLF', 4034749, 'Oceania', 'OC', 0),
(244, 'Samoa', 'WS', 'WST', '882', 'WSM', 4034894, 'Oceania', 'OC', 0),
(245, 'Kosovo', 'XK', 'EUR', '0', 'XKX', 831053, 'Europe', 'EU', 0),
(246, 'Yemen', 'YE', 'YER', '887', 'YEM', 69543, 'Asia', 'AS', 0),
(247, 'Mayotte', 'YT', 'EUR', '175', 'MYT', 1024031, 'Africa', 'AF', 0),
(248, 'South Africa', 'ZA', 'ZAR', '710', 'ZAF', 953987, 'Africa', 'AF', 0),
(249, 'Zambia', 'ZM', 'ZMW', '894', 'ZMB', 895949, 'Africa', 'AF', 0),
(250, 'Zimbabwe', 'ZW', 'ZWL', '716', 'ZWE', 878675, 'Africa', 'AF', 0);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `geo_countries_languages` (
  `country_code` varchar(10),
  `language_code` varchar(10),
  `geo_countries_id` bigint unsigned,
  PRIMARY KEY (`country_code`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `geo_countries_languages` (`country_code`, `language_code`, `geo_countries_id`) VALUES
('AD', 'ca', 1),
('AE', 'ar_AE', 2),
('AE', 'en', 2),
('AE', 'fa', 2),
('AE', 'hi', 2),
('AE', 'ur', 2),
('AF', 'fa_AF', 3),
('AF', 'ps', 3),
('AF', 'tk', 3),
('AF', 'uz_AF', 3),
('AG', 'en_AG', 4),
('AI', 'en_AI', 5),
('AL', 'el', 6),
('AL', 'sq', 6),
('AM', 'hy', 7),
('AO', 'pt_AO', 8),
('AR', 'de', 10),
('AR', 'en', 10),
('AR', 'es_AR', 10),
('AR', 'fr', 10),
('AR', 'gn', 10),
('AR', 'it', 10),
('AS', 'en_AS', 11),
('AS', 'sm', 11),
('AS', 'to', 11),
('AT', 'de_AT', 12),
('AT', 'hr', 12),
('AT', 'hu', 12),
('AT', 'sl', 12),
('AU', 'en_AU', 13),
('AW', 'en', 14),
('AW', 'es', 14),
('AW', 'nl_AW', 14),
('AX', 'sv_AX', 15),
('AZ', 'az', 16),
('AZ', 'hy', 16),
('AZ', 'ru', 16),
('BA', 'bs', 17),
('BA', 'hr_BA', 17),
('BA', 'sr_BA', 17),
('BB', 'en_BB', 18),
('BD', 'bn_BD', 19),
('BD', 'en', 19),
('BE', 'de_BE', 20),
('BE', 'fr_BE', 20),
('BE', 'nl_BE', 20),
('BF', 'fr_BF', 21),
('BG', 'bg', 22),
('BG', 'tr_BG', 22),
('BH', 'ar_BH', 23),
('BH', 'en', 23),
('BH', 'fa', 23),
('BH', 'ur', 23),
('BI', 'fr_BI', 24),
('BI', 'rn', 24),
('BJ', 'fr_BJ', 25),
('BL', 'fr', 26),
('BM', 'en_BM', 27),
('BM', 'pt', 27),
('BN', 'en_BN', 28),
('BN', 'ms_BN', 28),
('BO', 'ay', 29),
('BO', 'es_BO', 29),
('BO', 'qu', 29),
('BQ', 'en', 30),
('BQ', 'nl', 30),
('BQ', 'pap', 30),
('BR', 'en', 31),
('BR', 'es', 31),
('BR', 'fr', 31),
('BR', 'pt_BR', 31),
('BS', 'en_BS', 32),
('BT', 'dz', 33),
('BW', 'en_BW', 35),
('BW', 'tn_BW', 35),
('BY', 'be', 36),
('BY', 'ru', 36),
('BZ', 'en_BZ', 37),
('BZ', 'es', 37),
('CA', 'en_CA', 38),
('CA', 'fr_CA', 38),
('CA', 'iu', 38),
('CC', 'en', 39),
('CC', 'ms_CC', 39),
('CD', 'fr_CD', 40),
('CD', 'kg', 40),
('CD', 'ln', 40),
('CF', 'fr_CF', 41),
('CF', 'kg', 41),
('CF', 'ln', 41),
('CF', 'sg', 41),
('CG', 'fr_CG', 42),
('CG', 'kg', 42),
('CG', 'ln_CG', 42),
('CH', 'de_CH', 43),
('CH', 'fr_CH', 43),
('CH', 'it_CH', 43),
('CH', 'rm', 43),
('CI', 'fr_CI', 44),
('CK', 'en_CK', 45),
('CK', 'mi', 45),
('CL', 'es_CL', 46),
('CM', 'en_CM', 47),
('CM', 'fr_CM', 47),
('CN', 'dta', 48),
('CN', 'ug', 48),
('CN', 'wuu', 48),
('CN', 'yue', 48),
('CN', 'za', 48),
('CN', 'zh_CN', 48),
('CO', 'es_CO', 49),
('CR', 'en', 50),
('CR', 'es_CR', 50),
('CU', 'es_CU', 51),
('CV', 'pt_CV', 52),
('CW', 'nl', 53),
('CW', 'pap', 53),
('CX', 'en', 54),
('CX', 'ms_CC', 54),
('CX', 'zh', 54),
('CY', 'el_CY', 55),
('CY', 'en', 55),
('CY', 'tr_CY', 55),
('CZ', 'cs', 56),
('CZ', 'sk', 56),
('DE', 'de', 57),
('DJ', 'aa', 58),
('DJ', 'ar', 58),
('DJ', 'fr_DJ', 58),
('DJ', 'so_DJ', 58),
('DK', 'da_DK', 59),
('DK', 'de_DK', 59),
('DK', 'en', 59),
('DK', 'fo', 59),
('DM', 'en_DM', 60),
('DO', 'es_DO', 61),
('DZ', 'ar_DZ', 62),
('EC', 'es_EC', 63),
('EE', 'et', 64),
('EE', 'ru', 64),
('EG', 'ar_EG', 65),
('EG', 'en', 65),
('EG', 'fr', 65),
('EH', 'ar', 66),
('EH', 'mey', 66),
('ER', 'aa_ER', 67),
('ER', 'ar', 67),
('ER', 'kun', 67),
('ER', 'ti_ER', 67),
('ER', 'tig', 67),
('ES', 'ca', 68),
('ES', 'es_ES', 68),
('ES', 'eu', 68),
('ES', 'gl', 68),
('ES', 'oc', 68),
('ET', 'am', 69),
('ET', 'en_ET', 69),
('ET', 'om_ET', 69),
('ET', 'sid', 69),
('ET', 'so_ET', 69),
('ET', 'ti_ET', 69),
('FI', 'fi_FI', 70),
('FI', 'smn', 70),
('FI', 'sv_FI', 70),
('FJ', 'en_FJ', 71),
('FJ', 'fj', 71),
('FK', 'en_FK', 72),
('FM', 'chk', 73),
('FM', 'en_FM', 73),
('FM', 'kos', 73),
('FM', 'kpg', 73),
('FM', 'nkr', 73),
('FM', 'pon', 73),
('FM', 'uli', 73),
('FM', 'woe', 73),
('FM', 'yap', 73),
('FO', 'da_FO', 74),
('FO', 'fo', 74),
('FR', 'br', 75),
('FR', 'ca', 75),
('FR', 'co', 75),
('FR', 'eu', 75),
('FR', 'fr_FR', 75),
('FR', 'frp', 75),
('FR', 'oc', 75),
('GA', 'fr_GA', 76),
('GB', 'cy_GB', 77),
('GB', 'en_GB', 77),
('GB', 'gd', 77),
('GD', 'en_GD', 78),
('GE', 'az', 79),
('GE', 'hy', 79),
('GE', 'ka', 79),
('GE', 'ru', 79),
('GF', 'fr_GF', 80),
('GG', 'en', 81),
('GG', 'fr', 81),
('GH', 'ak', 82),
('GH', 'ee', 82),
('GH', 'en_GH', 82),
('GH', 'tw', 82),
('GI', 'en_GI', 83),
('GI', 'es', 83),
('GI', 'it', 83),
('GI', 'pt', 83),
('GL', 'da_GL', 84),
('GL', 'en', 84),
('GL', 'kl', 84),
('GM', 'en_GM', 85),
('GM', 'ff', 85),
('GM', 'mnk', 85),
('GM', 'wo', 85),
('GM', 'wof', 85),
('GN', 'fr_GN', 86),
('GP', 'fr_GP', 87),
('GQ', 'es_GQ', 88),
('GQ', 'fr', 88),
('GR', 'el_GR', 89),
('GR', 'en', 89),
('GR', 'fr', 89),
('GS', 'en', 90),
('GT', 'es_GT', 91),
('GU', 'ch_GU', 92),
('GU', 'en_GU', 92),
('GW', 'pov', 93),
('GW', 'pt_GW', 93),
('GY', 'en_GY', 94),
('HK', 'en', 95),
('HK', 'yue', 95),
('HK', 'zh', 95),
('HK', 'zh_HK', 95),
('HN', 'es_HN', 97),
('HR', 'hr_HR', 98),
('HR', 'sr', 98),
('HT', 'fr_HT', 99),
('HT', 'ht', 99),
('HU', 'hu_HU', 100),
('ID', 'en', 101),
('ID', 'id', 101),
('ID', 'jv', 101),
('ID', 'nl', 101),
('IE', 'en_IE', 102),
('IE', 'ga_IE', 102),
('IL', 'ar_IL', 103),
('IL', 'en_IL', 103),
('IL', 'he', 103),
('IM', 'en', 104),
('IM', 'gv', 104),
('IN', 'as', 105),
('IN', 'bh', 105),
('IN', 'bn', 105),
('IN', 'doi', 105),
('IN', 'en_IN', 105),
('IN', 'fr', 105),
('IN', 'gu', 105),
('IN', 'hi', 105),
('IN', 'inc', 105),
('IN', 'kn', 105),
('IN', 'kok', 105),
('IN', 'ks', 105),
('IN', 'lus', 105),
('IN', 'ml', 105),
('IN', 'mni', 105),
('IN', 'mr', 105),
('IN', 'ne', 105),
('IN', 'or', 105),
('IN', 'pa', 105),
('IN', 'sa', 105),
('IN', 'sat', 105),
('IN', 'sd', 105),
('IN', 'sit', 105),
('IN', 'ta', 105),
('IN', 'te', 105),
('IN', 'ur', 105),
('IO', 'en_IO', 106),
('IQ', 'ar_IQ', 107),
('IQ', 'hy', 107),
('IQ', 'ku', 107),
('IR', 'fa_IR', 108),
('IR', 'ku', 108),
('IS', 'da', 109),
('IS', 'de', 109),
('IS', 'en', 109),
('IS', 'is', 109),
('IS', 'no', 109),
('IS', 'sv', 109),
('IT', 'ca', 110),
('IT', 'co', 110),
('IT', 'de_IT', 110),
('IT', 'fr_IT', 110),
('IT', 'it_IT', 110),
('IT', 'sc', 110),
('IT', 'sl', 110),
('JE', 'en', 111),
('JE', 'pt', 111),
('JM', 'en_JM', 112),
('JO', 'ar_JO', 113),
('JO', 'en', 113),
('JP', 'ja', 114),
('KE', 'en_KE', 115),
('KE', 'sw_KE', 115),
('KG', 'ky', 116),
('KG', 'ru', 116),
('KG', 'uz', 116),
('KH', 'en', 117),
('KH', 'fr', 117),
('KH', 'km', 117),
('KI', 'en_KI', 118),
('KI', 'gil', 118),
('KM', 'ar', 119),
('KM', 'fr_KM', 119),
('KN', 'en_KN', 120),
('KP', 'ko_KP', 121),
('KR', 'en', 122),
('KR', 'ko_KR', 122),
('KW', 'ar_KW', 123),
('KW', 'en', 123),
('KY', 'en_KY', 124),
('KZ', 'kk', 125),
('KZ', 'ru', 125),
('LA', 'en', 126),
('LA', 'fr', 126),
('LA', 'lo', 126),
('LB', 'ar_LB', 127),
('LB', 'en', 127),
('LB', 'fr_LB', 127),
('LB', 'hy', 127),
('LC', 'en_LC', 128),
('LI', 'de_LI', 129),
('LK', 'en', 130),
('LK', 'si', 130),
('LK', 'ta', 130),
('LR', 'en_LR', 131),
('LS', 'en_LS', 132),
('LS', 'st', 132),
('LS', 'xh', 132),
('LS', 'zu', 132),
('LT', 'lt', 133),
('LT', 'pl', 133),
('LT', 'ru', 133),
('LU', 'de_LU', 134),
('LU', 'fr_LU', 134),
('LU', 'lb', 134),
('LV', 'lt', 135),
('LV', 'lv', 135),
('LV', 'ru', 135),
('LY', 'ar_LY', 136),
('LY', 'en', 136),
('LY', 'it', 136),
('MA', 'ar_MA', 137),
('MA', 'fr', 137),
('MC', 'en', 138),
('MC', 'fr_MC', 138),
('MC', 'it', 138),
('MD', 'gag', 139),
('MD', 'ro', 139),
('MD', 'ru', 139),
('MD', 'tr', 139),
('ME', 'bs', 140),
('ME', 'hr', 140),
('ME', 'hu', 140),
('ME', 'rom', 140),
('ME', 'sq', 140),
('ME', 'sr', 140),
('MF', 'fr', 141),
('MG', 'fr_MG', 142),
('MG', 'mg', 142),
('MH', 'en_MH', 143),
('MH', 'mh', 143),
('MK', 'mk', 144),
('MK', 'rmm', 144),
('MK', 'sq', 144),
('MK', 'sr', 144),
('MK', 'tr', 144),
('ML', 'bm', 145),
('ML', 'fr_ML', 145),
('MM', 'my', 146),
('MN', 'mn', 147),
('MN', 'ru', 147),
('MO', 'pt', 148),
('MO', 'zh', 148),
('MO', 'zh_MO', 148),
('MP', 'ch_MP', 149),
('MP', 'en_MP', 149),
('MP', 'fil', 149),
('MP', 'tl', 149),
('MP', 'zh', 149),
('MQ', 'fr_MQ', 150),
('MR', 'ar_MR', 151),
('MR', 'fr', 151),
('MR', 'fuc', 151),
('MR', 'mey', 151),
('MR', 'snk', 151),
('MR', 'wo', 151),
('MS', 'en_MS', 152),
('MT', 'en_MT', 153),
('MT', 'mt', 153),
('MU', 'bho', 154),
('MU', 'en_MU', 154),
('MU', 'fr', 154),
('MV', 'dv', 155),
('MV', 'en', 155),
('MW', 'ny', 156),
('MW', 'swk', 156),
('MW', 'tum', 156),
('MW', 'yao', 156),
('MX', 'es_MX', 157),
('MY', 'en', 158),
('MY', 'ml', 158),
('MY', 'ms_MY', 158),
('MY', 'pa', 158),
('MY', 'ta', 158),
('MY', 'te', 158),
('MY', 'th', 158),
('MY', 'zh', 158),
('MZ', 'pt_MZ', 159),
('MZ', 'vmw', 159),
('NA', 'af', 160),
('NA', 'de', 160),
('NA', 'en_NA', 160),
('NA', 'hz', 160),
('NA', 'naq', 160),
('NC', 'fr_NC', 161),
('NE', 'dje', 162),
('NE', 'fr_NE', 162),
('NE', 'ha', 162),
('NE', 'kr', 162),
('NF', 'en_NF', 163),
('NG', 'en_NG', 164),
('NG', 'ff', 164),
('NG', 'ha', 164),
('NG', 'ig', 164),
('NG', 'yo', 164),
('NI', 'en', 165),
('NI', 'es_NI', 165),
('NL', 'fy_NL', 166),
('NL', 'nl_NL', 166),
('NO', 'fi', 167),
('NO', 'nb', 167),
('NO', 'nn', 167),
('NO', 'no', 167),
('NO', 'se', 167),
('NP', 'en', 168),
('NP', 'ne', 168),
('NR', 'en_NR', 169),
('NR', 'na', 169),
('NU', 'en_NU', 170),
('NU', 'niu', 170),
('NZ', 'en_NZ', 171),
('NZ', 'mi', 171),
('OM', 'ar_OM', 172),
('OM', 'bal', 172),
('OM', 'en', 172),
('OM', 'ur', 172),
('PA', 'en', 173),
('PA', 'es_PA', 173),
('PE', 'ay', 174),
('PE', 'es_PE', 174),
('PE', 'qu', 174),
('PF', 'fr_PF', 175),
('PF', 'ty', 175),
('PG', 'en_PG', 176),
('PG', 'ho', 176),
('PG', 'meu', 176),
('PG', 'tpi', 176),
('PH', 'en_PH', 177),
('PH', 'fil', 177),
('PH', 'tl', 177),
('PK', 'brh', 178),
('PK', 'en_PK', 178),
('PK', 'pa', 178),
('PK', 'ps', 178),
('PK', 'sd', 178),
('PK', 'ur_PK', 178),
('PL', 'pl', 179),
('PM', 'fr_PM', 180),
('PN', 'en_PN', 181),
('PR', 'en_PR', 182),
('PR', 'es_PR', 182),
('PS', 'ar_PS', 183),
('PT', 'mwl', 184),
('PT', 'pt_PT', 184),
('PW', 'en_PW', 185),
('PW', 'fil', 185),
('PW', 'ja', 185),
('PW', 'pau', 185),
('PW', 'sov', 185),
('PW', 'tox', 185),
('PW', 'zh', 185),
('PY', 'es_PY', 186),
('PY', 'gn', 186),
('QA', 'ar_QA', 187),
('QA', 'es', 187),
('RE', 'fr_RE', 188),
('RO', 'hu', 189),
('RO', 'ro', 189),
('RO', 'rom', 189),
('RS', 'bs', 190),
('RS', 'hu', 190),
('RS', 'rom', 190),
('RS', 'sr', 190),
('RU', 'ady', 191),
('RU', 'ava', 191),
('RU', 'ba', 191),
('RU', 'bua', 191),
('RU', 'cau', 191),
('RU', 'ce', 191),
('RU', 'chm', 191),
('RU', 'cv', 191),
('RU', 'inh', 191),
('RU', 'kbd', 191),
('RU', 'krc', 191),
('RU', 'kv', 191),
('RU', 'mdf', 191),
('RU', 'mns', 191),
('RU', 'myv', 191),
('RU', 'nog', 191),
('RU', 'ru', 191),
('RU', 'sah', 191),
('RU', 'tt', 191),
('RU', 'tut', 191),
('RU', 'tyv', 191),
('RU', 'udm', 191),
('RU', 'xal', 191),
('RW', 'en_RW', 192),
('RW', 'fr_RW', 192),
('RW', 'rw', 192),
('RW', 'sw', 192),
('SA', 'ar_SA', 193),
('SB', 'en_SB', 194),
('SB', 'tpi', 194),
('SC', 'en_SC', 195),
('SC', 'fr_SC', 195),
('SD', 'ar_SD', 196),
('SD', 'en', 196),
('SD', 'fia', 196),
('SE', 'fi_SE', 197),
('SE', 'se', 197),
('SE', 'sma', 197),
('SE', 'sv_SE', 197),
('SG', 'cmn', 198),
('SG', 'en_SG', 198),
('SG', 'ms_SG', 198),
('SG', 'ta_SG', 198),
('SG', 'zh_SG', 198),
('SH', 'en_SH', 199),
('SI', 'sh', 200),
('SI', 'sl', 200),
('SJ', 'no', 201),
('SJ', 'ru', 201),
('SK', 'hu', 202),
('SK', 'sk', 202),
('SL', 'en_SL', 203),
('SL', 'men', 203),
('SL', 'tem', 203),
('SM', 'it_SM', 204),
('SN', 'fr_SN', 205),
('SN', 'fuc', 205),
('SN', 'mnk', 205),
('SN', 'wo', 205),
('SO', 'ar_SO', 206),
('SO', 'en_SO', 206),
('SO', 'it', 206),
('SO', 'so_SO', 206),
('SR', 'en', 207),
('SR', 'hns', 207),
('SR', 'jv', 207),
('SR', 'nl_SR', 207),
('SR', 'srn', 207),
('SS', 'en', 208),
('ST', 'pt_ST', 209),
('SV', 'es_SV', 210),
('SX', 'en', 211),
('SX', 'nl', 211),
('SY', 'ar_SY', 212),
('SY', 'arc', 212),
('SY', 'en', 212),
('SY', 'fr', 212),
('SY', 'hy', 212),
('SY', 'ku', 212),
('SZ', 'en_SZ', 213),
('SZ', 'ss_SZ', 213),
('TC', 'en_TC', 214),
('TD', 'ar_TD', 215),
('TD', 'fr_TD', 215),
('TD', 'sre', 215),
('TF', 'fr', 216),
('TG', 'dag', 217),
('TG', 'ee', 217),
('TG', 'fr_TG', 217),
('TG', 'ha', 217),
('TG', 'hna', 217),
('TG', 'kbp', 217),
('TH', 'en', 218),
('TH', 'th', 218),
('TJ', 'ru', 219),
('TJ', 'tg', 219),
('TK', 'en_TK', 220),
('TK', 'tkl', 220),
('TL', 'en', 221),
('TL', 'id', 221),
('TL', 'pt_TL', 221),
('TL', 'tet', 221),
('TM', 'ru', 222),
('TM', 'tk', 222),
('TM', 'uz', 222),
('TN', 'ar_TN', 223),
('TN', 'fr', 223),
('TO', 'en_TO', 224),
('TO', 'to', 224),
('TR', 'av', 225),
('TR', 'az', 225),
('TR', 'diq', 225),
('TR', 'ku', 225),
('TR', 'tr_TR', 225),
('TT', 'en_TT', 226),
('TT', 'es', 226),
('TT', 'fr', 226),
('TT', 'hns', 226),
('TT', 'zh', 226),
('TV', 'en', 227),
('TV', 'gil', 227),
('TV', 'sm', 227),
('TV', 'tvl', 227),
('TW', 'hak', 228),
('TW', 'nan', 228),
('TW', 'zh', 228),
('TW', 'zh_TW', 228),
('TZ', 'ar', 229),
('TZ', 'en', 229),
('TZ', 'sw_TZ', 229),
('UA', 'hu', 230),
('UA', 'pl', 230),
('UA', 'rom', 230),
('UA', 'ru_UA', 230),
('UA', 'uk', 230),
('UG', 'ar', 231),
('UG', 'en_UG', 231),
('UG', 'lg', 231),
('UG', 'sw', 231),
('UM', 'en_UM', 232),
('US', 'en_US', 233),
('US', 'es_US', 233),
('US', 'fr', 233),
('US', 'haw', 233),
('UY', 'es_UY', 234),
('UZ', 'ru', 235),
('UZ', 'tg', 235),
('UZ', 'uz', 235),
('VA', 'fr', 236),
('VA', 'it', 236),
('VA', 'la', 236),
('VC', 'en_VC', 237),
('VC', 'fr', 237),
('VE', 'es_VE', 238),
('VG', 'en_VG', 239),
('VI', 'en_VI', 240),
('VN', 'en', 241),
('VN', 'fr', 241),
('VN', 'km', 241),
('VN', 'vi', 241),
('VN', 'zh', 241),
('VU', 'bi', 242),
('VU', 'en_VU', 242),
('VU', 'fr_VU', 242),
('WF', 'fr_WF', 243),
('WF', 'fud', 243),
('WF', 'wls', 243),
('WS', 'en_WS', 244),
('WS', 'sm', 244),
('XK', 'sq', 245),
('XK', 'sr', 245),
('YE', 'ar_YE', 246),
('YT', 'fr_YT', 247),
('ZA', 'af', 248),
('ZA', 'en_ZA', 248),
('ZA', 'nr', 248),
('ZA', 'nso', 248),
('ZA', 'ss', 248),
('ZA', 'st', 248),
('ZA', 'tn', 248),
('ZA', 'ts', 248),
('ZA', 've', 248),
('ZA', 'xh', 248),
('ZA', 'zu', 248),
('ZM', 'bem', 249),
('ZM', 'en_ZM', 249),
('ZM', 'loz', 249),
('ZM', 'lue', 249),
('ZM', 'lun', 249),
('ZM', 'ny', 249),
('ZM', 'toi', 249),
('ZW', 'en_ZW', 250),
('ZW', 'nd', 250),
('ZW', 'nr', 250),
('ZW', 'sn', 250);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `geo_currencies` (
  `id` bigint(20) unsigned,
  `code` varchar(3),
  `is_published` tinyint(3) unsigned,
  `symbol` varchar(250),
  `unicode_symbol` varchar(250),
  `position` tinyint(3) unsigned COMMENT '0: before, 1: after',
  `comments` varchar(255),
  `is_default` tinyint(3) unsigned,
  `rate` decimal(10,5),	
  `rate_time_updated` datetime,	
  `decimals` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;-- END_OF_COMMAND

INSERT INTO `geo_currencies` (`id`, `code`, `symbol`, `unicode_symbol`, `position`, `comments`, `is_default`, `is_published`, `rate`, `rate_time_updated`, `decimals`) VALUES
(1, 'AED', 'د.إ', '', 1, 'Can not find', 0, 0, '3.98900', '2016-01-02 18:34:18', 2),
(2, 'ANG', 'ƒ', '&#x0192;', 0, '', 0, 0, '1.94400', '2016-01-02 18:34:20', 2),
(3, 'AOA', 'AOA', 'AOA', 0, '', 0, 0, '146.86430', '2016-01-02 18:34:22', 2),
(4, 'ARS', '$', '&#36;', 0, '', 0, 0, '14.10100', '2016-01-02 18:34:23', 2),
(5, 'AUD', '$', '&#36;', 0, '', 0, 0, '1.48710', '2016-01-02 18:34:25', 2),
(6, 'BAM', 'KM', 'KM', 0, '', 0, 0, '1.95460', '2016-01-02 18:34:26', 2),
(7, 'BBD', '$', '&#36;', 0, '', 0, 0, '2.17210', '2016-01-02 18:34:28', 2),
(8, 'BGN', 'лв', '&#1083;&#1074;', 0, '', 0, 0, '1.95580', '2016-01-02 18:34:29', 2),
(9, 'BHD', 'BD', 'BD', 1, '', 0, 0, '0.40950', '2016-01-02 18:34:31', 3),
(10, 'BND', '$', '&#36;', 0, '', 0, 0, '1.53990', '2016-01-02 18:34:32', 2),
(11, 'BRL', 'R$', 'R&#36;', 0, '', 0, 0, '4.29970', '2016-01-02 18:34:34', 2),
(12, 'CAD', '$', '&#36;', 0, '', 0, 0, '1.50460', '2016-01-02 18:34:36', 2),
(13, 'CHF', 'Fr', 'Fr', 0, '', 0, 0, '1.08720', '2016-01-02 18:34:37', 2),
(14, 'CLF', 'UF', 'UF', 1, '', 0, 0, '0.02670', '2016-01-02 18:34:39', 2),
(15, 'CLP', '$', '&#36;', 0, '', 0, 0, '769.63710', '2016-01-02 18:34:40', 0),
(16, 'CNY', '¥', '&#165;', 0, '', 0, 0, '7.07230', '2016-01-02 18:34:42', 2),
(17, 'COP', '$', '&#36;', 0, '', 0, 0, '3452.86550', '2016-01-02 18:34:44', 2),
(18, 'CRC', '₡', '&#x20A1;', 0, '', 0, 0, '583.55840', '2016-01-02 18:34:45', 2),
(19, 'CZK', 'Kč', 'K&#269;', 1, '', 0, 0, '27.03480', '2016-01-02 18:34:47', 2),
(20, 'DKK', 'kr', 'kr', 0, '', 0, 0, '7.46400', '2016-01-02 18:34:48', 2),
(21, 'EEK', 'KR', 'KR', 0, '', 0, 0, NULL, '2016-01-02 18:34:50', 2),
(22, 'EGP', 'E£', 'E&#163;', 0, '', 0, 0, '8.50390', '2016-01-02 18:34:51', 2),
(23, 'EUR', '€', '&#128;', 0, '', 1, 1, '1.00000', '2016-01-02 18:34:53', 2),
(24, 'FJD', 'FJ$', 'FJ&#36;', 0, '', 0, 0, '2.31030', '2016-01-02 18:34:54', 2),
(25, 'GBP', '£', '&#163;', 0, '', 0, 0, '0.73510', '2016-01-02 18:34:56', 2),
(26, 'GTQ', 'Q', 'Q', 0, '', 0, 0, '8.29040', '2016-01-02 18:34:58', 2),
(27, 'HKD', '$', '&#36;', 0, '', 0, 0, '8.41750', '2016-01-02 18:35:09', 2),
(28, 'HRK', 'kn', 'kn', 0, '', 0, 0, '7.63980', '2016-01-02 18:35:20', 2),
(29, 'HUF', 'Ft', 'Ft', 0, '', 0, 0, '314.83540', '2016-01-02 18:35:21', 0),
(30, 'IDR', 'Rp', 'Rp', 0, '', 0, 0, '15025.57320', '2016-01-02 18:35:23', 2),
(31, 'ILS', '₪', '&#x20AA;', 0, '', 0, 0, '4.22350', '2016-01-02 18:35:25', 2),
(32, 'INR', 'Rs', 'Rs', 0, 'There is not a unicode', 0, 0, '71.93490', '2016-01-02 18:35:26', 2),
(33, 'JOD', 'د.ا', '', 0, 'Can not find', 0, 0, '0.77010', '2016-01-02 18:35:28', 3),
(34, 'JPY', '¥', '&#165;', 0, '', 0, 0, '131.01190', '2016-01-02 18:35:29', 0),
(35, 'KES', 'KSh', 'Ksh', 0, '', 0, 0, '111.42390', '2016-01-02 18:35:31', 2),
(36, 'KRW', '₩', '&#x20A9;', 0, '', 0, 0, '1276.81570', '2016-01-02 18:35:32', 0),
(37, 'KWD', 'KD', 'KD', 1, '', 0, 0, '0.32950', '2016-01-02 18:35:34', 3),
(38, 'KYD', '$', '&#36;', 0, '', 0, 0, '0.89060', '2016-01-02 18:35:35', 2),
(39, 'LTL', 'Lt', 'Lt', 0, '', 0, 0, '3.45280', '2016-01-02 18:35:37', 2),
(40, 'LVL', 'Ls', 'Ls', 0, '', 0, 0, '0.70280', '2016-01-02 18:35:39', 2),
(41, 'MAD', 'د.م.', '', 0, 'Can not find', 0, 0, '10.77320', '2016-01-02 18:35:40', 2),
(42, 'MVR', 'Rf', 'Rf', 0, '', 0, 0, '16.73600', '2016-01-02 18:35:42', 2),
(43, 'MXN', '$', '&#36;', 0, '', 0, 0, '18.68490', NULL, 2),
(44, 'MYR', 'RM', 'RM', 0, '', 0, 0, '4.67330', '2016-01-02 18:36:05', 2),
(45, 'NGN', '₦', '&#x20A6;', 0, '', 0, 0, '216.17850', '2016-01-02 18:36:07', 2),
(46, 'NOK', 'kr', 'kr', 0, '', 0, 0, '9.60610', '2016-01-02 18:36:09', 2),
(47, 'NZD', '$', '&#36;', 0, '', 0, 0, '1.58620', '2016-01-02 18:36:10', 2),
(48, 'OMR', 'OMR', '&#65020;', 1, 'Unsure if this is correct', 0, 0, '0.41820', '2016-01-02 18:36:12', 3),
(49, 'PEN', 'S/.', 'S/.', 0, '', 0, 0, '3.70840', '2016-01-02 18:36:14', 2),
(50, 'PHP', '₱', '&#x20B1;', 0, '', 0, 0, '50.89440', '2016-01-02 18:36:16', 2),
(51, 'PLN', 'zł', 'Z&#322;', 0, '', 0, 0, '4.28430', '2016-01-02 18:36:18', 2),
(52, 'QAR', 'QAR', '&#65020;', 1, 'Unsure if this is correct', 0, 0, '3.95470', '2016-01-02 18:36:22', 2),
(53, 'RON', 'lei', 'lei', 0, '', 0, 0, '4.52590', '2016-01-02 18:36:23', 2),
(54, 'RUB', 'руб.', '&#1088;&#1091;&#1073;', 0, '', 0, 0, '79.28230', '2016-01-02 18:36:25', 2),
(55, 'SAR', 'SAR', '&#65020;', 1, 'Unsure if this is correct', 0, 0, '4.07710', '2016-01-02 18:36:26', 2),
(56, 'SEK', 'kr', 'kr', 0, '', 0, 0, '9.18840', '2016-01-02 18:36:28', 2),
(57, 'SGD', '$', '&#36;', 0, '', 0, 0, '1.53260', '2016-01-02 18:36:38', 2),
(58, 'THB', '฿', '&#322;', 0, '', 0, 0, '39.04750', '2016-01-02 18:36:40', 2),
(59, 'TRY', 'TL', 'TL', 0, 'There is not a unicode', 0, 0, '3.16470', '2016-01-02 18:36:41', 2),
(60, 'TTD', '$', '&#36;', 0, '', 0, 0, '6.97530', '2016-01-02 18:36:43', 2),
(61, 'TWD', '$', '&#36;', 0, '', 0, 0, '35.75060', '2016-01-02 18:36:45', 2),
(62, 'UAH', '₴', '&#8372;', 0, '', 0, 0, '26.09600', '2016-01-02 18:36:46', 2),
(63, 'USD', '$', '&#36;', 0, '', 0, 0, '1.08610', '2016-01-02 18:36:48', 2),
(64, 'VEF', 'Bs F', 'Bs.', 0, '', 0, 0, '6.89670', '2016-01-02 18:36:50', 2),
(65, 'VND', '₫', '&#x20AB;', 0, '', 0, 0, '24419.85740', '2016-01-02 18:37:00', 2),
(66, 'XCD', '$', '&#36;', 0, '', 0, 0, '2.93230', '2016-01-02 18:37:02', 2),
(67, 'ZAR', 'R', 'R', 0, '', 0, 0, '16.80230', '2016-01-02 18:37:04', 2);-- END_OF_COMMAND

CREATE TABLE IF NOT EXISTS `geo_regions` (
  `id` bigint unsigned,
  `geo_countries_id` bigint unsigned,
  `title` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;-- END_OF_COMMAND

INSERT IGNORE INTO `geo_regions` (`id`, `geo_countries_id`, `title`) VALUES
(1, 89, 'Νομός Αττικής'),
(2, 89, 'Νομός Εύβοιας'),
(3, 89, 'Νομός Ευρυτανίας'),
(4, 89, 'Νομός Φωκίδας'),
(5, 89, 'Νομός Φθιώτιδας'),
(6, 89, 'Νομός Βοιωτίας'),
(7, 89, 'Νομός Χαλκιδικής'),
(8, 89, 'Νομός Ημαθίας'),
(9, 89, 'Νομός Κιλκίς'),
(10, 89, 'Νομός Πέλλας'),
(11, 89, 'Νομός Πιερίας'),
(12, 89, 'Νομός Σερρών'),
(13, 89, 'Νομός Θεσσαλονίκης'),
(14, 89, 'Νομός Χανίων'),
(15, 89, 'Νομός Ηρακλείου'),
(16, 89, 'Νομός Λασιθίου'),
(17, 89, 'Νομός Ρεθύμνου'),
(18, 89, 'Νομός Δράμας'),
(19, 89, 'Νομός Έβρου'),
(20, 89, 'Νομός Καβάλας'),
(21, 89, 'Νομός Ροδόπης'),
(22, 89, 'Νομός Ξάνθης'),
(23, 89, 'Νομός Άρτας'),
(24, 89, 'Νομός Ιωαννίνων'),
(25, 89, 'Νομός Πρέβεζας'),
(26, 89, 'Νομός Θεσπρωτίας'),
(27, 89, 'Νομός Κέρκυρας'),
(28, 89, 'Νομός Κεφαλληνίας'),
(29, 89, 'Νομός Λευκάδας'),
(30, 89, 'Νομός Ζακύνθου'),
(31, 89, 'Νομός Χίου'),
(32, 89, 'Νομός Λέσβου'),
(33, 89, 'Νομός Σάμου'),
(34, 89, 'Νομός Αρκαδίας'),
(35, 89, 'Νομός Αργολίδας'),
(36, 89, 'Νομός Κορινθίας'),
(37, 89, 'Νομός Λακωνίας'),
(38, 89, 'Νομός Μεσσηνίας'),
(39, 89, 'Νομός Κυκλάδων'),
(40, 89, 'Νομός Δωδεκανήσου'),
(41, 89, 'Νομός Καρδίτσας'),
(42, 89, 'Νομός Λάρισας'),
(43, 89, 'Νομός Μαγνησίας'),
(44, 89, 'Νομός Τρικάλων'),
(45, 89, 'Νομός Αχαΐας'),
(46, 89, 'Νομός Αιτωλοακαρνανίας'),
(47, 89, 'Νομός Ηλείας'),
(48, 89, 'Νομός Φλώρινας'),
(49, 89, 'Νομός Γρεβενών'),
(50, 89, 'Νομός Καστοριάς'),
(51, 89, 'Νομός Κοζάνης'),
(52, 89, 'Νομός Πειραιά'),
(53, 89, 'Νομαρχία Αθηνών'),
(54, 89, 'Νομαρχία Ανατολικής Αττικής'),
(55, 89, 'Νομαρχία Δυτικής Αττικής');-- END_OF_COMMAND

ALTER TABLE `geo_countries_languages` 
  ADD CONSTRAINT `fk_geo_countries_languages_geo_countries` FOREIGN KEY (`geo_countries_id`) REFERENCES `geo_countries` (`id`);-- END_OF_COMMAND

ALTER TABLE `geo_regions`
  ADD CONSTRAINT `fk_geo_regions_geo_countries` FOREIGN KEY (`geo_countries_id`) REFERENCES `geo_countries` (`id`);-- END_OF_COMMAND
  
--
-- GEO
--
SET foreign_key_checks = 1;-- END_OF_COMMAND