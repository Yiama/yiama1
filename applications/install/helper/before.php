<?php
	class Helper_Before
	{
		public static function checkUser( $roles = array( 'superadmin', 'admin' ) )
		{
			/* User */
			$user = new Model_Yiama_User();
			if( ! ( $logged_user = $user->getLogged() ) || ! $logged_user->hasRoles( $roles ) ) {
				Core_Response::getInstance()->redirect(
					Core_Request::getInstance()->url(
						'cont_act_id',
						array( 
							'controller' => 'user',
							'action' => 'loginform' 
						)
					)
				);
			}
			/* Απενεργοποίηση των Links ώστε να μην μπορεί να γίνει right-click->open in new tab */
			Core_HTML::addFiles( 'library\basic.js' );
			Core_HTML::addJs( " 
				function handleAnchors()
				{
					var anchors = document.getElementsByTagName( 'a' );
					for( var i = 0; i < anchors.length; i++ ){
					    if( attr = anchors[i].getAttribute( 'href' ) ) {
						    anchors[i].setAttribute( 'data-href', attr );
						    anchors[i].removeAttribute( 'href' );
						    anchors[i].onclick = function(){
						        window.location = this.getAttribute( 'data-href' );
						    }
						}
					}
				}
				_addOnLoad( 'handleAnchors()' );
			" );
		}
	}
?>