<?php
 
class Helper_Message extends Core_Controller
{
	private static $instansiated = false;
	
	public static function get()
	{
		/* Js call */
		if( ! self::$instansiated ){
			self::$instansiated = true;
			Core_HTML::addJs( "
				var message_timeout;
				function removeMessageItems( id )
				{
					var msgEl = _get( id );
					var items = msgEl.getElementsByTagName( 'div' );
					if( items ) {
						for( var i = 0; i < items.length; i++ ) {
							msgEl.removeChild( items[i] );
						}
						_hide( msgEl );
					}
				}
				function message( type, text )
				{
					clearTimeout( message_timeout );
					removeMessageItems();
					var msgEl = _get( 'message' );
					var item = document.createElement( 'div' );
					item.innerHTML = text;
					item.setAttribute( 'class', 'item ' + type );
					msgEl.appendChild( item );
					_show( msgEl );
					message_timeout = setTimeout( function(){ removeMessageItems() }, 5000 );
				}
			" );
		}
		
		/* URL Request */
		Core_HTML::addFiles( 'admin\message.css' );
		$language = new Model_Yiama_Language();
		$translate = Core_Translate::get( $language->getDefault()->code, 'admin.error', 'messages' );
		$params = Core_Request::getInstance()->getParams();
		Core_Request::getInstance()->deleteParams( 'msg' );
		$msg_types_symbols = array( 'success', 'error', 'warning' );
		$items = array();
		$show = false;
		foreach( $msg_types_symbols as $val ) {
			if( isset( $params['msg'][ $val ] ) ) {
				$show = true;
				foreach( array_filter( ( array ) $params['msg'][ $val ] ) as $param ) {
					foreach( explode( ',', $param ) as $m ) {
						$items[] = HTML_Tag::closed( 'div', array('class' => 'item ' . $val),$translate[ $val ][ $m ]);
					}
				}
			}
		}
		$s = "<script type='text/javascript'>message_timeout = setTimeout( function(){ removeMessageItems() }, 5000 );</script>";
		return $s . HTML_Tag::closed( 'div', array( 
				'class' => 'message',
				'id' => 'message',
				'style' => $show ? 'display: block;' : ''
			), implode( '', $items ) 
		);
	}
}

?>