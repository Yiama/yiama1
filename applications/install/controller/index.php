<?php
class Controller_Index extends Core_Controller
{	
	public function index()
	{
		$form = Helper_Form::openForm( $this->request->url(
			'cont_act_id',
			array(  
				'controller' => 'index',
				'action' => 'db'
		) ) );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::openSubWrapper( 'Ομάδες πινάκων' );
		$files = array( 'yiama' );//, 'shop' );
		foreach( $files as $f ) {
			$form .= Helper_Form::checkbox( "db_files[{$f}]", true, null, null, $f );
		}
                $form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::hiddenReturn( $this->request->url(
			'cont_act_id',
			array(  
				'controller' => 'index',
				'action' => 'formbasic'
		) ) );
		$form .= Helper_Form::submit();
		$form .= HTML_Tag::closed( 'a', array( 
                'class' => 'button save', 
                'id' => 'main_form_right_top_button', 
                'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
            ), '<span>Next</span>' 
        );
		$form .= Helper_Form::closeForm();

		/* View */
		$this->setView( dirname(__DIR__) . DS . 'views' . DS . 'install' . DS . 'index' );
		$this->view->form = $form;
		$this->view->show_instructions = true;
		$this->subView = $this->view->render();
		$this->setView( dirname( __DIR__ ) . DS . 'views' . DS . 'main' . DS . 'default' ); 
		$this->response->setBody($this->view->render());
	}
	
	/* 
	 * the php API forbids you to issue multiple queries in a single call to reduce the chance of an SQL injection attack to your code
	 * γιαυτό κόβουμε τις εντολές με το ';'
	 */				
	public function db()
	{
		$params = $this->request->getParams(); 
        $error = null;
		DB_ActiveRecord_Model::init();
		$db_config_name = Core_App::getConfig( "applications." . Core_App::getAppName() . ".database" );
		$database = Core_App::getConfig( 'databases.' . $db_config_name . '.connection.dbname' );
        $db_files_path = dirname(dirname(__FILE__)) . DS . 'db' . DS;
        DB_ActiveRecord_Model::$db_model->startTransaction();
        if( isset( $params['db_files'] ) ) {
            foreach( $params['db_files'] as $key => $f ) { 
                $sql_commands = explode( '-- END_OF_COMMAND', file_get_contents( $db_files_path . $key . '.sql' ) );
                foreach( $sql_commands as $com ) {
                    if( ! empty($com) && ! DB_ActiveRecord_Model::$db_model->query( $com )->execute() ) {
                        $error[] = 'insert_row';
                        break;
                    }
                }
            }
        }
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();	
			Helper_Redirect::send( $params['return_url'], null, array( 'insert', $error ) );	
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
	}
	
	public function formbasic()
	{
		$form = Helper_Form::openForm( $this->request->url(
			'cont_act_id',
			array(  
				'controller' => 'index',
				'action' => 'insertbasic'
		) ) );
		$form .= Helper_Form::openWrapperModule();
		$form .= Helper_Form::openSubWrapper( 'Στοιχεία διαχειριστή' );
		$form .= Helper_Form::input( 'admin_email', null, true, array('required' => 'required', 'type' => 'email'), 'Email', array( 'class' => 'small' ) );
		$form .= Helper_Form::input( 'admin_username', null, true, array('required' => 'required', 'pattern' => '[a-zA-Z0-9 ]+'), 'Διακριτικό', array( 'class' => 'small' ) );
		$form .= Helper_Form::input( 'admin_password', null, true, array( 
                'type' => 'text',
                'onfocus' => 'if( ! this.value ) this.value = generatePassword();',
		          'required' => 'required'
            ), 'Κωδικός', array( 'class' => 'small' ) 
        );
		$form .= Helper_Form::closeSubWrapper();
		$form .= Helper_Form::closeWrapperModule();
		$form .= Helper_Form::hiddenReturn( $this->request->url(
			'default',
			array(),
            null,
            array('application' => 'admin')
		) );
		$form .= Helper_Form::submit();
		$form .= HTML_Tag::closed( 'a', array( 
				'class' => 'button button-blue', 
				'id' => 'main_form_right_top_button', 
				'onclick' => "_addClass( this, 'loading' ); document.getElementById( 'main_form_submit' ).click();"
			), '<span>Επόμενο</span>' 
		);
		$form .= Helper_Form::closeForm();
		
		/* View */
		$this->setView( dirname(__DIR__) . DS . 'views' . DS . 'install' . DS . 'index' );
		$this->view->form = $form;
		$this->subView = $this->view->render();
		$this->setView( dirname( __DIR__ ) . DS . 'views' . DS . 'main' . DS . 'default' );
		$this->response->setBody($this->view->render());
	}	
			
	public function insertbasic()
	{
		$params = $this->request->getParams(); 
		$error = null;
		$user = new Model_Yiama_User();
		$user->ym_usergroups_id = 1;
		$user->username = $params['admin_username'];
		$user->password = $user->hash( $params['admin_password'] );
		$user->email = $params['admin_email'];
		$user->time_created = '@func:NOW()';
		$user->is_published = 1;
		$user->user_groups_id = 1;
		DB_ActiveRecord_Model::$db_model->startTransaction();
		if( ! $user->insert() ) {
			$error[] = 'insert_row';
		} else {
			$role = new Model_Yiama_Role();
			foreach( $role->findAll() as $r ) {
				DB_ActiveRecord_Model::$db_model
					->table( 'ym_users_roles' )
					->insert( array( 'ym_users_id', 'ym_roles_id' ) )
					->values( array( $user->id, $r->id ) )
					->execute();
			}
		}
		if( empty( $error ) ) {
			DB_ActiveRecord_Model::$db_model->commit();		
		} else {
			DB_ActiveRecord_Model::$db_model->rollback();	
		}
		Helper_Redirect::send( $params['return_url'], null, array( 'insert', $error ) );
	}
}
?>