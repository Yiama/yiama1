<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<?php 
	Core_HTML::addFiles( array(
		'admin\global.css',
		'admin\templates\default.css'
	) ); 
?>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='el' lang='el'>
<head>
<?php 
	echo Core_HTML::getHeaders();
	echo "<link rel='shortcut icon' href='" . PATH_REL_IMAGES . "admin/default/favicon.ico'/>";
	echo Core_HTML::getFiles();
	echo Core_HTML::getScripts(); 
?>
</head>
<body onload='_onLoad()'>
  <div class='body_wrapper'>
    <?php echo $this->controller->subView; ?>
  </div>
</body>
</html>