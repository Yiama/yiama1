<?php 
Core_HTML::addFiles( array(
        'admin\templates\default.css',
	    'admin\form.css',
	    'install\form.css',
		'htmltag\yiamamsg.css',
		'library\basic.js',
		'library\form.js',
		'library\preload.js',
		'admin\form\formErrorHandler.js'
	) ); 
?>
<div class='form admin' style='margin: auto; width: 50%'>
  	<a name='anc'></a>
  	<img src='<?php echo PATH_REL_ASSETS.DS.'images'.DS.'admin'.DS.'login'.DS.'logo.png'?>' />
  	<?php if (!empty($this->show_instructions)): ?>
  	<table class='instructions'>
    	<tr>
    		<td>database</td>
        	<td>δημιουργία βάσης και χρήστη<br />
           	 <i><small>CREATE DATABASE databasename CHARACTER SET utf8 COLLATE utf8_general_ci;</small></i><br />
           	 <i><small>CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';</small></i><br />
           	 <i><small>GRANT ALL ON databasename.* TO 'username'@'localhost';</small></i><br />
           	 <i><small>FLUSH PRIVILEGES;</small></i>
            </td>
        </tr>
        <tr><td>/.htaccess</td><td>έλεγχος αν υπάρχει και είναι ρυθμισμένο</td></tr>
    	<tr><td>config/conf.php</td><td>ρύθμιση: domain, base directory, database, security salt</td></tr>
	</table>
	<?php endif; ?>
  	<div><?php echo $this->form; ?></div>
</div>
<script type='text/javascript'>
    function generatePassword() {
        var length  = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    + "0123456789",
            result  = "";
        for( var i = 0, n = charset.length; i < length; ++i ) {
            result += charset.charAt( Math.floor( Math.random() * n ) );
        }
        return result;
    }
</script>
