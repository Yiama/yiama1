<?php
	class Model_Yiama_User extends DB_ActiveRecord_Model
	{
		protected static $last_inserted_id;
		protected static $table_name = 'ym_users';
		protected static $primary_key = 'id';
		private static $logged_user;
		protected static $relations = array(
			'roles' => array( 
				'type' => 'one_to_many',
				'model_name' => 'Model_Yiama_Role',
				'foreign_key' => array( 'id' => 'ym_users_roles.ym_users_id' ),
				'join' => array( array( 'ym_users_roles', 'ym_users_roles.ym_roles_id = ym_roles.id' ) )
			)
		);
		
		public function __construct()
		{
			parent::__construct();
		}
			
		public function __destruct(){}
		
		public function getDBModelStateSelect()
		{
			return self::$db_model
				->table( self::$table_name )
				->select()
				->order( 'username ASC' );
		}
	
		/*
		 * @param $password string
		 */
		public function hash( $password )
		{
			/* Με το base64 έχει έγκυρους χαρακτήρες για να εισαχθεί σε string πεδίο στην Βάση */
			$salt = bin2hex( Core_App::getConfig( 'security.salt' ) );
			return hash( 'sha256', $password . $salt );
		}
			
		public function getLogged()
		{	
			if( ! empty( self::$logged_user ) && self::$logged_user->login_token == Core_Cookie::get( 'ym_user', true ) ) {
				return self::$logged_user;
			} else {
				return self::$logged_user = $this->query()
					->search( array(
						'login_token IS NOT NULL',
						"login_token = '" . Core_Cookie::get( 'ym_user', true ) . "'"
					) )
					->combine( 'roles' )
					->find( ':first' );
			}
			return false;	
		}
		
		public function login()
		{
			$tokens = $this->query()
				->search( "login_token IS NOT NULL" )
				->find( 'login_token' );
			do{
				$this->login_token = hash( 'sha256', uniqid( mt_rand(), true ) );
			}
			while( in_array( $this->login_token, $tokens ) );
			if( $this->update() ) {
				self::$logged_user = $this;
				Core_Cookie::set( array(
					'key' => 'ym_user', 
					'value' => $this->login_token, 
					'expire' => 2592000000,
					'path' => '/',
					'encrypt' => true 
				) );
				
				return true;
			}
			return false;
		}
	
		public function logout()
		{
			$this->login_token = null;
			if( $this->update() ) {
				self::$logged_user = null;
				Core_Cookie::remove( 'ym_user' );
				return true;
			}
			return false;
		}
	
		public function hasRoles( $roles )
		{
			foreach( $this->roles as $r ) {
				if( in_array( $r->title, ( array ) $roles ) ) {
					return true;
				}
			}
			return false;
		}
	
		/*
		 * @param $token string { Reset password token που πρέπει να ελεγχθεί }
		 */
		public function isValidResetPassToken( $token )
		{
			if( strtotime( "+1 day", strtotime( $this->reset_password_time ) ) < strtotime( "now" ) /* Αν έχει λήξει η δυνατότητα αλλαγής Password */
			|| $this->reset_password_token != $token ) {
				return false;
			}
			return true;
		}
		
		public function updateResetPassToken()
		{
			$this->reset_password_token = crypt( 'sha256', uniqid( mt_rand(), true ) );
			$this->reset_password_time = date( "Y-m-d H:i:s" );
			$this->update();
		}
	}
?>