<?php

	/* PHP version >= 5.3. */
	
	/* Set timezone. */
	date_default_timezone_set( 'Europe/Athens' );
	
	/* 
	 * Set internal character encoding.
	 *  
	 * @require mb_string extension
	 */
	mb_internal_encoding( 'UTF-8' );
	
	/* Large image upload */
	ini_set( 'memory_limit', '256M' );
	
	/* Define paths. */
	define( 'PATH_APPS', PATH_ROOT . 'applications' . DS );
	define( 'PATH_LIBRARY', PATH_ROOT . 'library' . DS );
	define( 'PATH_MODULES', PATH_ROOT . 'module' . DS );
	define( 'PATH_LIBRARY_CORE', PATH_LIBRARY . 'core' . DS );
	define( 'PATH_CONFIG', PATH_APPS . 'config' . DS );
	define( 'PATH_ASSETS', PATH_ROOT . 'public' . DS . 'assets' . DS );
	define( 'PATH_REL_ASSETS', str_replace( '\\', '/', (!empty(PATH_BASEDIR) ? DS . PATH_BASEDIR : "") . DS . 'public' . DS . 'assets' . DS ) );
	define( 'PATH_IMAGES', PATH_ASSETS . 'images' . DS );
	define( 'PATH_REL_IMAGES', str_replace( '\\', '/', PATH_REL_ASSETS . 'images' . DS ) );
	define( 'PATH_AUDIOS', PATH_ASSETS . 'audios' . DS );
	define( 'PATH_REL_AUDIOS', str_replace( '\\', '/', PATH_REL_ASSETS . 'audios' . DS ) );
	define( 'PATH_VIDEOS', PATH_ASSETS . 'videos' . DS );
	define( 'PATH_REL_VIDEOS', str_replace( '\\', '/', PATH_REL_ASSETS . 'videos' . DS ) );
	define( 'PATH_DOCS', PATH_ASSETS . 'docs' . DS );
	define( 'PATH_REL_DOCS', str_replace( '\\', '/', PATH_REL_ASSETS . 'docs' . DS ) );
	define( 'PATH_TEMPLATES', PATH_ASSETS . 'templates' . DS );
    
    include('environment.php');
	
	/* Initialize Autoloader. */
	require_once PATH_LIBRARY_CORE . 'autoload.php';
	Core_Autoload::init( array( PATH_ROOT, PATH_LIBRARY ) );

	/* Initialize application. */
	Core_App::init( include 'config' . DS . 'conf.php', include 'config' . DS . APP_ENVIRONMENT . '.conf.php' );

	ob_start();

	/* Initialize Request. */
	Core_Request::getInstance()->init(); 
	
	/* Router. */
	Core_Router::route();
	
	/* Response. */
	echo Core_Response::getInstance()->get();

	ob_flush();
?>
